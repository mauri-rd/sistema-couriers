(function () {
    var submitted = false;
    $('form').on('submit', function () {
        if (!submitted) {
            submitted = true;
            return true;
        }
        return false;
    });
})();