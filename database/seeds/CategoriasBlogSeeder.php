<?php

use Illuminate\Database\Seeder;

use App\CategoriaPost;

class CategoriasBlogSeeder extends Seeder
{

    private $categorias = [
        'Noticias',
        'Obispado',
        'Decanatos',
        'Pastorales',
        'Decretos'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->categorias as $categoria) {
            CategoriaPost::create([
                'nombre' => $categoria
            ]);
        }
    }
}
