<?php

use Illuminate\Database\Seeder;

class ChinaAereoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            ['Dirección de envío aéreo (casilla) China', 'direccion-casilla-china-aereo'],
        ];
        foreach ($configs as $config) {
            if (is_array($config)) {
                \App\Configuracion::firstOrCreate([
                    'nombre' => $config[0],
                    'slug' => $config[1]
                ]);
            } else {
                \App\Configuracion::firstOrCreate(['nombre' => $config]);
            }
        }
    }
}
