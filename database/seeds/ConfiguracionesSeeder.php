<?php

use Illuminate\Database\Seeder;

use App\Configuracion;

class ConfiguracionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            ['Precio de envío Miami-Paraguay', 'envio-miami'],
            ['Precio de envío Miami-Paraguay (Maritimo)', 'envio-miami-maritimo'],
            ['Precio de envío China-Paraguay', 'envio-china'],
            'Instagram',
            'Facebook',
            'Numero WhatsApp',
        ];
        Configuracion::truncate();
        foreach ($configs as $config) {
            if (is_array($config)) {
                Configuracion::firstOrCreate([
                    'nombre' => $config[0],
                    'slug' => $config[1]
                ]);
            } else {
                Configuracion::firstOrCreate(['nombre' => $config]);
            }
        }
    }
}
