<?php

use Illuminate\Database\Seeder;

use App\Paquete;

class PackagesImportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packagesJson = json_decode(file_get_contents(storage_path('app/packages.json')), true);
        $notFoundUsers = 0;
        foreach ($packagesJson as $package) {
            $casilla = substr($package['codCliente'], 3);
            $user = \App\User::whereCasilla($casilla)->first();
            if (!is_null($user)) {
                Paquete::create([
                    'id_usuario' => $user->id,
                    'id_packlist' => null,
                    'codigo' => $package['codigo'],
                    'descripcion' => $package['contenido'],
                    'cant_cajas' => 0,
                    'tracking' => $package['tracking'],
    //                'courier' => ,
                    'gramos' => $package['peso'] * 1000,
                    'precio_kg' => $package['precio'],
                    'estado' => $package['estado'] == 'Pendiente' ? \App\Enum\EstadoPaquete::$OFICINA : \App\Enum\EstadoPaquete::$RETIRADO,
                    'tipo_flete' => \App\Enum\TipoFlete::NORMAL,
                    'destino' => \App\Enum\Destino::PARAGUAY,
    //                'seguro' => 0,
    //                'consolidacao' => 0,
    //                'consolidacao_trackings' => '',
                    'tipo_precio' => \App\Enum\TipoPrecioPaquete::POR_PESO,
                    'unidades' => 0,
                ]);
            } else {
                $notFoundUsers++;
                $this->command->info("Usuario no encontrado: " . $package['codCliente']);
            }
        }
        $this->command->info('Usuarios no encontrados: ' . $notFoundUsers);
    }
}
