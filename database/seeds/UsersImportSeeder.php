<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Sucursal;
use Illuminate\Support\Str;

class UsersImportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersJson = json_decode(file_get_contents(storage_path('app/users.json')), true);
        foreach ($usersJson as $user) {
            $prefix = substr($user['codigo'], 0, 3);
            $casilla = substr($user['codigo'], 3);
            if (User::whereCasilla($casilla)->count() == 0 && $user['birthDate'] != null) {
                $idSucursal = Sucursal::wherePrefix($prefix)->first()->id;

                try {
                    User::create([
                        'name' => $user['names'],
                        'last_name' => '',
                        'phone' => $user['phone'],
                        'country' => 'Paraguay',
                        'city' => $user['city'],
                        'state' => '',
                        'zip' => '',
                        'ci' => $user['document'],
                        'birthday' => substr($user['birthDate'], 0, 10),
                        'email' => $user['email'],
                        'password' => Str::random(10),
                        'type' => \App\Enum\UserType::CLIENTE,
                        'active' => 1,
                        'casilla' => $casilla,
                        'id_sucursal' => $idSucursal,
                    ]);
                } catch (\Illuminate\Database\QueryException $e) {
                    $this->command->info($e->getCode());
                    $this->command->info($e->getMessage());
                    if (str_contains($e->getMessage(), "Duplicate entry")) {
                        $this->command->info("Usuario con e-mail " . $user['email'] . " ya creado..");
                    }
                }
            }
        }
    }
}
