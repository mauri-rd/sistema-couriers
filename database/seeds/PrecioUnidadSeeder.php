<?php

use Illuminate\Database\Seeder;

use App\Configuracion;

class PrecioUnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            ['Precio por unidad (envío a Paraguay)', 'precio-unidad-py'],
            ['Precio por unidad (envío a Brasil)', 'precio-unidad-brasil'],
        ];

        foreach ($configs as $config) {
            if (is_array($config)) {
                Configuracion::firstOrCreate([
                    'nombre' => $config[0],
                    'slug' => $config[1]
                ]);
            } else {
                Configuracion::firstOrCreate(['nombre' => $config]);
            }
        }
    }
}
