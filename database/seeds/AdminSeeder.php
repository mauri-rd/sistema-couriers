<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Administrador2',
            'email' => 'admin2@strbox.com.py',
            'password' => 'StrBox2021',
            'birthday' => '2023-10-11',
            'casilla' => '---',
            'phone' => '---',
            'address' => '---',
        ]);
    }
}
