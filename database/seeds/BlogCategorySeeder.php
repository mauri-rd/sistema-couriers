<?php

use Illuminate\Database\Seeder;

use App\CategoriaMultimedia;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoriaMultimedia::create([
            'nombre' => 'Videos'
        ]);
    }
}
