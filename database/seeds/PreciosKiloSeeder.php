<?php

use Illuminate\Database\Seeder;

class PreciosKiloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            ['Precio de envío Miami-Brasil', 'envio-brasil'],
            ['Precio de envío Miami-Paraguay Express', 'envio-express'],
        ];
        foreach ($configs as $config) {
            if (is_array($config)) {
                \App\Configuracion::firstOrCreate([
                    'nombre' => $config[0],
                    'slug' => $config[1]
                ]);
            } else {
                \App\Configuracion::firstOrCreate(['nombre' => $config]);
            }
        }
    }
}
