<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(ConfiguracionesSeeder::class);
        $this->call(ChinaAereoSeeder::class);
        $this->call(PreciosKiloSeeder::class);
        $this->call(CategoriasBlogSeeder::class);
        $this->call(BlogCategorySeeder::class);
        $this->call(PrecioUnidadSeeder::class);
        $this->call(SucursalesSeeder::class);
    }
}
