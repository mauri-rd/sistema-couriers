<?php

use Illuminate\Database\Seeder;

use App\Sucursal;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sucursales = [
            ['Santa Rita', 'STR'],
            ['Hernandarias', 'HER'],
            ['Mayor Otaño', 'MYO'],
        ];

        foreach ($sucursales as $sucursal) {
            Sucursal::create([
                'nombre' => $sucursal[0],
                'prefix' => $sucursal[1],
            ]);
        }
    }
}
