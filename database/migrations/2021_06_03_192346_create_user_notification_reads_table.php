<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNotificationReadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notification_reads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_user_notification');
            $table->boolean('read')->default(false);
            $table->timestamps();

            $table->foreign('id_user', 'user_notification_reads_users_fk')
                ->references('id')
                ->on('users');
            $table->foreign('id_user_notification', 'user_notification_reads_user_notifications_fk')
                ->references('id')
                ->on('user_notifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_notification_reads', function (Blueprint $table) {
            $table->dropForeign('user_notification_reads_users_fk');
            $table->dropForeign('user_notification_reads_user_notifications_fk');
        });
        Schema::drop('user_notification_reads');
    }
}
