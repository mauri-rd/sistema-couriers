<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class
CreateInvoiceManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_manuals', function (Blueprint $table) {
            $table->increments('id');
            $table->text('header');
            $table->unsignedInteger('number');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->unsignedInteger('id_user')
                ->nullable()
                ->default(null);
            $table->text('user_data');
            $table->timestamps();

            $table->foreign('id_user', 'invoice_manuals_users_fk')
                ->references('id')
                ->on('users');
        });

        \DB::statement('ALTER TABLE invoice_manuals AUTO_INCREMENT = 500000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_manuals', function (Blueprint $table) {
            $table->dropForeign('invoice_manuals_users_fk');
        });
        Schema::drop('invoice_manuals');
    }
}
