<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsFieldsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('operador_invoices')->default(false);
            $table->boolean('operador_pagos')->default(false);
            $table->boolean('operador_usuarios')->default(false);
            $table->boolean('operador_mensajes')->default(false);
            $table->boolean('operador_sucursales')->default(false);
            $table->boolean('operador_reportes')->default(false);
            $table->boolean('operador_informaciones')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('operador_invoices');
            $table->dropColumn('operador_pagos');
            $table->dropColumn('operador_usuarios');
            $table->dropColumn('operador_mensajes');
            $table->dropColumn('operador_sucursales');
            $table->dropColumn('operador_reportes');
            $table->dropColumn('operador_informaciones');
        });
    }
}
