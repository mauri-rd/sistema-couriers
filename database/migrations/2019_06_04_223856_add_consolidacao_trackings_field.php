<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsolidacaoTrackingsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packlists', function (Blueprint $table) {
            $table->string('consolidacao_trackings')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packlists', function (Blueprint $table) {
            $table->dropColumn('consolidacao_trackings');
        });
    }
}
