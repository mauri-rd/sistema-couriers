<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenProductosFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imagen_productos', function (Blueprint $table) {
            $table->foreign('id_producto', 'imagen_productos_productos_fk')->references('id')->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagen_productos', function (Blueprint $table) {
            $table->dropForeign('imagen_productos_productos_fk');
        });
    }
}
