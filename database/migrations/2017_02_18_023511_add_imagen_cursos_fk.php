<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenCursosFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imagen_cursos', function (Blueprint $table) {
            $table->foreign('id_curso', 'imagen_cursos_cursos_fk')->references('id')->on('cursos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagen_cursos', function (Blueprint $table) {
            $table->dropForeign('imagen_cursos_cursos_fk');
        });
    }
}
