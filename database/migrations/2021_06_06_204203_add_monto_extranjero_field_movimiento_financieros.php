<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMontoExtranjeroFieldMovimientoFinancieros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->float('monto_extranjero', 15, 2)->nullable()->default(null);
            $table->float('cambio_registrado', 15, 2)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->dropColumn('monto_extranjero');
            $table->dropColumn('cambio_registrado');
        });
    }
}
