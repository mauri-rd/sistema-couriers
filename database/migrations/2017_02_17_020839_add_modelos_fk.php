<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModelosFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modelos', function(Blueprint $table) {
            $table->foreign('id_marca', 'modelos_marcas_fk')->references('id')->on('marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modelos', function(Blueprint $table) {
            $table->dropForeign('modelos_marcas_fk');
        });
    }
}
