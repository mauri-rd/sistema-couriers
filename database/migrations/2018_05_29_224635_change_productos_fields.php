<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductosFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign('productos_modelos_fk');
            $table->dropForeign('productos_categoria_productos_fk');

            $table->dropColumn('descripcion');
            $table->dropColumn('precio');
            $table->dropColumn('id_modelo');
            $table->dropColumn('id_categoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->text('descripcion');
            $table->string('precio');
            $table->unsignedInteger('id_modelo')->nullable();
            $table->unsignedInteger('id_categoria');

            $table->foreign('id_modelo', 'productos_modelos_fk')->references('id')->on('modelos');
            $table->foreign('id_categoria', 'productos_categoria_productos_fk')->references('id')->on('categoria_productos');
        });
    }
}
