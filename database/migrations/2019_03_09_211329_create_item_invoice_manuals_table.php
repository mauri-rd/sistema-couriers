<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemInvoiceManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_invoice_manuals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_invoice');
            $table->string('item');
            $table->string('description');
            $table->float('price', 15, 2);
            $table->float('quantity', 15, 2);
            $table->float('amount', 15, 2);
            $table->boolean('paid')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_invoice_manuals');
    }
}
