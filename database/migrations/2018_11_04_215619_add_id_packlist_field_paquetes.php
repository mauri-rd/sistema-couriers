<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdPacklistFieldPaquetes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->unsignedInteger('id_packlist')->nullable()->default(null);

            $table->foreign('id_packlist', 'invoices_paquetes_fk')
                ->references('id')
                ->on('packlists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->dropForeign('invoices_paquetes_fk');
            $table->dropColumn('id_packlist');
        });
    }
}
