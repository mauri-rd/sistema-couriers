<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_comprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_comprobante');
            $table->unsignedInteger('id_paquete');
            $table->timestamps();

            $table->foreign('id_comprobante', 'item_comprobantes_comprobantes_fk')
                ->references('id')->on('comprobantes');
            $table->foreign('id_paquete', 'item_comprobantes_paquetes_fk')
                ->references('id')->on('paquetes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_comprobantes', function (Blueprint $table) {
            $table->dropForeign('item_comprobantes_comprobantes_fk');
            $table->dropForeign('item_comprobantes_paquetes_fk');
        });
        Schema::drop('item_comprobantes');
    }
}
