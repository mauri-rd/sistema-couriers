<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_paquete');
            $table->unsignedTinyInteger('estado');
            $table->timestamps();

            $table->foreign('id_paquete', 'package_histories_paquetes_fk')
                ->references('id')
                ->on('paquetes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_histories', function (Blueprint $table) {
            $table->dropForeign('package_histories_paquetes_fk');
        });
        Schema::drop('package_histories');
    }
}
