<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientoFinancierosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento_financieros', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('tipo_movimiento');
            $table->string('titulo');
            $table->text('observaciones')->nullable()->default(null);
            $table->float('monto', 15, 2);
            $table->date('fecha');
            $table->unsignedInteger('id_categoria_ingreso')->nullable()->default(null);
            $table->unsignedInteger('id_categoria_egreso')->nullable()->default(null);
            $table->unsignedInteger('id_caja')->nullable()->default(null);
            $table->unsignedInteger('id_cliente')->nullable()->default(null);
            $table->unsignedInteger('id_proveedor')->nullable()->default(null);
            $table->unsignedInteger('id_registrador')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_caja', 'movimiento_financieros_cajas_fk')->references('id')->on('cajas');
            $table->foreign('id_categoria_ingreso', 'movimiento_financieros_categoria_ingresos_fk')->references('id')->on('categoria_ingresos');
            $table->foreign('id_categoria_egreso', 'movimiento_financieros_categoria_egresos_fk')->references('id')->on('categoria_egresos');
            $table->foreign('id_cliente', 'movimiento_financieros_clientes_fk')->references('id')->on('users');
            $table->foreign('id_proveedor', 'movimiento_financieros_proveedores_fk')->references('id')->on('proveedors');
            $table->foreign('id_registrador', 'movimiento_financieros_registradores_fk')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->dropForeign('movimiento_financieros_cajas_fk');
            $table->dropForeign('movimiento_financieros_categoria_ingresos_fk');
            $table->dropForeign('movimiento_financieros_categoria_egresos_fk');
            $table->dropForeign('movimiento_financieros_clientes_fk');
            $table->dropForeign('movimiento_financieros_proveedores_fk');
            $table->dropForeign('movimiento_financieros_registradores_fk');
        });
        Schema::drop('movimiento_financieros');
    }
}
