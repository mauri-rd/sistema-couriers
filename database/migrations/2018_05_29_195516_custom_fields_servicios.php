<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomFieldsServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicios', function (Blueprint $table) {
            $table->string('direccion');
            $table->string('telefono');
            $table->string('gmap');
            $table->dropColumn('descripcion');
            $table->dropColumn('precio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicios', function (Blueprint $table) {
            $table->dropColumn('direccion');
            $table->dropColumn('telefono');
            $table->dropColumn('gmap');
            $table->text('descripcion');
            $table->string('precio');
        });
    }
}
