<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_usuario');
            $table->float('total');
            $table->timestamps();

            $table->foreign('id_usuario', 'invoices_users_fk')
                ->references('id')
                ->on('users');
        });

        \DB::statement("ALTER TABLE invoices AUTO_INCREMENT = 10000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function(Blueprint $table) {
            $table->dropForeign('invoices_users_fk');
        });
        Schema::drop('invoices');
    }
}
