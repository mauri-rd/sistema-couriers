<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServicesFieldsPacklists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packlists', function (Blueprint $table) {
            $table->unsignedTinyInteger('tipo_flete')->default(\App\Enum\TipoFlete::NORMAL);
            $table->boolean('seguro')->default(false);
            $table->boolean('consolidacao')->default(false);
            $table->boolean('destino')->default(\App\Enum\Destino::PARAGUAY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packlists', function (Blueprint $table) {
            $table->dropColumn('tipo_flete');
            $table->dropColumn('seguro');
            $table->dropColumn('consolidacao');
            $table->dropColumn('destino');
        });
    }
}
