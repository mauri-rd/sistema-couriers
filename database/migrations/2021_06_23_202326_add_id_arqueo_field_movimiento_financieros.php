<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdArqueoFieldMovimientoFinancieros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->unsignedInteger('id_arqueo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->dropColumn('id_arqueo');
        });
    }
}
