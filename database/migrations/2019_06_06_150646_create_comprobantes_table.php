<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_cliente');
            $table->float('total_usd');
            $table->float('total_brl');
            $table->float('arbitraje');
            $table->string('comprobante');
            $table->boolean('aceptado')->default(false);
            $table->boolean('procesado')->default(false);
            $table->timestamps();

            $table->foreign('id_cliente', 'comprobantes_users_fk')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->dropForeign('comprobantes_users_fk');
        });
        Schema::drop('comprobantes');
    }
}
