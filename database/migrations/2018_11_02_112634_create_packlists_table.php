<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packlists', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_usuario');
            $table->string('tracking')->unique();
            $table->string('courier');
            $table->text('descripcion');
            $table->float('valor');
            $table->string('moneda');
            $table->string('file');
            $table->string('mime');
            $table->timestamps();

            $table->foreign('id_usuario', 'packlists_users_fk')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packlists', function(Blueprint $table) {
            $table->dropForeign('packlists_users_fk');
        });
        Schema::drop('packlists');
    }
}
