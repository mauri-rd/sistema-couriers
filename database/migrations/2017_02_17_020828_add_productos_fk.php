<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductosFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->foreign('id_modelo', 'productos_modelos_fk')->references('id')->on('modelos');
            $table->foreign('id_categoria', 'productos_categoria_productos_fk')->references('id')->on('categoria_productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->dropForeign('productos_modelos_fk');
            $table->dropForeign('productos_categoria_productos_fk');
        });
    }
}
