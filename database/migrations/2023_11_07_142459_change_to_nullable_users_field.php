<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeToNullableUsersField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable()->change();
            // $table->string('country')->nullable()->change();
            // $table->string('city')->nullable()->change();
            // $table->string('state')->nullable()->change();
            // $table->string('zip')->nullable()->change();
            $table->date('birthday')->nullable()->change();
            $table->string('casilla')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('password')->nullable()->change();
            $table->text('address')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->change();
            // $table->string('country')->change();
            // $table->string('city')->change();
            // $table->string('state')->change();
            // $table->string('zip')->change();
            $table->date('birthday')->change();
            $table->string('casilla')->change();
            $table->string('email')->change();
            $table->string('password')->change();
            $table->text('address')->change();
        });
    }
}
