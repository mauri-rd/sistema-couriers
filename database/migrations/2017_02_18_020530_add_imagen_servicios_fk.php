<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenServiciosFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imagen_servicios', function (Blueprint $table) {
            $table->foreign('id_servicio', 'imagen_servicios_servicios_fk')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagen_servicios', function (Blueprint $table) {
            $table->dropForeign('imagen_servicios_servicios_fk');
        });
    }
}
