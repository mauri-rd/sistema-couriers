<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiciosUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedTinyInteger('tipo_flete')->default(\App\Enum\TipoFlete::NORMAL);
            $table->boolean('seguro')->default(false);
            $table->boolean('consolidacao')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tipo_flete');
            $table->dropColumn('seguro');
            $table->dropColumn('consolidacao');
        });
    }
}
