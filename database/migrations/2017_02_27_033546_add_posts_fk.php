<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostsFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('id_categoria', 'posts_categoria_posts_fk')
                ->references('id')->on('categoria_posts');
            $table->foreign('id_autor', 'posts_users_fk')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_categoria_posts_fk');
            $table->dropForeign('posts_users_fk');
        });
    }
}
