<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceFieldMovimientoFinancieros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->unsignedInteger('id_invoice')->nullable()->default(null);

            $table->foreign('id_invoice', 'movimiento_financieros_invoices_fk')
                ->references('id')
                ->on('invoices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimiento_financieros', function (Blueprint $table) {
            $table->dropForeign('movimiento_financieros_invoices_fk');
            $table->dropColumn('id_invoice');
        });
    }
}
