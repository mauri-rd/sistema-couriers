<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_invoice');
            $table->unsignedInteger('id_paquete');
            $table->timestamps();

            $table->foreign('id_invoice', 'item_invoices_invoices_fk')
                ->references('id')
                ->on('invoices');

            $table->foreign('id_paquete', 'item_invoices_paquetes_fk')
                ->references('id')
                ->on('paquetes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_invoices', function(Blueprint $table) {
            $table->dropForeign('item_invoices_invoices_fk');
            $table->dropForeign('item_invoices_paquetes_fk');
        });
        Schema::drop('item_invoices');
    }
}
