<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_usuario');
            $table->string('codigo');
            $table->text('descripcion');
            $table->string('tracking');
            $table->unsignedTinyInteger('courier');
            $table->float('gramos');
            $table->float('precio_kg');
            $table->unsignedTinyInteger('estado');
            $table->timestamps();

            $table->foreign('id_usuario', 'paquetes_usuarios_fk')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquetes', function(Blueprint $table) {
            $table->dropForeign('paquetes_usuarios_fk');
        });
        Schema::drop('paquetes');
    }
}
