<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPorUnidadFieldPaquetes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->unsignedTinyInteger('tipo_precio')->default(\App\Enum\TipoPrecioPaquete::POR_PESO);
            $table->double('unidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->dropColumn('tipo_precio');
            $table->dropColumn('unidades');
        });
    }
}
