<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperadorCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operador_cajas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_caja');
            $table->timestamps();

            $table->foreign('id_user', 'operador_cajas_users_fk')
                ->references('id')
                ->on('users');
            $table->foreign('id_caja', 'operador_cajas_cajas_fk')
                ->references('id')
                ->on('cajas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operador_cajas', function (Blueprint $table) {
            $table->dropForeign('operador_cajas_users_fk');
            $table->dropForeign('operador_cajas_cajas_fk');
        });
        Schema::drop('operador_cajas');
    }
}
