<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'API'], function() {

    // Route::get('blog/categorias', 'BlogCategoryController@index');

    // Route::get('blog', 'BlogController@index');
    // Route::get('blog/portada', 'BlogController@portada');
    // Route::get('blog/{id}', 'BlogController@show');

    // Route::get('multimedia/categorias', 'MultimediaCategoryController@index');

    // Route::get('multimedia', 'MultimediaController@index');
    // Route::get('multimedia/{id}', 'MultimediaController@show');

    Route::get('social', 'SocialController@all');
    Route::get('banner', 'PatrocinadoresController@index');
    Route::get('productos', 'ProductosController@index');
    Route::get('sucursales', 'SucursalesController@index');

    Route::get('informaciones', 'InfoController@index');
    Route::get('contactos', 'ContactosController@index');

    Route::get('send', 'EmailController@send');

    Route::get('send-paquetes-multas', 'MultasController@send');

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', ['as' => 'api.auth.login', 'uses' => 'AuthController@login']);
        Route::post('register', ['as' => 'api.auth.register', 'uses' => 'AuthController@register']);
        Route::post('resetPassword', ['as' => 'api.auth.resetPassword', 'uses' => 'AuthController@resetPassword']);
    });

    Route::group(['prefix' => 'packages'], function () {
        Route::post('allPackages', ['as' => 'api.packages.allPackages', 'uses' => 'PackagesController@allPackages']);
        Route::post('packageDetails', ['as' => 'api.packages.packageDetails', 'uses' => 'PackagesController@packageDetails']);
        Route::post('rateService', ['as' => 'api.packages.rateService', 'uses' => 'PackagesController@rateService']);
    });

    Route::group(['prefix' => 'user'], function () {
        Route::post('getUserData', ['as' => 'api.users.getUserData', 'uses' => 'UserController@getUserData']);
        Route::post('updateUserData', ['as' => 'api.users.updateUserData', 'uses' => 'UserController@updateUserData']);
        Route::post('updateUserPassword', ['as' => 'api.users.updateUserPassword', 'uses' => 'UserController@updateUserPassword']);
        Route::post('updateUserFcmToken', ['as' => 'api.users.updateUserFcmToken', 'uses' => 'UserController@updateUserFcmToken']);
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::post('allNotifications', ['as' => 'api.notifications.allNotifications', 'uses' => 'NotificationsController@allNotifications']);
        Route::post('notificationDetails', ['as' => 'api.notifications.notificationDetails', 'uses' => 'NotificationsController@notificationDetails']);
    });

    Route::group(['prefix' => 'support'], function () {
        Route::post('submit', ['as' => 'api.support.submit', 'uses' => 'SupportController@submit']);
    });

});
