<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', function(){
    return redirect()->route('admin.login');
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('logout', 'Auth\AuthController@logout');
    Route::get('/', function() {
        return redirect()->route('admin.dashboard');
    });
    Route::group(['middleware' => ['guest']], function() {
        Route::get('login', ['as' => 'admin.login', 'uses' => 'Auth\AuthController@showLoginForm']);
        Route::post('login', 'Auth\AuthController@login');
    });
    Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'operador']], function() {
        Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

        Route::group(['prefix' => 'catproductos'], function() {
            Route::get('/', ['as' => 'admin.catproductos.index', 'uses' => 'CategoriaProductosController@index']);
            Route::get('registrar', ['as' => 'admin.catproductos.create', 'uses' => 'CategoriaProductosController@create']);
            Route::post('registrar', ['as' => 'admin.catproductos.store', 'uses' => 'CategoriaProductosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.catproductos.edit', 'uses' => 'CategoriaProductosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.catproductos.update', 'uses' => 'CategoriaProductosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.catproductos.delete', 'uses' => 'CategoriaProductosController@delete']);
        });

        Route::group(['prefix' => 'containers'], function() {
            Route::get('/', ['as' => 'admin.containers.index', 'uses' => 'ContainersController@index']);
            Route::get('create', ['as' => 'admin.containers.create', 'uses' => 'ContainersController@create']);
            Route::post('registrar', ['as' => 'admin.containers.store', 'uses' => 'ContainersController@store']);
            Route::post('registrar-modal', ['as' => 'admin.containers.storeModal', 'uses' => 'ContainersController@storeModal']);
            Route::get('editar/{id}', ['as' => 'admin.containers.edit', 'uses' => 'ContainersController@edit']);
            Route::put('actualizar/{id}', ['as' => 'admin.containers.update', 'uses' => 'ContainersController@update']);
            Route::put('actualizar-estado/{id}', ['as' => 'admin.containers.update-estado', 'uses' => 'ContainersController@updateEstado']);
            Route::get('eliminar/{id}', ['as' => 'admin.containers.delete', 'uses' => 'ContainersController@delete']);
            Route::get('get-containers', ['as' => 'admin.containers.get-containers', 'uses' => 'ContainersController@getContainers']);
        });

        Route::group(['prefix' => 'usuarios'], function() {
            Route::get('/', ['as' => 'admin.usuarios.index', 'uses' => 'UsuariosController@index']);
            Route::get('exportar-csv', ['as' => 'admin.usuarios.exportar-csv', 'uses' => 'UsuariosController@exportarCsv']);
            Route::get('registrar', ['as' => 'admin.usuarios.create', 'uses' => 'UsuariosController@create']);
            Route::post('registrar', ['as' => 'admin.usuarios.store', 'uses' => 'UsuariosController@store']);
            Route::get('activar/{id}', ['as' => 'admin.usuarios.activate', 'uses' => 'UsuariosController@activate']);
            Route::get('editar/{id}', ['as' => 'admin.usuarios.edit', 'uses' => 'UsuariosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.usuarios.update', 'uses' => 'UsuariosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.usuarios.delete', 'uses' => 'UsuariosController@delete']);
        });

        Route::group(['prefix' => 'paquetes'], function() {
            Route::get('/', ['as' => 'admin.paquetes.index', 'uses' => 'PaquetesController@index']);
            Route::get('registrar', ['as' => 'admin.paquetes.create', 'uses' => 'PaquetesController@create']);
            Route::get('data-paquete', ['as' => 'admin.paquetes.data-paquete', 'uses' => 'PaquetesController@dataPaquete']);
            Route::post('registrar', ['as' => 'admin.paquetes.store', 'uses' => 'PaquetesController@store']);
            Route::post('update-estado', ['as' => 'admin.paquetes.update-estado', 'uses' => 'PaquetesController@updateEstado']);
            Route::post('change-user-paquete', ['as' => 'admin.paquetes.change-user-paquete', 'uses' => 'PaquetesController@changeUserPaquete']);
            Route::get('editar/{id}', ['as' => 'admin.paquetes.edit', 'uses' => 'PaquetesController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.paquetes.update', 'uses' => 'PaquetesController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.paquetes.delete', 'uses' => 'PaquetesController@delete']);
            Route::get('buscar-ajax', ['as' => 'admin.paquetes.buscar-ajax', 'uses' => 'PaquetesController@buscarAjax']);
            Route::get('buscar-ajax-multi', ['as' => 'admin.paquetes.buscar-ajax-multi', 'uses' => 'PaquetesController@buscarAjaxMulti']);
            Route::get('actualizar-estado', ['as' => 'admin.paquetes.form-cambio-estado', 'uses' => 'PaquetesController@formCambioEstado']);
            Route::get('actualizar-estado-test', ['as' => 'admin.paquetes.form-cambio-estado-test', 'uses' => 'PaquetesController@formCambioEstadoTest']);
            Route::post('actualizar-estado', ['as' => 'admin.paquetes.cambio-estado', 'uses' => 'PaquetesController@cambioEstado']);
            Route::get('packlist', ['as' => 'admin.paquetes.find-packlist', 'uses' => 'PaquetesController@findPacklist']);
            Route::get('generar-etiquetas/{id}', ['as' => 'admin.paquetes.generar-etiquetas', 'uses' => 'PaquetesController@generarEtiquetas']);
            Route::get('dias', ['as' => 'admin.paquetes.dias', 'uses' => 'DiasController@dias']);
        });

        Route::group(['prefix' => 'pagos'], function() {
            Route::get('/', ['as' => 'admin.pagos.index', 'uses' => 'PagosController@index']);
            Route::get('procesar/{pago}', ['as' => 'admin.pagos.procesar', 'uses' => 'PagosController@procesar']);
        });

        Route::group(['prefix' => 'message'], function() {
            Route::get('/', ['as' => 'admin.messages.index', 'uses' => 'MessageController@index']);
            Route::post('/', ['as' => 'admin.messages.submit', 'uses' => 'MessageController@submit']);
        });

        Route::group(['prefix' => 'invoices'], function() {
            Route::get('/', ['as' => 'admin.invoices.index', 'uses' => 'InvoicesController@index']);
            Route::get('registrar', ['as' => 'admin.invoices.create', 'uses' => 'InvoicesController@create']);
            Route::post('registrar', ['as' => 'admin.invoices.store', 'uses' => 'InvoicesController@store']);
            Route::post('join', ['as' => 'admin.invoices.join', 'uses' => 'InvoicesController@join']);
            Route::get('{id}', ['as' => 'admin.invoices.show', 'uses' => 'InvoicesController@show']);
            Route::get('{id}/pago', ['as' => 'admin.invoices.pago', 'uses' => 'InvoicesController@pago']);
            Route::get('{id}/hide', ['as' => 'admin.invoices.hide', 'uses' => 'InvoicesController@hide']);
            Route::get('{id}/send-email', ['as' => 'admin.invoices.email', 'uses' => 'InvoicesController@email']);
            Route::post('{id}/payment-amount', ['as' => 'admin.invoices.pay-amount', 'uses' => 'InvoicesController@payAmount']);
        });


        Route::get('invoice-manual/get-user-data', ['as' => 'admin.invoice-manual.get-user-data', 'uses' => 'InvoiceManualController@getUserData']);
        // Route::resource('invoice-manual', 'InvoiceManualController');
        Route::get('invoice-manual', ['as' => 'admin.invoice-manual.index', 'uses' => 'InvoiceManualController@index']);
        Route::get('invoice-manual/create', ['as' => 'admin.invoice-manual.create', 'uses' => 'InvoiceManualController@create']);
        Route::post('invoice-manual/store', ['as' => 'admin.invoice-manual.store', 'uses' => 'InvoiceManualController@store']);
        Route::get('invoice-manual/{id}/print-view', ['as' => 'admin.invoice-manual.print-view', 'uses' => 'InvoiceManualController@printView']);

        Route::group(['prefix' => 'empresas'], function() {
            Route::get('/', ['as' => 'admin.empresas.index', 'uses' => 'EmpresasController@index']);
            Route::get('registrar', ['as' => 'admin.empresas.create', 'uses' => 'EmpresasController@create']);
            Route::post('registrar', ['as' => 'admin.empresas.store', 'uses' => 'EmpresasController@store']);
            Route::get('editar/{id}', ['as' => 'admin.empresas.edit', 'uses' => 'EmpresasController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.empresas.update', 'uses' => 'EmpresasController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.empresas.delete', 'uses' => 'EmpresasController@delete']);
        });

        Route::group(['prefix' => 'sucursales'], function() {
            Route::get('/', ['as' => 'admin.sucursales.index', 'uses' => 'SucursalesController@index']);
            Route::get('registrar', ['as' => 'admin.sucursales.create', 'uses' => 'SucursalesController@create']);
            Route::post('registrar', ['as' => 'admin.sucursales.store', 'uses' => 'SucursalesController@store']);
            Route::get('editar/{id}', ['as' => 'admin.sucursales.edit', 'uses' => 'SucursalesController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.sucursales.update', 'uses' => 'SucursalesController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.sucursales.delete', 'uses' => 'SucursalesController@delete']);
        });

        Route::group(['prefix' => 'preguntas'], function() {
            Route::get('/', ['as' => 'admin.preguntas.index', 'uses' => 'PreguntasController@index']);
            Route::get('registrar', ['as' => 'admin.preguntas.create', 'uses' => 'PreguntasController@create']);
            Route::post('registrar', ['as' => 'admin.preguntas.store', 'uses' => 'PreguntasController@store']);
            Route::post('reordenar', ['as' => 'admin.preguntas.reordenar', 'uses' => 'PreguntasController@reordenar']);
            Route::get('editar/{id}', ['as' => 'admin.preguntas.edit', 'uses' => 'PreguntasController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.preguntas.update', 'uses' => 'PreguntasController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.preguntas.delete', 'uses' => 'PreguntasController@delete']);
        });

        Route::group(['prefix' => 'marcas'], function() {
            Route::get('/', ['as' => 'admin.marcas.index', 'uses' => 'MarcasController@index']);
            Route::get('registrar', ['as' => 'admin.marcas.create', 'uses' => 'MarcasController@create']);
            Route::post('registrar', ['as' => 'admin.marcas.store', 'uses' => 'MarcasController@store']);
            Route::get('editar/{id}', ['as' => 'admin.marcas.edit', 'uses' => 'MarcasController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.marcas.update', 'uses' => 'MarcasController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.marcas.delete', 'uses' => 'MarcasController@delete']);
        });

        Route::group(['prefix' => 'modelos'], function() {
            Route::get('/', ['as' => 'admin.modelos.index', 'uses' => 'ModelosController@index']);
            Route::get('registrar', ['as' => 'admin.modelos.create', 'uses' => 'ModelosController@create']);
            Route::post('registrar', ['as' => 'admin.modelos.store', 'uses' => 'ModelosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.modelos.edit', 'uses' => 'ModelosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.modelos.update', 'uses' => 'ModelosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.modelos.delete', 'uses' => 'ModelosController@delete']);
        });

        Route::group(['prefix' => 'productos'], function() {
            Route::get('/', ['as' => 'admin.productos.index', 'uses' => 'ProductosController@index']);
            Route::get('registrar', ['as' => 'admin.productos.create', 'uses' => 'ProductosController@create']);
            Route::post('registrar', ['as' => 'admin.productos.store', 'uses' => 'ProductosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.productos.edit', 'uses' => 'ProductosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.productos.update', 'uses' => 'ProductosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.productos.delete', 'uses' => 'ProductosController@delete']);
        });

        Route::group(['prefix' => 'servicios'], function() {
            Route::get('/', ['as' => 'admin.servicios.index', 'uses' => 'ServiciosController@index']);
            Route::get('registrar', ['as' => 'admin.servicios.create', 'uses' => 'ServiciosController@create']);
            Route::post('registrar', ['as' => 'admin.servicios.store', 'uses' => 'ServiciosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.servicios.edit', 'uses' => 'ServiciosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.servicios.update', 'uses' => 'ServiciosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.servicios.delete', 'uses' => 'ServiciosController@delete']);
        });

        Route::group(['prefix' => 'cursos'], function() {
            Route::get('/', ['as' => 'admin.cursos.index', 'uses' => 'CursosController@index']);
            Route::get('registrar', ['as' => 'admin.cursos.create', 'uses' => 'CursosController@create']);
            Route::post('registrar', ['as' => 'admin.cursos.store', 'uses' => 'CursosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.cursos.edit', 'uses' => 'CursosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.cursos.update', 'uses' => 'CursosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.cursos.delete', 'uses' => 'CursosController@delete']);
        });
        
        Route::group(['prefix' => 'contactos'], function() {
            Route::get('/', ['as' => 'admin.contactos.index', 'uses' => 'ContactosController@index']);
            Route::get('registrar', ['as' => 'admin.contactos.create', 'uses' => 'ContactosController@create']);
            Route::post('registrar', ['as' => 'admin.contactos.store', 'uses' => 'ContactosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.contactos.edit', 'uses' => 'ContactosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.contactos.update', 'uses' => 'ContactosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.contactos.delete', 'uses' => 'ContactosController@delete']);
        });

        Route::group(['prefix' => 'slides'], function() {
            Route::get('/', ['as' => 'admin.patrocinadores.index', 'uses' => 'SlidesController@index']);
            Route::get('registrar', ['as' => 'admin.patrocinadores.create', 'uses' => 'SlidesController@create']);
            Route::post('registrar', ['as' => 'admin.patrocinadores.store', 'uses' => 'SlidesController@store']);
            Route::get('editar/{id}', ['as' => 'admin.patrocinadores.edit', 'uses' => 'SlidesController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.patrocinadores.update', 'uses' => 'SlidesController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.patrocinadores.delete', 'uses' => 'SlidesController@delete']);
        });

        Route::group(['prefix' => 'catmultimedias'], function() {
            Route::get('/', ['as' => 'admin.catmultimedias.index', 'uses' => 'CategoriaMultimediaController@index']);
            Route::get('registrar', ['as' => 'admin.catmultimedias.create', 'uses' => 'CategoriaMultimediaController@create']);
            Route::post('registrar', ['as' => 'admin.catmultimedias.store', 'uses' => 'CategoriaMultimediaController@store']);
            Route::get('editar/{id}', ['as' => 'admin.catmultimedias.edit', 'uses' => 'CategoriaMultimediaController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.catmultimedias.update', 'uses' => 'CategoriaMultimediaController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.catmultimedias.delete', 'uses' => 'CategoriaMultimediaController@delete']);
        });
        Route::group(['prefix' => 'multimedias'], function() {
            Route::get('/', ['as' => 'admin.multimedias.index', 'uses' => 'MultimediaController@index']);
            Route::get('registrar', ['as' => 'admin.multimedias.create', 'uses' => 'MultimediaController@create']);
            Route::post('registrar', ['as' => 'admin.multimedias.store', 'uses' => 'MultimediaController@store']);
            Route::get('editar/{id}', ['as' => 'admin.multimedias.edit', 'uses' => 'MultimediaController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.multimedias.update', 'uses' => 'MultimediaController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.multimedias.delete', 'uses' => 'MultimediaController@delete']);
        });

        Route::get('informaciones', ['as' => 'admin.configuraciones.index', 'uses' => 'ConfiguracionesController@index']);
        Route::post('informaciones', ['as' => 'admin.configuraciones.save', 'uses' => 'ConfiguracionesController@save']);

        Route::group(['prefix' => 'catposts'], function() {
            Route::get('/', ['as' => 'admin.catposts.index', 'uses' => 'CategoriaPostsController@index']);
            Route::get('registrar', ['as' => 'admin.catposts.create', 'uses' => 'CategoriaPostsController@create']);
            Route::post('registrar', ['as' => 'admin.catposts.store', 'uses' => 'CategoriaPostsController@store']);
            Route::get('editar/{id}', ['as' => 'admin.catposts.edit', 'uses' => 'CategoriaPostsController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.catposts.update', 'uses' => 'CategoriaPostsController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.catposts.delete', 'uses' => 'CategoriaPostsController@delete']);
        });

        Route::group(['prefix' => 'posts'], function() {
            Route::get('/', ['as' => 'admin.posts.index', 'uses' => 'PostsController@index']);
            Route::get('registrar', ['as' => 'admin.posts.create', 'uses' => 'PostsController@create']);
            Route::post('registrar', ['as' => 'admin.posts.store', 'uses' => 'PostsController@store']);
            Route::get('editar/{id}', ['as' => 'admin.posts.edit', 'uses' => 'PostsController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.posts.update', 'uses' => 'PostsController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.posts.delete', 'uses' => 'PostsController@delete']);
        });

        /* FINANCIERO */
        Route::group(['prefix' => 'cajas'], function() {
            Route::get('/', ['as' => 'admin.cajas.index', 'uses' => 'CajasController@index']);
            Route::get('registrar', ['as' => 'admin.cajas.create', 'uses' => 'CajasController@create']);
            Route::post('registrar', ['as' => 'admin.cajas.store', 'uses' => 'CajasController@store']);
            Route::get('editar/{id}', ['as' => 'admin.cajas.edit', 'uses' => 'CajasController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.cajas.update', 'uses' => 'CajasController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.cajas.delete', 'uses' => 'CajasController@delete']);
            Route::get('arqueos/{id}', ['as' => 'admin.cajas.arqueos', 'uses' => 'CajasController@arqueos']);
        });
        Route::group(['prefix' => 'cajas'], function() {
            Route::get('{caja}/movimientos/proveedores', ['as' => 'admin.cajas.movimientos.proveedores-form', 'uses' => 'MovimientosFinancierosController@getProveedores']);
            Route::get('{caja}/movimientos/clientes', ['as' => 'admin.cajas.movimientos.clientes-form', 'uses' => 'MovimientosFinancierosController@getClientes']);

            Route::get('{caja}/movimientos', ['as' => 'admin.cajas.movimientos.index', 'uses' => 'MovimientosFinancierosController@index']);
            Route::get('{caja}/movimientos/registrar', ['as' => 'admin.cajas.movimientos.create', 'uses' => 'MovimientosFinancierosController@create']);
            Route::post('{caja}/movimientos/registrar', ['as' => 'admin.cajas.movimientos.store', 'uses' => 'MovimientosFinancierosController@store']);
            Route::get('{caja}/movimientos/editar/{id}', ['as' => 'admin.cajas.movimientos.edit', 'uses' => 'MovimientosFinancierosController@edit']);
            Route::post('{caja}/movimientos/editar/{id}', ['as' => 'admin.cajas.movimientos.update', 'uses' => 'MovimientosFinancierosController@update']);
            Route::get('{caja}/movimientos/eliminar/{id}', ['as' => 'admin.cajas.movimientos.delete', 'uses' => 'MovimientosFinancierosController@delete']);

            Route::get('{caja}/movimientos/transferencia', ['as' => 'admin.cajas.movimientos.transferencia-form', 'uses' => 'MovimientosFinancierosController@transferenciaForm']);
            Route::post('{caja}/movimientos/transferencia', ['as' => 'admin.cajas.movimientos.transferencia', 'uses' => 'MovimientosFinancierosController@transferencia']);

            Route::get('{caja}/movimientos/abrir-caja', ['as' => 'admin.cajas.movimientos.abrir-arqueo-form', 'uses' => 'MovimientosFinancierosController@abrirArqueoForm']);
            Route::post('{caja}/movimientos/abrir-caja', ['as' => 'admin.cajas.movimientos.abrir-arqueo', 'uses' => 'MovimientosFinancierosController@abrirArqueo']);
            Route::get('{caja}/movimientos/cerrar-caja', ['as' => 'admin.cajas.movimientos.cerrar-arqueo', 'uses' => 'MovimientosFinancierosController@cerrarArqueo']);
        });

        Route::group(['prefix' => 'categorias-ingresos'], function() {
            Route::get('/', ['as' => 'admin.categorias-ingresos.index', 'uses' => 'CategoriasIngresosController@index']);
            Route::get('registrar', ['as' => 'admin.categorias-ingresos.create', 'uses' => 'CategoriasIngresosController@create']);
            Route::post('registrar', ['as' => 'admin.categorias-ingresos.store', 'uses' => 'CategoriasIngresosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.categorias-ingresos.edit', 'uses' => 'CategoriasIngresosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.categorias-ingresos.update', 'uses' => 'CategoriasIngresosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.categorias-ingresos.delete', 'uses' => 'CategoriasIngresosController@delete']);
        });
        Route::group(['prefix' => 'categorias-egresos'], function() {
            Route::get('/', ['as' => 'admin.categorias-egresos.index', 'uses' => 'CategoriasEgresosController@index']);
            Route::get('registrar', ['as' => 'admin.categorias-egresos.create', 'uses' => 'CategoriasEgresosController@create']);
            Route::post('registrar', ['as' => 'admin.categorias-egresos.store', 'uses' => 'CategoriasEgresosController@store']);
            Route::get('editar/{id}', ['as' => 'admin.categorias-egresos.edit', 'uses' => 'CategoriasEgresosController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.categorias-egresos.update', 'uses' => 'CategoriasEgresosController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.categorias-egresos.delete', 'uses' => 'CategoriasEgresosController@delete']);
        });
        Route::group(['prefix' => 'proveedores'], function() {
            Route::get('/', ['as' => 'admin.proveedores.index', 'uses' => 'ProveedoresController@index']);
            Route::get('registrar', ['as' => 'admin.proveedores.create', 'uses' => 'ProveedoresController@create']);
            Route::post('registrar', ['as' => 'admin.proveedores.store', 'uses' => 'ProveedoresController@store']);
            Route::get('editar/{id}', ['as' => 'admin.proveedores.edit', 'uses' => 'ProveedoresController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.proveedores.update', 'uses' => 'ProveedoresController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.proveedores.delete', 'uses' => 'ProveedoresController@delete']);
        });

        Route::group(['prefix' => 'cotizaciones'], function() {
            Route::get('/', ['as' => 'admin.cotizaciones.index', 'uses' => 'CotizacionesController@index']);
            Route::get('registrar', ['as' => 'admin.cotizaciones.create', 'uses' => 'CotizacionesController@create']);
            Route::post('registrar', ['as' => 'admin.cotizaciones.store', 'uses' => 'CotizacionesController@store']);
            Route::get('editar/{id}', ['as' => 'admin.cotizaciones.edit', 'uses' => 'CotizacionesController@edit']);
            Route::post('editar/{id}', ['as' => 'admin.cotizaciones.update', 'uses' => 'CotizacionesController@update']);
            Route::get('eliminar/{id}', ['as' => 'admin.cotizaciones.delete', 'uses' => 'CotizacionesController@delete']);
        });

        /* FIN FINANCIERO */

        Route::get('reportes', ['as' => 'admin.reportes.index', 'uses' => 'ReportesController@index']);
        Route::post('reportes/generar', ['as' => 'admin.reportes.generar', 'uses' => 'ReportesController@generar']);
        Route::get('dias', ['as' => 'admin.dias.index', 'uses' => 'DiasController@dias']);
    });
});

Route::group(['namespace' => 'Front'], function() {
    Route::get('/', ['as' => 'front.home', 'uses' => 'HomeController@index']);
    
    Route::group(['prefix' => 'my-account'], function() {

        Route::group(['middleware' => ['auth.client-redirect']], function () {
            Route::get('acceder', ['as' => 'front.login-form', 'uses' => 'UserLoginController@loginForm']);
            Route::post('login', ['as' => 'front.login', 'uses' => 'UserLoginController@doLogin']);
            Route::post('register', ['as' => 'front.register', 'uses' => 'UserLoginController@doRegister']);

            Route::get('password-reset', ['as' => 'front.password-reset.form', 'uses' => 'PasswordController@form']);
            Route::post('password-reset', ['as' => 'front.password-reset.request-reset', 'uses' => 'PasswordController@requestReset']);
            Route::get('password-reset/reset', ['as' => 'front.password-reset.reset-form', 'uses' => 'PasswordController@resetForm']);
            Route::post('password-reset/reset', ['as' => 'front.password-reset.reset', 'uses' => 'PasswordController@reset']);
        });

        Route::group(['middleware' => ['auth.client']], function() {
            Route::get('logout', ['as' => 'front.logout', 'uses' => 'UserLoginController@logout']);

            Route::group(['middleware' => ['user-lang']], function () {

                Route::get('set-language', ['as' => 'front.set-language', 'uses' => 'LanguageController@setLanguage']);

                Route::get('my-account', ['as' => 'front.my-account.perfil.index', 'uses' => 'PerfilController@index']);
                Route::post('my-account', ['as' => 'front.my-account.perfil.save', 'uses' => 'PerfilController@save']);

                Route::group(['middleware' => ['ci']], function() {
                    Route::get('addresses', ['as' => 'front.my-account.addresses', 'uses' => 'AddressesController@index']);

                    Route::get('paquetes', ['as' => 'front.my-account.index', 'uses' => 'PaquetesController@index']);
                    Route::get('paquetes/miami-china', ['as' => 'front.my-account.paquetes-miami', 'uses' => 'PaquetesController@miami']);
                    Route::get('paquetes/transito', ['as' => 'front.my-account.paquetes-transito', 'uses' => 'PaquetesController@transito']);
                    Route::get('paquetes/aduana', ['as' => 'front.my-account.paquetes-paraguay', 'uses' => 'PaquetesController@paraguay']);
                    Route::get('paquetes/oficina', ['as' => 'front.my-account.paquetes-oficina', 'uses' => 'PaquetesController@oficina']);
                    Route::get('paquetes/retirado', ['as' => 'front.my-account.paquetes-retirado', 'uses' => 'PaquetesController@retirado']);

                    Route::post('paquetes/pagar-form', ['as' => 'front.my-account.pagar-form', 'uses' => 'PaquetesController@pagarForm']);
                    Route::post('paquetes/pagar', ['as' => 'front.my-account.pagar', 'uses' => 'PaquetesController@pagar']);


                    Route::group(['prefix' => 'services'], function() {
                        Route::get('/', ['as' => 'front.my-account.services.index', 'uses' => 'ServicesController@index']);
                        Route::post('/', ['as' => 'front.my-account.services.store', 'uses' => 'ServicesController@store']);
                    });

                    Route::group(['prefix' => 'packlists'], function() {
                        Route::get('/', ['as' => 'front.my-account.packlists.index', 'uses' => 'PacklistsController@index']);
                        Route::get('registrar', ['as' => 'front.my-account.packlists.create', 'uses' => 'PacklistsController@create']);
                        Route::post('registrar', ['as' => 'front.my-account.packlists.store', 'uses' => 'PacklistsController@store']);
                        Route::get('editar/{id}', ['as' => 'front.my-account.packlists.edit', 'uses' => 'PacklistsController@edit']);
                        Route::post('editar/{id}', ['as' => 'front.my-account.packlists.update', 'uses' => 'PacklistsController@update']);
                        Route::get('eliminar/{id}', ['as' => 'front.my-account.packlists.delete', 'uses' => 'PacklistsController@delete']);
                        Route::get('check-tracking', ['as' => 'front.my-account.packlists.check-tracking', 'uses' => 'PacklistsController@checkTracking']);
                    });
                });
            });
        });
    });
});
