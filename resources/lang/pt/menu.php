<?php

return [
    'welcome' => 'Bem vindo/a,',
    'logout' => 'Sair',
    'my-profile' => 'Meu Perfil',
    'my-addresses' => 'Meus Endereços',
    'my-services' => 'Meus Serviços',
    'my-packages' => 'Meus Pacotes',
    'my-packages-miami' => 'Em Miami/China',
    'my-packages-transito' => 'Em Transito',
    'my-packages-aduana' => 'Em Aduana',
    'my-packages-oficina' => 'Para Retirar',
    'my-packages-retirado' => 'Retirado',
    'my-packages-pay' => 'Pagar Pacotes',

    'my-packlists' => 'Meus Packlists',
];