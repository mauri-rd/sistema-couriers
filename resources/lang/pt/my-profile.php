<?php

return [
    'name-field' => 'Nome',
    'lastname-field' => 'Sobrenome',
    'phone-field' => 'Telefone',
    'address-field' => 'Endereço',
    'country-field' => 'Pais',
    'city-field' => 'Cidade',
    'state-field' => 'Estado',
    'postalcode-field' => 'CEP/Código postal',
    'password-field' => 'Sennha (Somente preencha se deseja alterar a senha)',
];