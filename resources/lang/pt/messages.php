<?php

return [
    'required-field' => 'Campos obrigatórios',
    'reconfirm-name-lastname' => 'Por favor, reconfirme seu nome e sobrenome',
];