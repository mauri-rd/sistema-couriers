<?php

return [
    'transito-selected-title' => 'Pacotes em Transito',
    'aduana-selected-title' => 'Pacotes em Aduana',
    'retirar-selected-title' => 'Pacotes em Para Retirar',
    'retirados-selected-title' => 'Pacotes Retirados',
    'miami-selected-title' => 'Pacotes em Miami/China',
    'table-select' => 'Selecionar',
    'table-code' => 'Codigo do pacote',
    'table-tracking' => 'Tracking',
    'table-weight' => 'Peso',
    'table-description' => 'Descrição',
    'table-status' => 'Estado',
    'table-destiny' => 'Destino',
    'table-payment' => 'Pagamento',
    'table-updated-at' => 'Data de atualização',
    'table-subtotal' => 'Subtotal',
    'table-payed' => 'Pago',
    'table-not-payed' => 'Não pago',
    'table-empty' => 'Não existem pacotes nessa categoria.',
    'pay-button' => 'Pagar',
    'alert-select-package' => 'Por favor, selecione um pacote como mínimo para pagar',
    'paying-title' => 'Você está pagando os seguintes pacotes',
    'paying-total' => 'Total a pagar',
    'paying-wire-transfer-data' => 'Dados para pagamento',
    'paying-total-instructions' => 'Para efetuar o pagamento, você deve fazer uma transferência de <strong>#TOTAL</strong> para uma das seguintes contas. Posteriormente, você deve enviar a foto do comprovante de transferência por meio deste formulário.',
    'paying-send-button' => 'Enviar Comprobante',
];