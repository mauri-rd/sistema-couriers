<?php

return [
    'title' => 'A StrBox sempre pensa nos clientes e traz novidades para sua tranquilidade e para economizar!',
    'insurance' => 'Seguro',
    'insurance-text' => 'Com o valor adicional de 3% do seu invoice. Você pode ficar tranquilo pedindo com nós, que no evento de ocorrer algum problema com seu pacote, seja problema na aduana ou perda, devolveremos 100% do valor do invoice!',
];