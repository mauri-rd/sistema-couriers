<?php

return [
    'reset-password-title' => 'Recuperar senha',
    'reset-password-email-button' => 'Solicitar mudança de senha',
    'reset-password-new-password-field' => 'Nova senha',
    'reset-password-new-password-button' => 'Alterar senha',
];