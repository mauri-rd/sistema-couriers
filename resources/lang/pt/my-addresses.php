<?php

return [
    'airplane-delivery' => 'Envios Aéreos',
    'us-address' => 'Endereço nos EUA',
    'us-address-name' => 'NOME',
    'us-address-address' => 'ENDEREÇO',
    'us-address-city' => 'CIDADE',
    'us-address-state' => 'ESTADO',
    'us-address-country' => 'PAÍS',
    'us-address-postalcode' => 'COD POSTAL',
    'us-address-phone' => 'NUMERO',
    'china-address' => 'Endereço na China',
    'china-address-message' => 'Escreva-nos por whatsapp para solicitar o serviço',
    'boat-delivery' => 'Envios Maritimos',
];