<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login-email-placeholder' => 'Insira seu email',
    'login-password-placeholder' => 'Insira sua senha',
    'login-button' => 'Autenticar-se',

    'register-link' => 'Ainda não tenho cadastro',
    'reset-password-link' => 'Esqueci minha senha',
    'register-name-field' => 'Insira seu nome',
    'register-lastname-field' => 'Insira seu sobrenome',
    'register-phone-field' => 'Insira seu telefone',
    'register-birthday-field' => 'Insira sua data de nascimento',
    'register-address-field' => 'Insira seu endereço',
    'register-country-field' => 'Insira seu país',
    'register-city-field' => 'Insira sua cidade',
    'register-state-field' => 'Insira seu estado',
    'register-postalcode-field' => 'Insira seu codigo postal',
    'register-button' => 'Cadastrar-se',
    'register-login-link' => 'Já tenho cadastro',

];
