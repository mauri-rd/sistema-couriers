<?php

return [
    'required-field' => 'Campos obligatorios',
    'reconfirm-name-lastname' => 'Por favor, reconfirme su nombre y apellido',
];