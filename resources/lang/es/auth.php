<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'No fueron encontrados estos registros en nuestra base de datos',
    'throttle' => 'Muchos intentos fallidos. Por favor, intente nuevamente en :seconds segundos.',
    'login-email-placeholder' => 'Ingrese su e-mail',
    'login-password-placeholder' => 'Ingrese su contraseña',
    'login-button' => 'Iniciar Sesión',

    'register-link' => 'Registrarme',
    'reset-password-link' => 'Olvidé mi contraseña',
    'register-name-field' => 'Ingrese su nombre',
    'register-lastname-field' => 'Ingrese su apellido',
    'register-phone-field' => 'Ingrese su número de teléfono',
    'register-birthday-field' => 'Ingrese su fecha de nacimiento',
    'register-address-field' => 'Ingrese su dirección',
    'register-country-field' => 'Ingrese su país',
    'register-city-field' => 'Ingrese su ciudad',
    'register-state-field' => 'Ingrese su departamento/estado',
    'register-postalcode-field' => 'Ingrese su código postal',
    'register-button' => 'Registrarme',
    'register-login-link' => 'Iniciar Sesión',

];
