<?php

return [
    'name-field' => 'Nombre',
    'lastname-field' => 'Apellido',
    'phone-field' => 'Teléfono',
    'address-field' => 'Dirección',
    'country-field' => 'País',
    'city-field' => 'Ciudad',
    'state-field' => 'Estado',
    'postalcode-field' => 'Código postal',
    'password-field' => 'Contraseña (llene solamente si quiera cambiar la contraseña)',
];