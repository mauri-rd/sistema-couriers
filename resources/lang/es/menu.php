<?php

return [
    'welcome' => 'Bienvenido/a,',
    'logout' => 'Cerrar Sesión',
    'my-profile' => 'Mi Perfil',
    'my-addresses' => 'Mis Direcciones',
    'my-services' => 'Mis Servicios',
    'my-packages' => 'Mis Paquetes',
    'my-packages-miami' => 'En Miami/China',
    'my-packages-transito' => 'En Transito',
    'my-packages-aduana' => 'En Aduana',
    'my-packages-oficina' => 'Para Retirar',
    'my-packages-retirado' => 'Retirado',
    'my-packages-pay' => 'Pagar Paquetes',

    'my-packlists' => 'Mis Invoices de compras',
];