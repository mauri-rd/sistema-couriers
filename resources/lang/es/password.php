<?php

return [
    'reset-password-title' => 'Recuperar contraseña',
    'reset-password-email-button' => 'Solicitar cambio de contraseña',
    'reset-password-new-password-field' => 'Nueva contraseña',
    'reset-password-new-password-button' => 'Confirmar cambio de contraseña',
];