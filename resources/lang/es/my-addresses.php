<?php

return [
    'airplane-delivery' => 'Envíos Aereos',
    'us-address' => 'Dirección en U.S.A.',
    'us-address-name' => 'NOMBRE',
    'us-address-address' => 'DIRECCIÓN',
    'us-address-city' => 'CIUDAD',
    'us-address-state' => 'ESTADO',
    'us-address-country' => 'PAÍS',
    'us-address-postalcode' => 'COD POSTAL',
    'us-address-phone' => 'NÚMERO',
    'china-address' => 'Dirección en China',
    'china-address-message' => 'Contactar por whatsapp para solicitar servicio',
    'boat-delivery' => 'Envios Maritimos',
];