<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        <h1 style="text-align: center;">{{ $pago->cliente->name }}, {{ $pago->aceptado ? 'recebemos seu pagamento!' : 'não achamos o seu pagamento' }}</h1>

        @if($pago->aceptado)
            Muchas gracias por su pago! Puede pasar por nuestras oficinas a retirar su paquete.
        @else
            Disculpe {{ $pago->cliente->name }}, no encontramos los datos de su pago. Por favor, entre en contacto con nosotros o envíe nuevamente los datos de su pago desde el panel.
        @endif

        <p>
            <h4>Tracking de los paquetes:</h4>
        <ul>
            @foreach($pago->items as $item)
                <li>{{ $item->paquete->tracking }}</li>
            @endforeach
        </ul>
        </p>
    </div>
</div>