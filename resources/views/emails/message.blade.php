<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        @if(!is_null($image))
            <img src="{{ url($image) }}" style="width: 90%; margin-left: 10%; margin-right: 10%;">
        @endif
        <p>
            {!! nl2br($text) !!}
        </p>
        <p style="margin-top: 20px;">
            <i>Enviado {{ $date->format('d/m/Y H:i') }}</i>
        </p>
    </div>
</div>