<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px; text-align: right;">
        <h1>Invoice</h1>
        <h1>Para: {{ $nombre }}</h1>
        <h1>Casilla: {{ $id }}</h1>
        <h3>Fecha del Invoice: {{ $date->format('d/m/Y H:i:s') }}</h3>
    </div>
    <div>
        <table style="width: 95%; margin: 0 auto; border-collapse: collapse">
            <thead style="border-bottom: 2px solid #aaa;">
                <tr>
                    <th>#</th>
                    <th>Descripción</th>
                    <th>Tracking</th>
                    <th>Peso</th>
                    <th>Precio unitario</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($paquetes as $index => $paquete)
                    <tr style="border-top: solid 1px #ddd; text-align: center;">
                        <td style="padding: 5px 10px;">{{ $index + 1 }}</td>
                        <td style="padding: 5px 10px;">{{ $paquete->descripcion }}</td>
                        @if($paquete->consolidacao)
                            <td style="padding: 5px 10px;">{{ $paquete->consolidacao_trackings }} <i>(consolidados)</i></td>
                        @else
                            <td style="padding: 5px 10px;">{{ $paquete->tracking }}</td>
                        @endif
                        <td style="padding: 5px 10px;">{{ $paquete->gramos / 1000 }} Kg.</td>
                        <td style="padding: 5px 10px;">US$ {{ $paquete->precio_kg }}</td>
                        <td style="padding: 5px 10px;">US$ {{ $paquete->valor_total }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div style="padding-bottom: 20px; text-align: right;">
        @if($invoice->has_seguro)
            <h1>Seguro: US$ {{ $invoice->valor_seguro }}</h1>
        @endif
        @if($invoice->valor_consolidacao)
            <h1>Consolidación: US$ {{ $invoice->valor_consolidacao }}</h1>
        @endif
        <h1>Total: US$ {{ $total }}</h1>
    </div>
</div>