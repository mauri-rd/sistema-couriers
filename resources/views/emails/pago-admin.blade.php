<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        <h1 style="text-align: center;">Se recibieron datos de pago por algunos paquetes!</h1>

        <h2>Cliente:</h2> {{ $pago->cliente->full_name }} (ID: {{ $pago->id_cliente }})
        <br>
        <h2>Valor pagado:</h2> R$ {{ $pago->total_brl }} (US$ {{ $pago->total_usd }})
        <h2>Trackings</h2>
        <ul>
            @foreach($pago->items as $item)
                <li>{{ $item->paquete->tracking }}</li>
            @endforeach
        </ul>

        <p>Puede ver la imagen del comprobante <a href="{{ $pago->comprobante }}" target="_blank">clicando aquí</a></p>

        <p>Puede marcar el pago como aceptado <a href="{{ route('admin.pagos.procesar', [$pago->id, 'aceptado' => 1]) }}">clicando aquí</a></p>
        <p>Puede marcar el pago como rechazado <a href="{{ route('admin.pagos.procesar', [$pago->id]) }}">clicando aquí</a></p>

        <p>
            Para ir al panel puede clicar <a href="{{ route('admin.pagos.index') }}">aquí</a>.
        </p>
    </div>
</div>