<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        <h1 style="text-align: center;">{{ $nombre }}, el estado de su paquete fue actualizado!</h1>
        @if($estado == App\Enum\EstadoPaquete::$MIAMI)
        <p>Su paquete con tracking {{ $tracking }} fue recibido en nuestras oficinas de Miami!</p>
        @elseif($estado == App\Enum\EstadoPaquete::$CHINA)
        <p>Su paquete con tracking{{ $tracking }} fue recibido en nuestras oficinas de China!</p>
        @elseif($estado == App\Enum\EstadoPaquete::$TRANSITO)
        <p>Su paquete con tracking{{ $tracking }} está en tránsito!</p>
        @elseif($estado == App\Enum\EstadoPaquete::$PARAGUAY)
        <p>Su paquete con tracking{{ $tracking }} ya está en Paraguay!</p>
        @elseif($estado == App\Enum\EstadoPaquete::$OFICINA)
        <p>Su paquete con tracking{{ $tracking }} ya está en nuestras oficinas de {{ isset($destino) && $destino === \App\Enum\Destino::BRASIL ? 'Brasil' : 'Paraguay' }}! Puede pasar a recogerlo!</p>
        @elseif($estado == App\Enum\EstadoPaquete::$RETIRADO)
        <p>Su paquete con tracking{{ $tracking }} fue retirado de nuestras oficinas. Gracias por la confianza y esperamos que haya tenido una buena experiencia comprando con StrBox!</p>
        @endif
        <p>
            Recuerde que también puede acompañar el estado de sus paquetes desde su <a href="{{ route('front.my-account.index') }}">panel</a>.
        </p>
    </div>
</div>