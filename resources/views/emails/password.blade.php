<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        <p>
            Recibimos una solicitud de cambio de contraseña.
        </p>
        <p>
            Puede reestablecer su contraseña desde el link <a href="{{ $link }}">{{ $link }}</a>
        </p>
    </div>
</div>