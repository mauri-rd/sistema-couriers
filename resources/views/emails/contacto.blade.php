<h1>Tiene un nuevo correo!</h1>
<p>
    <b>El correo viene de: </b>{{ $data['name'] }} ({{ $data['email'] }})
</p>
<p>
    El mensaje es el siguiente:
</p>
<p>
    {{ $data['message'] }}
</p>
<p>
    <i>P.D.: Lo que responda será enviado al usuário desde su correo.</i>
</p>