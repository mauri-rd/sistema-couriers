<div style="border: 1px solid #ddd; border-radius: 10px;
margin: 20px; font-family: sans-serif;">
    <div style="text-align: center; background-color: #211973;">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
    </div>
    <div style="padding: 20px">
        <p>
            Tem um ou vários pacotes aguardando no escritório há mais de 14 días. A partir do 15° día cobramos US$ 1 por cada kilo, por día. Por gentileza, pedimos retirar seu(s) pacote(s) antes possível.
        </p>
        <p>
            Saudações,<br>
            StrBox.
        </p>
        <p style="margin-top: 20px;">
            <i>Enviado {{ $date->format('d/m/Y H:i') }}</i>
        </p>
    </div>
</div>