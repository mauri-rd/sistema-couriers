@extends('front.master')
@section('contenido')
    <section class="page-section pb-0">
        <div class="relative">

            <!-- Works Filter -->
            <div class="works-filter align-center">
                <a href="#" class="filter active" data-filter="*">Todo</a>
                @foreach($categoriasMultimedias as $categoria)
                    <a href="#" class="filter" data-filter=".cat_{{ $categoria->id }}">{{ $categoria->nombre }}</a>
                @endforeach
            </div>
            <!-- End Works Filter -->

            <!-- Works Grid -->
            <ul class="works-grid work-grid-3 clearfix hover-white hide-titles" id="work-grid">
                @each('front.home.portafolio-item', $multimedias, 'multimedia')
            </ul>
            <!-- End Works Grid -->

        </div>
    </section>
@stop