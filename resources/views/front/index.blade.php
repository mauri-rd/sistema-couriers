
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Strbox</title>
    <!-- Stylesheets -->
    <!-- bootstrap v3.3.6 css -->
    <link href="front/strbox/css/bootstrap.css" rel="stylesheet" />
    <!-- font-awesome css -->
    <link href="front/strbox/css/font-awesome.css" rel="stylesheet" />
    <!-- flaticon css -->
    <link href="front/strbox/css/flaticon.css" rel="stylesheet" />
    <!-- factoryplus-icons css -->
    <link href="front/strbox/css/factoryplus-icons.css" rel="stylesheet" />
    <!-- animate css -->
    <link href="front/strbox/css/animate.css" rel="stylesheet" />
    <!-- owl.carousel css -->
    <link href="front/strbox/css/owl.css" rel="stylesheet" />
    <!-- fancybox css -->
    <link href="front/strbox/css/jquery.fancybox.css" rel="stylesheet" />
    <link href="front/strbox/css/hover.css" rel="stylesheet" />
    <link href="front/strbox/css/frontend.css" rel="stylesheet" />
    <link href="front/strbox/css/style.css" rel="stylesheet" />
    <link href="front/strbox/css/nineteen.css" rel="stylesheet" />
    <!-- switcher css -->
    <link href="front/strbox/css/switcher.css" rel="stylesheet" />
    <link rel="stylesheet" id="factoryhub-color-switcher-css" href="front/strbox/css/switcher/default.css" />
    <!-- revolution slider css -->
    <link rel="stylesheet" type="text/css" href="front/strbox/css/revolution/settings.css" />
    <link rel="stylesheet" type="text/css" href="front/strbox/css/revolution/layers.css" />
    <link rel="stylesheet" type="text/css" href="front/strbox/css/revolution/navigation.css" />

    <!--Favicon-->
    <link rel="shortcut icon" href="front/strbox/images/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="front/strbox/images/favicon.ico" type="image/x-icon" />
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link href="front/strbox/css/responsive.css" rel="stylesheet" />
    <!--[if lt IE 9
      ]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script
    ><![endif]-->
    <!--[if lt IE 9]><script src="front/strbox/js/respond.js"></script><![endif]-->
</head>

<body class="home header-transparent  header-sticky header-v1 hide-topbar-mobile">
{!! Form::hidden('contadorPaquetes', 10000 + $paquetesCount, ['id' => 'contadorPaquetes']) !!}
{!! Form::hidden('contadorSucursales', $sucursalesCount, ['id' => 'contadorSucursales']) !!}
{!! Form::hidden('contadorUsuarios', $usersCount, ['id' => 'contadorUsuarios']) !!}
<div id="page">
    <!-- Preloader-->
    <div class="preloader"></div>

    <!-- topbar -->
    <div id="fh-header-minimized" class="fh-header-minimized fh-header-v1"></div>
    <div id="topbar" class="topbar">
        <div class="container">
            <div class="topbar-left topbar-widgets text-left">
                <div id="cargo-office-location-widget-2" class="widget cargo-office-location-widget">
                    <div class="office-location clearfix">
                        <ul class="topbar-office active">
                            <li>
                                <i class="flaticon-telephone" aria-hidden="true"></i>Contacto: +595 973 221222
                            </li>
                            <li>
                                <i class="flaticon-web" aria-hidden="true"></i>
                                contacto@strbox.com.py
                            </li>
                            <li>
                                <i class="flaticon-pin" aria-hidden="true"></i>Dirección de
                                Miami: 5459 NW 72 ND Ave, Miami, Florida, 33166 Tel: +1 (786) 409 2811
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- topbar end -->

    <!-- masthead -->
    <header id="masthead" class="site-header clearfix">
        <div class="header-main clearfix">
            <div class="container mobile_relative">
                <div class="row">
                    <div class="site-logo col-md-2 col-sm-6 col-xs-6">
                        <a href="https://www.santaritabox.com/" class="logo">
                            <img src="front/strbox/images/logo.png" alt="CargoHub" class="logo-light show-logo" />
                            <img src="front/strbox/images/logo.png" alt="CargoHub" class="logo-dark hide-logo" />
                        </a>
                        <h1 class="site-title"><a href="#">CargoHub</a></h1>
                        <h2 class="site-description">
                            Just another Steel Themes Sites site
                        </h2>
                    </div>
                    <div class="site-menu col-md-10 col-sm-6 col-xs-6">
                        <nav id="site-navigation" class="main-nav primary-nav nav">
                            <ul class="menu">
                                <li class="active has-children">
                                    <a href="#topbar">INICIO</a>
                                </li>

                                <li>
                                    <a href="#quien-somos" class="dropdown-toggle">¿Quiénes somos?</a>
                                </li>
                                <li><a href="#servicios">Servicios</a></li>

                                <li>
                                    <a href="#contacto" class="dropdown-toggle">Atención al Cliente</a>
                                </li>
                                <li class="extra-menu-item menu-item-button-link">
                                    <a href="{{ route('front.login-form') }}#registro" class="fh-btn btn">Registrarse Gratis</a>
                                </li>
                                <li class="extra-menu-item menu-item-button-link">
                                    <a href="{{ route('front.login-form') }}" class="fh-btn btn-outline">Entrar</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <a href="#" class="navbar-toggle">
            <span class="navbar-icon">
              <span class="navbars-line"></span>
            </span>
                </a>
            </div>
        </div>
    </header>

    <!-- masthead end -->

    <!--Main Slider-->
    <section class="rev_slider_wrapper">
        <div id="slider1" class="rev_slider" data-version="5.0">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="front/strbox/images/main-slider/slide1.jpg" alt="" title="Home Page 1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina />
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme" id="slide-1-layer-9" data-x="['left','left','left','left']" data-hoffset="['495','0','0','0']" data-y="['top','top','top','top']" data-voffset="['310','210','310','170']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','off','off']" data-type="image" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                        <img src="front/strbox/images/main-slider/slide-bg.png" alt="" data-ww="['674px','674px','674px','674px']" data-hh="['398px','398px','398px','398px']" width="674" height="398" data-no-retina />
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption ch_title   tp-resizeme" id="slide-1-layer-1" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['350','250','350','200']" data-fontsize="['55','50','45','25']" data-lineheight="['55','50','45','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":500,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; letter-spacing: 0px;">

                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption ch_title   tp-resizeme" id="slide-1-layer-2" data-x="['left','left','left','left']" data-hoffset="['550','40','40','40']" data-y="['top','top','top','top']" data-voffset="['420','330','410','245']" data-fontsize="['55','50','45','25']" data-lineheight="['55','50','45','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":500,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; letter-spacing: 0px;">
                        Encuentra tu mercadería
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-1-layer-3" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['505','415','515','340']" data-fontsize="['24','24','18','18']" data-lineheight="['24','24','18','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; letter-spacing: 0px;">
                        ¡Consulte en tiempo real donde se encuentra tu mercadería,
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-1-layer-4" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['540','455','540','365']" data-fontsize="['24','24','18','18']" data-lineheight="['24','24','18','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; letter-spacing: 0px;">
                        en cualquier momento y desde cualquier lugar!
                    </div>

                    <!-- LAYER NR. 6 -->
                    <a class="tp-caption ch_button rev-btn " href="https://www.packagetrackr.com/track" target="_blank" id="slide-1-layer-5" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['600','520','620','430']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 10; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Consultar Pedido
                    </a>

                    <!-- LAYER NR. 7 -->
                    <a class="tp-caption ch_button rev-btn " href="{{ route('front.login-form') }}#registro" target="_blank" id="slide-1-layer-7" data-x="['left','left','left','left']" data-hoffset="['750','250','270','40']" data-y="['top','top','top','top']" data-voffset="['600','520','620','500']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 11; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Quiero Registrarme Gratis
                    </a>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="front/strbox/images/main-slider/slide2.jpg" alt="" title="Home Page 1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina />
                    <!-- LAYERS -->

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme" id="slide-2-layer-9" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['285','185','255','185']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','off','off']" data-type="image" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                        <img src="front/strbox/images/main-slider/slide-bg.png" alt="" data-ww="['755px','755px','755px','755px']" data-hh="['446px','446px','446px','446px']" width="674" height="398" data-no-retina />
                    </div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption ch_title   tp-resizeme" id="slide-2-layer-1" data-x="['left','left','left','left']" data-hoffset="['40','40','40','40']" data-y="['top','top','top','top']" data-voffset="['340','250','320','220']" data-fontsize="['55','50','45','25']" data-lineheight="['55','50','45','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":500,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; letter-spacing: 0px;">
                        Atención de Calidad
                    </div>

                    <!-- LAYER NR. 10 -->


                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-2-layer-3" data-x="['left','left','left','left']" data-hoffset="['40','40','40','40']" data-y="['top','top','top','top']" data-voffset="['505','415','480','355']" data-fontsize="['24','24','24','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; letter-spacing: 0px;">
                        Profesionales de alto nivel, siempre dispuestos
                    </div>

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-2-layer-4" data-x="['left','left','left','left']" data-hoffset="['40','40','40','40']" data-y="['top','top','top','top']" data-voffset="['545','455','515','385']" data-fontsize="['24','24','24','18']" data-lineheight="['24','24','24','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; letter-spacing: 0px;">
                        a sacar todas tus dudas.
                    </div>

                    <!-- LAYER NR. 13 -->
                    <a class="tp-caption ch_button rev-btn " href="{{ route('front.login-form') }}#registro" target="_blank" id="slide-2-layer-5" data-x="['left','left','left','left']" data-hoffset="['40','40','40','40']" data-y="['top','top','top','top']" data-voffset="['615','520','590','450']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 10; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Registrarme
                    </a>

                    <!-- LAYER NR. 14 -->
                    <a class="tp-caption ch_button rev-btn " href="{{ route('front.login-form') }}" target="_blank" id="slide-2-layer-7" data-x="['left','left','left','left']" data-hoffset="['250','250','270','40']" data-y="['top','top','top','top']" data-voffset="['615','520','590','530']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 11; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Entrar
                    </a>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="front/strbox/images/main-slider/slide3.jpg" alt="" title="Home Page 1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="0" class="rev-slidebg" data-no-retina />
                    <!-- LAYERS -->

                    <!-- LAYER NR. 15 -->
                    <div class="tp-caption   tp-resizeme" id="slide-3-layer-9" data-x="['left','left','left','left']" data-hoffset="['495','0','0','0']" data-y="['top','top','top','top']" data-voffset="['310','210','210','154']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','off','off']" data-type="image" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                        <img src="front/strbox/images/main-slider/slide-bg.png" alt="" data-ww="['674px','674px','674px','674px']" data-hh="['398px','398px','398px','398px']" width="674" height="398" data-no-retina />
                    </div>

                    <!-- LAYER NR. 16 -->
                    <div class="tp-caption ch_title   tp-resizeme" id="slide-3-layer-1" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['350','250','250','210']" data-fontsize="['55','50','45','25']" data-lineheight="['55','50','45','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":500,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; letter-spacing: 0px;">
                        Compra de USA,
                    </div>

                    <!-- LAYER NR. 17 -->
                    <div class="tp-caption ch_title   tp-resizeme" id="slide-3-layer-2" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['420','330','330','250']" data-fontsize="['55','50','45','25']" data-lineheight="['55','50','45','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":500,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; letter-spacing: 0px;">
                        China y Todo el Mundo!
                    </div>

                    <!-- LAYER NR. 18 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-3-layer-3" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['505','415','415','315']" data-fontsize="['24','24','24','18']" data-lineheight="['24','24','24','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; letter-spacing: 0px;">
                        ¡Desde la comodidad
                    </div>

                    <!-- LAYER NR. 19 -->
                    <div class="tp-caption ch_content   tp-resizeme" id="slide-3-layer-4" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['545','455','455','340']" data-fontsize="['24','24','24','18']" data-lineheight="['24','24','24','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; letter-spacing: 0px;">
                        de tu domicilio!
                    </div>

                    <!-- LAYER NR. 20 -->
                    <a class="tp-caption ch_button rev-btn " href="#special_services" id="slide-3-layer-5" data-x="['left','left','left','left']" data-hoffset="['540','40','40','40']" data-y="['top','top','top','top']" data-voffset="['610','520','520','400']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 10; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">¿Cómo funciona?
                    </a>

                    <!-- LAYER NR. 21 -->
                    <a class="tp-caption ch_button rev-btn " href="#servicios" id="slide-3-layer-7" data-x="['left','left','left','left']" data-hoffset="['750','250','270','40']" data-y="['top','top','top','top']" data-voffset="['610','520','520','480']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":800,"speed":1000,"text_c":"transparent","bg_c":"transparent","use_text_c":false,"use_bg_c":false,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"use_text_c":false,"use_bg_c":false,"text_c":"transparent","bg_c":"transparent","frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,0,0);bc:rgb(255,0,0);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 11; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Nuestros Servicios
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <!--Main Slider  end-->

    <!-- Welcome sec -->
    <section class="welcomesec secpadd" id="quien-somos">

    </section>

    <div class="container">
        <div class="fh-section-title clearfix  text-center version-dark paddbtm20">
            <h2>
                ¡Bienvenidos a Str<span class="main-color">box</span>!
            </h2>
        </div>
        <p class="haeadingpara text-center paddbtm40">
            ¡Strbox es sinónimo de calidad! <br />

            Strbox es una empresa que te ofrece la facilidad de realizar tus compras en Estados Unidos por
            intermedio de una dirección física. Strbox se encarga de recibir, controlar, clasificar los
            paquetes y enviarlos hasta la comodidad de tu hogar o empresa manteniendo los mejores

            estándares en precio y calidad.
        </p>

        <div class="welservices text-center">
            <div class="col-md-6 col-sm-6 anime-right">
                <div class="fh-icon-box icon-type-theme_icon style-1 version-dark hide-button icon-left">
                    <span class="fh-icon"><i class="flaticon-transport-3"></i></span>
                    <h4 class="box-title"><a href="#">Servicio Aéreo</a></h4>
                    <div class="desc">
                        <p>US$ {{ number_format($configuraciones['envio-miami']->valor, 2, ',', '.') }} x KG</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 anime-left">
                <div class="fh-icon-box icon-type-theme_icon style-1 version-dark hide-button icon-left">
                    <span class="fh-icon"><i class="flaticon-transport-4"></i></span>
                    <h4 class="box-title"><a href="#">Servicio Marítimo</a></h4>
                    <div class="desc">
                        <p>US$ 11,50 x KG</p>
                        <p>
                            Strbox cuenta con su división de transporte marítimo
                            para todo tipo de cargas desde cualquier parte del mundo.
                            Solicite presupuesto y le enviaremos en menos de 24hs.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <!-- Welcome sec   end-->

    <!-- special services -->
    <section class="special_services  secpadd " id="special_services">
        <div class="fh-section-title clearfix bluebg  text-center version-dark  paddtop20 paddbtm20">
            <h2 class="color-blanco">
                ¿Cómo <span class="main-color">comprar?</span>
            </h2>
        </div>
        <!--services Welcome sec -->
        <div class="dtloffsec secpadd background-blanco secpadd">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 anime-top">
                        <div class="col-md-12">
                            <div class="fh-icon-box text-center  style-2  icon-center">
                                <span class="fh-icon">1</span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="fh-service-box-2 _icon box-style-1 ">
                                <div class="box-thumb">
                                    <img class="" src="front/strbox/images/services/registrese.jpg" alt="" />
                                </div>
                                <div class="box-wrapper">
                                    <div class="box-header clearfix">
                                        <span class="fh-icon"><i class="fa fa-user-plus"></i></span>
                                        <h4 class="box-title">Regístrese Gratis</h4>
                                        <span class="sub-title main-color">¡Es rápido y sencillo!</span>
                                    </div>
                                    <div class="box-content">
                                        <p>
                                            Regístrese gratis en Strbox para obtener tu número de casilla, una vez registrado te
                                            facilitamos nuestra dirección donde las tiendas online podrán enviar tus compras.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 anime-top">
                        <div class="col-md-12">
                            <div class="fh-icon-box  style-2 icon-center">
                                <span class="fh-icon">2</span>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="fh-service-box-2  box-style-1 ">
                                <div class="box-thumb">
                                    <img class="" src="front/strbox/images/services/comprar.jpg" alt="" />
                                </div>
                                <div class="box-wrapper">
                                    <div class="box-header clearfix">
                                        <span class="fh-icon"><i class="fa fa-shopping-cart"></i></span>
                                        <h4 class="box-title">Comprá</h4>
                                        <span class="sub-title main-color">¡En Amazon, Ebay y mucho más!</span>
                                    </div>
                                    <div class="box-content">
                                        <p>
                                            Con tu casilla de Strbox podés empezar a comprar en las mejores tiendas online y una vez
                                            que elijas el producto sólo debes colocar nuestra dirección de Miami con tu número de casilla
                                            como dirección de envío o shipping address.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 anime-top">
                        <div class="col-md-12">
                            <div class="fh-icon-box  style-2 icon-center">
                                <span class="fh-icon">3</span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="fh-service-box-2  box-style-1 ">
                                <div class="box-thumb">
                                    <img class="" src="front/strbox/images/services/delivery.jpg" alt="" />
                                </div>
                                <div class="box-wrapper">
                                    <div class="box-header clearfix">
                                        <span class="fh-icon"><i class="fa fa-dropbox"></i></span>
                                        <h4 class="box-title">Recibí</h4>
                                        <span class="sub-title main-color">¡De forma segura y garantizada!</span>
                                    </div>
                                    <div class="box-content">
                                        <p>
                                            Una vez realizada tu compra el vendedor lo envía a nuestro depósito donde recibimos tus
                                            paquetes, procesamos el ingreso al sistema y los enviamos a Paraguay en el vuelo disponible
                                            todos los días viernes de cada semana. Una vez en Paraguay, nos encargamos de la liberación
                                            de Aduana y lo ponemos a tu disposición.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Welcome sec   end-->
    </section>
    <!-- special services end -->

    <!-- special services -->
    <section class="special_services ">
        <div class="row paddbtm40 ">
            <div class="fh-section-title clearfix bluebg  text-center version-dark  paddtop20 paddbtm20">
                <h2 class="color-blanco">
                    ¿Dónde <span class="main-color">comprar?</span>
                </h2>
            </div>
        </div>
        <div class="container secpadd anime">
            <div class="">
                <div class=" background-blanco">
                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb ">
                                <img class="" src="front/strbox/images/services/amazon.png" alt="" />
                                <a class="box-link" href="https://www.amazon.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/walmart.png" alt="" />
                                <a class="box-link" href="http://www.walmart.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/bestbuy.png" alt="" />
                                <a class="box-link" href="http://www.bestbuy.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/aliexpress.png" alt="" />
                                <a class="box-link" href="https://aliexpress.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/victoriasecret.png" alt="" />
                                <a class="box-link" href="http://www.victoriassecret.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/hollister.png" alt="" />
                                <a class="box-link" href="http://www.hollisterco.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/forever21.png" alt="" />
                                <a class="box-link" href="http://www.forever21.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/louisvuitton.png" alt="" />
                                <a class="box-link" href="http://www.vuitton.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/apple.png" alt="" />
                                <a class="box-link" href="https://www.apple.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-12 box-logo">
                        <div class="fh-service-box icon-type-theme_icon style-1 link">
                            <div class="box-thumb">
                                <img class="" src="front/strbox/images/services/armani.png" alt="" />
                                <a class="box-link" href="http://www.armaniexchange.com/">
                                    <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12 box-logo">
                    <div class="fh-service-box icon-type-theme_icon style-1 link">
                        <div class="box-thumb">
                            <img class="" src="front/strbox/images/services/ebay.png" alt="" />
                            <a class="box-link" href="http://www.ebay.com/">
                                <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12 box-logo">
                    <div class="fh-service-box icon-type-theme_icon style-1 link">
                        <div class="box-thumb">
                            <img class="" src="front/strbox/images/services/diesel.png" alt="" />
                            <a class="box-link" href="http://www.diesel.com/">
                                <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12 box-logo">
                    <div class="fh-service-box icon-type-theme_icon style-1 link">
                        <div class="box-thumb">
                            <img class="" src="front/strbox/images/services/sony.png" alt="" />
                            <a class="box-link" href="http://www.sony.com/all-electronics">
                                <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12 box-logo">
                    <div class="fh-service-box icon-type-theme_icon style-1 link">
                        <div class="box-thumb">
                            <img class="" src="front/strbox/images/services/asics.png" alt="" />
                            <a class="box-link" href="http://www.asics.com/">
                                <i class="fa fa-arrow-circle-right fa-1x icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- special services end -->

    <section class="services secpadd background-azul-claro" id="servicios">
        <div class="fh-section-title clearfix  text-center version-dark  paddtop20 paddbtm40">
            <h2 class="color-blanco">
                <span class="main-color">Servicios</span>
            </h2>
        </div>
        <div class="container paddsec">
            <div class="row  anime-right">
                <div class="col-md-6 ">
                    <div class="abotinforgt">
                        <div class="fh-section-title clearfix f25  text-left version-dark paddbtm40">
                            <h2>Logística</h2>
                        </div>

                        <p>Compre desde la comodidad de su hogar y nuestro servicio de logística integral se encargará
                            de todo. Nuestra entrega es guiada hasta la comodidad de tu domicilio!</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="abotimglft">
                        <img src="front/strbox/images/services/avion.png" class="img-responsive">
                    </div>
                </div>
            </div>

            <div class="row  anime-left">
                <div class="col-md-6">
                    <div class="abotinfolft">
                        <div class="fh-section-title clearfix f25  text-left version-dark paddbtm40">
                            <h2>Compra</h2>
                        </div>

                        <p>Si quiere comprar en la web pero no tiene tarjeta de crédito, no se preocupe, nosotros
                            realizamos tus compras con una recarga del 15% del costo del mismo. Strbox se encarga de
                            todo!</p>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="abotimgrgt">
                        <img src="front/strbox/images/services/comprando.png" class="img-responsive">
                    </div>

                </div>
            </div>

            <div class="row secpadd  anime-right">
                <div class="col-md-6">
                    <div class="abotinforgt">
                        <div class="fh-section-title clearfix f25  text-left version-dark paddbtm40">
                            <h2>Entrega a Domicilio</h2>
                        </div>

                        <p>Así mismo, no tiene porqué dejar la comodidad
                            de su hogar para recibir su compra. Strbox ofrece
                            un servicio integral de transporte de mercaderías. Su pedido va directo de USA a su puerta!</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="abotimglft">
                        <img src="front/strbox/images/services/domicilio.png" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- special services -->
    <section class="special_services bluebg secpadd">
        <div class="container">
            <div class="row paddbtm40">
                <div class="col-md-4 col-sm-12">
                    <div class="fh-section-title clearfix  text-left version-light">
                        <h2>Servicios Especiales</h2>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="hdrgtpara lftredbrdr">
                        <p>Strbox es tu solución para poder acceder a ese repuesto difícil de encontrar, lo último en tecnología y la ropa de marca con ahorros increíbles. Strbox es la empresa que se encarga de recibir en origen tus compras y entregártelas sin que tengas que preocuparte por ningún trámite adicional.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="col-md-6 col-md-offset-3 col-sm-12">
                        <div class="fh-service-box icon-type-theme_icon style-1">
                            <span class="fh-icon"><i class="fa fa-shopping-cart"></i></span>
                            <h4 class="service-title"><a href="#" class="link" target="_blank">Compramos por vos</a></h4>
                            <div class="desc">
                                <p>En Strbox estamos para servirte, y podemos ayudarlo a realizar sus compras, contamos con
                                    medios de pagos internacionales y asesores de compras con experiencia. No importa si es tu
                                    primera compra, sabemos como hacerlo. Primeramente debe registrarse en nuestro sistema.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="fh-section-title clearfix  text-center version-light paddbtm40">
                    <h2>Formas de Pago</h2>
                </div>
                <div class="col-md-4 box-md-4">
                    <div class="fh-service-box icon-type-theme_icon style-1">
                        <span class="fh-icon"><i class="fa fa-credit-card"></i></span>
                        <h4 class="service-title"><a href="#" class="link" target="_blank">Tarjeta de Crédito</a></h4>
                        <div class="desc">
                            <p>Si usted tiene tarjeta de crédito, puede realizar sus compras con el mismo.


                            </p>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-sm-12">
                    <div class="fh-service-box icon-type-theme_icon style-1">
                        <span class="fh-icon"><i class="fa fa-dollar"></i></span>
                        <h4 class="service-title"><a href="#" class="link" target="_blank">Pago en Efectivo</a></h4>
                        <div class="desc">
                            <p>Si usted no tiene tarjeta de crédito, Te esperamos en nuestra oficina para que puedas abonar por tus compras.
                                A un coste del 15% del valor de las compras</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="fh-service-box icon-type-theme_icon style-1">
                        <span class="fh-icon"><i class="fa fa-comments"></i></span>
                        <h4 class="service-title"><a href="#" class="link" target="_blank">Giros Tigo</a></h4>
                        <div class="desc">
                            <p>Otra opción si usted no tiene tarjeta de crédito. Podés realizar tus pagos a través de Giros Tigo al número (0984)302-222.
                                A un coste del 15% del valor de las compras</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- special services end -->






    <section class="homecounts background-navio">
        <div class="container" id="counter">
            <h2 class="count-title">
                Datos relevantes de Str<span class="main-color">box</span>
            </h2>
            <div class="row">
                <div class="col-lg-4 col-md-3 col-lg-offset-2">
                    <div class="fh-counter icon-type-none">
                        <div class="">
                            <div class="contadorPaquetes counter">-</div>
                            <h4>Paquetes entregados</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="fh-counter icon-type-none">
                        <div class="">
                            <div class="contadorSucursales counter">-</div>
                            <h4>Sucursales</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="fh-counter icon-type-none">
                        <div class="">
                            <div class="contadorUsuarios counter">-</div>
                            <h4>Usuarios</h4>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <!--why choose us -->
    <section class="whychoose-1 paddtop30" id="contacto">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6  secpaddlf">
                    <div class="fh-section-title clearfix  text-left version-dark paddbtm40">
                        <h2>¿Por qué elegirnos?</h2>
                    </div>

                    <div class="fh-icon-box  style-2 icon-left has-line">
                        <span class="fh-icon"><i class="flaticon-people"></i></span>
                        <h4 class="box-title">
                            <span>Soporte Técnico Especializado</span>
                        </h4>
                        <div class="desc">
                            <p>
                                ¡Contamos con un equipo muy experiente que está a tu disposición para aclarar las
                                dudas que tengas!
                            </p>
                        </div>
                    </div>
                    <div class="fh-icon-box  style-2 icon-left has-line">
                        <span class="fh-icon"><i class="flaticon-route"></i></span>
                        <h4 class="box-title"><span>Localización de mercaderías en tiempo real</span></h4>
                        <div class="desc">
                            <p>
                                Todas las mercaderías que llegan a nuestro depósito, genera un código que estará
                                disponible en tu cuenta de usuario de Strbox. Para saber dónde está tu mercadería,
                                podes consultar ingresando el código <a href="https://www.packagetrackr.com/track" target="_blank" style="color:green;">aquí</a>
                            </p>
                        </div>
                    </div>
                    <div class="fh-icon-box  style-2 icon-left">
                        <span class="fh-icon"><i class="flaticon-open-cardboard-box"></i></span>
                        <h4 class="box-title">
                            <span>El menor costo, y la mayor confiabilidad</span>
                        </h4>
                        <div class="desc">
                            <p>
                                ¡Solamente necesitas hacer tus compras, nosotros nos encargamos de traerlo hasta tu
                                casa!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-md-6 quofrm1  secpaddlf">
                    <div class="fh-section-title clearfix  text-left version-dark paddbtm40">
                        <h2>Envíanos un mensaje</h2>
                    </div>
                    <form method="post" action="ajax/mail.php">
                        <div class="fh-form-1 fh-form">
                            <div class="row fh-form-row">
                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <p class="field">
                                        <select name="service" required>
                                            <option value="servicio">Servicio</option>
                                            <option value="ayuda">Ayuda</option>
                                            <option value="recomendacion">Recomendación</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <p class="field">
                                        <input name="city" value="" placeholder="Ciudad*" required type="text" />
                                    </p>
                                </div>


                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <p class="field">
                                        <input name="name" value="" placeholder="Nombre*" required type="text" />
                                    </p>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <p class="field">
                                        <input name="email" value="" placeholder="Email*" required type="email" />
                                    </p>
                                </div>
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <p class="field single-field">
                                        <textarea cols="40" name="message" required placeholder="Mensaje*"></textarea>
                                    </p>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                    <div class="field submit">
                                        <button class="fh-btn" type="submit">Enviar <i class="fa fa-envelope"></i></button>
                                        <a href="https://api.whatsapp.com/send?phone=595973221222&text=¡Me%20interesa!" target="blank" class="fh-btn btn-outline color-principal">WhatsApp <i class="fa fa-whatsapp"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--why choose us end -->

    <!--google map-->
    <div class="google-map-area">
        <iframe class="actAsDiv" style="width:100%;height:100%;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;q=-25.800154%2C%20-55.090904&amp;aq=0&amp;ie=UTF8&amp;t=k&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>

    </div>

    <!--footer sec-->
    <div class="footer-widgets widgets-area">
        <div class="oscurecer"></div>
        <div class="contact-widget">
            <div class="container">
                <div class="row">
                    <div class="contact col-md-3 col-xs-12 col-sm-12">
                        <a href="#" class="footer-logo"><img src="front/strbox/images/logo.png" alt="Footer Logo" /></a>
                    </div>
                    <div class="contact col-md-3 col-xs-12 col-sm-12">
                        <i class="flaticon-signs"></i>
                        <p>Dirección:</p>
                        <h4>Rodriguez de Francia c/ Acosta ñu, a 50mts del Banco Continental, debajo de la Radio Santa Rita</h4>
                    </div>
                    <div class="contact col-md-3 col-xs-12 col-sm-12">
                        <i class="flaticon-phone-call "></i>
                        <p>Contacto:</p>
                        <h4>+595 973 221 222</h4>
                    </div>
                    <div class="contact col-md-3 col-xs-12 col-sm-12">
                        <i class="flaticon-clock-1"></i>
                        <p>Horário de Atención:</p>
                        <h4>Lunes a viernes de 08:00 a 12:00 hs, 13:30 a 17:30hs</h4>
                        <h4>Sábados de 08:30 a 11:30 hs</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-sidebars">
            <div class="container">
                <div class="row">
                    <div class="footer-sidebar footer-1 col-xs-12 col-sm-6 col-md-6">
                        <div class="widget widget_text">
                            <h4 class="widget-title">Sobre Strbox </h4>
                            <div class="textwidget">
                                <p>
                                    Strbox es la empresa que le ofrece la facilidad de realizar sus compras en Estados Unidos por intermedio de una dirección física. Strbox se encarga de recibir,
                                    controlar, clasificar sus paquetes y enviarlos a la comodidad de su hogar o empresa manteniendo los mejores estándares en precio y calidad.
                                </p>
                            </div>
                        </div>
                        <div class="widget cargohub-social-links-widget">
                            <div class="list-social style-1">
                                <a href="https://www.facebook.com/santaritaboxx/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://api.whatsapp.com/send?phone=595973221222&text=¡Me%interesa!" target="_blank"><i class="fa fa-whatsapp"></i></a>

                            </div>
                        </div>
                    </div>


                    <div class="footer-sidebar footer-4 col-xs-12 col-sm-6 col-md-6">
                        <div class="widget widget_mc4wp_form_widget">
                            <h4 class="widget-title">¡Regístrate Gratis!</h4>
                            <form>
                                <div class="footform">
                                    <div class="fh-form-field">
                                        <p>
                                            ¡Registrarte posibilita que tengas control total de tus paquetes!.
                                        </p>
                                        <div class="subscribe">

                                            <a href="{{ route('front.login-form') }}#registro" class="fh-btn btn">Registrarse Gratis</a>

                                            <a href="{{ route('front.login-form') }}" class="fh-btn btn-outline">Entrar</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- / MailChimp for WordPress Plugin -->
                        </div>
                        <div class="widget widget_text">
                            <div class="textwidget">
                                <p>Tu información es totalmente protegida</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--footer sec end-->

    <!--copyright sec-->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="footer-copyright col-md-12 col-sm-12 col-sx-12">
                    <div class="site-info">
                        Copyright &copy; 2019 <a href="#">Strbox</a>, Todos los derechos reservados. Desarrollado por <a href="https://sofmarsistema.net/" target="_blank">Sofmar Sistema</a>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!--copyright sec end-->
</div>
<!--End pagewrapper-->


<div id="off-canvas-layer" class="off-canvas-layer"></div>
<!--primary-mobile-nav end-->
<!--primary-mobile-nav-->
<div class="primary-mobile-nav header-v1" id="primary-mobile-nav" role="navigation">
    <a href="#" class="close-canvas-mobile-panel">×</a>
    <ul class="menu">
        <li class="active has-children">
            <a href="index.html">Inicio</a>
        </li>

        <li>
            <a href="#quien-somos" class="dropdown-toggle">¿Quiénes somos?</a>
        </li>
        <li><a href="#servicios">Servicios</a></li>

        <li>
            <a href="#contacto" class="dropdown-toggle">Atención al Cliente</a>
        </li>
        <li class="extra-menu-item menu-item-button-link">
            <a href="{{ route('front.login-form') }}#registro" class="fh-btn btn">Registrarse Gratis</a>
        </li>
        <li class="extra-menu-item menu-item-button-link">
            <a href="{{ route('front.login-form') }}" class="fh-btn ">Entrar</a>
        </li>
    </ul>

</div>

<a target="_blank" href="https://api.whatsapp.com/send?phone=595973221222">
    <div class="whatsapp">
        <i class="fa fa-whatsapp"></i>
    </div>
</a>

<!--Scroll to top-->
<a id="scroll-top" class="backtotop" href="#page-top"><i class="fa fa-angle-up"></i></a>

<!-- jquery Liabrary -->
<script src="front/strbox/js/jquery-1.12.4.min.js"></script>
<script src="front/strbox/js/jquery.animateNumber.min.js"></script>

<!-- bootstrap v3.3.6 js -->
<script src="front/strbox/js/bootstrap.min.js"></script>
<!-- fancybox js -->
<script src="front/strbox/js/jquery.fancybox.pack.js"></script>
<script src="front/strbox/js/jquery.fancybox-media.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<!-- owl.carousel js -->
<script src="front/strbox/js/owl.js"></script>
<!-- counter js -->
<script src="front/strbox/js/jquery.appear.js"></script>
<script src="front/strbox/js/jquery.countTo.js"></script>
<!-- validate js -->
<script src="front/strbox/js/validate.js"></script>
<!-- switcher js -->
<script src="front/strbox/js/switcher.js"></script>

<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<script src="front/strbox/js/gmap.js"></script>
<script src="front/strbox/js/map-helper.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="front/strbox/js/revolution/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="front/strbox/js/revolution/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript">
</script>

<!-- script JS  -->
<script src="front/strbox/js/scripts.min.js"></script>

<script src="front/strbox/js/script.js"></script>




</body>

</html>