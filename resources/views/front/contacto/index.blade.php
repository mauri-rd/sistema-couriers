@extends('front.master')
@section('contenido')
<section class="page-section">
    <div class="container relative">
        @if(session('msg'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('msg') }}
        </div>
        @endif

    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="index.html" class="breadcrumbs__url">Inicio</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Contactos
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">            
            <!-- post content -->
            <div class="blog__content mb-72">
              <h1 class="page-title">Contactanos</h1>
      
              <div class="row justify-content-center">
                <div class="col-lg-12">
                  <h4>Déjanos un mensaje</h4>
                  <p>No dude en ponerse en contacto. Te responderemos tan pronto como sea posible.</p>
                  <ul class="contact-items">
                    <li class="contact-item"><address>{{ $configuraciones['direccion']->valor }}</address></li>
                    <li class="contact-item"><a href="#"> {{ $configuraciones['numeros-de-telefonos']->valor }} </a></li>
                    <li class="contact-item"><a href="#">{{ $configuraciones['correos']->valor }}</a></li>
                  </ul>            
      
                  <!-- Contact Form -->
                  <form id="contact-form" class="contact-form mt-30 mb-30" method="post" action="#">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="contact-name">
                      <label for="name">Nombre <abbr title="required" class="required">*</abbr></label>
                      <input name="name" id="name" type="text">
                    </div>
                      </div>
                    <div class="col-lg-4">
                      <div class="contact-email">
                      <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                      <input name="email" id="email" type="email">
                    </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="contact-subject">
                      <label for="email">Tema</label>
                      <input name="subject" id="subject" type="text">
                    </div>
                    </div>
                  </div>
                    <div class="contact-message">
                      <label for="message">Mensaje <abbr title="required" class="required">*</abbr></label>
                      <textarea id="message" name="message" rows="7" required="required"></textarea>
                    </div>
      
                    <input type="submit" class="btn btn-lg btn-color btn-button" value="Enviar" id="submit-message">
                    <div id="msg" class="message"></div>
      
                  </form>
                </div>
              </div>
            </div> <!-- end post content -->
            <!-- Google Map -->
            <div id="google-map" class="gmap" data-address="{lat: {{ $configuraciones['latitud']->valor }}, lng: {{ $configuraciones['longitud']->valor }} }" ></div>
          </div>
          
@stop
@section('top-js')
    <script>
        var pos = {
            lat : Number({{ $configuraciones['latitud']->valor }}),
            lng : Number({{ $configuraciones['longitud']->valor }})
        };
    </script>
@stop