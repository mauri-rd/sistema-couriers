<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>StrBox</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="{{ url('style.css') }}">

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('assets/css/custom.css') }}">

    <!-- Favicon and Touch Icons  -->
    <link rel="shortcut icon" href="assets/icon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page topbar-style-1 header-fixed no-sidebar site-layout-full-width header-style-9 header-opacity has-topbar topbar-style-9  menu-has-search menu-has-cart">

<div id="wrapper" class="animsition">
    @yield('contenido')
</div><!-- /#wrapper -->

<a id="scroll-top"></a>

<!-- Javascript -->
<script src="{{ url('assets/js/jquery.min.js') }}"></script>
<script src="{{ url('assets/js/plugins.js') }}"></script>
<script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/animsition.js') }}"></script>
<script src="{{ url('assets/js/countto.js') }}"></script>
<script src="{{ url('assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ url('assets/js/magnific.popup.min.js') }}"></script>
<script src="{{ url('assets/js/equalize.min.js') }}"></script>
<script src="{{ url('assets/js/typed.js') }}"></script>
<script src="{{ url('assets/js/vegas.js') }}"></script>
<script src="{{ url('assets/js/shortcodes.js') }}"></script>
<script src="{{ url('assets/js/main.js') }}"></script>

<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZ2z7aoo8okwvyHbaxfKwUi-sblBj5QYk"></script>
<script src="assets/js/gmap3.min.js"></script>

<!-- Revolution Slider -->
<script src="{{ url('includes/rev-slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ url('assets/js/rev-slider.js') }}"></script>
<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->  
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ url('includes/rev-slider/js/extensions/revolution.extension.video.min.js') }}"></script>

</body>
</html>

