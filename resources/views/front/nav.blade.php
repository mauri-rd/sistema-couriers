<nav class="main-nav stick-fixed">
  <div class="top-bar d-none d-lg-block">
    <div class="container">
      <div class="row">                
        <!-- Socials -->
        <div class="col-lg-12">
          <div class="socials nav__socials socials--nobase socials--white justify-content-end"> 
            <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
              <i class="ui-facebook"></i>
            </a>
            <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
              <i class="ui-twitter"></i>
            </a>
            <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
              <i class="ui-google"></i>
            </a>
            <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
              <i class="ui-youtube"></i>
            </a>
            <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
              <i class="ui-instagram"></i>
            </a>
          </div>
        </div>    
      </div>
    </div>
  </div> <!-- end top bar -->        

  <!-- Navigation -->
  <header >        
    <div class="nav__holder nav--sticky">
      <div class="container relative">
        <div class="flex-parent">

          <!-- Logo -->
          <a href="index.html" class="logo">
            <img class="logo__img" src="img/logo_default.png" srcset="img/logo_default.png 1x, img/logo_default@2x.png 2x" alt="logo">
          </a>

          <!-- Nav-wrap -->
          <nav class="flex-child nav__wrap d-none d-lg-block">              
            <ul class="nav__menu">
              <li>
                <a href="index.html">Inicio</a>
              </li>

              <li>
                <a href="#">Noticias</a>
              </li>
              
              <li>
                <a href="#">Obispado</a>
              </li>
              
              <li>
                <a href="#">Decanatos</a>
              </li>
              
              <li>
                <a href="#">Pastorales</a>
              </li>
              
              <li>
                <a href="#">Decretos</a>
              </li>
              
              <li>
                <a href="contact.html">Contactos</a>
              </li>
            </ul> <!-- end menu -->
          </nav> <!-- end nav-wrap -->
          
          <!-- Nav Right -->
          <div class="nav__right">
          
            <!-- Search -->
            <div class="nav__right-item nav__search">
              <a href="#" class="nav__search-trigger" id="nav__search-trigger">
                <i class="ui-search nav__search-trigger-icon"></i>
              </a>
              <div class="nav__search-box" id="nav__search-box">
                <form class="nav__search-form">
                  <input type="text" placeholder="Search an article" class="nav__search-input">
                    <button type="submit" class="search-button btn btn-lg btn-color btn-button">
                      <i class="ui-search nav__search-icon"></i>
                    </button>
                </form>
              </div>                
            </div>
                         
          </div> <!-- end nav right -->                    
        
        </div> <!-- end flex-parent -->
      
      </div> <!-- end container -->
    </div>
  </header>    
</nav>