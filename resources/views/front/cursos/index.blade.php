@extends('front.master')
@section('contenido')
    <section class="page-section" style="padding-bottom: 0">
        <div class="container relative">
            <form class="form-inline form" role="form" action="{{ route('front.cursos.index') }}" method="get">
                <div class="search-wrap">
                    <button class="search-button animate" type="submit" title="Buscar">
                        <i class="fa fa-search"></i>
                    </button>
                    <input name="q" type="text" class="form-control search-field" placeholder="Buscar">
                </div>
            </form>
        </div>
    </section>
    @foreach($cursos as $i => $curso)
    @if($i % 2 == 0)
        @include('front.cursos.post-left')
    @else
        @include('front.cursos.post-right')
    @endif

    <!-- Divider -->
    <hr class="mt-0 mb-0 "/>
    <!-- End Divider -->

    @endforeach

    @if($cursos->total() > 12)
    <section class="page-section">
        <div class="container relative">
            <!-- Pagination -->
            {!! (new \App\Helpers\CustomPresenter($cursos))->render() !!}
            <!-- End Pagination -->
        </div>
    </section>
    @endif
@stop