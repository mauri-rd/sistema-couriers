<!-- About Project -->
<div class="text">

    <h3 class="mb-30 mb-xxs-10">{{ $curso->nombre }}</h3>
    <p>
        {!! nl2br(\App\Helpers\Truncate::truncate($curso->descripcion, 200)) !!}
    </p>

    <div class="mt-40">
        <a href="{{ route('front.cursos.find', [$curso->id]) }}" class="btn btn-mod btn-border btn-medium">Ver más</a>
    </div>

</div>
<!-- End About Project -->