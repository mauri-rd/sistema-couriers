@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container relative">

            <div class="row">

                <!-- Content -->
                <div class="col-sm-10 col-sm-offset-1">

                    <!-- Post -->
                    <div class="blog-item mb-80 mb-xs-40">

                        <!-- Text -->
                        <div class="blog-item-body">

                            <h1 class="mt-0">{{ $curso->nombre }}</h1>

                            <!-- End Text -->

                            <!-- Media Gallery -->
                            <div class="blog-media mt-40 mb-40 mb-xs-30">
                                <ul class="clearlist content-slider">
                                    @foreach($curso->imagenes as $imagen)
                                        <li>
                                            <img src="{{ $imagen->url }}" alt="{{ $curso->nombre }}" />
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <p>
                                {!! nl2br($curso->descripcion) !!}
                            </p>
                        </div>

                        <!-- End Text -->

                    </div>
                    <!-- End Post -->

                </div>
                <!-- End Content -->

            </div>

        </div>
    </section>
@stop