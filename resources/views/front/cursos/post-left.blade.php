<section class="page-section">
    <div class="container relative">

        <div class="row">

            <div class="col-md-7 mb-sm-40">

                @include('front.cursos.galeria')

            </div>

            <div class="col-md-5 col-lg-4 col-lg-offset-1">

                @include('front.cursos.about')

            </div>
        </div>
    </div>
</section>