<section class="page-section bg-gray-lighter">
    <div class="container relative">

        <div class="row">

            <div class="col-md-5 col-lg-4 mb-sm-40">

                @include('front.cursos.about')

            </div>

            <div class="col-md-7 col-lg-offset-1">

                @include('front.cursos.galeria')

            </div>

        </div>

    </div>
</section>