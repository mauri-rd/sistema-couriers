<!-- Work Gallery -->
<div class="work-full-media mt-0 white-shadow">
    <ul class="clearlist work-full-slider owl-carousel">
        @foreach($curso->imagenes as $imagen)
        <li>
            <img src="{{ $imagen->url }}" alt="{{ $curso->nombre }}" />
        </li>
        @endforeach
    </ul>
</div>
<!-- End Work Gallery -->