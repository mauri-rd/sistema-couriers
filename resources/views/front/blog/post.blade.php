<div class="blog-item">

    <!-- Post Title -->
    <h2 class="blog-item-title"><a href="{{ route('front.blog.find', [$post->id]) }}">{{ $post->titulo }}</a></h2>

    <!-- Author, Categories, Comments -->
    <div class="blog-item-data">
        <a href="#"><i class="fa fa-clock-o"></i> {{ $post->fecha }}</a>
        <span class="separator">&nbsp;</span>
        <i class="fa fa-user"></i> {{ $post->autor->name }}
        <span class="separator">&nbsp;</span>
        <i class="fa fa-folder-open"></i>
        <a href="{{ route('front.blog.index', ['cat_id' => $post->id_categoria]) }}">{{ $post->categoria->nombre }}</a>
    </div>

    <!-- Media Gallery -->
    <div class="blog-media">
        @if(!empty($post->img_url))
        <ul class="clearlist content-slider">
            <li>
                <img src="{{ $post->img_url }}" alt="{{ $post->nombre }}" />
            </li>
        </ul>
        @endif
    </div>

    <!-- Text Intro -->
    <div class="blog-item-body">
        <p>
            {!! nl2br(\App\Helpers\Truncate::truncate($post->cuerpo, 200)) !!}
        </p>
    </div>

    <!-- Read More Link -->
    <div class="blog-item-foot">
        <a href="{{ route('front.blog.find', [$post->id]) }}" class="btn btn-mod btn-color btn-small">Leer más <i class="fa fa-angle-right"></i></a>
    </div>

</div>