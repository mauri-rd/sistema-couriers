@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container relative">

            <div class="row">

                <!-- Content -->
                <div class="col-sm-8 col-md-9">

                    <!-- Post -->
                    <div class="blog-item mb-80 mb-xs-40">

                        <!-- Text -->
                        <div class="blog-item-body">

                            <h1 class="mt-0">{{ $post->titulo }}</h1>

                            <!-- End Text -->

                            <!-- Media Gallery -->
                            @if(!empty($post->img_url))
                            <div class="blog-media mt-40 mb-40 mb-xs-30">
                                <ul class="clearlist content-slider">
                                    <li>
                                        <img src="{{ $post->img_url }}" alt="" />
                                    </li>
                                </ul>
                            </div>
                            @endif

                            {!! nl2br($post->cuerpo) !!}


                        </div>
                        <!-- End Text -->

                    </div>
                    <!-- End Post -->

                </div>
                <!-- End Content -->

                @include('front.blog.sidebar')

            </div>

            @include('front.blog.galeria')

        </div>
    </section>
@stop