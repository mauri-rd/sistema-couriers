<!-- Sidebar -->
<div class="col-sm-4 col-md-3 sidebar">

    <!-- Search Widget -->
    <div class="widget">
        <form class="form-inline form" role="form" action="{{ route('front.blog.index') }}" method="get">
            <div class="search-wrap">
                <button class="search-button animate" type="submit" title="Buscar">
                    <i class="fa fa-search"></i>
                </button>
                <input name="q" type="text" class="form-control search-field" placeholder="Buscar">
            </div>
        </form>
    </div>
    <!-- End Search Widget -->

    <!-- Widget -->
    <div class="widget">

        <h5 class="widget-title">Categorias</h5>

        <div class="widget-body">
            <ul class="clearlist widget-menu">
                @foreach($categorias as $categoria)
                    <li>
                        <a href="{{ route('front.blog.index', ['cat_id' => $categoria->id]) }}" title="{{ $categoria->nombre }}">{{ $categoria->nombre }}</a>
                        <small>
                            - {{ count($categoria->posts) }}
                        </small>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
    <!-- End Widget -->

    <!-- Widget -->
    <div class="widget">

        <h5 class="widget-title">Últimos posts</h5>

        <div class="widget-body">
            <ul class="clearlist widget-posts">
                @foreach($ultimosPosts as $post)
                    <li class="clearfix">
                        {{--!<a href="{{ route('front.blog.find', [$post->id]) }}"><img src="{{ $post->img_url }}" alt="{{ $post->titulo }}" class="widget-posts-img" /></a>--}}
                        <div class="widget-posts-descr">
                            <a href="{{ route('front.blog.find', [$post->id]) }}" title="{{ $post->titulo }}">{{ $post->titulo }}</a>
                            {{ $post->autor->name }} | {{ $post->fecha }}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
    <!-- End Widget -->

    <!-- Widget -->
    <div class="widget">

        <h5 class="widget-title">Archivo</h5>

        <div class="widget-body">
            <ul class="clearlist widget-menu">
                @foreach($periodos as $periodo)
                    <li>
                        <a href="{{ route('front.blog.index', ['mes' => $periodo->mes, 'anho' => $periodo->anho]) }}">{{ \App\Helpers\PeriodosHelper::$meses[$periodo->mes] }} {{ $periodo->anho }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
    <!-- End Widget -->

    <!-- Widget -->
    <div class="widget">

        <div class="widget-body">
            <div class="tags">
                <a href="{{ route('front.blog.index') }}">Limpiar filtros</a>
            </div>
        </div>

    </div>
    <!-- End Widget -->

</div>
<!-- End Sidebar -->