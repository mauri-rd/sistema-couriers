@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container relative">

            <div class="row">

                <!-- Content -->
                <div class="col-sm-8 col-md-9">
                    <div class="left section-text mt-10">
                        @if(count($posts) == 0)
                            No se encontraron posts con este criterio de búsqueda.
                        @endif
                    </div>

                    @each('front.blog.post', $posts, 'post')

                    <!-- Pagination -->
                    {{ (new \App\Helpers\CustomPresenter($posts))->render() }}
                    <!-- End Pagination -->

                </div>
                <!-- End Content -->

                @include('front.blog.sidebar')

            </div>

        </div>
    </section>
@stop