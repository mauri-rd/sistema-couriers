<ul class="works-grid work-grid-5 work-grid-gut clearfix hover-white" id="work-grid">

    @foreach($post->multimedias as $multimedia)
    <!-- Work Item (Lightbox) -->
    <li class="work-item mix photography">
        <a href="{{ $multimedia->multimedia->isImg ? $multimedia->multimedia->url : $multimedia->multimedia->link_youtube }}" class="work-lightbox-link mfp-{{ $multimedia->multimedia->isImg ? 'image' : 'iframe' }}">
            <div class="work-img">
                <img src="{{ $multimedia->multimedia->url }}" alt="{{ $multimedia->multimedia->nombre }}" />
            </div>
        </a>
    </li>
    <!-- End Work Item -->
    @endforeach
</ul>