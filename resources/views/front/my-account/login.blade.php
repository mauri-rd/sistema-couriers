@extends('auth.master')
@section('titulo-pagina', 'Login')
@section('contenido')
    <div id="login-form">
        {!! Form::open(['route' => 'front.login', 'method' => 'post', 'class' => 'login-form']) !!}
            <span class="required">
                {!! Form::email('email', null, ['required', 'placeholder' => trans('auth.login-email-placeholder'), 'autofocus' => 'autofocus']) !!}
            </span>
            <span class="required">
                {!! Form::password('password', ['required', 'minlength' => 8, 'placeholder' => trans('auth.login-password-placeholder')]) !!}
            </span>
            <button>{{ trans('auth.login-button') }}</button>
            <p class="required-field">* {{ trans('messages.required-field') }}</p>
            <a href="javascript:showRegister()" class="sub-link">{{ trans('auth.register-link') }}</a> <br>
            <a href="{{ route('front.password-reset.form') }}" class="sub-link">{{ trans('auth.reset-password-link') }}</a>
        {!! Form::close() !!}
    </div>

    <div id="register-form" style="display: none;">
        {!! Form::open(['route' => 'front.register', 'method' => 'post', 'class' => 'login-form']) !!}
            <span class="required">
                {!! Form::text('name', null, ['required', 'placeholder' => trans('auth.register-name-field'), 'autofocus' => 'autofocus']) !!}
            </span>
            <span class="required">
                {!! Form::text('last_name', null, ['required', 'placeholder' => trans('auth.register-lastname-field')]) !!}
            </span>
            <span class="required">
                {!! Form::text('phone', null, ['required', 'placeholder' => trans('auth.register-phone-field')]) !!}
            </span>
            <span class="required">
                {!! Form::date('birthday', null, ['required', 'placeholder' => trans('auth.register-birthday-field')]) !!}
            </span>
            <span class="required">
                {!! Form::text('address', null, ['required', 'placeholder' => trans('auth.register-address-field')]) !!}
            </span>
            <span class="required">
                {!! Form::select('id_sucursal', $sucursales, null, ['class' => 'form-control', 'required', 'style' => 'margin-bottom: 15px;']) !!}
            </span>

            {{--<span class="required">--}}
                {{--{!! Form::text('city', null, ['required', 'placeholder' => trans('auth.register-city-field')]) !!}--}}
            {{--</span>--}}
            {{--<span class="required">--}}
                {{--{!! Form::text('state', null, ['required', 'placeholder' => trans('auth.register-state-field')]) !!}--}}
            {{--</span>--}}
            {{--<span class="required">--}}
                {{--{!! Form::text('zip', null, ['required', 'placeholder' => trans('auth.register-postalcode-field')]) !!}--}}
            {{--</span>--}}
            <span class="required">
                {!! Form::email('email', null, ['required', 'placeholder' => trans('auth.login-email-placeholder')]) !!}
            </span>
            <span class="required">
                {!! Form::password('password', ['required', 'minlength' => 8, 'placeholder' => trans('auth.login-password-placeholder')]) !!}
            </span>

            <button>{{ trans('auth.register-button') }}</button>
            <p class="required-field">* {{ trans('messages.required-field') }}</p>
            <a href="javascript:showLogin()" class="sub-link">{{ trans('auth.register-login-link') }}</a>
            {{--!<p class="message"><a href="{{ url('password/reset') }}">Olvidó su contraseña?</a></p>--}}
        {!! Form::close() !!}
    </div>
    <style>
        .sub-link {
            display: inline-block;
            margin-top: 10px;
        }
    </style>
    <script src="{{ url('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script>
        function showRegister() {
            $('#login-form').css({display: 'none'});
            $('#register-form').css({display: 'block'});
        }
        function showLogin() {
            $('#register-form').css({display: 'none'});
            $('#login-form').css({display: 'block'});
        }
        $(document).ready(function () {
            if (window.location.href.indexOf('#registro') !== -1) {
                showRegister();
            }
        })
    </script>
@stop
@section('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop