@extends('front.my-account.master')
@section('titulo-pagina', trans('menu.my-packages-pay'))
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                {!! Form::open(['route' => 'front.my-account.pagar', 'method' => 'POST', 'files' => true]) !!}
                <div class="content">
                    <h1>{{ trans('my-packages.paying-title') }}</h1>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ trans('my-packages.table-code') }}</th>
                                <th>{{ trans('my-packages.table-tracking') }}</th>
                                <th>{{ trans('my-packages.table-weight') }}</th>
                                <th>{{ trans('my-packages.table-description') }}</th>
                                <th>{{ trans('my-packages.table-subtotal') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!! Form::hidden('total_usd', $totalUsd) !!}
                            {!! Form::hidden('total_brl', $totalReales) !!}
                            {!! Form::hidden('arbitraje', $precioArbitrage) !!}
                            @foreach($paquetes as $paquete)
                                <tr>
                                    {!! Form::hidden('id_paquete[]', $paquete->id) !!}
                                    <td>{{ $paquete->codigo }}</td>
                                    <td>{{ $paquete->tracking }}</td>
                                    <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                    <td>{{ $paquete->descripcion }}</td>
                                    <td>US$ {{ round($paquete->total_sumado, 2) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="container">
                        <h4 class="pull-right">{{ trans('my-packages.paying-total') }}: {{ $cambioSymbol }} {{ number_format($totalCambio, $cambioSymbol == 'RS$' ? 2 : 0, ',', '.') }}</h4>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('my-packages.paying-wire-transfer-data') }}</h3>
                        </div>
                        <div class="panel-body">
                                {!! str_replace('#TOTAL', ( $cambioSymbol . ' ' . number_format($totalCambio, $cambioSymbol == 'RS$' ? 2 : 0, ',', '.') ), trans('my-packages.paying-total-instructions')) !!}

                            <p class="dado-conta">
                                <strong>CUENTA USD</strong><br>
                                Banco: Interfisa <br>
                                Cta. Cte.: 60001053<br>
                                Nombre: Ruben Apodaca <br>
                                C.I. Nro.: 2.899.786
                            </p>

                            <p class="dado-conta">
                                <strong>CUENTA GS</strong> <br>
                                Banco: Continental <br>
                                Cta. Ahorro: 26 - 00566830 - 07 <br>
                                Nombre: Cesar Apodaca <br>
                                C.I. Nto.: 3.573.199
                            </p>

                            <p class="dado-conta">
                                <strong>GIROS TIGO</strong> <br>
                                (0984) 302 - 222 <br>
                                Monto: Gs. {{ number_format(round($totalCambio * 1.15), 0, ',', '.') }}
                            </p>

                            <p class="dado-conta">
                                <strong>BILLETERA PERSONAL</strong> <br>
                                (0973) 221 - 222 <br>
                                Monto: Gs. {{ number_format(round($totalCambio * 1.15), 0, ',', '.') }}
                            </p>

                        </div>

                    </div>

                    <div class="form-group">
                        {!! Form::label('comprobante') !!}
                        {!! Form::file('comprobante', ['class' => 'form-control', 'required']) !!}
                    </div>

                    <button type="submit" class="btn btn-success pull-right">
                        <i class="fa fa-check"></i> {{ trans('my-packages.paying-send-button') }}
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('css')
    <style>
        .dado-conta {
            padding-left: 20px;
            margin: 10px 0;
        }
    </style>
@stop