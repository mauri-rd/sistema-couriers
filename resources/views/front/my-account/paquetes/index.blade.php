@extends('front.my-account.master')
@section('titulo-pagina', trans('menu.my-packages'))
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content packages-count">
                    <div class="full">
                        <a href="{{ route('front.my-account.paquetes-miami') }}">
                            <div class="column {{ $estado == [App\Enum\EstadoPaquete::$MIAMI, App\Enum\EstadoPaquete::$CHINA] ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-4 img-container">
                                        <img src="{{ url('img/box.png') }}" alt="">
                                    </div>
                                    <div class="col-md-6 text-container">
                                        {{ trans('menu.my-packages-miami') }}
                                    </div>
                                    <div class="col-md-2 count">
                                        {{ $miami }}
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('front.my-account.paquetes-transito') }}">
                            <div class="column {{ $estado == App\Enum\EstadoPaquete::$TRANSITO ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-4 img-container">
                                        <img src="{{ url('img/box.png') }}" alt="">
                                    </div>
                                    <div class="col-md-6 text-container">
                                        {{ trans('menu.my-packages-transito') }}
                                    </div>
                                    <div class="col-md-2 count">
                                        {{ $transito }}
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('front.my-account.paquetes-paraguay') }}">
                            <div class="column {{ $estado == App\Enum\EstadoPaquete::$PARAGUAY ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-4 img-container">
                                        <img src="{{ url('img/box.png') }}" alt="">
                                    </div>
                                    <div class="col-md-6 text-container">
                                        {{ trans('menu.my-packages-aduana') }}
                                    </div>
                                    <div class="col-md-2 count">
                                        {{ $paraguay }}
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('front.my-account.paquetes-oficina') }}">
                            <div class="column {{ $estado == App\Enum\EstadoPaquete::$OFICINA ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-4 img-container">
                                        <img src="{{ url('img/box.png') }}" alt="">
                                    </div>
                                    <div class="col-md-6 text-container">
                                        {{ trans('menu.my-packages-oficina') }}
                                    </div>
                                    <div class="col-md-2 count">
                                        {{ $oficina }}
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{ route('front.my-account.paquetes-retirado') }}">
                            <div class="column {{ $estado == App\Enum\EstadoPaquete::$RETIRADO ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-4 img-container">
                                        <img src="{{ url('img/box.png') }}" alt="">
                                    </div>
                                    <div class="col-md-6 text-container">
                                        {{ trans('menu.my-packages-retirado') }}
                                    </div>
                                    <div class="col-md-2 count">
                                        {{ $retirado }}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                    {!! Form::open(['route' => 'front.my-account.pagar-form', 'method' => 'POST', 'onsubmit' => 'return validatePagarForm()']) !!}
                @endif
                <div class="content">
                    @if($estado === App\Enum\EstadoPaquete::$TRANSITO)
                        <h1>{{ trans('my-packages.transito-selected-title') }}</h1>
                    @elseif($estado === App\Enum\EstadoPaquete::$PARAGUAY)
                        <h1>{{ trans('my-packages.aduana-selected-title') }}</h1>
                    @elseif($estado === App\Enum\EstadoPaquete::$OFICINA)
                        <h1>{{ trans('my-packages.retirar-selected-title') }}</h1>
                    @elseif($estado === App\Enum\EstadoPaquete::$RETIRADO)
                        <h1>{{ trans('my-packages.retirados-selected-title') }}</h1>
                    @else
                        <h1>{{ trans('my-packages.miami-selected-title') }}</h1>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                                @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                                    <th>{{ trans('my-packages.table-select') }}</th>
                                @endif
                                <th>{{ trans('my-packages.table-code') }}</th>
                                <th>{{ trans('my-packages.table-tracking') }}</th>
                                <th>{{ trans('my-packages.table-weight') }}</th>
                                <th>{{ trans('my-packages.table-description') }}</th>
                                @if(in_array($estado, [App\Enum\EstadoPaquete::$MIAMI, App\Enum\EstadoPaquete::$CHINA]))
                                    <th>{{ trans('my-packages.table-status') }}</th>
                                @endif
                                <th>{{ trans('my-packages.table-destiny') }}</th>
                                @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                                    <th>{{ trans('my-packages.table-payment') }}</th>
                                @endif
                                <th>{{ trans('my-packages.table-updated-at') }}</th>
                                <th>{{ trans('my-packages.table-subtotal') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($paquetes) == 0)
                            <tr>
                                <td colspan="7">
                                    <i>{{ trans('my-packages.table-empty') }}</i>
                                </td>
                            </tr>
                            @endif
                            @foreach($paquetes as $paquete)
                                <tr{!! $paquetesPagados->contains($paquete->id) ? ' class="text-success"' : '' !!}>
                                    @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                                        @if($paquetesPagados->contains($paquete->id))
                                            <td></td>
                                        @else
                                            <td>{!! Form::checkbox('id_paquete[]', $paquete->id, false, ['class' => 'paquete-checkbox']) !!}</td>
                                        @endif
                                    @endif
                                    <td>{{ $paquete->codigo }}</td>
                                    <td>{{ $paquete->tracking }}</td>
                                    <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                    <td>{{ $paquete->descripcion }}</td>
                                    @if(in_array($estado, [App\Enum\EstadoPaquete::$MIAMI, App\Enum\EstadoPaquete::$CHINA]))
                                        <td>{{ $paquete->estado == App\Enum\EstadoPaquete::$MIAMI ? 'MIAMI' : 'CHINA' }}</td>
                                    @endif
                                    <td>{{ intval($paquete->destino) === \App\Enum\Destino::PARAGUAY ? 'PARAGUAY' : 'BRASIL' }}</td>
                                    @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                                        <td>{{ $paquetesPagados->contains($paquete->id) ? trans('my-packages.table-payed') : trans('my-packages.table-not-payed') }}</td>
                                    @endif
                                    <td>{{ $paquete->updated_at->format('d/m/Y H:i:s') }}</td>
                                    <td>US$ {{ round($paquete->total_sumado, 2) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-check"></i> {{ trans('my-packages.pay-button') }}
                        </button>
                    @endif
                </div>
                @if($estado === App\Enum\EstadoPaquete::$OFICINA)
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        function validatePagarForm() {
            if($('.paquete-checkbox:checked').length === 0) {
                alert('{!! trans('my-packages.alert-select-package') !!}');
                return false;
            }
            return true;
        }
    </script>
@stop