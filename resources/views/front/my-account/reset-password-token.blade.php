@extends('auth.master')
@section('titulo-pagina', trans('password.reset-password-title'))
@section('contenido')
    {!! Form::open(['route' => 'front.password-reset.reset', 'method' => 'post']) !!}
        <span class="required">
            <label>E-mail</label>
            {!! Form::email('email', $email, ['required', 'readonly']) !!}
            {!! Form::hidden('token', $token) !!}
            <label>{{ trans('password.reset-password-new-password-field') }}</label>
            {!! Form::password('password', ['required', 'autofocus']) !!}
        </span>
        <button>{{ trans('password.reset-password-new-password-button') }}</button>
    {!! Form::close() !!}
@stop