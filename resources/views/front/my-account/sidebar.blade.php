<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <ul class="nav side-menu">
                <li>
                    <a href="{{ route('front.my-account.perfil.index') }}">
                        <i class="fa fa-user"></i>
                        {{ trans('menu.my-profile') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('front.my-account.addresses') }}">
                        <i class="fa fa-map"></i>
                        {{ trans('menu.my-addresses') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('front.my-account.services.index') }}">
                        <i class="fa fa-cart-plus"></i>
                        {{ trans('menu.my-services') }}
                    </a>
                </li>

                <li>
                    <a href="javascript:;">
                        <i class="fa fa-archive"></i>
                        {{ trans('menu.my-packages') }}
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('front.my-account.paquetes-miami') }}">{{ trans('menu.my-packages-miami') }}</a></li>
                        <li><a href="{{ route('front.my-account.paquetes-transito') }}">{{ trans('menu.my-packages-transito') }}</a></li>
                        <li><a href="{{ route('front.my-account.paquetes-paraguay') }}">{{ trans('menu.my-packages-aduana') }}</a></li>
                        <li><a href="{{ route('front.my-account.paquetes-oficina') }}">{{ trans('menu.my-packages-oficina') }}</a></li>
                        <li><a href="{{ route('front.my-account.paquetes-retirado') }}">{{ trans('menu.my-packages-retirado') }}</a></li>
                    </ul>
                </li>

                {{--<li>--}}
                    {{--<a href="{{ route('front.my-account.packlists.index') }}">--}}
                        {{--<i class="fa fa-book"></i>--}}
                        {{--{{ trans('menu.my-packlists') }}--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>