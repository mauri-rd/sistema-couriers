@extends('front.my-account.master')
@section('titulo-pagina', 'Meus Packlists enviados')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('front.my-account.packlists.create') }}" class="btn btn-fill btn-success">Cadastrar Packlist/Lista de produtos comprados</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Tracking number</th>
                            <th>Courier</th>
                            <th>Tipo de Frete</th>
                            <th>Destino</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Anexo</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($packlists as $packlist)
                            <tr>
                                <td>{{ $packlist->tracking }}</td>
                                @if($packlist->courier == App\Enum\Couriers::$DHL)
                                    <td>DHL</td>
                                @elseif($packlist->courier == App\Enum\Couriers::$UPS)
                                    <td>UPS</td>
                                @elseif($packlist->courier == App\Enum\Couriers::$USPS)
                                    <td>USPS</td>
                                @elseif($packlist->courier == App\Enum\Couriers::$FedEx)
                                    <td>FedEx</td>
                                @endif

                                @if($packlist->tipo_flete == App\Enum\TipoFlete::NORMAL)
                                    <td>Normal</td>
                                @elseif($packlist->tipo_flete == App\Enum\TipoFlete::EXPRESS)
                                    <td>Express</td>
                                @endif

                                @if($packlist->destino == App\Enum\Destino::PARAGUAY)
                                    <td>Paraguai</td>
                                @elseif($packlist->destino == App\Enum\Destino::BRASIL)
                                    <td>Brasil</td>
                                @endif

                                <td>{{ $packlist->descripcion }}</td>
                                <td>{{ $packlist->moeda }} {{ $packlist->valor }}</td>
                                <td>
                                    <a href="{{ $packlist->file }}" target="_blank" class="anexo">
                                        @if(str_contains($packlist->mime, 'image'))
                                            <img src="{{ $packlist->file }}">
                                        @else
                                            <i class="fa fa-file-pdf-o"></i>
                                        @endif
                                        <div class="legend">Clique para abrir o arquivo</div>
                                    </a>
                                </td>
                                <td>
                                    @if(is_null($packlist->paquete))
                                        <a href="{{ route('front.my-account.packlists.edit', [$packlist->id]) }}" class="btn btn-info">Editar</a>
                                        <a href="{{ route('front.my-account.packlists.delete', [$packlist->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                    @else
                                        <i>Já existe um pacote associado a esse packlist e qualquer alteração está bloqueada.</i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $packlists->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <style>
        .anexo img {
            height: 40px;
        }

        .anexo i.fa {
            font-size: 40px;
            color: red;
        }
    </style>
@endsection