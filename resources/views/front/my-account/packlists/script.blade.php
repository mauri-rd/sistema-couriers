<link rel="stylesheet" href="{{ url('css/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script src="{{ url('js/bootstrap-tagsinput.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    (function () {
        const tracking = $('#tracking');
        const consolidacao = $('#consolidacao');
        const consolidacaoTrackingNumbers = $('#consolidacao_tracking_numbers');
        const trackingList = $('#tracking_list');
        let oldValue = null;
        function replaceInTags(oldValue, newValue) {
            trackingList.tagsinput('remove', oldValue);
            trackingList.tagsinput('add', newValue);
            searchTrackingConsolidar(newValue);
        }

        function checkConsolidar() {
            if (consolidacao.prop('checked')) {
                consolidacaoTrackingNumbers.css({ display: 'block' });
            } else {
                consolidacaoTrackingNumbers.css({ display: 'none' });
            }
        }

        function checkDestino() {
            if ($('#destino').val() === '{{ \App\Enum\Destino::PARAGUAY }}') {
                $('#tipo_flete_container').css({ display: 'block' });
            } else {
                $('#tipo_flete_container').css({ display: 'none' });
            }
        }

        function searchTrackingConsolidar(tracking) {
            if (!tracking) return;
            $.ajax({
                url: '{{ route('front.my-account.packlists.check-tracking') }}',
                data: {
                    tracking: tracking
                },
                success: function (res) {
                    if (res) {
                        const options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": function () {
                                consolidar(res);
                            },
                            "showDuration": "10000",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success('Achados pacotes para consolidar!', 'Deseja consolidar esse pacote com os tracking' + res + '? Clique aqui para consolidar', options);
                    }
                }
            })
        }

        function consolidar(trackingNumbers) {
            trackingNumbers.split(',').forEach(function (value) {
                trackingList.tagsinput('add', value);
            })
        }

        tracking.on('keydown', function () {
            oldValue = this.value;
        });

        tracking.on('keyup', function () {
            replaceInTags(oldValue, this.value);
        });

        consolidacao.on('change', checkConsolidar);
        $('#destino').on('change', checkDestino);

        checkDestino();
        checkConsolidar();
    })();
</script>