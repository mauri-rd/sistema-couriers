@extends('front.my-account.master')
@section('titulo-pagina', 'Editar Packlist')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($packlist, ['route' => ['front.my-account.packlists.update', $packlist->id], 'method' => 'post', 'files' => true]) !!}
                    @include('front.my-account.packlists.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('front.my-account.packlists.script')
@stop
@section('css')
    <style>
        .anexo {
            height: 200px;
        }
    </style>
@endsection