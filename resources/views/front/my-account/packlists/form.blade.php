<div class="row">
    <div class="col-md-12">
        <label>Tracking number</label>
        {!! Form::text('tracking', null, ['class' => 'form-control', 'placeholder' => 'Insira seu tracking number ou codigo de rastreamento', 'required', 'autofocus', 'id' => 'tracking']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Courier</label>
        {!! Form::select('courier', $couriers, null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Courier</label>
        {!! Form::select('moneda', $monedas, null, ['class' => 'form-control', 'required']) !!}
        {!! Form::number('valor', null, ['class' => 'form-control', 'placeholder' => 'Insira o valor da compra em numeros', 'min' => '0.01', 'step' => '0.01', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Destino</label>
        {!! Form::select('destino', $destinos, null, ['class' => 'form-control', 'required', 'id' => 'destino']) !!}
    </div>
</div>

<div class="row" id="tipo_flete_container">
    <div class="col-md-12">
        <label>Tipo Frete</label>
        {!! Form::select('tipo_flete', $fletes, isset($packlist) ? $packlist->tipo_flete : $user->tipo_flete, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="consolidacao">
            {!! Form::checkbox('consolidacao', 0, isset($packlist) ? $packlist->consolidacao : $user->consolidacao, ['id' => 'consolidacao']) !!}
            Consolidar pacotes
        </label>
    </div>
</div>

<div class="row" id="consolidacao_tracking_numbers">
    <div class="col-md-12">
        <label>Tracking numbers para consolidar</label>
        <div class="form-group">
            {!! Form::text('consolidacao_trackings', null, ['class' => 'form-control', 'id' => 'tracking_list', 'data-role' => 'tagsinput', 'placeholder' => 'Insira seu tracking number ou codigo de rastreamento', 'required']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="seguro">
            {!! Form::checkbox('seguro', 0, isset($packlist) ? $packlist->seguro : $user->seguro, ['id' => 'seguro']) !!}
            Solicitar seguro
        </label>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Descrição da compra</label>
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Insira a descrição da compra, liste os items comprados', 'required']) !!}
    </div>
</div>
    
<div class="row">
    <div class="col-md-12">
        <label>Anexo</label>
        {!! Form::file('file', array_merge(['accept' => 'image/*,application/pdf'], isset($packlist) ? [] : ['required'])) !!}
        <p class="help-block">Selecione o invoice ou print da compra em PDF ou formato imagem</p>
    </div>
    @if(isset($packlist))
        @if(str_contains($packlist->mime, 'image'))
            <a href="{{ $packlist->file }}" target="_blank">
                <img src="{{ $packlist->file }}" class="anexo">
            </a>
        @else
            <a href="{{ $packlist->file }}" target="_blank" class="btn btn-primary">
                <i class="fa fa-file-pdf-o"></i> Abrir documento anexado
            </a>
        @endif
    @endif
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Enviar</button>
<div class="clearfix"></div>