@extends('front.my-account.master')
@section('titulo-pagina', 'Registrar nuevo Packlist/nova lista de produtos enviados')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'front.my-account.packlists.store', 'method' => 'post', 'files' => true]) !!}
                    @include('front.my-account.packlists.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('front.my-account.packlists.script')
@stop