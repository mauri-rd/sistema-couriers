<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>StrBox | @yield('titulo-pagina')</title>

    <!-- Bootstrap -->
    <link href="{{ url('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ url('vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ url('build/css/custom.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
    @yield('css')
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="clearfix"></div>
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="Logo" class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>{{ trans('menu.welcome') }}</span>
                        <h2>{{ auth()->user()->short_name }}</h2>
                        <h2>{{ auth()->user()->prefix . auth()->user()->casilla }}</h2>
                    </div>
                </div>

                <br />

                <!-- sidebar menu -->

                @include('front.my-account.sidebar')
            </div>
        </div>
    </div>

    <div class="main-panel">
        @include('front.my-account.top-menu')
        <div class="right_col" role="main">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('msg'))
                <div class="alert alert-success">
                    {{ session('msg') }}
                </div>
            @endif
            @if(auth()->user()->last_name == '')
                <div class="alert alert-danger">
                    {{ trans('messages.reconfirm-name-lastname') }} <a
                            href="{{ route('front.my-account.perfil.index') }}">clicando aquí</a>
                </div>
            @endif

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>@yield('titulo-pagina')</h3>
                    </div>
                </div>

                <div class="clearfix"></div>
                @yield('contenido')
            </div>
        </div>


        @include('admin.footer')
        </div>
    </div>

<script src="{{ url('vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ url('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ url('vendors/nprogress/nprogress.js') }}"></script>
<!-- Dropzone.js -->
<script src="{{ url('vendors/dropzone/dist/min/dropzone.min.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ url('build/js/custom.min.js') }}"></script>

<script type="text/javascript">
    $('.to-delete').on('click', function() {
        return confirm('Está seguro que desea eliminar el registro?');
    });
</script>
@yield('js')

</body>
</html>
