@extends('auth.master')
@section('titulo-pagina', trans('password.reset-password-title'))
@section('contenido')
    {!! Form::open(['route' => 'front.password-reset.request-reset', 'method' => 'post']) !!}
        <span class="required">
            {!! Form::email('email', null, ['required', 'placeholder' => trans('auth.login-email-placeholder'), 'autofocus' => 'autofocus']) !!}
        </span>
        <button>{{ trans('password.reset-password-email-button') }}</button>
    {!! Form::close() !!}
@stop