@extends('front.my-account.master')
@section('titulo-pagina', trans('menu.my-services'))
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {{--{!! Form::open(['route' => 'front.my-account.services.store', 'method' => 'post']) !!}--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<label for="type">Tipo de frete</label>--}}
                            {{--{!! Form::select('tipo_flete', $fletes, $user->tipo_flete, ['class' => 'form-control']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<label for="consolidacao">--}}
                                {{--{!! Form::checkbox('consolidacao', 0, $user->consolidacao, ['id' => 'consolidacao']) !!}--}}
                                {{--Consolidar pacotes--}}
                            {{--</label>--}}
                            {{--<p class="help-block">--}}
                                {{--Você economiza consolidando seus pacotes, pois agrupamos vários pacotes e os enviamos em um só, pagando somente US$ 2 por pacote, além do peso--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<label for="seguro">--}}
                                {{--{!! Form::checkbox('seguro', 0, $user->seguro, ['id' => 'seguro']) !!}--}}
                                {{--Solicitar seguro--}}
                            {{--</label>--}}
                            {{--<p class="help-block">--}}
                                {{--Em casos de problemas com o pacote, com extravio, perdas, e outros, o seguro cobre 100% do valor do invoice enviado--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<button type="submit" class="btn btn-info btn-fill pull-right">Enviar</button>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--{!! Form::close() !!}--}}
                    <h2>{{ trans('my-services.title') }}</h2>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{ trans('my-services.insurance') }}</h3>
                                    </div>
                                    <div class="panel-body">
                                        {{ trans('my-services.insurance-text') }}
                                        <img src="{{ url('img/cajaseguridad-min.png') }}" class="img-responsive" alt="Seguro">
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop