@extends('front.my-account.master')
@section('titulo-pagina', trans('menu.my-addresses'))
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    <div class="row" id="addresses">
                        <div class="col-md-6">
                            <h1>
                                <i class="fa fa-plane"></i> {{ trans('my-addresses.airplane-delivery') }}
                            </h1>
                            <div class="col-md-12 address-block">
                                <img src="{{ url('assets/img/flag-usa.png') }}" alt="USA">
                                <h2> {{ trans('my-addresses.us-address') }}</h2>
                                <p>
                                    {{ trans('my-addresses.us-address-name') }}: {{ auth()->user()->short_name }} {{ auth()->user()->prefix . auth()->user()->casilla }}<br>
                                    {{ trans('my-addresses.us-address-address') }}: 5459 NW 72 ND AVE <br>
                                    {{ trans('my-addresses.us-address-city') }}: MIAMI <br>
                                    {{ trans('my-addresses.us-address-state') }}: FLORIDA <br>
                                    {{ trans('my-addresses.us-address-country') }}: ESTADOS UNIDOS <br>
                                    {{ trans('my-addresses.us-address-postalcode') }}: 33166 <br>
                                    {{ trans('my-addresses.us-address-phone') }}: (786) 409 2811
                                </p>
                            </div>

                            <div class="col-md-12 address-block">
                                <img src="{{ url('assets/img/flag-china.png') }}" alt="China">
                                <h2>{{ trans('my-addresses.china-address') }}</h2>
                                <p>
                                    {{ trans('my-addresses.china-address-message') }}
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h1>
                                <i class="fa fa-ship"></i> {{ trans('my-addresses.boat-delivery') }}
                            </h1>
                            <div class="col-md-12 address-block">
                                <img src="{{ url('assets/img/flag-usa.png') }}" alt="USA">
                                <h2> {{ trans('my-addresses.us-address') }}</h2>
                                <p>
                                    {{ trans('my-addresses.us-address-name') }}: {{ auth()->user()->short_name }} {{ auth()->user()->prefix . auth()->user()->casilla }}<br>
                                    {{ trans('my-addresses.us-address-address') }}: 5459 NW 72 ND AVE <br>
                                    {{ trans('my-addresses.us-address-city') }}: MIAMI <br>
                                    {{ trans('my-addresses.us-address-state') }}: FLORIDA <br>
                                    {{ trans('my-addresses.us-address-country') }}: ESTADOS UNIDOS <br>
                                    {{ trans('my-addresses.us-address-postalcode') }}: 33166 <br>
                                    {{ trans('my-addresses.us-address-phone') }}: (786) 409 2811
                                </p>
                            </div>
                            <div class="col-md-12 address-block">
                                <img src="{{ url('assets/img/flag-china.png') }}" alt="China">
                                <h2>{{ trans('my-addresses.china-address') }}</h2>
                                {{ trans('my-addresses.china-address-message') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <style>
        #addresses {
            text-align: center;
        }

        #addresses img {
            height: 80px;
        }

        .address-block {
            width: 100%;
        }
    </style>
@endsection