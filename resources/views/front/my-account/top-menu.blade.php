<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{ auth()->user()->short_name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ route('front.logout') }}">{{ trans('menu.logout') }}</a></li>
                        @if(auth()->user()->lang == \App\Enum\UserLanguage::ESPANOL)
                            <li><a href="{{ route('front.set-language', ['language' => \App\Enum\UserLanguage::PORTUGUES]) }}">Mudar para Portugués</a></li>
                        @elseif(auth()->user()->lang == \App\Enum\UserLanguage::PORTUGUES)
                            <li><a href="{{ route('front.set-language', ['language' => \App\Enum\UserLanguage::ESPANOL]) }}">Cambiar a Español</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>