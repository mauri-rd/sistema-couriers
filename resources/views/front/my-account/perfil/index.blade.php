@extends('front.my-account.master')
@section('titulo-pagina', trans('menu.my-profile'))
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($user, ['route' => 'front.my-account.perfil.save', 'method' => 'post', 'files' => true]) !!}
                    @include('front.my-account.perfil.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop