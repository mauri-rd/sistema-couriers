<div class="row">
    <div class="col-md-12">
        <label for="type">{{ trans('my-profile.name-field') }}</label>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="type">{{ trans('my-profile.lastname-field') }}</label>
        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="type">{{ trans('my-profile.phone-field') }}</label>
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="type">{{ trans('my-profile.address-field') }}</label>
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
    </div>
</div>
{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label for="type">{{ trans('my-profile.country-field') }}</label>--}}
        {{--{!! Form::text('country', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label for="type">{{ trans('my-profile.city-field') }}</label>--}}
        {{--{!! Form::text('city', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label for="type">{{ trans('my-profile.state-field') }}</label>--}}
        {{--{!! Form::text('state', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label for="type">{{ trans('my-profile.postalcode-field') }}</label>--}}
        {{--{!! Form::text('zip', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label for="type">{{ trans('my-profile.password-field') }}</label>
        {!! Form::password('password', ['class' => 'form-control', 'minlength' => 8]) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Enviar</button>
<div class="clearfix"></div>