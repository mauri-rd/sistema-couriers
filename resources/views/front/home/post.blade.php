<!-- Post Item -->
<div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

    <div class="post-prev-img post-prev-border">
        <a href="{{ route('front.blog.find', [$post->id]) }}"><img src="{{ $post->img_url }}" alt="{{ $post->titulo }}" /></a>
    </div>

    <div class="post-prev-title">
        <a href="{{ route('front.blog.find', [$post->id]) }}">{{ $post->titulo }}</a>
    </div>

    <div class="post-prev-info">
        {{ $post->autor->name }} | {{ $post->fecha }}
    </div>

    <div class="post-prev-text">
        {!! nl2br(\App\Helpers\Truncate::truncate($post->cuerpo, 200)) !!}
    </div>

    <div class="post-prev-more">
        <a href="{{ route('front.blog.find', [$post->id]) }}" class="btn btn-mod btn-color">Lea más <i class="fa fa-angle-right"></i></a>
    </div>

</div>
<!-- End Post Item -->