<!-- Work Item (Lightbox) -->
<li class="work-item mix cat_{{ $multimedia->id_categoria }}">
    <a href="{{ $multimedia->isImg ? $multimedia->url : $multimedia->link_youtube }}" class="work-lightbox-link mfp-{{ $multimedia->isImg ? 'image' : 'iframe' }}">
        <div class="work-img">
            <img src="{{ $multimedia->url }}" alt="{{ $multimedia->nombre }}" />
        </div>
        <div class="work-intro">
            <h3 class="work-title">{{ $multimedia->nombre }}</h3>
        </div>
    </a>
</li>
<!-- End Work Item -->