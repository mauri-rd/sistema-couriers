@extends('front.master')
@section('contenido')
<div id="page" class="clearfix">
        <!-- Header Wrap -->
        <div id="site-header-wrap">
            
            <!-- Header -->
            <header id="site-header">
                <div class="wrap-themesflat-container">
                    <div id="site-header-inner" class="themesflat-container">
                        <div class="wrap-inner">
                            <div id="site-logo" class="clearfix">
                                <div id="site-logo-inner">
                                    <a href="home.html" title="StrBox" rel="home" class="main-logo"><img src="{{ url('assets/img/logo-strbox.png') }}" width="184" height="40" alt="Finance" data-retina="{{ url('assets/img/logo-strbox.png') }}" data-width="184" data-height="40"></a>
                                </div>
                            </div><!-- /#site-logo -->
    
                            <div class="mobile-button"><span></span></div><!-- //mobile menu button -->
    
                            <nav id="main-nav" class="main-nav">
                                <ul id="menu-primary-menu" class="menu">
                                    
                                    <li class="menu-item menu-item-has-children"><a class="scroll-target" href="#howto">COMO COMPRAR</a></li>
                                    <li class="menu-item menu-item-has-children"><a class="scroll-target" href="#scrolldown">QUEM SOMOS</a></li>
                                    <li class="menu-item menu-item-has-children"><a class="scroll-target" href="#pricing">PREÇOS</a></li>
                                    <li class="menu-item menu-item-has-children"><a class="scroll-target" href="#frecuent-ask">PERGUNTAS FREQUENTES</a></li>
                                    <li class="menu-item menu-item-has-children"><a class="scroll-target" href="#footer">CONTATO</a></li>
                                    <li class="menu-item menu-item-has-children"><a href="{{ route('front.login-form') }}">LOGIN / CADASTRO</a></li>
    
                                </ul>
                            </nav><!-- /#main-nav -->
    
                        </div>
                    </div><!-- /#site-header-inner -->
                </div><!-- /.wrap-themesflat-container -->
            </header><!-- /#site-header -->
        </div><!-- #site-header-wrap -->
    
        <!-- Main Content -->
        <div id="main-content" class="site-main clearfix">
            <div id="content-wrap">
                <div id="site-content" class="site-content clearfix">
                    <div id="inner-content" class="inner-content-wrap">
                        <div class="page-content">

                            <div id="main-banners-carousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    @foreach($patrocinadores as $index => $patrocinador)
                                        <li data-target="#main-banners-carousel" data-slide-to="{{ $index }}"{!! $index === 0 ? ' class="active"' : '' !!}></li>
                                    @endforeach
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    @foreach($patrocinadores as $index => $patrocinador)
                                    <div class="item{{ $index === 0 ? ' active' : '' }}">
                                        <img src="{{ $patrocinador->url }}" alt="Slide BRL">
                                        <div class="carousel-caption">
                                            <div class="elm-btn">
                                                {{--<a href="{{ route('front.login-form') }}" class="themesflat-button accent">CADASTRE-SE</a>--}}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#main-banners-carousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                                <a class="right carousel-control" href="#main-banners-carousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Seguinte</span>
                                </a>
                            </div>


                            <!-- VIDEO -->
                            <div class="row-video" id="howto">
                                <div class="container">
                                    <div class="themesflat-spacer clearfix" data-desktop="84" data-mobi="60" data-smobi="60"></div>
                                    <div class="row equalize sm-equalize-auto">
                                        <div class="col-md-6">
                                            <div class="themesflat-headings style-1 color-white text-center clearfix" style="padding: 106px;">
                                                <h2 class="heading clearfix" style="color: red;">StrBox <br>SIMPLES DEMAIS DE USAR</h2>
                                            </div>
                                            <!-- /.themesflat-headings -->
                                        </div>
                                        <!-- /.col-md-6 -->
    
                                        <div class="col-md-6 half-background style-2">
                                            <div class="themesflat-icon style-1 clearfix background">
                                                <a class="icon-wrap popup-video" href="{{ url('assets/video/how-to-buy.mp4') }}">
                                                    <span class="icon"><i class="finance-icon-playbutton"></i></span>
                                                </a>
                                            </div>
                                            <!-- /.themesflat-icon -->
                                        </div>
                                        <!-- /.col-md-6 -->
                                    </div>
                                    <div class="themesflat-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div>
                            </div>
                            <!-- END VIDEO -->

                            <!-- VIDEO -->
                            <div class="row-video" id="economy">
                                <div class="container">
                                    <div class="themesflat-spacer clearfix" data-desktop="84" data-mobi="60" data-smobi="60"></div>
                                    <div class="row equalize sm-equalize-auto">

                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="91" data-mobi="60" data-smobi="60"></div>

                                            <div class="themesflat-headings style-1 clearfix text-center">
                                                <h2 class="heading clearfix">COMO ECONOMIZAR?</h2>
                                                <div class="sep clearfix"></div>
                                            </div><!-- /.themesflat-headings -->

                                            <div class="themesflat-spacer clearfix" data-desktop="51" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-12 -->

                                        <div class="col-md-6 border-right-1 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </div>

                                                <h3 class="heading"><a href="#">SEGURO PARA SEUS PACOTES</a></h3>

                                                <p class="desc">Com o valor adicional de 3% do seu invoice. Você pode ficar tranquilo pedindo com nós, que no evento de ocorrer algum problema com seu pacote, seja problema na aduana ou perda, devolveremos 100% do valor do invoice!</p>
                                            </div><!-- /.themesflat-icon-box -->

                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-4 -->

                                        <div class="col-md-6 border-left-1 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-clone"></i>
                                                </div>

                                                <h3 class="heading"><a href="#">CONSOLIDE SEUS PACOTES</a></h3>

                                                <p class="desc">Com a consolidação de pacotes ficou muito mais facil economizar com nós. Pagando US$ 2 por pacote, colocamos todos seus pacotes numa caixa só e somente cobramos pelo peso da caixa final que contém todos os pacotes.</p>
                                            </div><!-- /.themesflat-icon-box -->

                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-4 -->

                                    </div>
                                    <div class="themesflat-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div>
                            </div>
                            <!-- END VIDEO -->

                            <!-- FACTS -->
                            <div id="facts" class="themesflat-row row-facts-1 parallax parallax-overlay">
                                    <div class="bg-parallax-overlay"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="themesflat-spacer clearfix" data-desktop="150" data-mobi="60" data-smobi="60"></div>
                                            </div><!-- /.col-md-12 -->
        
                                            <div class="col-md-12">
                                                <div class="themesflat-animation-block" data-animate="fadeInUpSmall" data-duration="1s" data-delay="0" data-position="80%">
                                                    <div class="themesflat-counter style-4 mobi-center clearfix icon-top margin-top--42">
                                                        <div class="inner">
                                                            <div class="sub-heading"></div>
        
                                                            <div class="text-wrap">
                                                                <div class="number-wrap font-heading quem-somos-american-title">
                                                                    StrBox
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.themesflat-counter -->
                                                </div><!-- /.themesflat-animation-block -->
        
                                                <div class="themesflat-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                            </div><!-- /.col-md-4 -->
                                            <div class="col-md-12">
                                                
                                                {{-- <div class="col span_1_of_4 relative"> --}}
                                                <div class="col-md-3 col-sm-12 relative">
                                                    <div class="themesflat-animation-block" data-animate="fadeInUpSmall" data-duration="1s" data-delay="0.15s" data-position="80%">
                                                    <div class="themesflat-counter style-2 clearfix icon-top text-center">
                                                        <div class="inner">
                                                            <div class="icon-wrap facts-icon">
                                                                {{-- <div class="icon"><i class="finance-icon-bulb"></i></div> --}}
                                                                <img src="{{ url('assets/img/icon-client.png') }}">
                                                            </div>
        
                                                            <div class="text-wrap">
                                                                <div class="number-wrap font-heading">
                                                                    <span class="prefix"></span><span class="number accent" data-speed="3000" data-to="{{ $configuraciones['clientes-satisfeitos']->valor }}" data-inviewport="yes">{{ $configuraciones['clientes-satisfeitos']->valor }}</span><span class="suffix">+</span>
                                                                </div>
        
                                                                <div class="sep"></div>
                                                                <h3 class="heading">CLIENTES SATISFEITOS</h3>
                                                            </div>
        
                                                            <div class="border-right-2"></div>
                                                        </div>
                                                    </div><!-- /.themesflat-counter -->
                                                    </div><!-- /.themesflat-animation-block -->
        
                                                    <div class="themesflat-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                                </div><!-- /.span_1_of_3 -->
        
                                                <div class="col-md-3 col-sm-12 relative">
                                                    <div class="themesflat-animation-block" data-animate="fadeInUpSmall" data-duration="1s" data-delay="0.3s" data-position="80%">
                                                    <div class="themesflat-counter style-2 clearfix icon-top text-center">
                                                        <div class="inner">
                                                            <div class="icon-wrap facts-icon">
                                                                {{-- <div class="icon"><i class="finance-icon-businessman-1"></i></div> --}}
                                                                <img src="{{ url('assets/img/icon-box.png') }}">
                                                            </div>
        
                                                            <div class="text-wrap">
                                                                <div class="number-wrap font-heading">
                                                                    <span class="prefix"></span><span class="number accent" data-speed="3000" data-to="{{ $configuraciones['pacotes-entregues']->valor }}" data-inviewport="yes">{{ $configuraciones['pacotes-entregues']->valor }}</span><span class="suffix">+</span>
                                                                </div>
        
                                                                <div class="sep"></div>
                                                                <h3 class="heading">PACOTES ENTREGUES</h3>
        
                                                                <div class="border-right-2"></div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.themesflat-counter -->
                                                    </div><!-- /.themesflat-animation-block -->
        
                                                    <div class="themesflat-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                                </div><!-- /.span_1_of_3 -->
        
                                                <div class="col-md-3 col-sm-12 relative">
                                                    <div class="themesflat-animation-block" data-animate="fadeInUpSmall" data-duration="1s" data-delay="0.45s" data-position="80%">
                                                    <div class="themesflat-counter style-2 clearfix icon-top text-center">
                                                        <div class="inner">
                                                            <div class="icon-wrap facts-icon">
                                                                {{-- <div class="icon"><i class="finance-icon-award"></i></div> --}}
                                                                <img src="{{ url('assets/img/icon-home.png') }}">
                                                            </div>
        
                                                            <div class="text-wrap">
                                                                <div class="number-wrap font-heading">
                                                                    <span class="prefix"></span><span class="number accent" data-speed="3000" data-to="{{ $configuraciones['numero-sucursais']->valor }}" data-inviewport="yes">{{ $configuraciones['numero-sucursais']->valor }}</span><span class="suffix">+</span>
                                                                </div>
        
                                                                <div class="sep"></div>
                                                                <h3 class="heading">SUCURSAL</h3>
                                                                <div class="border-right-2"></div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.themesflat-counter -->
                                                    </div><!-- /.themesflat-animation-block -->
                                                </div><!-- /.span_1_of_3 -->
                                                
                                                <div class="col-md-3 col-sm-12 relative">
                                                    <div class="themesflat-animation-block" data-animate="fadeInUpSmall" data-duration="1s" data-delay="0.45s" data-position="80%">
                                                    <div class="themesflat-counter style-2 clearfix icon-top text-center">
                                                        <div class="inner">
                                                            <div class="icon-wrap facts-icon">
                                                                {{-- <div class="icon"><i class="finance-icon-award"></i></div> --}}
                                                                <img src="{{ url('assets/img/icon-cake.png') }}">
                                                            </div>
        
                                                            <div class="text-wrap">
                                                                <div class="number-wrap font-heading">
                                                                    <span class="prefix"></span><span class="number accent" data-speed="3000" data-to="{{ $configuraciones['anos-mercado']->valor }}" data-inviewport="yes">{{ $configuraciones['anos-mercado']->valor }}</span><span class="suffix">+</span>
                                                                </div>
        
                                                                <div class="sep"></div>
                                                                <h3 class="heading">ANOS NO MERCADO</h3>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.themesflat-counter -->
                                                    </div><!-- /.themesflat-animation-block -->
                                                </div><!-- /.span_1_of_3 -->
                                            </div><!-- /.col-md-8 -->
        
                                            <div class="col-md-12">
                                                <div class="themesflat-spacer clearfix" data-desktop="113" data-mobi="60" data-smobi="60"></div>
                                            </div><!-- /.col-md-12 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.container -->
                                </div>
                                <!-- END FACTS 1 -->
    
                            <!-- ICONBOX -->
                            <div class="row-services" id="scrolldown">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="91" data-mobi="60" data-smobi="60"></div>
    
                                            <div class="themesflat-headings style-1 clearfix text-center">
                                                <h2 class="heading clearfix">QUEM SOMOS?</h2>
                                                <div class="sep clearfix"></div>
                                            </div><!-- /.themesflat-headings -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="51" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-12 -->

                                        <div class="col-md-3 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-dolly-flatbed"></i>
                                                </div>
    
                                                <h3 class="heading"><a href="#">SOBRE NÓS</a></h3>
    
                                                <p class="desc">{!! nl2br($configuraciones['texto-footer']->valor) !!}</p>
                                            </div><!-- /.themesflat-icon-box -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="0" data-smobi="0"></div>
                                        </div><!-- /.col-md-4 -->
    
                                        <div class="col-md-3 border-left-1 border-right-1 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-23">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-shopping-bag"></i>
                                                </div>
    
                                                <h3 class="heading"><a href="#">COMPRE AS MELHORES OFERTAS</a></h3>
    
                                                <p class="desc">Compre as melhores marcas e grandes ofertas disponíveis apenas nas lojas dos EUA, como Amazon, Gap, Ralph Lauren, Nordstrom e 6PM. Basta digitar o seu endereço StrBox no checkout, e suas compras serão enviadas (geralmente de graça) para as nossas instalações nos EUA.</p>
                                            </div><!-- /.themesflat-icon-box -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-4 -->
    
                                        <div class="col-md-3 border-left-1 border-right-1 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-laptop"></i>
                                                </div>
    
                                                <h3 class="heading"><a href="#">TUDO DESDE SEU PAINEL</a></h3>
    
                                                <p class="desc">Assim que você se registrar, você será redirecionado para o seu próprio painel. Em seu painel, você verá imediatamente seu endereço nos EUA, que pode ser usado enquanto estiver na loja (on-line). Compras da China mas precisa de um endereço? Não tem problema, no seu painel você verá o endereço Chinês.</p>
                                            </div><!-- /.themesflat-icon-box -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-4 -->
    
                                        <div class="col-md-3 dark">
                                            <div class="themesflat-icon-box style-1 clearfix w70 text-center rounded-100 has-width padding-left-36 padding-right-30">
                                                <div class="icon-wrap">
                                                    <i class="fa fa-map-signs"></i>
                                                </div>
    
                                                <h3 class="heading"><a href="#">ACOMPANHE SEUS PEDIDOS</a></h3>
    
                                                <p class="desc">Depois que você fizer suas compras nas lojas (nos EUA ou na China) faça o login no seu painel e forneça seu número de rastreamento, lista de itens comprados e anexe uma cópia do recibo de compra. Se você tiver feito pedidos separados, também deverá fornecer uma cópia do recibo de compra para cada um junto com um número de rastreamento.</p>
                                            </div><!-- /.themesflat-icon-box -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="36" data-mobi="0" data-smobi="0"></div>
                                        </div><!-- /.col-md-4 -->

                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="110" data-mobi="60" data-smobi="60"></div>
                                        </div><!-- /.col-md-12 -->
                                    </div><!-- /.row -->
                                </div><!-- /.container -->
                            </div>
                            <!-- END ICONBOX -->
    
                            <div id="pricing" class="row-pricing">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="92" data-mobi="60" data-smobi="60" style="height:92px"></div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.themesflat-row -->
        
                                    <div class="row clearfix">
                                        <div class="col-md-6">
                                            <div class="themesflat-spacer clearfix" data-desktop="20" data-mobi="30" data-smobi="30" style="height:20px"></div>
        
                                            <div class="themesflat-content-box clearfix" data-margin="0 4px 0 0" data-mobimargin="0 0 0 0" style="margin:0 4px 0 0">
                                                <div class="themesflat-price-table style-1 bg-white">
                                                    <div class="price-table-price">
                                                        <div class="price-wrap">
                                                            <span class="figure">MIAMI<br><span class="red">BRASIL</span></span>
                                                            <div class="title price-dolars">US$ {{ $configuraciones['envio-miami']->valor }}/KG</div>
                                                            <ul>
                                                                    <li><span class="figure2">Cargas pessoais e comerciais</span></li>
                                                                    <li><span class="figure2">Cargas em conteineres e carga solta </span></li>
                                                                    <li><span class="figure2">Não cobramos pelo volume </span></li>
                                                                    {{--<li><span class="figure2">Serviço de entrega porta a porta no Paraguay </span></li>--}}
                                                                    <li><span class="figure2">Prazo de entrega 10 dias </span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
        
                                                </div>
                                                <!-- /.themesflat-price-table -->
                                            </div>
                                            <!-- /.themesflat-content-box -->
                                        </div>
                                        <!-- /.col -->
        
                                        {{--<div class="col-md-3">--}}
                                            {{-- <div class="themesflat-spacer clearfix" data-desktop="0" data-mobi="30" data-smobi="30" style="height:0px"></div>--}}
        {{----}}
                                            {{--<img src="{{ ('assets/img/bolsadinero.png') }}" class="bolsa"> --}}
                                            {{--<div class="themesflat-spacer clearfix" data-desktop="20" data-mobi="30" data-smobi="30" style="height:20px"></div>--}}
        {{----}}
                                            {{--<div class="themesflat-content-box clearfix" data-margin="0 4px 0 0" data-mobimargin="0 0 0 0" style="margin:0 4px 0 0">--}}
                                                {{--<div class="themesflat-price-table style-1 bg-white">--}}
                                                    {{--<div class="price-table-price">--}}
                                                        {{--<div class="price-wrap">--}}
                                                            {{--<span class="figure"> MIAMI <br> <span class="red">BRASIL</span></span>--}}
                                                            {{-- <div class="title price-dolars">US$ {{ $configuraciones['envio-miami']->valor }}/KG</div> --}}
                                                            {{--<div class="title price-dolars">US$ {{ $configuraciones['envio-brasil']->valor }}/KG</div>--}}
                                                            {{--<ul>--}}
                                                                    {{--<li><span class="figure2">Vôo direto </span></li>--}}
                                                                    {{--<li><span class="figure2">Miami - São Paulo </span></li>--}}
                                                                    {{--<li><span class="figure2">Prazo de entrega 15 dias </span></li>--}}
                                                            {{--</ul>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
        {{----}}
                                                {{--</div>--}}
                                                {{--<!-- /.themesflat-price-table -->--}}
                                            {{--</div>--}}
                                            {{--<!-- /.themesflat-content-box -->--}}
                                        {{--</div>--}}
                                        {{--<!-- /.col -->--}}

                                        <div class="col-md-6">
                                            <div class="themesflat-spacer clearfix" data-desktop="20" data-mobi="30" data-smobi="30" style="height:20px"></div>
        
                                            <div class="themesflat-content-box clearfix" data-margin="0 0 0 5px" data-mobimargin="0 0 0 0" style="margin:0 0 0 5px">
                                                <div class="themesflat-price-table style-1 bg-white">
                                                    <div class="price-table-price">
                                                        <div class="price-wrap">
                                                            <span class="figure">CHINA<br><span class="red">URUGUAI</span></span>
                                                            <div class="title price-dolars">US$ {{ $configuraciones['envio-china']->valor }}/KG</div>
                                                            <ul>
                                                                <li><span class="figure2">Cargas pessoais e comerciais</span></li>
                                                                <li><span class="figure2">Cargas em conteineres e carga solta </span></li>
                                                                <li><span class="figure2">Não cobramos pelo volume </span></li>
                                                                {{--<li><span class="figure2">Serviço de entrega porta a porta no Paraguay </span></li>--}}
                                                                <li><span class="figure2">Prazo de entrega 15 dias</span></li>
                                                            </ul>
        
                                                        </div>
                                                    </div>
        
                                                </div>
                                                <!-- /.themesflat-price-table -->
                                            </div>
                                            <!-- /.themesflat-content-box -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.themesflat-row -->
        
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="92" data-mobi="60" data-smobi="60" style="height:92px"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.themesflat-container -->
                            </div>

                            <div class="row">
                                <div class="container" id="frecuent-ask">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="34" data-mobi="40" data-smobi="40"></div>
                                            <div class="themesflat-headings style-1 clearfix text-center">
                                                <h2 class="heading clearfix">PERGUNTAS FREQUENTES</h2>
                                                <div class="sep clearfix"></div>
                                            </div><!-- /.themesflat-headings -->
                                            <div class="themesflat-spacer clearfix" data-desktop="51" data-mobi="40" data-smobi="40" style="height:51px"></div>
                                            <div class="panel-group" id="facts-acordion" role="tablist" aria-multiselectable="true">
                                                @foreach($preguntas as $index => $pregunta)
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="heading_question_{{ $pregunta->id }}" data-toggle="collapse" data-parent="#facts-acordion" href="#question_{{ $pregunta->id }}" aria-expanded="{{ $index == 0 ? 'true' : 'false' }}" aria-controls="question_{{ $pregunta->id }}">
                                                            <h4 class="panel-title">
                                                                {{-- <a role="button"> --}}
                                                                {{ $pregunta->pregunta }}
                                                                {{-- </a> --}}
                                                            </h4>
                                                        </div>
                                                        <div id="question_{{ $pregunta->id }}" class="panel-collapse collapse{{ $index == 0 ? ' in' : '' }}" role="tabpanel" aria-labelledby="heading_question_{{ $pregunta->id }}">
                                                        <div class="panel-body">
                                                            {!! nl2br($pregunta->respuesta) !!}
                                                        </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="themesflat-spacer clearfix" data-desktop="34" data-mobi="40" data-smobi="40"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- PARTNERS -->
                            <div class="row-partner">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="themesflat-spacer clearfix" data-desktop="34" data-mobi="40" data-smobi="40"></div>
    
                                            <div class="themesflat-partner style-3 offset30 offset-v0" data-auto="true" data-loop="true" data-column="5" data-column2="3" data-column3="2" data-gap="60"><div class="owl-carousel owl-theme">
                                                @foreach($empresas as $empresa)
                                                    <div class="partner-item clearfix">
                                                        <div class="inner">
                                                            <a target="_blank" href="{{ $empresa->link }}">
                                                                <div class="thumb">
                                                                    <img src="{{ url($empresa->imagen_url) }}" alt="{{ $empresa->nombre }}">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
    
                                            </div></div><!-- /.themesflat-partner -->
    
                                            <div class="themesflat-spacer clearfix" data-desktop="25" data-mobi="40" data-smobi="40"></div>
                                        </div><!-- /.col-md-12 -->
                                    </div><!-- /.row -->
                                </div><!-- /.container -->
                            </div>
                            <!-- END PARTNERS -->                        
                        </div><!-- /.page-content -->
                    </div><!-- /#inner-content -->
                </div><!-- /#site-content -->
            </div><!-- /#content-wrap -->
        </div><!-- /#main-content -->
    
        <!-- Footer -->
        <footer id="footer">
            <div id="footer-widgets" class="themesflat-container title-style-1">
                <div class="themesflat-row gutter-30">
                    <div class="span_1_of_4 col">
                        <div class="widget widget_text padding-left-7">
                            <div class="textwidget">
                                <p class="margin-bottom-22">
                                    <img src="{{ url('assets/img/logo-strbox.png') }}" width="226" height="50" alt="Finance" data-retina="assets/img/logo-footer@2x.png" data-width="226" data-height="50">
                                </p>
                                <p>{!! nl2br($configuraciones['texto-footer']->valor) !!}</p>
                            </div>
                        </div><!-- /.widget_text -->
    
                    </div><!-- /.span_1_of_4 -->
    
                    <div class="span_1_of_4 col">
                        <div class="widget widget_information margin-top-6 padding-left-13">
                            <h2 class="widget-title"><span>BRL no Paraguai</span></h2>
                            <ul>
                                @if($configuraciones['direccion-paraguay']->valor != '')
                                    <li class="address clearfix">
                                        <div class="inner">
                                            <i class=" finance-icon-map"></i>
                                            <span class="text">{{ $configuraciones['direccion-paraguay']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if($configuraciones['correo-paraguay']->valor != '')
                                    <li class="email clearfix">
                                        <div class="inner">
                                        <i class=" finance-icon-Email02"></i>
                                        <span class="text">{{ $configuraciones['correo-paraguay']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if($configuraciones['telefono-paraguay']->valor != '')
                                    <li class="phone clearfix">
                                        <div class="inner">
                                        <i class=" finance-icon-Phone"></i>
                                        <span class="text">{{ $configuraciones['telefono-paraguay']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div><!-- /.widget_recent_news -->
                    </div><!-- /.span_1_of_4 -->
    
                    <div class="span_1_of_4 col">
                        <div class="widget widget_information margin-top-6 padding-left-9">
                            <h2 class="widget-title margin-bottom-43"><span>BRL em Miami</span></h2>
                            <ul>
                                @if($configuraciones['direccion-miami']->valor != '')
                                    <li class="address clearfix">
                                        <div class="inner">
                                            <i class=" finance-icon-map"></i>
                                            <span class="text">{{ $configuraciones['direccion-miami']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if($configuraciones['correo-miami']->valor != '')
                                    <li class="email clearfix">
                                        <div class="inner">
                                        <i class=" finance-icon-Email02"></i>
                                        <span class="text">{{ $configuraciones['correo-miami']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if($configuraciones['telefono-miami']->valor != '')
                                    <li class="phone clearfix">
                                        <div class="inner">
                                        <i class=" finance-icon-Phone"></i>
                                        <span class="text">{{ $configuraciones['telefono-miami']->valor }}</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div><!-- /.widget_tag_cloud -->
                    </div><!-- /.span_1_of_4 -->
    
                    <div class="span_1_of_4 col" id="footer-social-networks">
                        <div class="widget widget_flickr margin-top-6 padding-left-6">
                            <h2 class="widget-title"><span>Nossas redes</span></h2>
    
                            @if ($configuraciones['facebook']->valor)
                                <a href="{{ $configuraciones['facebook']->valor }}">
                                    <div class="item">
                                        <i class="fa fa-facebook"></i>
                                    </div>
                                </a>
                            @endif
                            @if ($configuraciones['instagram']->valor)
                                <a href="{{ $configuraciones['instagram']->valor }}">
                                    <div class="item">
                                        <i class="fa fa-instagram"></i>
                                    </div>
                                </a>
                            @endif
                        </div><!-- /.widget_instagram -->
                    </div><!-- /.span_1_of_4 -->
                </div><!-- /.themesflat-row -->
            </div><!-- /#footer-widgets -->
        </footer><!-- /#footer -->
    
        <!-- Bottom -->
        <div id="bottom" class="clearfix style-1">
            <div id="bottom-bar-inner" class="themesflat-container">
                <div class="bottom-bar-inner-wrap">
                    <div class="bottom-bar-content">
                        <div id="copyright" class="padding-left-9">
                            Hecho con mucho <i _ngcontent-c3="" class="fa fa-coffee"></i> por <a _ngcontent-c3="" href="https://nineteen.solutions">Nineteen Solutions</a>
                        </div><!-- /#copyright -->
                    </div><!-- /.bottom-bar-content -->
    
                </div>
            </div>
        </div><!-- /#bottom -->
    </div><!-- /#page -->

    @if($configuraciones['numero-whatsapp']->valor != '')
        <a target="_blank" href="https://api.whatsapp.com/send?phone={{ $configuraciones['numero-whatsapp']->valor }}">
            <div class="whatsapp">
                <i class="fa fa-whatsapp"></i>
            </div>
        </a>
    @endif
@endsection