@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container">

            <div class="row mb-70 mb-xs-40">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-text align-center">
                        Le mostramos los 5 registros más relevantes de cada sección del sitio,
                        referente a su búsqueda.
                        Si desea ver más resultados en una sección específica puede continuar
                        la búsqueda en una de las siguientes secciones.
                    </div>
                </div>
            </div>

            <!-- Pricing row -->
            <div class="row multi-columns-row">

                <!-- Pricing Item -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="pricing-item">
                        <div class="pricing-item-inner">
                            <div class="pricing-wrap">

                                <!-- Icon (Simple-line-icons) -->
                                <div class="pricing-icon color">
                                    <i class="fa fa-child"></i>
                                </div>

                                <!-- Pricing Title -->
                                <div class="pricing-title">
                                    Servicios
                                </div>

                                <!-- Pricing Features -->
                                <div class="pricing-features">
                                    @unless(count($servicios) > 0)
                                        No fueron encontrados servicios con este critério de búsqueda.
                                    @endunless
                                    <ul class="sf-list pr-list">
                                        @foreach($servicios as $servicio)
                                            <li><a href="{{ route('front.servicios.find', [$servicio->id]) }}">{{ $servicio->nombre }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>

                                <!-- Button -->
                                <div class="pr-button">
                                    <a href="{{ route('front.servicios.index', ['q' => $query]) }}" class="btn btn-mod btn-medium btn-color">Buscar más servicios</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Pricing Item -->

                <!-- Pricing Item -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="pricing-item">
                        <div class="pricing-item-inner">
                            <div class="pricing-wrap">

                                <!-- Icon (Simple-line-icons) -->
                                <div class="pricing-icon color">
                                    <i class="fa fa-archive"></i>
                                </div>

                                <!-- Pricing Title -->
                                <div class="pricing-title">
                                    Productos
                                </div>

                                <!-- Pricing Features -->
                                <div class="pricing-features">
                                    @unless(count($productos) > 0)
                                    No fueron encontrados productos con este critério de búsqueda.
                                    @endunless
                                    <ul class="sf-list pr-list">
                                        @foreach($productos as $producto)
                                        <li><a href="{{ route('front.productos.find', [$producto->id]) }}">{{ $producto->nombre }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>

                                <!-- Button -->
                                <div class="pr-button">
                                    <a href="{{ route('front.productos.index', ['q' => $query]) }}" class="btn btn-mod btn-medium btn-color">Buscar más productos</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Pricing Item -->

                <!-- Pricing Item -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="pricing-item">
                        <div class="pricing-item-inner">
                            <div class="pricing-wrap">

                                <!-- Icon (Simple-line-icons) -->
                                <div class="pricing-icon color">
                                    <i class="fa fa-newspaper-o"></i>
                                </div>

                                <!-- Pricing Title -->
                                <div class="pricing-title">
                                    Cursos
                                </div>

                                <!-- Pricing Features -->
                                <div class="pricing-features">
                                    @unless(count($cursos) > 0)
                                        No fueron encontrados cursos con este critério de búsqueda.
                                    @endunless
                                    <ul class="sf-list pr-list">
                                        @foreach($cursos as $curso)
                                            <li><a href="{{ route('front.cursos.find', [$curso->id]) }}">{{ $curso->nombre }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>

                                <!-- Button -->
                                <div class="pr-button">
                                    <a href="{{ route('front.cursos.index', ['q' => $query]) }}" class="btn btn-mod btn-medium btn-color">Buscar más cursos</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Pricing Item -->

                <!-- Pricing Item -->
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="pricing-item">
                        <div class="pricing-item-inner">
                            <div class="pricing-wrap">

                                <!-- Icon (Simple-line-icons) -->
                                <div class="pricing-icon color">
                                    <i class="fa fa-pencil-square"></i>
                                </div>

                                <!-- Pricing Title -->
                                <div class="pricing-title">
                                    Blog
                                </div>

                                <!-- Pricing Features -->
                                <div class="pricing-features">
                                    @unless(count($posts) > 0)
                                        No fueron encontrados posts con este critério de búsqueda.
                                    @endunless
                                    <ul class="sf-list pr-list">
                                        @foreach($posts as $post)
                                            <li><a href="{{ route('front.blog.find', [$post->id]) }}">{{ $post->titulo }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                @if(count($posts) > 0)
                                <!-- Button -->
                                <div class="pr-button">
                                    <a href="{{ route('front.blog.index', ['q' => $query]) }}" class="btn btn-mod btn-medium btn-color">Buscar más posts</a>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Pricing Item -->

            </div>
            <!-- End Pricing row -->

        </div>
    </section>
@stop