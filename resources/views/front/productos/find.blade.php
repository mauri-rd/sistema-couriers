@extends('front.master')
@section('contenido')
    <section class="page-section">
    <div class="container relative">

        <!-- Product Content -->
        <div class="row mb-60 mb-xs-30">

            <!-- Product Images -->
            <div class="col-md-4 mb-md-30">

                <div class="post-prev-img">
                    <a href="{{ count($producto->imagenes) ? $producto->imagenes[0]->url : '' }}" class="lightbox-gallery-3 mfp-image"><img src="{{ count($producto->imagenes) ? $producto->imagenes[0]->url : '' }}" alt="{{ $producto->nombre }}" /></a>
                </div>

                <div class="row">
                    @foreach($producto->imagenes as $imagen)
                    <div class="col-xs-3 post-prev-img">
                        <a href="{{ $imagen->url }}" class="lightbox-gallery-3 mfp-image"><img src="{{ $imagen->url }}" alt="{{ $producto->nombre }}" /></a>
                    </div>
                    @endforeach
                </div>

            </div>
            <!-- End Product Images -->

            <!-- Product Description -->
            <div class="col-sm-8 col-md-5 mb-xs-40">

                <h3 class="mt-0">{{ $producto->nombre }}</h3>

                <hr class="mt-0 mb-30"/>

                <div class="row">
                    <div class="col-xs-6 lead mt-0 mb-20">

                        <strong>{{ $producto->precio }}</strong>

                    </div>
                </div>

                <hr class="mt-0 mb-30"/>

                <div class="section-text small">
                    <div>Categoria: <a href="{{ route('front.productos.index', ['cat_id' => $producto->id_categoria]) }}"> {{ $producto->categoria->nombre }}</a></div>
                    @if(!is_null($producto->modelo))
                        <div>Marca: <a href="{{ route('front.productos.index', ['marca_id' => $producto->modelo->id_marca]) }}">{{ $producto->modelo->marca->nombre }}</a></div>
                        <div>Modelo: <a href="{{ route('front.productos.index', ['modelo_id' => $producto->id_modelo]) }}">{{ $producto->modelo->nombre }}</a></div>
                    @endif
                </div>

            </div>
            <!-- End Product Description -->

        </div>
        <!-- End Product Content -->


        <!-- Nav tabs -->
        <ul class="nav nav-tabs tpl-tabs animate">
            <li class="active">
                <a href="#one" data-toggle="tab">Descripción</a>
            </li>
        </ul>
        <!-- End Nav tabs -->

        <!-- Tab panes -->
        <div class="tab-content tpl-tabs-cont">
            <div class="tab-pane fade in active" id="one">
                {!! nl2br($producto->descripcion) !!}
            </div>
        </div>
        <!-- End Tab panes -->

    </div>
</section>
@stop