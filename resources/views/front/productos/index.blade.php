@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container relative">

            <div class="row">

                <!-- Content -->
                <div class="col-sm-8">

                    <!-- Shop options -->
                    <div class="clearfix mb-40">

                        <div class="left section-text mt-10">
                            @if(count($productos) == 0)
                                No se encontraron productos con este criterio de búsqueda.
                            @else
                                Mostrando {{ $productos->toArray()['from'] }}–{{ $productos->toArray()['to'] }} de {{ $productos->toArray()['total'] }} resultados
                            @endif
                        </div>
                        @if(count($productos) > 0)
                        <div class="right">
                            <form method="get" action="{{ url()->current() }}" class="form">
                                @if(Request::has('q'))
                                    {!! Form::hidden('q', Request::only(['q'])['q']) !!}
                                @endif
                                @if(Request::has('marca_id'))
                                    {!! Form::hidden('marca_id', Request::only(['marca_id'])['marca_id']) !!}
                                @endif
                                @if(Request::has('modelo_id'))
                                    {!! Form::hidden('modelo_id', Request::only(['modelo_id'])['modelo_id']) !!}
                                @endif
                                @if(Request::has('cat_id'))
                                    {!! Form::hidden('cat_id', Request::only(['cat_id'])['cat_id']) !!}
                                @endif
                                <select name="order" class="input-md" onchange="this.form.submit()">
                                    <option value="az">Ordenar alfabeticamente - Ascendente</option>
                                    <option value="za">Ordenar alfabeticamente - Descendente</option>
                                    <option value="09">Ordenar por precio - De menor a mayor</option>
                                    <option value="90">Ordenar por precio - De mayor a menor</option>
                                </select>
                            </form>
                        </div>
                        @endif

                    </div>
                    <!-- End Shop options -->

                    <div class="row multi-columns-row">

                        @each('front.productos.post', $productos, 'producto')
                    </div>

                    <!-- Pagination -->
                    {!! (new \App\Helpers\CustomPresenter($productos))->render() !!}
                    <!-- End Pagination -->

                </div>
                <!-- End Content -->

                <!-- Sidebar -->
                <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">

                    <!-- Search Widget -->
                    <div class="widget">
                        <form class="form-inline form" role="form" action="{{ route('front.productos.index') }}" method="get">
                            <div class="search-wrap">
                                <button class="search-button animate" type="submit" title="Buscar">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input name="q" type="text" class="form-control search-field" placeholder="Buscar">
                            </div>
                        </form>
                    </div>
                    <!-- End Search Widget -->

                    {{--!<!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title">Filter by price</h5>

                        <div class="widget-body">
                            <form method="post" action="#" class="form">

                                <div class="row mb-20">
                                    <div class="col-xs-6">
                                        <input type="text" name="price-from" id="price-from" class="input-md form-control" placeholder="From, $" maxlength="100">
                                    </div>

                                    <div class="col-xs-6">
                                        <input type="text" name="price-to" id="price-to" class="input-md form-control" placeholder="To, $" maxlength="100">
                                    </div>
                                </div>

                                <button class="btn btn-mod btn-medium btn-full">Filter</button>

                            </form>
                        </div>

                    </div>
                    <!-- End Widget -->--}}

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title">Categorias</h5>

                        <div class="widget-body">
                            <ul class="clearlist widget-menu">
                                @foreach($categorias as $categoria)
                                <li>
                                    <a href="{{ route('front.productos.index', ['cat_id' => $categoria->id]) }}" title="{{ $categoria->nombre }}">{{ $categoria->nombre }}</a>
                                    <small>
                                        - {{ count($categoria->productos) }}
                                    </small>
                                </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                    <!-- End Widget -->

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title">Marcas y Modelos</h5>

                        <div class="widget-body">
                            <ul class="clearlist widget-menu">
                                @foreach($marcas as $marca)
                                    <li>
                                        <a href="{{ route('front.productos.index', ['marca_id' => $marca->id]) }}" title="{{ $marca->nombre }}">{{ $marca->nombre }}</a>
                                        <ul>
                                            @foreach($marca->modelos as $modelo)
                                                <li>
                                                    <a href="{{ route('front.productos.index', ['modelo_id' => $modelo->id]) }}" title="{{ $modelo->nombre }}">{{ $modelo->nombre }}</a>
                                                    <small>
                                                        - {{ count($modelo->productos) }}
                                                    </small>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                    <!-- End Widget -->


                    <!-- Widget -->
                    <div class="widget">


                        <div class="widget-body">
                            <div class="tags">
                                <a href="{{ route('front.productos.index') }}">Limpiar filtros</a>
                            </div>
                        </div>

                    </div>
                    <!-- End Widget -->

                </div>
                <!-- End Sidebar -->
            </div>

        </div>
    </section>
@stop