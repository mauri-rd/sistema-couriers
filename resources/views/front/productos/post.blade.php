<!-- Shop Item -->
<div class="col-md-4 col-lg-4 mb-60 mb-xs-40">

    <div class="post-prev-img">

        <a href="{{ route('front.productos.find', [$producto->id]) }}">
            <img src="{{ count($producto->imagenes) > 0 ? $producto->imagenes[0]->url : '' }}" alt="{{ $producto->nombre }}" /></a>

    </div>

    <div class="post-prev-title align-center">
        <a href="{{ route('front.productos.find', [$producto->id]) }}">{{ $producto->nombre }}</a>
    </div>

    <div class="post-prev-text align-center">
        <strong>{{ $producto->precio }}</strong>
    </div>

    <div class="post-prev-more align-center">
        <a href="{{ route('front.productos.find', [$producto->id]) }}" class="btn btn-mod btn-color">Sepa más <i class="fa fa-angle-right"></i></a>
    </div>

</div>
<!-- End Shop Item -->