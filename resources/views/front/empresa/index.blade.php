@extends('front.master')
@section('contenido')
    <!-- Historia -->
    <section class="split-section bg-gray">
        <div class="clearfix relative">

            <!-- Section Headings -->
            <div class="split-section-headings left">
                <div class="ssh-table">
                    <div class="ssh-cell page-section bg-scroll" data-background="{{ url('img/historia.png') }}"></div>
                </div>
            </div>
            <!-- End Section Headings -->

            <!-- Section Content -->
            <div class="split-section-content right small-section bg-gray-lighter">

                <div class="split-section-wrapper">

                    <h1 class="section-title"><br></h1>

                    <h2 class="section-heading uppercase strong">Nuestra Historia</h2>

                    <div class="section-text mb-60">
                        {!! nl2br($configs['historia']->valor) !!}
                    </div>

                </div>

            </div>
            <!-- End Section Content -->

        </div>
    </section>
    <!-- Fin Historia -->

    <!-- Misión -->
    <section class="split-section bg-gray">
        <div class="clearfix relative">

            <!-- Section Headings -->
            <div class="split-section-headings right">
                <div class="ssh-table">
                    <div class="ssh-cell page-section bg-scroll" data-background="{{ url('img/mision.png') }}"></div>
                </div>
            </div>
            <!-- End Section Headings -->

            <!-- Section Content -->
            <div class="split-section-content small-section bg-gray-lighter">

                <div class="split-section-wrapper left">

                    <h1 class="section-title"></h1>

                    <h2 class="section-heading uppercase strong">Nuestra Misión</h2>

                    <div class="section-text mb-60">
                        {!! nl2br($configs['mision']->valor) !!}
                    </div>

                </div>

            </div>
            <!-- End Section Content -->

        </div>
    </section>
    <!-- Fin Misión -->

    <!-- Visión -->
    <section class="split-section bg-gray">
    <div class="clearfix relative">

        <!-- Section Headings -->
        <div class="split-section-headings left">
            <div class="ssh-table">
                <div class="ssh-cell page-section bg-scroll" data-background="{{ url('img/vision.png') }}"></div>
            </div>
        </div>
        <!-- End Section Headings -->

        <!-- Section Content -->
        <div class="split-section-content right small-section bg-gray-lighter">

            <div class="split-section-wrapper">

                <h1 class="section-title"></h1>

                <h2 class="section-heading uppercase strong">Nuestra Visión</h2>

                <div class="section-text mb-60">
                    {!! nl2br($configs['vision']->valor) !!}
                </div>

            </div>

        </div>
        <!-- End Section Content -->

    </div>
</section>
    <!-- Fin Visión -->
@stop