<!-- Post Item -->
<div class="col-sm-6 col-md-4 col-lg-4 mb-60 mb-xs-40">

    <div class="post-prev-img">
        <a href="{{ route('front.servicios.find', [$servicio->id]) }}">
            <img src="{{ count($servicio->imagenes) > 0 ? $servicio->imagenes[0]->url : '' }}" alt="{{ $servicio->nombre }}" /></a>
    </div>

    <div class="post-prev-title">
        <a href="{{ route('front.servicios.find', [$servicio->id]) }}">{{ $servicio->nombre }}</a>
    </div>

    <div class="post-prev-text">
        {{ \App\Helpers\Truncate::truncate($servicio->descripcion, 200) }}
    </div>

    @if(!empty($servicio->precio))
    <div class="post-prev-title">
        {{ $servicio->precio }}
    </div>
    @endif

    <div class="post-prev-more">
        <a href="{{ route('front.servicios.find', [$servicio->id]) }}" class="btn btn-mod btn-color">Sepa más <i class="fa fa-angle-right"></i></a>
    </div>

</div>
<!-- End Post Item -->