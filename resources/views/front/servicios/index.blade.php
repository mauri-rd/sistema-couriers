@extends('front.master')
@section('contenido')
    <section class="page-section">
        <div class="container relative">

            <div class="row multi-columns-row">
                <div class="widget">
                    <form class="form-inline form" role="form" action="{{ route('front.servicios.index') }}" method="get">
                        <div class="search-wrap">
                            <button class="search-button animate" type="submit" title="Buscar">
                                <i class="fa fa-search"></i>
                            </button>
                            <input name="q" type="text" class="form-control search-field" placeholder="Buscar">
                        </div>
                    </form>
                </div>
                @each('front.servicios.post', $servicios, 'servicio')
            </div>

            <!-- Pagination -->
            {!! (new \App\Helpers\CustomPresenter($servicios))->render() !!}
            <!-- End Pagination -->


        </div>
    </section>
@stop