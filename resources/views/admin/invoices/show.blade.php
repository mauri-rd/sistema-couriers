@extends('admin.master')
@section('titulo-pagina', 'Invoice #' . $invoice->id)
@section('contenido')
    <div class="row">
        <div class="col-md-12" style="width: 100%">
            <div class="x_panel">
                <div class="content">
                        <div id="invoice">
                            <div class="invoice-logo">
                                <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
                            </div>
                            <div class="invoice-header">
                                <h1>Invoice #{{ $invoice->id }}</h1>
                                <h1>Para: {{ $invoice->usuario->full_name }}</h1>
                                <h1>Casilla: {{ $invoice->usuario->prefix }}{{ $invoice->usuario->casilla }}</h1>
                                <h3>Fecha del Invoice: {{ $invoice->created_at->format('d/m/Y H:i:s') }}</h3>
                            </div>
                            <div>
                                <table class="invoice-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Descripción</th>
                                            <th>Tracking</th>
                                            <th>Peso</th>
                                            <th>Volumen</th>
                                            <th>Precio unitario</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($invoice->items as $index => $item)
                                            <tr>
                                                <td style="padding: 5px 10px;">{{ $index + 1 }}</td>
                                                <td style="padding: 5px 10px;">{{ $item->paquete->descripcion }}</td>
                                                @if($item->paquete->consolidacao)
                                                    <td style="padding: 5px 10px;">{{ $item->paquete->consolidacao_trackings }} <i>(consolidados)</i></td>
                                                @else
                                                    <td style="padding: 5px 10px;">{{ $item->paquete->tracking }}</td>
                                                @endif
                                                <td style="padding: 5px 10px;">{{ $item->paquete->gramos / 1000 }} Kg.</td>
                                                <td>{{ $item->paquete->volumen }}</td>
                                                <td style="padding: 5px 10px;">US$ {{ $item->paquete->precio_kg }}</td>
                                                <td style="padding: 5px 10px;">US$ {{ $item->paquete->valor_total }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="invoice-footer">
                                @if($invoice->has_seguro)
                                    <h1>Seguro: US$ {{ $invoice->valor_seguro }}</h1>
                                @endif
                                @if($invoice->valor_consolidacao)
                                    <h1>Consolidação: US$ {{ $invoice->valor_consolidacao }}</h1>
                                @endif

                                <h1>Total: US$ {{ $invoice->total }}</h1>

                                <h1>Saldo a pagar: US$ {{ $invoice->saldo }}</h1>

                                <button class="btn btn-primary print-no-show" data-toggle="modal" data-target="#paymentModal">
                                    <i class="fa fa-money"></i> Abrir formulario de pago
                                </button>

                                <button class="btn btn-primary print">
                                    <i class="fa fa-print"></i> Imprimir
                                </button>
                            </div>
                            <span class="signature">
                                {{ $invoice->usuario->name }}
                            </span>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div id="paymentModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            {!! Form::open(['route' => ['admin.invoices.pay-amount', $invoice->id], 'method' => 'post']) !!}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pagar invoice</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('id_caja', 'Caja') !!}
                                {!! Form::select('id_caja', $cajas, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('monto', 'Monto en dólares') !!}
                                <div class="input-group">
                                    <span class="input-group-addon">US$</span>
                                    {!! Form::number('monto', $invoice->saldo, ['class' => 'form-control', 'placeholder' => 'Ingrese monto a pagar', 'required', 'step' => '0.01', 'min' => '1', 'max' => $invoice->saldo]) !!}
                                </div>
                                <p class="help-block">Saldo pendiente de {{ format_dolares($invoice->saldo) }}</p>
                            </div>
                        </div>

                        <div class="row" id="monto-guaranies-container">
                            <div class="col-md-12">
                                <strong>Detectamos que fue seleccionada una caja en guaraníes. Por favor, ingrese abajo el monto en guaraníes. <span class="text-red">EL MONTO EN GUARANIES SERA SUMADO AL TOTAL DE CAJA</span></strong>
                            </div>
                            <div class="col-md-12">
                                {!! Form::hidden('cotizacion', $cotizacion, ['id' => 'cotizacion']) !!}
                                {!! Form::label('monto_guaranies', 'Monto en Guaraníes') !!}
                                <div class="input-group">
                                    <span class="input-group-addon">Gs.</span>
                                    {!! Form::number('monto_guaranies', null, ['class' => 'form-control', 'placeholder' => 'Ingrese monto en guaraníes', 'step' => '1', 'min' => '0']) !!}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Confirmar pago</button>
                    </div>
                </div><!-- /.modal-content -->
            {!! Form::close() !!}
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        #monto-guaranies-container {
            display: none;
        }

        .text-red {
            color: red;
        }

        #invoice {
            margin: 20px;
            font-family: sans-serif;
        }

        #invoice .invoice-logo {
            text-align: center;
            background-color: #211973;
        }

        #invoice .invoice-header {
            padding: 20px;
            text-align: right;
        }

        #invoice .invoice-table {
            width: 95%;
            margin: 0 auto;
            border-collapse: collapse
        }

        #invoice .invoice-table thead {
            border-bottom: 2px solid #aaa;
        }

        #invoice .invoice-table thead th {
            text-align: center;
        }

        #invoice .invoice-table tbody tr {
            border-top: solid 1px #ddd; text-align: center;
        }
        
        #invoice .invoice-table tbody td {
            padding: 12px 0;
        }

        #invoice .invoice-footer {
            padding-bottom: 20px;
            text-align: right;
        }

        .signature {
            display: none;
            border-top: 1px solid #000000;
            padding: 15px 80px;
            text-align: left;
        }

        @media print {
            .nav_menu, footer, .print, .print-no-show {
                display: none;
            }

            .signature {
                display: inline;
            }
        }
    </style>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        $('.print').click(function() {
            print();
        });
    </script>
    @if($print)
        <script>
            print();
        </script>
    @endif
    <script>
        function pagar() {
            // $invoice->items->pago == true;
        }

        function checkGuaraniesSelected() {
            $('#monto-guaranies-container').css({ display: $('#id_caja option:selected').html().indexOf('(Guaranies)') > -1 ? 'block' : 'none' })
        }

        function calculateValueGuaranies() {
            $('#monto_guaranies').val(Number($('#monto').val()) * Number($('#cotizacion').val()))
        }

        $('#monto').on('change', calculateValueGuaranies)
            .on('keyup', calculateValueGuaranies)

        $('#id_caja').on('change', checkGuaraniesSelected)

        checkGuaraniesSelected()
        calculateValueGuaranies()
    </script> 
@endsection