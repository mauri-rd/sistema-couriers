<div class="row">
    <div class="col-md-12">
        <label>Usuario</label>
        {!! Form::select('id_usuario', $usuarios, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="button" class="btn btn-primary" id="add-item-btn" onclick="addItem()">
            <i class="fa fa-plus"></i> Agregar item
        </button>
    </div>
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th>Código</th>
            <th>Tracking</th>
            <th>Descripción</th>
            <th>Precio/KG</th>
            <th>Gramos</th>
            <th>Volumen</th>
            <th>Eliminar</th>
        </tr>
    </thead>
    <tbody id="invoice-items"></tbody>
</table>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>