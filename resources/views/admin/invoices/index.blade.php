@extends('admin.master')
@section('titulo-pagina', 'Invoices')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    {!! Form::open(['route' => 'admin.invoices.index', 'method' => 'GET']) !!}
                        <div class="form-group">
                            <label for="usuario">Filtrar por usuario</label>
                            {!! Form::select('usuario', $usuarios, $usuario, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                        </div>
                        <div class="form-group">
                            <label for="usuario">Filtrar por tracking</label>
                            {!! Form::select('tracking', $trackings, $tracking, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                        </div>
                        <div class="form-group">
                            <label>
                                {!! Form::checkbox('hidden', 'hidden', $hidden) !!} Mostrar invoices ocultos
                            </label>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Filtrar</button>
                            <a href="{{ route('admin.invoices.create') }}" class="btn btn-primary">Registrar invoice</a>
                        </div>
                    {!! Form::close() !!}
                </div>
                {!! Form::open(['route' => 'admin.invoices.join', 'method' => 'post']) !!}
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Seleccionar</th>
                            <th>#</th>
                            <th>Nombre del usuario</th>
                            <th>Tracking numbers</th>
                            <th>Total</th>
                            <th>Fecha de creación</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>
                                    {!! Form::checkbox('invoice[]', $invoice->id, false) !!}
                                </td>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ $invoice->usuario->full_name }}</td>
                                <td>
                                    <ul class="tracking-list">
                                        @foreach($invoice->items as $item)
                                            <li{!! $item->pago ? ' class="text-success"' : '' !!}>
                                                {!! $item->pago ? '<i class="fa fa-check"></i>' : '' !!}{{ $item->paquete->tracking }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>US$ {{ $invoice->total }}</td>
                                <td>{{ $invoice->created_at->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('admin.invoices.show', [$invoice->id]) }}" class="btn btn-info">
                                        <i class="fa fa-eye"></i> Ver Invoice
                                    </a>
                                    <a href="{{ route('admin.invoices.show', [$invoice->id, 'print' => 'yes']) }}" class="btn btn-info">
                                        <i class="fa fa-print"></i> Imprimir Invoice
                                    </a>
                                    <a href="{{ route('admin.invoices.email', [$invoice->id]) }}" class="btn btn-info">
                                        <i class="fa fa-envelope"></i> Enviar Invoice por E-mail
                                    </a>
                                    @if(!$invoice->hidden)
                                        <a onclick="return confirm('Seguro que desea ocultar este invoice?')" href="{{ route('admin.invoices.hide', [$invoice->id]) }}" class="btn btn-info">
                                            <i class="fa fa-eye-slash"></i> Ocultar Invoice
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    <button class="btn btn-primary">
                        Generar Invoice unificado
                    </button>
                    {!! $invoices->appends(request()->input())->render() !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        .tracking-list {
            list-style-type: none;
        }
    </style>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
@endsection