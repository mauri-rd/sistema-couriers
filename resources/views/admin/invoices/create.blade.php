@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo invoice')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.invoices.store', 'method' => 'post']) !!}
                    @include('admin.invoices.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        const invoiceItems = $('#invoice-items');
        function addItem() {
            const id = "item_" + (new Date()).getTime();
            invoiceItems.append(`
            <tr id="${id}">
                <td>
                    <input name="codigo[]" placeholder="Código" type="text" class="form-control" required>
                </td>
                <td>
                    <input name="tracking[]" placeholder="Tracking number" type="text" class="form-control" required>
                </td>
                <td>
                    <input name="descripcion[]" placeholder="Descripción del paquete" type="text" class="form-control" required>
                </td>
                <td>
                    <input name="precio[]" placeholder="Precio por KG" type="number" min="1" class="form-control" required>
                </td>
                <td>
                    <input name="gramos[]" placeholder="Pero en gramos" type="number" min="1" class="form-control" required>
                </td>
                <td>
                    <input name="volumen[]" placeholder="Cantidad de volumenes" type="number" min="0" class="form-control">
                </td>
                <td>
                    <button class="btn btn-danger" onclick="deleteItem('${id}')">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>

            </tr>
            `);
        }

        function deleteItem(id) {
            if (confirm('¿Seguro que desea eliminar este item?')) {
                $('#' + id).remove();
            }
        }
    </script>
@stop