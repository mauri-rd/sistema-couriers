@extends('admin.master')
@section('titulo-pagina', 'Arqueos de caja ' . $caja->nombre)
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Fecha de Apertura</th>
                            <th>Fecha de Cierre</th>
                            <th>Monto de Apertura</th>
                            <th>Monto de Cierre</th>
                            <th>Total Movimientos</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arqueos as $arqueo)
                            <tr>
                                <td>{{ $arqueo->fecha_inicio_date->format('d/m/Y') }}</td>
                                <td>
                                    @if(is_null($arqueo->fecha_fin))
                                        <i>Caja abierta actualmente</i>
                                    @else
                                        {{ $arqueo->fecha_fin_date->format('d/m/Y') }}
                                    @endif
                                </td>
                                <td>{{ $arqueo->monto_inicio_formateado }}</td>
                                <td>
                                    @if(is_null($arqueo->fecha_fin))
                                        <i>Caja abierta actualmente</i>
                                    @else
                                        {{ $arqueo->monto_fin_formateado }}
                                    @endif
                                </td>
                                <td>{{ $arqueo->total_movimientos_formateado }}</td>
                                <td>
                                    <a href="{{ route('admin.cajas.movimientos.index', [$caja->id, 'arqueo' => $arqueo->id]) }}" class="btn btn-success">
                                        <i class="fa fa-money"></i> Movimientos
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{--<div class="footer">--}}
                    {{--{!! $cajas->render() !!}--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@stop