@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva caja')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.cajas.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.cajas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop