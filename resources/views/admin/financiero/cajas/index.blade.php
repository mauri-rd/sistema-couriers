@extends('admin.master')
@section('titulo-pagina', 'Cajas')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                @if(auth()->user()->type != \App\Enum\UserType::OPERADOR)
                    <div class="header">
                        <a href="{{ route('admin.cajas.create') }}" class="btn btn-fill btn-success">Registrar</a>
                    </div>
                @endif
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Total</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cajas as $caja)
                            <tr>
                                <td>{{ $caja->nombre }}</td>
                                <td>{{ $caja->moneda == \App\Enum\MonedaCaja::GUARANIES ? format_guaranies($caja->total) : format_dolares($caja->total) }}</td>
                                <td>
                                    <a href="{{ route('admin.cajas.movimientos.index', [$caja->id]) }}" class="btn btn-success">
                                        <i class="fa fa-money"></i> Movimientos
                                    </a>
                                    @if(auth()->user()->type != \App\Enum\UserType::OPERADOR)
                                        <a href="{{ route('admin.cajas.arqueos', [$caja->id]) }}" class="btn btn-warning">
                                            <i class="fa fa-calculator"></i> Arqueos de Caja
                                        </a>
                                    @endif
                                    @if(auth()->user()->type != \App\Enum\UserType::OPERADOR)
                                        <a href="{{ route('admin.cajas.edit', [$caja->id]) }}" class="btn btn-info">Editar</a>
                                        <a href="{{ route('admin.cajas.delete', [$caja->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $cajas->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop