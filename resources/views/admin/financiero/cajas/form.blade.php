<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de caja', 'required', 'autofocus']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('moneda', 'Moneda') !!}
        {!! Form::select('moneda', \App\Enum\MonedaCaja::nombres(), null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>