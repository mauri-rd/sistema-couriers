@extends('admin.master')
@section('titulo-pagina', 'Editar caja')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($caja, ['route' => ['admin.cajas.update', $caja->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.cajas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop