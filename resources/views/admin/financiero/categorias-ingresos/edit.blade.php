@extends('admin.master')
@section('titulo-pagina', 'Editar categoría de ingresos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($categoria, ['route' => ['admin.categorias-ingresos.update', $categoria->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.categorias-ingresos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop