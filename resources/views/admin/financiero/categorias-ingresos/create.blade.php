@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva categoría de ingresos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.categorias-ingresos.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.categorias-ingresos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop