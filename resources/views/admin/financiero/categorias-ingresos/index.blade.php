@extends('admin.master')
@section('titulo-pagina', 'Categorías de Ingresos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.categorias-ingresos.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Icono</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categorias as $categoria)
                            <tr>
                                <td>{{ $categoria->nombre }}</td>
                                <td><i class="fa {{ $categoria->icon }}"></i></td>
                                <td>
                                    <a href="{{ route('admin.categorias-ingresos.edit', [$categoria->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.categorias-ingresos.delete', [$categoria->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $categorias->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop