@extends('admin.master')
@section('titulo-pagina', 'Proveedores')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.proveedores.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($proveedores as $proveedor)
                            <tr>
                                <td>{{ $proveedor->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.proveedores.edit', [$proveedor->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.proveedores.delete', [$proveedor->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $proveedores->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @if(session()->get('closeWhenFinished'))
        <script>
            window.close()
        </script>
    @endif
@stop