@extends('admin.master')
@section('titulo-pagina', 'Editar proveedor')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($proveedor, ['route' => ['admin.proveedores.update', $proveedor->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.proveedores.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop