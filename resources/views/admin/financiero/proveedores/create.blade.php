@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo proveedor')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.proveedores.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.proveedores.form')
                    @if($closeWhenFinished && !is_null($closeWhenFinished))
                        {!! Form::hidden('closeWhenFinished', 1) !!}
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop