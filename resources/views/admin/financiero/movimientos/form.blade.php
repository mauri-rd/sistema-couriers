<div class="row">
    <div class="col-md-12">
        {!! Form::label('tipo_movimiento', 'Tipo de movimiento') !!}
        {!! Form::select('tipo_movimiento', $tipos, null, ['class' => 'form-control', 'required', 'autofocus']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label>Título</label>
        {!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Título del movimiento', 'required']) !!}
    </div>
</div>
<div class="row ingreso">
    <div class="col-md-12">
        <label for="id_categoria_ingreso">Categoría</label>
        <select name="id_categoria_ingreso" id="id_categoria_ingreso" class="form-control">
            @foreach($categoriasIngresos as $index => $categoria)
                <option value="{{ $categoria->id }}" data-icon="{{ $categoria->icon }}"{!! (isset($movimiento) && $movimiento->id_categoria_ingreso == $categoria->id) || $index == 0 ? ' selected': '' !!}>{{ $categoria->nombre }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row egreso">
    <div class="col-md-12">
        <label for="id_categoria_egreso">Categoría</label>
        <select name="id_categoria_egreso" id="id_categoria_egreso" class="form-control">
            @foreach($categoriasEgresos as $index => $categoria)
                <option value="{{ $categoria->id }}" data-icon="{{ $categoria->icon }}"{!! isset($movimiento) ? ($movimiento->id_categoria_egreso == $categoria->id ? ' selected' : '') : ($index == 0 ? ' selected' : '') !!}>{{ $categoria->nombre }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row ingreso">
    <div class="col-md-12">
        {!! Form::label('id_cliente', 'Cliente') !!}
        {!! Form::select('id_cliente', $clientes, null, ['class' => 'form-control selectpicker', 'data-live-search' => "true"]) !!}
    </div>
    <div class="col-md-12 text-right">
        <a href="{{ route('admin.usuarios.create', ['closeWhenFinished' => 1, 'registerCliente' => 1]) }}" class="btn btn-success" target="_blank">
            <i class="fa fa-plus"></i> Agregar nuevo
        </a>
        <button type="button" class="btn btn-info" id="update-clientes-btn">
            <i class="fa fa-refresh"></i> Actualizar lista
        </button>
    </div>
</div>

<div class="row egreso">
    <div class="col-md-12">
        {!! Form::label('id_proveedor', 'Proveedor') !!}
        {!! Form::select('id_proveedor', $proveedores, null, ['class' => 'form-control selectpicker', 'data-live-search' => "true"]) !!}
    </div>
    <div class="col-md-12 text-right">
        <a href="{{ route('admin.proveedores.create', ['closeWhenFinished' => 1]) }}" class="btn btn-success" target="_blank">
            <i class="fa fa-plus"></i> Agregar nuevo
        </a>
        <button type="button" class="btn btn-info" id="update-proveedores-btn">
            <i class="fa fa-refresh"></i> Actualizar lista
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Monto</label>
        <div class="input-group">
            <span class="input-group-addon">{{ $caja->simbolo_moneda }}</span>
            {!! Form::number('monto', null, ['class' => 'form-control', 'placeholder' => 'Ingrese monto del movimiento', 'required', 'step' => '0.01', 'min' => '1', 'autocomplete' => 'monto-new']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label>Fecha</label>
        {!! Form::date('fecha', null, ['class' => 'form-control', 'placeholder' => 'Ingrese fecha del movimiento', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Observación</label>
        {!! Form::textarea('observacion', null, ['class' => 'form-control', 'placeholder' => 'Ingrese una observación del movimiento (opcional)']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>