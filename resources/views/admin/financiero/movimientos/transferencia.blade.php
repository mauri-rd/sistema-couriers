@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo transferencia')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => ['admin.cajas.movimientos.transferencia', $from], 'method' => 'post', 'onsubmit' => 'return confirm("¿Está seguro de realizar esta transferencia? Esta acción no puede deshacerse")']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('from', 'Transferir de') !!}
                                {!! Form::select('from', $cajas, $from, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'autofocus']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('to', 'Transferir a') !!}
                                {!! Form::select('to', $cajas, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('monto', 'Monto') !!}
                                <div class="input-group">
                                    <span class="input-group-addon" id="from-symbol"></span>
                                    {!! Form::number('monto', null, ['class' => 'form-control', 'placeholder' => 'Ingrese monto del movimiento', 'required', 'step' => '0.01', 'min' => '1']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row" id="different-currencies-container">
                            <div class="col-md-12">
                                <p><strong>Hemos detectado que las cajas de origen y destino no son de la misma moneda. Por favor, ingrese abajo el monto a ser sumado en la moneda de destino.</strong></p>
                            </div>
                            <div class="col-md-12">
                                {!! Form::hidden('cotizacion', $cotizacion, ['id' => 'cotizacion']) !!}
                                {!! Form::label('monto_extranjero', 'Monto en moneda destino') !!}
                                <div class="input-group">
                                    <span class="input-group-addon" id="to-symbol"></span>
                                    {!! Form::number('monto_extranjero', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el monto convertido a la moneda la caja de destino', 'step' => '0.01', 'min' => '1']) !!}
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info btn-fill pull-right">Realizar transferencia</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        function updateFromSymbol() {
            $('#from-symbol').html($('#from option:selected').html().indexOf('(Guaranies)') > -1 ? 'Gs.' : 'US$')
            checkDifferentCurrencies()
        }

        function updateToSymbol() {
            $('#to-symbol').html($('#to option:selected').html().indexOf('(Guaranies)') > -1 ? 'Gs.' : 'US$')
            checkDifferentCurrencies()
        }

        function checkDifferentCurrencies() {
            const fromGuaranies = $('#from option:selected').html().indexOf('(Guaranies)') > -1
            const toGuaranies = $('#to option:selected').html().indexOf('(Guaranies)') > -1

            const differentCurrenciesContainer = $('#different-currencies-container')

            differentCurrenciesContainer.css({ display: ( fromGuaranies && toGuaranies ) || ( !fromGuaranies && !toGuaranies ) ? 'none' : 'block' })
        }

        function calculateExchangedAmount() {
            const fromGuaranies = $('#from option:selected').html().indexOf('(Guaranies)') > -1
            const toGuaranies = $('#to option:selected').html().indexOf('(Guaranies)') > -1

            if (( fromGuaranies && toGuaranies ) || ( !fromGuaranies && !toGuaranies )) {
                return false
            }

            const monto = $('#monto')
            const montoExtranjero = $('#monto_extranjero')
            const cotizacion = $('#cotizacion').val()

            montoExtranjero.val(
                fromGuaranies ? ( monto.val() / cotizacion ).toFixed(2) : ( monto.val() * cotizacion )
            )
        }


        $('#from').on('change', updateFromSymbol)
        $('#to').on('change', updateToSymbol)

        $('#monto').on('change', calculateExchangedAmount)
            .on('keyup', calculateExchangedAmount)

        updateFromSymbol()
        updateToSymbol()
    </script>
@endsection