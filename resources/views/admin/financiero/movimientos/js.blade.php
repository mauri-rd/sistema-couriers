<script>
    const tipoMovimiento = $('#tipo_movimiento')

    $('#id_categoria_egreso').selectpicker({
        iconBase: 'fa'
    })
    $('#id_categoria_ingreso').selectpicker({
        iconBase: 'fa'
    })

    $('#update-proveedores-btn').on('click', function () {
        $.ajax({
            url: '{{ route('admin.cajas.movimientos.proveedores-form', [$caja->id]) }}',
            method: 'get',
            success: function (res) {
                const proveedorSelect = $('#id_proveedor')
                proveedorSelect.empty()
                let lastValue
                for (const item of Object.keys(res)) {
                    proveedorSelect.append(`<option value="${item}">${res[item]}</option>`)
                    lastValue = item
                }
                proveedorSelect.selectpicker('val', lastValue)
                proveedorSelect.selectpicker('refresh')
            }
        })
    })

    $('#update-clientes-btn').on('click', function () {
        $.ajax({
            url: '{{ route('admin.cajas.movimientos.clientes-form', [$caja->id]) }}',
            method: 'get',
            success: function (res) {
                const clienteSelect = $('#id_cliente')
                clienteSelect.empty()
                let lastValue
                for (const item of Object.keys(res)) {
                    clienteSelect.append(`<option value="${item}">${res[item]}</option>`)
                    lastValue = item
                }
                clienteSelect.selectpicker('val', lastValue)
                clienteSelect.selectpicker('refresh')
            }
        })
    })

    function toggleIngresoEgresoVisibility() {
        if (tipoMovimiento.val().toString() === '{{ \App\Enum\TipoMovimientoFinanciero::INGRESO }}') {
            $('.ingreso').css({ display: 'block' })
            $('.egreso').css({ display: 'none' })
        } else {
            $('.ingreso').css({ display: 'none' })
            $('.egreso').css({ display: 'block' })
        }
    }

    tipoMovimiento.on('change', toggleIngresoEgresoVisibility)

    toggleIngresoEgresoVisibility()

</script>