@extends('admin.master')
@section('titulo-pagina', 'Abrir caja')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => ['admin.cajas.movimientos.abrir-arqueo', $caja->id], 'method' => 'post']) !!}

                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('monto', 'Monto de apertura de caja') !!}
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $caja->simbolo_moneda }}</span>
                                    {!! Form::number('monto', is_null($lastArqueo) ? 0 : $lastArqueo->monto_fin, array_merge(['class' => 'form-control', 'placeholder' => 'Ingrese monto del apertura', 'required', 'autofocus'], ( !is_null($lastArqueo) ? ['readonly'] : [] ))) !!}
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info btn-fill pull-right">Abrir Caja</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop