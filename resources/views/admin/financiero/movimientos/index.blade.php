@extends('admin.master')
@section('titulo-pagina')
    Movimientos de caja {{ $caja->nombre }} <br>
    @if(is_null($lastArqueo))
        <i>La caja no fue abierta aún</i>
    @else
        <i>Fecha de apertura de caja: {{ $lastArqueo->fecha_inicio_date->format('d/m/Y') }}</i> <br>
        <i>Monto de apertura de caja: {{ $lastArqueo->monto_inicio_formateado }}</i> <br>
        <i>Total en caja: {{ $lastArqueo->total_formateado }}</i> <br>
    @endif
@stop
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    {!! Form::open(['route' => ['admin.cajas.movimientos.index', $caja->id], 'method' => 'GET', 'class' => 'form-inline']) !!}
                    <div class="form-group">
                        <label for="q">Buscar</label>
                        {!! Form::text('q', $q, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('from', 'Desde fecha') !!}
                        {!! Form::date('from', $from, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('to', 'Desde hasta') !!}
                        {!! Form::date('to', $to, ['class' => 'form-control']) !!}
                    </div>
                    @if(!$lastArqueo->abierto)
                        {!! Form::hidden('arqueo', $lastArqueo->id) !!}
                    @endif

                    <button type="submit" class="btn btn-primary">Buscar</button>
                    @if(!is_null($q))
                        <a href="{{ route('admin.cajas.movimientos.index', array_merge([$caja->id], !$lastArqueo->abierto ? ['arqueo' => $lastArqueo->id] : [])) }}" class="btn btn-warning">Limpiar busqueda</a>
                    @endif
                    {!! Form::close() !!}
                </div>
                @if($lastArqueo->abierto)
                    <div class="header">
                        <a href="{{ route('admin.cajas.movimientos.create', [$caja->id]) }}" class="btn btn-fill btn-success">
                            <i class="fa fa-plus"></i>
                            Registrar ingreso/egreso
                        </a>
                        @if(!is_null($lastArqueo))
                            <a href="{{ route('admin.cajas.movimientos.cerrar-arqueo', [$caja->id]) }}" class="btn btn-danger" onclick="return confirm('¿Está seguro de cerrar la caja? Esta acción no puede deshacerse')">
                                <i class="fa fa-close"></i>
                                Cerrar Caja
                            </a>
                        @endif
                        @if(auth()->user()->type !== \App\Enum\UserType::OPERADOR)
                            <a href="{{ route('admin.cajas.movimientos.transferencia-form', [$caja->id]) }}" class="btn btn-fill btn-info">
                                <i class="fa fa-exchange"></i>
                                Registrar transferencia
                            </a>
                        @endif
                    </div>
                @else
                    <a href="{{ route('admin.cajas.movimientos.abrir-arqueo', [$caja->id]) }}" class="btn btn-fill btn-success">
                        <i class="fa fa-plus"></i>
                        Abrir caja
                    </a>
                @endif
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Titulo</th>
                            <th>Monto</th>
                            <th>Fecha</th>
                            @if(auth()->user()->type !== \App\Enum\UserType::OPERADOR)
                                <th>Acciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($movimientos as $movimiento)
                            <tr class="{{ $movimiento->background_class }}">
                                <td class="text-center">
                                    <i class="fa {{ $movimiento->icon }} fa-lg"></i>
                                </td>
                                <td>{{ $movimiento->titulo }}</td>
                                <td>{{ $movimiento->monto_formateado }}{!! $movimiento->monto_extranjero ? ' <i>(' . $movimiento->monto_extranjero_formateado . ')</i>' : '' !!}</td>
                                <td>{{ $movimiento->fecha_date->format('d/m/Y') }}</td>
                                @if(auth()->user()->type !== \App\Enum\UserType::OPERADOR)
                                    @if($movimiento->es_ingreso || $movimiento->es_egreso)
                                        <td>
                                            <a href="{{ route('admin.cajas.movimientos.edit', [$caja->id, $movimiento->id]) }}" class="btn btn-info">Editar</a>
                                            <a href="{{ route('admin.cajas.movimientos.delete', [$caja->id, $movimiento->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                        </td>
                                    @else
                                        <td>
                                            <i>Sin acciones disponibles</i>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $movimientos->appends(request()->input())->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop