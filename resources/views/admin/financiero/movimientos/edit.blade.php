@extends('admin.master')
@section('titulo-pagina', 'Editar movimiento de caja ' . $caja->nombre)
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($movimiento, ['route' => ['admin.cajas.movimientos.update', $caja->id, $movimiento->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.movimientos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    @include('admin.financiero.movimientos.js')
@endsection