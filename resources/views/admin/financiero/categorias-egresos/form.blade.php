<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de categoría', 'required', 'autofocus']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Ícono de la categoría</label>
    </div>
    @foreach($icons->icons as $i => $icon)
        <div class="col-md-1 categoria-icon-container text-center">
            <label for="icon_{{ $icon }}">
                <i class="fa {{ $icon }} fa-lg"></i>
            </label>
            <br>
            {!! Form::radio('icon', $icon, (isset($categoria) && $categoria->icon === $icon) || $i == 0, ['id' => 'icon_' . $icon]) !!}
        </div>
        @if(($i + 1) % 12 == 0)
            <div class="clearfix"></div>
        @endif
    @endforeach
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>