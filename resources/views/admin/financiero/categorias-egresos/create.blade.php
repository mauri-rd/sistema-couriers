@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva categoría de egresos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.categorias-egresos.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.financiero.categorias-egresos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop