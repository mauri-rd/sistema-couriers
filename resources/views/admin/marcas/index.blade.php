@extends('admin.master')
@section('titulo-pagina', 'Marcas')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.marcas.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($marcas as $marca)
                            <tr>
                                <td>{{ $marca->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.marcas.edit', [$marca->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.marcas.delete', [$marca->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $marcas->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop