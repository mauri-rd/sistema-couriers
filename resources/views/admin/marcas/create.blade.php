@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva marca')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.marcas.store', 'method' => 'post']) !!}
                    @include('admin.marcas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop