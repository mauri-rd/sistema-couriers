@extends('admin.master')
@section('titulo-pagina', 'Editar Marca de productos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($marca, ['route' => ['admin.marcas.update', $marca->id], 'method' => 'post']) !!}
                    @include('admin.marcas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop