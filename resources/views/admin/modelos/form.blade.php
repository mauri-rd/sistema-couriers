<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del modelo', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Marca</label>
        {!! Form::select('id_marca', $marcas, null, ['class' => 'form-control']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>