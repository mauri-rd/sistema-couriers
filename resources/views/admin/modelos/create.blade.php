@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo modelo')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.modelos.store', 'method' => 'post']) !!}
                    @include('admin.modelos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop