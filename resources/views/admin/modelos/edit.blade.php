@extends('admin.master')
@section('titulo-pagina', 'Editar Modelo de productos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($modelo, ['route' => ['admin.modelos.update', $modelo->id], 'method' => 'post']) !!}
                    @include('admin.modelos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop