@extends('admin.master')
@section('titulo-pagina', 'Modelos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.modelos.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($modelos as $modelo)
                            <tr>
                                <td>{{ $modelo->nombre }}</td>
                                <td>{{ $modelo->marca->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.modelos.edit', [$modelo->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.modelos.delete', [$modelo->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $modelos->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop