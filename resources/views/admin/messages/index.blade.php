@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo producto')
@section('contenido')
    <div class="row">
        <div class="col-md-6">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.messages.submit', 'method' => 'post', 'files' => true]) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <label>Destinatarios</label>
                                {!! Form::select('emails[]', $usuarios, null, ['class' => 'form-control selectpicker', 'id' => 'emails', 'required', 'autofocus', 'data-live-search' => 'true', 'multiple']) !!}

                                <div>
                                    <button type="button" class="btn btn-primary" onclick="selectAll()">Seleccionar todos</button>
                                    <button type="button" class="btn btn-primary" onclick="deselectAll()">Deseleccionar todos</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label>Asunto</label>
                                {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Ingrese asunto del email', 'required']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label>Mensaje</label>
                                {!! Form::textarea('text', null, ['class' => 'form-control', 'placeholder' => 'Ingrese cuerpo del email', 'required', 'id' => 'email-text']) !!}
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <label>Adjunto</label>
                                {!! Form::file('emailfile', ['class' => 'form-control', 'accept' => 'image/*', 'id' => 'email-file']) !!}
                                <p class="help-block">El tamaño de la imagen no debe sobrepasar de {{ $maxSize }} MB.</p>
                                <img id="email-image-preview">
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-info btn-fill pull-right">
                            <i class="fa fa-paper-plane"></i>
                            Enviar
                        </button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="x_panel">
                <div class="header">
                    Vista previa do email
                </div>
                <div class="content" id="preview">
                    <div class="row">
                        <div class="cont">
                            <div class="header">
                                <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox">
                            </div>
                            <div class="text-cont">
                                <img id="preview-image" class="attachment">
                                <p id="preview-text"></p>
                                <p class="date">
                                    <i>Enviado {{ $date->format('d/m/Y H:i') }}</i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        #email-image-preview {
            height: 200px;
            display: none;
        }

        #preview .cont {
            border: 1px solid #ddd;
            border-radius: 10px;
            margin: 20px;
            font-family: sans-serif;
        }

        #preview .header {
            text-align: center;
            background-color: #211973;
        }

        #preview .header img {
            height: 80px;
            margin: 10px 0;
        }

        #preview .text-cont {
            padding: 20px
        }

        #preview .attachment {
            display: none;
            width: 90%;
            margin-left: 10%;
            margin-right: 10%;
        }

        #preview .date {
            margin-top: 20px;
        }
    </style>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>

        $('#email-text').on('keyup', updatePreviewText);
        $('#email-file').on('change', emailFileChange);

        function updatePreviewText() {
            $('#preview-text').html(
                $('#email-text').val().replace(/\n/g, '<br>')
            );
        }

        function emailFileChange() {
            updateFilePreview($('#email-file')[0].files[0]);
        }

        const updateFilePreview = function() {
            const fr = new FileReader();
            fr.onload = function() {
                $('#email-image-preview').attr('src', fr.result);
                $('#preview-image').attr('src', fr.result);
                $('#email-image-preview').css({'display': 'block'});
                $('#preview-image').css({'display': 'block'});
            }
            return (file) => {
                fr.readAsDataURL(file);
            }
        }();

        function selectAll() {
            $('#emails').selectpicker('selectAll');
        }

        function deselectAll() {
            $('#emails').selectpicker('deselectAll');
        }

    </script>
@stop