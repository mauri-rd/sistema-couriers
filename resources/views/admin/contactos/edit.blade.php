@extends('admin.master')
@section('titulo-pagina', 'Editar Contacto')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($contacto, ['route' => ['admin.contactos.update', $contacto->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.contactos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop