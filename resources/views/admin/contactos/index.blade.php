@extends('admin.master')
@section('titulo-pagina', 'Contactos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.contactos.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contactos as $contacto)
                            <tr>
                                <td>{{ $contacto->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.contactos.edit', [$contacto->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.contactos.delete', [$contacto->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $contactos->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop