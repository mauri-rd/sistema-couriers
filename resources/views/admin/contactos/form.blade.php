<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del contacto', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Departamento</label>
        {!! Form::text('departamento', null, ['class' => 'form-control', 'placeholder' => 'Departamento al que pertenece', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>E-mail</label>
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Dirección de e-mail del contacto', 'required']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>