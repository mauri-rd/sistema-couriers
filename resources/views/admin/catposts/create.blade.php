@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva categoría de posts')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.catposts.store', 'method' => 'post']) !!}
                    @include('admin.catposts.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop