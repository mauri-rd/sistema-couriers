@extends('admin.master')
@section('titulo-pagina', 'Editar Sucursal')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($servicio, ['route' => ['admin.servicios.update', $servicio->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.servicios.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <div class="row hidden" id="image-example">
        <div class="col-md-12">
            {!! Form::file('imagenes[]', ['accept' => 'image/*']) !!}
        </div>
    </div>
    <script>
        function addImage() {
            var html = $('#image-example').html();
            $('#images-container').append('<div class="row">' + html + '</div>')
        }
    </script>
@stop