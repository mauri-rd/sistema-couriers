<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la sucursal', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Dirección</label>
        {!! Form::text('direccion', null, ['class' => 'form-control', 'placeholder' => 'Dirección de la sucursal', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Telefono</label>
        {!! Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Telefono de la sucursal', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Link de Google Maps</label>
        {!! Form::text('gmap', null, ['class' => 'form-control', 'placeholder' => 'Dirección en Google Maps de la sucursal', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imagenes <button type="button" onclick="addImage()" class="btn btn-circle btn-info btn-fill">+</button></label>
    </div>
    <div class="col-md-6" id="images-container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::file('imagenes[]', ['accept' => 'image/*']) !!}
            </div>
        </div>
    </div>
    @if(isset($servicio) && count($servicio->imagenes) > 0)
        <div class="col-md-6">
            <strong>Seleccione solamente las imagenes que desea eliminar</strong>
            @foreach($servicio->imagenes as $imagen)
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <img src="{{ $imagen->url }}" style="height: 100px;">
                            {!! Form::checkbox('eliminar[]', $imagen->id) !!}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>