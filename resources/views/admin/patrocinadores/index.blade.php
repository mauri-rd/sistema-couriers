@extends('admin.master')
@section('titulo-pagina', 'Banners')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.patrocinadores.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Imágen</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($patrocinadores as $patrocinador)
                            <tr>
                                <td><img src="{{ $patrocinador->url }}" style="height: 100px;"></td>
                                <td>
                                    <a href="{{ route('admin.patrocinadores.edit', [$patrocinador->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.patrocinadores.delete', [$patrocinador->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $patrocinadores->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop