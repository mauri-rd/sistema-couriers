<div class="row">
    <div class="col-md-12">
        <label>Imagen</label>
        {!! Form::file('url', ['class' => 'form-control', 'accept' => 'image/*']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>