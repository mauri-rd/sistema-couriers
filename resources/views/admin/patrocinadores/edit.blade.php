@extends('admin.master')
@section('titulo-pagina', 'Editar Banner')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($patrocinador, ['route' => ['admin.patrocinadores.update', $patrocinador->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.patrocinadores.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop