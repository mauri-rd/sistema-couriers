@extends('admin.master')
@section('titulo-pagina', 'Reporte generado')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <h2>
                        Reporte de paquetes {{ $estado }} entre {{ $from->format('d/m/Y') }} y {{ $to->format('d/m/Y') }}
                    </h2>
                    <a href="{{ route('admin.reportes.index') }}" class="btn btn-primary">Generar otro reporte</a>
                </div>
                <div class="content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Cantidad de paquetes</th>
                            <th>Peso total en paquetes</th>
                            <th>Monto total en paquetes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $cant }}</td>
                            <td>{{ number_format($peso, 2, ',', '.') }} en gramos ({{ $peso / 1000 }} kg.)</td>
                            <td>US$ {{ number_format($total, 2, ',', '.') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <h3>Detalle del Reporte</h3>
                </div>
                <div class="content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Casilla del Usuario</th>
                                <th>Nombre</th>
                                <th>Tracking Number</th>
                                <th>Descripción</th>
                                <th>Tipo de Flete</th>
                                <th>Peso</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($paquetes as $paquete)
                                <tr>
                                    <td>{{ $paquete->usuario->prefix . $paquete->usuario->casilla }}</td>
                                    <td>{{ $paquete->usuario->full_name }}</td>
                                    <td>{{ $paquete->tracking }}</td>
                                    <td>{!! nl2br($paquete->descripcion) !!}</td>
                                    <td>{{ intval($paquete->tipo_flete) === \App\Enum\TipoFlete::NORMAL ? 'NORMAL' : 'EXPRESS' }}</td>
                                    <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                    <td>US$ {{ $paquete->valor_total }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="float: right; margin:10px auto; display:none;">
                <button type="button" class="btn btn-danger">Exportar reporte como : </button>
                <button type="button" class="btn btn-warning">Imprimir reporte</button>
            </div>
        </div>
    </div>
@stop