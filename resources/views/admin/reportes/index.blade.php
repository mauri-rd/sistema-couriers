@extends('admin.master')
@section('titulo-pagina', 'Reportes')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.reportes.generar', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {!! Form::label('from', 'Fecha de inicio') !!}
                        {!! Form::date('from', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('to', 'Fecha limite') !!}
                        {!! Form::date('to', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('show[]', 'Estado') !!}
                        {!! Form::select('show[]', $estados, null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Generar Reporte</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop