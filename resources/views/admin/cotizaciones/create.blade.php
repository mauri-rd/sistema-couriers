@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva cotizacion')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.cotizaciones.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.cotizaciones.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop