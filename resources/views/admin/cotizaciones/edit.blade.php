@extends('admin.master')
@section('titulo-pagina', 'Editar cotizacion')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($cotizacion, ['route' => ['admin.cotizaciones.update', $cotizacion->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.cotizaciones.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop