<div class="row">
    <div class="col-md-12">
        <label>Valor/Cotización</label>
        <div class="input-group">
            <span class="input-group-addon">Gs.</span>
            {!! Form::number('valor', null, ['class' => 'form-control', 'placeholder' => 'Ingrese valor del dolar en guaraníes', 'required', 'step' => '1', 'min' => '1', 'autofocus']) !!}
        </div>
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>