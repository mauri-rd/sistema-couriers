@extends('admin.master')
@section('titulo-pagina', 'Cotizaciones Dolar/Gs')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.cotizaciones.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Valor/Cotización</th>
                            <th>Fecha de registro</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cotizaciones as $index => $cotizacion)
                            <tr>
                                <td>{{ format_guaranies($cotizacion->valor) }}{!! $index == 0 ? ' <i>Actual</i>' : '' !!}</td>
                                <td>{{ $cotizacion->created_at_date->format('d/m/Y') }}</td>
                                <td>
                                    <a href="{{ route('admin.cotizaciones.edit', [$cotizacion->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.cotizaciones.delete', [$cotizacion->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $cotizaciones->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop