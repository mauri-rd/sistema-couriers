<footer>
    <div class="pull-right">
        &copy; {{ \Carbon\Carbon::now()->year }} StrBox | Hecho con mucho <i class="fa fa-coffee"></i> por <a href="https://nineteen.solutions">Nineteen Solutions</a>
    </div>
    <div class="clearfix"></div>
</footer>