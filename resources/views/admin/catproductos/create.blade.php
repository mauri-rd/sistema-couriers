@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva categoría de productos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.catproductos.store', 'method' => 'post']) !!}
                    @include('admin.catproductos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop