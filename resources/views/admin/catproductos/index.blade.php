@extends('admin.master')
@section('titulo-pagina', 'Categorías de Productos')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.catproductos.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cats as $cat)
                            <tr>
                                <td>{{ $cat->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.catproductos.edit', [$cat->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.catproductos.delete', [$cat->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $cats->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop