<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">

            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="fa fa-building"></i>
                        Dashboard
                    </a>
                </li>
            @endif

            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)
                <li>
                    <a href="{{ route('admin.containers.index') }}">
                        <i class="fa fa-building"></i>
                        Containers
                    </a>
                </li>
            @endif

            @if(auth()->user()->type != \App\Enum\UserType::OPERADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_paquetes ))
                <li>
                    <a href="{{ route('admin.paquetes.index') }}">
                        <i class="fa fa-archive"></i>
                        Paquetes
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->cajas()->count() > 0)
                <li>
                    <a href="{{ route('admin.cajas.index') }}">
                        <i class="fa fa-money"></i>
                        Cajas
                    </a>
                </li>
            @endif

            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_invoices ))
                <li>
                    <a href="{{ route('admin.invoices.index') }}">
                        <i class="fa fa-sticky-note-o"></i>
                        Invoices
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.invoice-manual.index') }}">
                        <i class="fa fa-sticky-note-o"></i>
                        Invoice Manual
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_pagos ))
                <li>
                    <a href="{{ route('admin.pagos.index') }}">
                        <i class="fa fa-credit-card"></i>
                        Pagos
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_usuarios ))
                <li>
                    <a href="{{ route('admin.usuarios.index') }}">
                        <i class="fa fa-users"></i>
                        Usuarios
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_mensajes ))
                <li>
                    <a href="{{ route('admin.messages.index') }}">
                        <i class="fa fa-envelope"></i>
                        Mensajes
                    </a>
                </li>
            @endif
                {{--<li>--}}
                    {{--<a href="{{ route('admin.patrocinadores.index') }}">--}}
                        {{--<i class="fa fa-file-image-o"></i>--}}
                        {{--Banners--}}
                    {{--</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="{{ route('admin.empresas.index') }}">--}}
                        {{--<i class="fa fa-building"></i>--}}
                        {{--Empresas--}}
                    {{--</a>--}}
                {{--</li>--}}
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_sucursales ))
                <li>
                    <a href="{{ route('admin.sucursales.index') }}">
                        <i class="fa fa-building"></i>
                        Sucursales
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)
                <li>
                    <a href="javascript:;">
                        <i class="fa fa-money"></i>
                        Financiero
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('admin.cajas.index') }}">Cajas</a></li>
                        <li><a href="{{ route('admin.usuarios.index') }}">Clientes (Usuarios)</a></li>
                        <li><a href="{{ route('admin.proveedores.index') }}">Proveedores</a></li>
                        <li><a href="{{ route('admin.categorias-ingresos.index') }}">Categorías de Ingresos</a></li>
                        <li><a href="{{ route('admin.categorias-egresos.index') }}">Categorías de Egresos</a></li>
                        <li><a href="{{ route('admin.cotizaciones.index') }}">Cotizaciones Dolar/Gs</a></li>
                    </ul>
                </li>
            @endif
                {{--<li>--}}
                    {{--<a href="{{ route('admin.preguntas.index') }}">--}}
                        {{--<i class="fa fa-question"></i>--}}
                        {{--Preguntas Frecuentes--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{-- <li>
                    <a href="javascript:;">
                        <i class="fa fa-file-image-o"></i>
                        Multimedia
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('admin.catmultimedias.index') }}">Categorías Multimedias</a></li>
                        <li><a href="{{ route('admin.multimedias.index') }}">Multimedias</a></li>
                    </ul>
                </li> --}}
                 {{-- <li>
                    <a href="{{ route('admin.cursos.index') }}">
                        <i class="fa fa-newspaper-o"></i>
                        Cursos
                    </a>
                </li>  --}}
                {{-- <li>
                    <a href="javascript:;">
                        <i class="fa fa-pencil-square"></i>
                        Páginas
                        <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('admin.catposts.index') }}">Categorías de Páginas</a></li>
                        <li><a href="{{ route('admin.posts.index') }}">Páginas</a></li>
                    </ul>
                </li> --}}
                {{-- <li>
                    <a href="{{ route('admin.contactos.index') }}">
                        <i class="fa fa-at"></i>
                        Contactos
                    </a>
                </li> --}}
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_reportes ))
                <li>
                    <a href="{{ route('admin.reportes.index') }}">
                        <i class="fa fa-calculator"></i>
                        Reportes
                    </a>
                </li>
            @endif
            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR || ( auth()->user()->type == \App\Enum\UserType::OPERADOR && auth()->user()->operador_informaciones ))
                <li>
                    <a href="{{ route('admin.configuraciones.index') }}">
                        <i class="fa fa-info"></i>
                        Informaciones
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>