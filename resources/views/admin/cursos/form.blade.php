<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del curso', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Descripción</label>
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripción del curso']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imagenes <button type="button" onclick="addImage()" class="btn btn-circle btn-info btn-fill">+</button></label>
    </div>
    <div class="col-md-6" id="images-container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::file('imagenes[]', ['accept' => 'image/*']) !!}
            </div>
        </div>
    </div>
    @if(isset($curso) && count($curso->imagenes) > 0)
        <div class="col-md-6">
            <strong>Seleccione solamente las imagenes que desea eliminar</strong>
            @foreach($curso->imagenes as $imagen)
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <img src="{{ $imagen->url }}" style="height: 100px;">
                            {!! Form::checkbox('eliminar[]', $imagen->id) !!}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>