@extends('admin.master')
@section('titulo-pagina', 'Editar Paquete')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($paquete, ['route' => ['admin.paquetes.update', $paquete->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.paquetes.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection