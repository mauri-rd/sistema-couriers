@extends('admin.master')
@section('titulo-pagina', 'Cambiar estado de paquetes')
@section('contenido')
    <div style="display: none">
        {!! Form::select('usuario', $usuarios, null, ['id' => 'usuarios-demo', 'class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
    </div>
    <div class="row">
        <audio src="{{ url('audio/buzz.wav') }}" id="buzz"></audio>
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            Código o Tracking number:
                            {!! Form::text('search', null, ['class' => 'form-control', 'id' => 'search', 'autofocus', 'onkeydown' => 'checkEnter(event)']) !!}
                            <button class="btn btn-primary" onclick="addPackage()">Agregar a lista</button>
                        </div>

                        <div class="col-md-6">
                            Fecha de inicio de búsqueda:
                            {!! Form::date('from', null, ['class' => 'form-control', 'id' => 'search-from']) !!}
                            Fecha de límite de búsqueda:
                            {!! Form::date('to', null, ['class' => 'form-control', 'id' => 'search-to']) !!}
                            Buscar paquetes con estado:
                            {!! Form::select('estado', $estados, null, ['class' => 'form-control', 'id' => 'search-estado']) !!}
                            Buscar paquetes con wh:
                            {!! Form::text('search', null, ['class' => 'form-control', 'id' => 'search-wh']) !!}
                            <button class="btn btn-primary" onclick="findPaquetesByDate()">Buscar y agregar a lista</button>
                        </div>
                    </div>
                </div>

                <div class="content">
                    {!! Form::open(['route' => 'admin.paquetes.update-estado', 'method' => 'post']) !!}
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Descripción</th>
                                <th>Código</th>
                                <th>Tracking</th>
                                <th>Peso (gramos)</th>
                                <th>Volumen</th>
                                <th>Precio/KG</th>
                                <th>Precio total</th>
                                <th>Estado actual</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody id="paquetes">
                            <tr class="empty-row">
                                <td colspan="8" style="text-align:center; padding: 20px 0;">
                                    <i>Ningún paquete seleccionado aún</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        {!! Form::select('estado', $estados, null, ['class' => 'form-control']) !!}
                        <p>
                            <label>
                                {!! Form::checkbox('email', '1', false) !!} Enviar Invoice por formulario (solo al seleccionar estado "En OFICINA")
                            </label>
                        </p>
                    </div>
                    <div class="row">
                        <button class="btn btn-primary">Cambiar estado</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('modal')
  <!-- Modal -->
    <div class="modal fade" id="userSelectionModal" tabindex="-1" role="dialog" aria-labelledby="userSelectionModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Lista de usuarios</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="input_search" class="form-control" placeholder="Buscar...">
                    <ul class="list-group" id="table_search">
                        @foreach($usuarios as $id => $usuario)
                            <li class="list-group-item" onclick="assignUser('{{ $id }}', '{{ $usuario }}')">{{ $usuario }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@stop

<!--
    <td>${paquete.usuario.id}</td>
    <td>${paquete.usuario.name} ${paquete.usuario.last_name}</td>
-->
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        {{--BUSCADOR DE USUARIO EN MODAL--}}
        $('#input_search').on('keyup', function() {
            var value = $(this).val().toLowerCase();
            $('#table_search li').filter( function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });


        let selectedId = null
        let paqueteSelectedId = null
        function checkEnter(e) {
            console.log('e');
            
            if (e.keyCode == 13) {
                e.preventDefault();
                addPackage();
            }
        }

        function findPaquetesByDate() {
            $.ajax({
                url: "{{ route('admin.paquetes.buscar-ajax-multi') }}",
                data: {
                    from: $('#search-from').val(),
                    to: $('#search-to').val(),
                    estado: $('#search-estado').val(),
                    wh: $('#search-wh').val()
                },
                method: 'GET',
                success: insertMultiplePackage
            })
        }

        function addPackage() {
            const search = $('#search').val();
            $.ajax({
                url: "{{ route('admin.paquetes.buscar-ajax') }}",
                data: {
                    search: $('#search').val()
                },
                method: 'GET',
                success: insertPackage
            })
        }

        function insertMultiplePackage(paquetes) {
            if (paquetes) {
                paquetes.forEach(insertPackage);
            }
        }

        function selectUser(selectId, paqueteId) {
            selectedId = selectId
            paqueteSelectedId = paqueteId
            $('#userSelectionModal').modal('show')
        }

        function assignUser(id, user) {
            if (!selectedId || !paqueteSelectedId) return
            $.ajax({
                url: "{{ route('admin.paquetes.change-user-paquete') }}",
                data: {
                    packageId: paqueteSelectedId,
                    userPackageId: id
                },
                method: 'POST',
                success: function(res){
                    if(res.status === 200){
                        $('#' + selectedId).find('input').val(id)
                        $('#' + selectedId).find('span').html(user)
                        $('#userSelectionModal').modal('hide')
                    }
                }
            })
        }
        
        // <input type="hidden" name="usuario[]" value="${paquete.usuario.id}"> DEBAJO DE <td id="${selectId}">
        function insertPackage(paquete) {
            if (paquete) {
                if ($('.empty-row').length) {
                    $('#paquetes').html('');
                }
                const row = paquete.id;
                if ($(`#row_${row}`).length != 0) {
                    alert('¡Paquete ya en la lista!');
                    return;
                }
                const selectId = 'paquete_user_' + $('#paquetes').children().length
                $('#paquetes').prepend(`
                        <tr id="row_${row}" class="bg-success">
                            <input type="hidden" name="id_paquete[]" value="${paquete.id}">
                            <td id="${selectId}">
                                
                                <span>${paquete.usuario.sucursal.prefix}${paquete.usuario.casilla} - ${paquete.usuario.name} ${paquete.usuario.last_name}</span>
                                <div class="mt-2">
                                    <a class="btn btn-warning" onclick="selectUser('${selectId}', '${paquete.id}')">Asignar usuario</a>
                                </div>
                            </td>
                            <td>${paquete.descripcion}</td>
                            <td>${paquete.codigo}</td>
                            <td>${paquete.tracking}</td>
                            <td><input type="number" name="gramos[]" class="form-control" value="${paquete.gramos}"></td>
                            <td><input type="number" name="volumen[]" class="form-control" value="${paquete.volumen}"></td>
                            <td><input type="number" name="precio_kg[]" class="form-control" value="${paquete.precio_kg}"></td>
                            <td><input type="number" name="precio_fijo[]" class="form-control" value="${paquete.precio_fijo}" step="0.01"></td>
                            <td>${paquete.estado == 1 ? 'En MIAMI' : ( paquete.estado == 2 ? 'En TRANSITO' : ( paquete.estado == 3 ? 'En PARAGUAY' : ( paquete.estado == 4 ? 'En OFICINA' : 'RETIRADO' ) ) )}</td>

                            <td><button type="button" class="btn btn-danger" onclick="deletePackage('row_${row}')"><i class="fa fa-trash"></i></button></td
                        </tr>
                        `);
                $('#' + selectId).selectpicker({
                    liveSearch: true
                })
                $('#' + selectId).selectpicker('val', paquete.id_usuario)
                $('#search').val('');
                $('#search').focus();
                if ($('#paquetes tr:first-child [name=precio_fijo\\[\\]]').val() == '') {
                    $('#paquetes tr:first-child [name=precio_fijo\\[\\]]').val(
                        ( Number($('#paquetes tr:first-child [name=gramos\\[\\]]').val()) / 1000 ) *
                        Number($('#paquetes tr:first-child [name=precio_kg\\[\\]]').val())
                    )
                }
                setTimeout(function() {
                    $(`#row_${row}`).removeClass('bg-success');
                }, 3000);
            } else {
                $('#buzz')[0].play();
            }
        }

        function deletePackage(rowId) {
            if (confirm('¿Seguro que desea eliminar este paquete de la lista?')) {
                $(`#${rowId}`).remove();
                if ($('#paquetes').children().length == 0) {
                    $('#paquetes').html(`
                    <tr class="empty-row">
                        <td colspan="5" style="text-align:center; padding: 20px 0;">
                            <i>Ningún paquete seleccionado aún</i>
                        </td>
                    </tr>
                    `);
                    $('#search').focus();
                }
            }
        }
    </script>
@endsection