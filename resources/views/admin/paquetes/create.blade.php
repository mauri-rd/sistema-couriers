@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo paquete')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content" style="position: relative;">
                    {!! Form::open(['route' => 'admin.paquetes.store', 'method' => 'post', 'files' => true, 'id' => 'paquete-form']) !!}
                    @include('admin.paquetes.form')
                    {!! Form::close() !!}
                    <div class="overlay" id="overlay">
                        <img src="{{ url('img/loading.gif') }}" alt="Cargando">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        var foundPacklists = [];
        $('#tracking').on('change', findPacklist);
        function findPacklist() {
            if ($('#tracking').val() == '') return;
            $('#overlay').css({ display: 'block' });
            $.ajax({
                url: '{{ route("admin.paquetes.find-packlist") }}',
                method: 'GET',
                data: {
                    tracking: $('#tracking').val()
                },
                success: function(packlists) {
                    foundPacklists = packlists;
                    $('#packlist-options').html(
                        packlists.map(function (packlist) {
                            return '<li class="list-group-item" onclick="selectPackListFromFound(' + packlist.id + ')"><strong>' + packlist.descripcion.replace(/\n/g, '<br>') + '</strong><br><strong>ID Usuario:</strong> ' + packlist.id_usuario + '<br><strong>Tracking:</strong> ' + packlist.tracking + '</li>';
                        }).join('')
                    );
                    $('#select-packlist').css({
                        display: 'block'
                    });

                    $('#overlay').css({ display: 'none' });
                }, error: function() {
                    $('#overlay').css({ display: 'none' });
                }
            });
        }

        function selectPackListFromFound(id) {
            selectPackList(foundPacklists.find(function (packlist) {
                return packlist.id === id;
            }));
            $('#select-packlist').css({
                display: 'none'
            });
        }

        function selectPackList(packlist) {
            $('#packlist-container').css({ display: 'block' });
            $('#usuario').val(packlist.id_usuario);
            $('#usuario').prop('disabled', true);
            $('#paquete-form').append(
                `
                            <input type="hidden" name="id_usuario" value="${packlist.id_usuario}">
                            `
            );
            $('#usuario').selectpicker('refresh');
            $('#packlist').val(packlist.id);
            $('#tipo_flete').val(packlist.tipo_flete);
            $('#destino').val(packlist.destino);
            $('#courier').val(packlist.courier);
            $('#consolidacao').prop('checked', packlist.consolidacao);
            $('#seguro').prop('checked', packlist.seguro);
            $('#packlist-descripcion').html(packlist.descripcion.replace(/\n/g, '<br>'));
            $('#packlist-valor').html('US$ ' + packlist.valor);
            if (packlist.consolidacao) {
                $('#trackings_consolidados').css({ display: 'block' });
                $('#consolidacao_trackings').val(packlist.consolidacao_trackings);
            }
            let packlistHtml = '';
            if (packlist.mime.indexOf('image') > -1) {
                packlistHtml = `
                            <a href="${packlist.file}" target="_blank" class="anexo">
                                <img src="${packlist.file}">
                                <div class="legend">Clique para abrir o arquivo</div>
                            </a>
                            `
            } else {
                packlistHtml = `
                            <a href="${packlist.file}" target="_blank" class="anexo">
                                <i class="fa fa-file-pdf-o"></i>
                                <div class="legend">Clique para abrir o arquivo</div>
                            </a>
                            `;
            }
            $('#packlist-file').html(packlistHtml);
        }
    </script>
@endsection
@section('css')
    <style>
        .overlay {
            position: absolute;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            background-color: rgba(255, 255, 255, 0.8);
        }

        .overlay img {
            margin-top: 100px;
        }

        .anexo img {
            height: 40px;
        }

        .anexo i.fa {
            font-size: 40px;
            color: red;
        }
    </style>
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection