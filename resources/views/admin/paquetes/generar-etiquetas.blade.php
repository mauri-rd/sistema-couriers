@extends('admin.master')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary print">
                <i class="fa fa-print"></i> Imprimir
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" style="width: 100%">
            @for($i = 0; $i < $paquete->cant_cajas; $i++)
                <div class="x_panel ticket">
                    <div class="content">
                            <div class="invoice">
                                <div class="invoice-logo">
                                    <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
                                </div>
                                <div>
                                    <strong>Descripción:</strong> {{ strlen($paquete->descripcion) > 100 ? (substr($paquete->descripcion, 0, 100) . '...') : $paquete->descripcion }}
                                </div>
                                @if($paquete->consolidacao)
                                    <div>
                                        <strong>Tracking:</strong> {{ $paquete->consolidacao_trackings }} <i>(consolidados)</i>
                                    </div>
                                @else
                                    <div>
                                        <strong>Tracking:</strong> {{ $paquete->tracking }}
                                    </div>
                                @endif
                                <div>
                                    <strong>Peso:</strong> {{ $paquete->gramos/1000 }} Kg.
                                </div>
                                <div>
                                    <strong>Volumen:</strong> {{ !is_null($paquete->volumen) ? $paquete->volumen : 0 }}
                                </div>

                                <div>
                                    <strong>Cliente:</strong> {{ $paquete->usuario->full_name }}
                                </div>
                                <div>
                                    <strong>ID Cliente:</strong> {{ $paquete->usuario->id }}
                                </div>
                                <div>
                                    <strong>Fecha recepción:</strong> {{ $paquete->created_at->format('d/m/Y H:i') }}
                                </div>
                                <div>
                                    <strong>Destino:</strong> {{ intval($paquete->destino) === \App\Enum\Destino::PARAGUAY ? 'PARAGUAY' : 'BRASIL' }}
                                </div>
                                <div>
                                    <strong>Tipo de Flete:</strong> {{ intval($paquete->tipo_flete) === \App\Enum\TipoFlete::NORMAL ? 'NORMAL' : 'EXPRESS' }}
                                </div>
                                <div>
                                    @if($paquete->consolidacao)
                                        <strong style="margin: 0 10px;">CONSOLIDACION</strong>
                                    @endif
                                    @if($paquete->seguro)
                                        <strong style="margin: 0 10px;">CON SEGURO</strong>
                                    @endif

                                </div>

                                <div>
                                    <strong>Caja:</strong> {{ $i + 1 }}/{{ $paquete->cant_cajas }}
                                </div>
                                <div class="invoice-footer">
                                    <svg class="barcode"></svg>
                                </div>
                            </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
@stop
@section('css')
    <style>
        .invoice {
            margin: 20px;
            font-size: 17px;
            font-family: sans-serif;
            text-align: center;
            color: #000;
        }

        .invoice .barcode {
            display: block;
            margin: 0 auto;
        }

        .invoice .invoice-logo {
            text-align: center;
            background-color: #211973;
        }

        .invoice .invoice-header {
            padding: 20px;
            text-align: right;
        }

        .invoice .invoice-table {
            width: 95%;
            margin: 0 auto;
            border-collapse: collapse
        }

        .invoice .ticket {
            margin-top: 50px;
            margin-bottom: 50px;
        }

        .invoice .invoice-table thead {
            border-bottom: 2px solid #aaa;
        }

        .invoice .invoice-table thead th {
            text-align: center;
        }

        .invoice .invoice-table tbody tr {
            border-top: solid 1px #ddd; text-align: center;
        }
        
        .invoice .invoice-table tbody td {
            padding: 12px 0;
        }

        .invoice .invoice-footer {
            padding-bottom: 20px;
            text-align: right;
        }

        .signature {
            display: none;
            border-top: 1px solid #000000;
            padding: 15px 80px;
            text-align: left;
        }

        @media print {
            .nav_menu, footer, .print, #nprogress {
                display: none;
            }

            .signature {
                display: inline;
            }

        }
    </style>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>
    <script>
        JsBarcode(".barcode", "{{ $paquete->codigo }}");
        $('.print').click(function() {
            print();
        });
        $(document).ready(function() {
            print();
        });
    </script>
@endsection