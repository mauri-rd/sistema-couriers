@extends('admin.master')
@section('titulo-pagina', 'Cambiar estado de paquetes')
@section('contenido')
<div id="app">

    {{-- <div class="row"> --}}
        {{-- <example-component>
    
        </example-component> --}}
        <div class="row">
            <audio :src="audioUrl" id="buzz"></audio>
        </div>
        <cambio-estado-formulario></cambio-estado-formulario>
    {{-- </div> --}}
    {{-- <div class="row">
        <audio src="{{ url('audio/buzz.wav') }}" id="buzz"></audio>
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            Código o Tracking number:
                            <input v-model="search" class="form-control" autofocus @keydown.enter="addPackage">
                            <button class="btn btn-primary" @click="addPackage">Agregar a lista</button>
                        </div>
                        <div class="col-md-6">
                            Fecha de inicio de búsqueda:
                            <input type="date" v-model="searchFrom" class="form-control">
                            Fecha de límite de búsqueda:
                            <input type="date" v-model="searchTo" class="form-control">
                            Buscar paquetes con estado:
                            <select v-model="searchEstado" class="form-control">
                                <option v-for="(estado, estadoId) in estados" :key="estadoId" :value="estadoId">@{{ estado }}</option>
                            </select>
                            Buscar paquetes con wh:
                            <input v-model="searchWh" class="form-control">
                            <button class="btn btn-primary" @click="findPaquetesByDate">Buscar y agregar a lista</button>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <form @submit.prevent="updateEstado">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Descripción</th>
                                    <th>Código</th>
                                    <th>Tracking</th>
                                    <th>Peso (gramos)</th>
                                    <th>Precio/KG</th>
                                    <th>Precio total</th>
                                    <th>Estado actual</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody id="paquetes">
                                <tr v-if="paquetes.length === 0" class="empty-row">
                                    <td colspan="8" style="text-align:center; padding: 20px 0;">
                                        <i>Ningún paquete seleccionado aún</i>
                                    </td>
                                </tr>
                                <tr v-for="(paquete, index) in paquetes" :key="paquete.id">
                                    <td>
                                        <span>@{{ paquete.usuario.sucursal.prefix }} @{{ paquete.usuario.casilla }} - @{{ paquete.usuario.name }} @{{ paquete.usuario.last_name }}</span>
                                        <div class="mt-2">
                                            <button class="btn btn-warning" @click="selectUser(index, paquete.id)">Asignar usuario</button>
                                        </div>
                                    </td>
                                    <td>@{{ paquete.descripcion }}</td>
                                    <td>@{{ paquete.codigo }}</td>
                                    <td>@{{ paquete.tracking }}</td>
                                    <td><input type="number" v-model="paquete.gramos" class="form-control"></td>
                                    <td><input type="number" v-model="paquete.precio_kg" class="form-control"></td>
                                    <td><input type="number" v-model="paquete.precio_fijo" class="form-control" step="0.01"></td>
                                    <td>@{{ estados[paquete.estado] }}</td>
                                    <td><button type="button" class="btn btn-danger" @click="deletePackage(index)"><i class="fa fa-trash"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <select v-model="selectedEstado" class="form-control">
                                <option v-for="(estado, estadoId) in estados" :key="estadoId" :value="estadoId">@{{ estado }}</option>
                            </select>
                            <p>
                                <label>
                                    <input type="checkbox" v-model="sendInvoice"> Enviar Invoice por formulario (solo al seleccionar estado "En OFICINA")
                                </label>
                            </p>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary">Cambiar estado</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
</div>
@stop

{{-- @section('modal')
<!-- Modal -->
<div class="modal fade" id="userSelectionModalTest" tabindex="-1" role="dialog" aria-labelledby="userSelectionModalTestLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lista de usuarios</h4>
            </div>
            <div class="modal-body">
                <input type="text" v-model="searchUser" class="form-control" placeholder="Buscar...">
                <ul class="list-group">
                    <li v-for="(usuario, id) in usuarios" :key="id" class="list-group-item" @click="assignUser(id, usuario)">{{ usuario }}</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop --}}

@section('css')
<link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop

@section('js')
{{-- <script src="{{ url('js/bootstrap-select.min.js') }}"></script> --}}
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    new Vue({
        el: '#app',
        data: {
            search: '',
            searchFrom: '',
            searchTo: '',
            searchEstado: '',
            searchWh: '',
            paquetes: [],
            selectedEstado: '',
            sendInvoice: false,
            selectedId: null,
            paqueteSelectedId: null,
            usuarios: {!! json_encode($usuarios) !!},
            estados: {!! json_encode($estados) !!},
            searchUser: '',
        },
        computed: {
            filteredUsers() {
                console.log('Texto de búsqueda:', this.searchUser);
                return this.usuarios.filter(usuario => usuario.includes(this.searchUser));
            }
        },
        methods: {
            async findPaquetesByDate() {
                try {
                    const response = await axios.get('{{ route("admin.paquetes.buscar-ajax-multi") }}', {
                        params: {
                            from: this.searchFrom,
                            to: this.searchTo,
                            estado: this.searchEstado,
                            wh: this.searchWh
                        }
                    });
                    this.paquetes = response.data;
                } catch (error) {
                    console.error(error);
                    // Mostrar mensaje de error al usuario
                    // this.showErrorMessage(error.response.data.message);
                }
            },
            async addPackage() {
                try {
                    const response = await axios.get('{{ route("admin.paquetes.buscar-ajax") }}', {
                        params: {
                            search: this.search
                        }
                    });
                    if (response.data) {
                        if (this.paquetes.length === 1 && this.paquetes[0].empty) {
                            this.paquetes = [];
                        }
                        const paquete = response.data;
                        if (!this.paquetes.find(p => p.id === paquete.id)) {
                            this.paquetes.push(paquete);
                            console.log(this.paquetes);
                            this.search = '';
                            this.searchWh = '';
                            this.$nextTick(() => {
                                const lastIndex = this.paquetes.length - 1;
                                this.$refs.gramos[lastIndex].focus();
                            });
                            console.log(this.usuarios);
                        } else {
                            alert('¡Paquete ya en la lista!');
                        }
                    } else {
                        $('#buzz')[0].play();
                    }
                } catch (error) {
                    console.error(error);
                    // Mostrar mensaje de error al usuario
                    // this.showErrorMessage(error.response.data.message);
                }
            },
            async selectUser(index, paqueteId) {
                this.selectedId = index;
                this.paqueteSelectedId = paqueteId;
                $('#userSelectionModalTest').modal('show');
            },
            async assignUser(id, user) {
                if (!this.selectedId || !this.paqueteSelectedId) return;
                try {
                    const response = await axios.post('{{ route("admin.paquetes.change-user-paquete") }}', {
                        packageId: this.paqueteSelectedId,
                        userPackageId: id
                    });
                    if (response.status === 200) {
                        this.paquetes[this.selectedId].usuario.id = id;
                        this.paquetes[this.selectedId].usuario.name = user;
                        $('#userSelectionModalTest').modal('hide');
                    }
                } catch (error) {
                    console.error(error);
                    // Mostrar mensaje de error al usuario
                    // this.showErrorMessage(error.response.data.message);
                }
            },
            async updateEstado() {
                try {
                    const response = await axios.post('{{ route("admin.paquetes.update-estado") }}', {
                        estado: this.selectedEstado,
                        email: this.sendInvoice ? 1 : 0,
                        paquetes: this.paquetes.map(p => p.id)
                    });
                    if (response.status === 200) {
                        // Actualizar la vista o realizar acciones adicionales si es necesario
                    }
                } catch (error) {
                    console.error(error);
                    // Mostrar mensaje de error al usuario
                    // this.showErrorMessage(error.response.data.message);
                }
            },
            deletePackage(index) {
                if (confirm('¿Seguro que desea eliminar este paquete de la lista?')) {
                    this.paquetes.splice(index, 1);
                    if (this.paquetes.length === 0) {
                        this.paquetes = [{ empty: true }];
                    }
                }
            }
        }
    });
</script> --}}
@stop