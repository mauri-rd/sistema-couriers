@extends('admin.master')
@section('titulo-pagina', 'Paquetes')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.paquetes.create') }}" class="btn btn-fill btn-success">Registrar</a>
                    {!! Form::open(['route' => 'admin.paquetes.index', 'method' => 'get']) !!}
                        <div class="form-group">
                            <label for="usuario">Filtrar por usuario</label>
                            {!! Form::select('usuario', $usuarios, $usuario, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                        </div>
                        <div class="form-group">
                            <label for="usuario">Filtrar por tracking</label>
                            {!! Form::select('tracking', $trackings, $tracking, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                        </div>
                        Mostrar solamente:
                        <div class="row">
                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 1, !is_null($show) && in_array(1, $show)) !!} En MIAMI
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 6, !is_null($show) && in_array(6, $show)) !!} En CHINA
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 2, !is_null($show) && in_array(2, $show)) !!} En TRANSITO
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 3, !is_null($show) && in_array(3, $show)) !!} En PARAGUAY
                                </label>
                            </div>

                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 4, !is_null($show) && in_array(4, $show)) !!} En OFICINA
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    {!! Form::checkbox('show[]', 5, !is_null($show) && in_array(5, $show)) !!} RETIRADO
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="from">Filtrar desde fecha (en el estado seleccionado)</label>
                            {!! Form::date('from', isset($from) ? $from : null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="from">Filtrar hasta fecha (en el estado seleccionado)</label>
                            {!! Form::date('to', isset($to) ? $to : null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary">Filtrar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                {!! Form::open(['route' => 'admin.paquetes.update-estado', 'method' => 'post']) !!}
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)
                                <th></th>
                            @endif
                            <th>Casilla del usuario</th>
                            <th>Nombre</th>
                            <th>Wh</th>
                            <th>Tracking Number</th>
                            <th>Descripción</th>
                            <th>Tipo de Flete</th>
                            <th>Servicios Adicionales</th>
                            <th>Estado</th>
                            <th>Peso</th>
                            <th>Volumen</th>
                            <th>Total</th>
                            <th>Archivo anexo</th>
                            <th>Descripción packlist</th>
                            <th>Fecha de registro</th>
                            <th>Fecha de actualización</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($paquetes as $paquete)
                            <tr>
                                @if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)
                                    <td>{!! Form::checkbox('id_paquete[]', $paquete->id, false) !!}</td>
                                @endif
                                <td>{{ $paquete->usuario->prefix . $paquete->usuario->casilla }}</td>
                                <td>{{ $paquete->usuario->full_name }}</td>
                                <td>{{ $paquete->wh }}</td>
                                <td>{{ $paquete->tracking }}</td>
                                <td>{!! nl2br($paquete->descripcion) !!}</td>
                                <td>{{ intval($paquete->tipo_flete) === \App\Enum\TipoFlete::NORMAL ? 'NORMAL' : 'EXPRESS' }}</td>
                                    <th>
                                        <ul>
                                            @if($paquete->consolidacao)
                                                <li><i>Consolidación ({{ $paquete->consolidacao_trackings }})</i></li>
                                            @endif
                                            @if($paquete->seguro)
                                                <li><i>Seguro</i></li>
                                            @endif

                                        </ul>
                                    </th>
                                
                                @if($paquete->estado == App\Enum\EstadoPaquete::$MIAMI)
                                    <td>En Miami</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$CHINA)
                                    <td>En China</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$TRANSITO)
                                    <td>En Transito</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$PARAGUAY)
                                    <td>En Paraguay</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$BRASIL)
                                    <td>En Brasil</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$OFICINA)
                                    <td>Para Retirar</td>
                                @elseif($paquete->estado == App\Enum\EstadoPaquete::$RETIRADO)
                                    <td>Retirado</td>
                                @endif
                                <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                <td>{{ !is_null($paquete->volumen) ? $paquete->volumen : 0 }}</td>
                                <td>US$ {{ $paquete->valor_total }}</td>
                                <td>
                                    @if(!is_null($paquete->packlist))
                                        <a href="{{ $paquete->packlist->file }}" target="_blank" class="anexo">
                                            @if(str_contains($paquete->packlist->mime, 'image'))
                                                <img src="{{ $paquete->packlist->file }}">
                                            @else
                                                <i class="fa fa-file-pdf-o"></i>
                                            @endif
                                            <div class="legend">Clique para abrir o arquivo</div>
                                        </a>
                                    @else
                                        <i>No existe packlist asociado</i>
                                    @endif
                                </td>
                                <td>
                                    @if(!is_null($paquete->packlist))
                                        {!! nl2br($paquete->packlist->descripcion) !!}
                                    @else
                                        <i>No existe packlist asociado</i>
                                    @endif
                                </td>
                                <td>{{ $paquete->created_at->format('d/m/Y H:i:s') }}</td>
                                <td>{{ $paquete->updated_at->format('d/m/Y H:i:s') }}</td>
                                <td>
                                    <a href="{{ route('admin.paquetes.edit', [$paquete->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.paquetes.delete', [$paquete->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {{--@if(auth()->user()->type == \App\Enum\UserType::ADMINISTRADOR)--}}
                        <div>
                            Cambiar estado de paquetes a {!! Form::select('estado', $estados, null, ['class' => 'form-control']) !!}
                            <p>
                                <label>
                                    {!! Form::checkbox('email', '1', false) !!} Enviar Invoice por formulario (solo al seleccionar estado "En OFICINA")
                                </label>
                            </p>
                            <button type="submit" class="btn btn-primary">Cambiar de estado</button>
                            {{-- <a href="{{ route('admin.paquetes.form-cambio-estado') }}" class="btn btn-primary">Cambiar estado por formulario</a> NORMAL --}}
                            {{-- CON VUE --}}
                            <a href="{{ route('admin.paquetes.form-cambio-estado-test') }}" class="btn btn-primary">Cambiar estado por formulario</a>
                        </div>
                    {{--@endif--}}
                    {!! $paquetes->appends(request()->input())->render() !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{-- <div id="app">

    </div> --}}
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        .anexo img {
            height: 40px;
        }

        .anexo i.fa {
            font-size: 40px;
            color: red;
        }
    </style>

@endsection