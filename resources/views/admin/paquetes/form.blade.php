{!! Form::hidden('id_packlist', null, ['id' => 'packlist']) !!}
<div class="row">
    <div class="col-md-12">
        <label>Usuario</label>
        {!! Form::select('id_usuario', $usuarios, null, ['class' => 'form-control selectpicker', 'required', 'autofocus', 'data-live-search' => 'true', 'id' => 'usuario']) !!}
    </div>
</div>

{{-- <div class="row">
    <div class="col-md-11">
        <label for="container">Container</label>
        {!! Form::select('id_container', $containers, null, ['class' => 'form-control selectpicker',  'data-live-search' => 'true']) !!}
    </div>
    <div class="col-md-1">
        <button class="btn btn-primary" style="margin-top: 23px" data-toggle="modal" data-target="#myModal">
            Nuevo
        </button>
    </div>
</div> --}}

<div class="row">
    <div class="col-md-11">
        <label for="id_container">Container</label>
        <div id="testSelect"></div>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-primary" style="margin-top: 23px" data-toggle="modal" data-target="#myModal">
            Nuevo
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Wh</label>
        {!! Form::text('wh', null, ['class' => 'form-control', 'placeholder' => 'Wh', 'required']) !!}
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Código</label>--}}
        {{--{!! Form::text('codigo', null, ['class' => 'form-control', 'placeholder' => 'Código del paquete', 'required']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label>Tracking Number</label>
        {!! Form::text('tracking', null, ['class' => 'form-control', 'placeholder' => 'Tracking number del paquete', 'required', 'id' => 'tracking']) !!}
    </div>
</div>

<div class="row" id="select-packlist" style="display: none; cursor:pointer;">
    <div class="col-md-12">
        <label>Seleccione Packlist correcto</label>
        <ul id="packlist-options" class="list-group"></ul>
    </div>
</div>

<!--PackList-->

<div class="row" id="packlist-container" style="display: none">
    <div class="col-md-12">
        <label>Wh</label>
        <p id="packlist-wh"></p>
    </div>    

    <div> class="col-md-12">
        <label>Datos de Packlist</label>
    </div>

    <div class="col-md-12">
        <label>Descripción</label>
        <p id="packlist-descripcion"></p>
    </div>
    
    <div class="col-md-12">
        <label>Valor</label>
        <p id="packlist-valor"></p>
    </div>
    
    <div class="col-md-12">
        <label>Adjunto</label>
        <p id="packlist-file"></p>
    </div>
    
</div>
<!---->

<div class="row">
    <div class="col-md-12">
        <label>Tipo de Flete</label>
        {!! Form::select('tipo_flete', $fletes, null, ['class' => 'form-control', 'required', 'id' => 'tipo_flete']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Destino</label>
        {!! Form::select('destino', $destinos, null, ['class' => 'form-control', 'required', 'id' => 'destino']) !!}
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label for="consolidacao">--}}
            {{--{!! Form::checkbox('consolidacao', 0, false, ['id' => 'consolidacao']) !!}--}}
            {{--Consolidar paquetes--}}
        {{--</label>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row" id="trackings_consolidados">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Trackings consolidados con paquete</label>--}}
        {{--{!! Form::text('consolidacao_trackings', null, ['class' => 'form-control', 'placeholder' => 'Trackings consolidados', 'id' => 'consolidacao_trackings']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label for="seguro">
            {!! Form::checkbox('seguro', 0, false, ['id' => 'seguro']) !!}
            Solicitar seguro
        </label>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Courier</label>
        {!! Form::select('courier', $couriers, null, ['class' => 'form-control', 'required', 'id' => 'courier']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Descripción</label>
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripción del paquete', 'required']) !!}
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Cantidad de cajas</label>--}}
        {{--{!! Form::number('cant_cajas', isset($paquete) ? null : 1, ['class' => 'form-control', 'placeholder' => 'Cantidad total de cajas', 'min' => 1, 'step' => 1, 'required']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label>Gramos</label>
        {!! Form::number('gramos', isset($paquete) ? null : 0, ['class' => 'form-control', 'placeholder' => 'Peso del paquete en gramos', 'min' => 0.01, 'step' => 0.01, 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Volumen</label>
        {!! Form::number('volumen', isset($paquete) ? null : 0, ['class' => 'form-control', 'placeholder' => 'Cantidad de volumenes', 'min' => 0, 'required']) !!}
    </div>
</div>

@if(isset($paquete))
    <div class="row">
        <div class="col-md-12">
            <label>Precio/KG.</label>
            {!! Form::number('precio_kg', 0, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>
@endif

@if(!isset($paquete))
    <div class="row">
        <div class="col-md-12">
            <label>Origen</label>
            {!! Form::select('estado', $estados, null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>
@endif

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Tipo de Producto</label>--}}
        {{--{!! Form::select('tipo_precio', $tiposPrecio, null, ['class' => 'form-control', 'required', 'id' => 'tipo-precio-select']) !!}--}}
        {{--<p class="help-block">Se utilizará esta información para estipular precio por Kilo o por unidad</p>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Unidades</label>--}}
        {{--{!! Form::number('unidades', isset($paquete) ? null : 0, ['class' => 'form-control', 'placeholder' => 'Unidades (cantidad de ítems)', 'min' => 0, 'step' => 0.01, 'required']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>
  
@section('modal')
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
            {!! Form::open(['route' => 'admin.containers.storeModal', 'method' => 'post', 'id' => 'addContainerForm']) !!}
                @include('admin.containers.form')
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
            <button type="button" id="btnModalClose" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
        </div>
    </div>
</div>
@endsection