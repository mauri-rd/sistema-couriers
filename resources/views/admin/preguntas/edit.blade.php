@extends('admin.master')
@section('titulo-pagina', 'Editar Producto')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($pregunta, ['route' => ['admin.preguntas.update', $pregunta->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.preguntas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop