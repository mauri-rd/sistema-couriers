@extends('admin.master')
@section('titulo-pagina', 'Preguntas Frecuentes')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                {!! Form::open(['route' => 'admin.preguntas.reordenar', 'method' => 'post']) !!}
                    <div class="header">
                        <a href="{{ route('admin.preguntas.create') }}" class="btn btn-fill btn-success">Registrar</a>
                        <button type="submit" class="btn btn-primary">Guardar Posiciones</button>
                    </div>
                    <div class="content">
                        <table class="table table-full-width table-hover">
                            <thead>
                            <tr>
                                <th>Pregunta</th>
                                <th>Respuesta</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody id="sortable">
                            @foreach($preguntas as $pregunta)
                                <tr>
                                    {!! Form::hidden('id[]', $pregunta->id) !!}
                                    <td>{{ $pregunta->pregunta }}</td>
                                    <td>{{ $pregunta->respuesta }}</td>
                                    <td>
                                        <a href="{{ route('admin.preguntas.edit', [$pregunta->id]) }}" class="btn btn-info">Editar</a>
                                        <a href="{{ route('admin.preguntas.delete', [$pregunta->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="footer">
                        {!! $preguntas->render() !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    </script>
@endsection