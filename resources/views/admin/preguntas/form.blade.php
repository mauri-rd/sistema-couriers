<div class="row">
    <div class="col-md-12">
        <label>Pregunta</label>
        {!! Form::text('pregunta', null, ['class' => 'form-control', 'placeholder' => 'Pregunta', 'required', 'autofocus']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Respuesta</label>
        {!! Form::textarea('respuesta', null, ['class' => 'form-control', 'placeholder' => 'Respuesta', 'required']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>