@extends('admin.master')
@section('titulo-pagina', 'Paquetes en tránsito')
@section('contenido')
    {{-- <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card-counter primary" style="min-height: 200px">
                    <i class="fa fa-code-fork"></i>
                    <span class="count-numbers">12</span>
                    <span class="count-name" style="margin-top: 30px">Cantidad de paquetes en tránsito</span>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card-counter danger" style="min-height: 200px">
                    <i class="fa fa-ticket"></i>
                    <span class="count-numbers">599</span>
                    <span class="count-name" style="margin-top: 30px">Volumen total de paquetes en tránsito</span>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card-counter success" style="min-height: 200px">
                    <i class="fa fa-database"></i>
                    <span class="count-numbers">6875</span>
                    <span class="count-name" style="margin-top: 30px">Cantidad de paquetes en oficina</span>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card-counter info" style="min-height: 200px">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">35</span>
                    <span class="count-name" style="margin-top: 30px">Volumen total de paquetes en oficina</span>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">

                <div class="content">
                    <table class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>ID de usuario</th>
                                <th>Nombre</th>
                                <th>Trackings</th>
                                <th>Valor en US$</th>
                                <th>Moneda</th>
                                <th>Valor en moneda</th>
                                <th>Valor del cambio</th>
                                <th>Foto de comprobante</th>
                                <th>Fecha de registro</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($pagos->count() == 0)
                                <tr>
                                    <td colspan="9" class="text-center">
                                        <i>Sin pagos pendientes de procesar</i>
                                    </td>
                                </tr>
                            @endif
                            @foreach ($pagos as $pago)
                                <tr>
                                    <td>{{ $pago->id_cliente }}</td>
                                    <td>{{ $pago->cliente->full_name }}</td>
                                    <td>
                                        <ul>
                                            @foreach ($pago->items as $item)
                                                <li>{{ $item->paquete->tracking }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>US$ {{ $pago->total_usd }}</td>
                                    <td>{{ $pago->moneda }}</td>
                                    <td>{{ $pago->total_cambio_formateado }}</td>
                                    <td>{{ $pago->valor_cambio_formateado }}</td>
                                    <td>
                                        <a href="{{ $pago->comprobante }}" target="_blank">
                                            <img src="{{ $pago->comprobante }}" alt="Comprobante" class="img-responsive">
                                        </a>
                                    </td>
                                    <td>{{ $pago->created_at->format('d/m/Y H:i:s') }}</td>
                                    <td>
                                        <a href="{{ route('admin.pagos.procesar', [$pago->id, 'aceptado' => 1]) }}"
                                            class="btn btn-success">
                                            <i class="fa fa-check"></i> Aceptar datos de pago
                                        </a>
                                        <a href="{{ route('admin.pagos.procesar', [$pago->id]) }}" class="btn btn-danger">
                                            <i class="fa fa-times"></i> Rechazar datos de pago
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
{{-- @section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<style>
    .card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
</style>
@endsection --}}