@extends('admin.master')
@section('titulo-pagina', 'Informaciones de la Empresa')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                {!! Form::open(['route' => 'admin.configuraciones.save', 'method' => 'post']) !!}
                <div class="header">
                    <button type="submit" class="btn btn-fill btn-success">Guardar</button>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <tbody>

                            <tr>
                                <td>{{ $configs['envio-miami']->nombre }}</td>
                                <td>
                                    {!! Form::number($configs['envio-miami']->slug, $configs['envio-miami']->valor, ['class' => 'form-control', 'step' => '0.01', 'autofocus']) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $configs['envio-miami-maritimo']->nombre }}</td>
                                <td>
                                    {!! Form::number($configs['envio-miami-maritimo']->slug, $configs['envio-miami-maritimo']->valor, ['class' => 'form-control', 'step' => '0.01']) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $configs['envio-china']->nombre }}</td>
                                <td>
                                    {!! Form::number($configs['envio-china']->slug, $configs['envio-china']->valor, ['class' => 'form-control', 'step' => '0.01']) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $configs['instagram']->nombre }}</td>
                                <td>
                                    {!! Form::text($configs['instagram']->slug, $configs['instagram']->valor, ['class' => 'form-control']) !!}
                                </td>
                            </tr>
                            
                            <tr>
                                <td>{{ $configs['facebook']->nombre }}</td>
                                <td>
                                    {!! Form::text($configs['facebook']->slug, $configs['facebook']->valor, ['class' => 'form-control']) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $configs['numero-whatsapp']->nombre }}</td>
                                <td>
                                    {!! Form::text($configs['numero-whatsapp']->slug, $configs['numero-whatsapp']->valor, ['class' => 'form-control']) !!}
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('js')
<script>
    function previewImage(url) {
        $('#preview').attr('src', url);
    }
</script>
@stop