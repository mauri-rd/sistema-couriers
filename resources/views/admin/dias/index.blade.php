@extends('admin.master')
@section('titulo-pagina', 'Paquetes hace mas de 14 dias')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                {!! Form::open(['route' => 'admin.dias.index', 'method' => 'post']) !!}
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID de usuario</th>
                            <th>Nombre</th>
                            <th>Tracking Number</th>
                            <th>Descripción</th>
                            <th>Cant. de Cajas</th>
                            <th>Courier</th>
                            <th>Estado</th>
                            <th>Peso</th>
                            <th>Archivo anexo</th>
                            <th>Descripción packlist</th>
                            <th>Fecha de registro</th>
                            <th>Fecha de actualización</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($paquetes as $paquete)
                            <tr>
                                <td>{!! Form::checkbox('id_paquete[]', $paquete->id, false) !!}</td>
                                <td>{{ $paquete->id_usuario }}</td>
                                <td>{{ $paquete->usuario->full_name }}</td>
                                <td>{{ $paquete->tracking }}</td>
                                <td>{!! nl2br($paquete->descripcion) !!}</td>
                                <td>{{ $paquete->cant_cajas }}</td>
                                @if($paquete->courier == App\Enum\Couriers::$DHL)
                                    <td>DHL</td>
                                @elseif($paquete->courier == App\Enum\Couriers::$UPS)
                                    <td>UPS</td>
                                @elseif($paquete->courier == App\Enum\Couriers::$USPS)
                                    <td>USPS</td>
                                @elseif($paquete->courier == App\Enum\Couriers::$FedEx)
                                    <td>FedEx</td>
                                @endif
                                
                                <td>{{ $paquete->estado }}</td>

                                <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                <td>
                                    @if(!is_null($paquete->packlist))
                                        <a href="{{ $paquete->packlist->file }}" target="_blank" class="anexo">
                                            @if(str_contains($paquete->packlist->mime, 'image'))
                                                <img src="{{ $paquete->packlist->file }}">
                                            @else
                                                <i class="fa fa-file-pdf-o"></i>
                                            @endif
                                            <div class="legend">Clique para abrir o arquivo</div>
                                        </a>
                                    @else
                                        <i>No existe packlist asociado</i>
                                    @endif
                                </td>
                                <td>
                                    @if(!is_null($paquete->packlist))
                                        {!! nl2br($paquete->packlist->descripcion) !!}
                                    @else
                                        <i>No existe packlist asociado</i>
                                    @endif
                                </td>
                                <td>{{ $paquete->created_at->format('d/m/Y H:i:s') }}</td>
                                <td>{{ $paquete->updated_at->format('d/m/Y H:i:s') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="footer">
                    <div>
                        Cambiar estado de paquetes a {!! Form::select('estado', $estados, null, ['class' => 'form-control']) !!}
                        <p>
                            <label>
                                {!! Form::checkbox('email', '1', false) !!} Enviar Invoice por formulario (solo al seleccionar estado "En OFICINA")
                            </label>
                        </p>
                        <button type="submit" class="btn btn-primary">Cambiar de estado</button>
                        <a href="{{ route('admin.paquetes.form-cambio-estado') }}" class="btn btn-primary">Cambiar estado por formulario</a>
                    </div>
                    {!! $paquetes->appends(request()->input())->render() !!}
                </div> --}}
                {!! Form::close() !!} 
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        .anexo img {
            height: 40px;
        }

        .anexo i.fa {
            font-size: 40px;
            color: red;
        }
    </style>

@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
@endsection