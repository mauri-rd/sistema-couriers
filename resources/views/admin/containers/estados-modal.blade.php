<!-- Modal -->
<div class="modal fade" id="container-estados-{{$container->id}}" tabindex="-1" role="dialog" aria-labelledby="containerLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="containerLabel">Cambiar estado del container {{ $container->nombre }}</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($container, ['route' => ['admin.containers.update-estado', $container->id], 'method' => 'PUT']) !!}
                    <div class="col-md-12 mt-2 mb-2">
                        <label>Estado</label>
                        {!! Form::select('estado', $estados_container, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado', 'required']) !!}
                    </div>
                    <div class="col-md-12 mt-2 mb-2">
                        <button type="submit" class="btn btn-info">Cambiar estado</button>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" id="btnModalClose" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>