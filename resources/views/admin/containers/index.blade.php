@extends('admin.master')
@section('titulo-pagina', 'Containers')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.containers.create') }}" class="btn btn-fill btn-success">Crear Container</a>
                </div>
                <div class="header mb-2">
                    {!! Form::open(['route' => 'admin.containers.index', 'method' => 'GET']) !!}
                        <div class="form-group">
                            <label for="q">Buscar</label>
                            {!! Form::text('q', null, ['class' => 'form-control', 'placeholder' => 'Buscar por nombre del contenedor ..']) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Buscar</button>
                        @if(isset($q) && !is_null($q))
                            <a href="{{ route('admin.containers.index') }}" class="btn btn-warning">Limpiar busqueda</a>
                        @endif
                    {!! Form::close() !!}
                </div>
                <div class="content">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Cantidad de paquetes</th>
                                <th>Estado del contenedor</th>
                                <th>Fecha de registro</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($containers->count() === 0)
                                <tr>
                                    <td colspan="6" class="text-center">
                                        No se ha registrado ningun container hasta ahora
                                    </td>
                                </tr>
                            @endif
                            @foreach($containers as $container)
                                <tr>
                                    <td>{{ $container->id }}</td>
                                    <td>{{ $container->nombre }}</td>
                                    <td>{{ $container->paquetes->count() }}</td>
                                    <td>{{ !is_null($container->estado_string) ? $container->estado_string : 'Sin estado' }}</td>
                                    <td>{{ $container->created_at->format('d/m/Y H:i:s') }}</td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#container-paquetes-{{$container->id}}" class="btn btn-primary">Ver paquetes</a>
                                        <a href="#" data-toggle="modal" data-target="#container-estados-{{$container->id}}" class="btn btn-warning">Cambiar estado</a>
                                        <a href="{{ route('admin.containers.edit', [$container->id]) }}" class="btn btn-info">Editar</a>
                                        <a href="{{ route('admin.containers.delete', [$container->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                        @include('admin.containers.paquetes-modal')
                                        @include('admin.containers.estados-modal')
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $containers->appends(request()->input())->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop