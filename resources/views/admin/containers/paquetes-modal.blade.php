<!-- Modal -->
<div class="modal fade" id="container-paquetes-{{$container->id}}" tabindex="-1" role="dialog" aria-labelledby="containerLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="containerLabel">Paquetes del container {{ $container->nombre }}</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Casilla del usuario</th>
                                <th>Nombre</th>
                                <th>Tipo de Flete</th>
                                <th>Servicios Adicionales</th>
                                <th>Estado</th>
                                <th>Peso</th>
                                <th>Volumen</th>
                                <th>Total</th>
                                <th>Fecha de registro</th>
                                <th>Fecha de actualización</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($container->paquetes->count() == 0)
                                <tr>
                                    <td colspan="11" class="text-center">No se ha registrado ningun paquete en este container</td>
                                </tr>
                            @endif
                            @foreach($container->paquetes as $paquete)
                                <tr>
                                    <td>{{ $paquete->usuario->prefix . $paquete->usuario->casilla }}</td>
                                    <td>{{ $paquete->usuario->full_name }}</td>
                                    <td>{{ intval($paquete->tipo_flete) === \App\Enum\TipoFlete::NORMAL ? 'NORMAL' : 'EXPRESS' }}</td>
                                        <th>
                                            <ul>
                                                @if($paquete->consolidacao)
                                                    <li><i>Consolidación ({{ $paquete->consolidacao_trackings }})</i></li>
                                                @endif
                                                @if($paquete->seguro)
                                                    <li><i>Seguro</i></li>
                                                @endif

                                            </ul>
                                        </th>
                                    
                                    @if($paquete->estado == App\Enum\EstadoPaquete::$MIAMI)
                                        <td>En Miami</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$CHINA)
                                        <td>En China</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$TRANSITO)
                                        <td>En Transito</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$PARAGUAY)
                                        <td>En Paraguay</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$BRASIL)
                                        <td>En Brasil</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$OFICINA)
                                        <td>Para Retirar</td>
                                    @elseif($paquete->estado == App\Enum\EstadoPaquete::$RETIRADO)
                                        <td>Retirado</td>
                                    @endif
                                    <td>{{ $paquete->gramos / 1000 }} Kg.</td>
                                    <td>{{ !is_null($paquete->volumen) ? $paquete->volumen : 0 }}</td>
                                    <td>US$ {{ $paquete->valor_total }}</td>
                                    <td>{{ $paquete->created_at->format('d/m/Y H:i:s') }}</td>
                                    <td>{{ $paquete->updated_at->format('d/m/Y H:i:s') }}</td>
                                    <td>
                                        <a href="{{ route('admin.paquetes.edit', [$paquete->id]) }}" class="btn btn-info">Editar</a>
                                        <a href="{{ route('admin.paquetes.delete', [$paquete->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnModalClose" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>