@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo Container')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.containers.store', 'method' => 'post', 'files' => true]) !!}
                        @include('admin.containers.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop