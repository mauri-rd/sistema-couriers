<div class="row">
    <div class="col-md-12 mt-2 mb-2">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del container', 'required', 'autofocus']) !!}
    </div>

    <div class="col-md-12 mt-2 mb-2">
        <label>Estado</label>
        {!! Form::select('estado', $estados_container, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado', 'required']) !!}
    </div>

    <div class="col-md-12 mt-2 mb-2">
        <button type="submit" class="btn btn-info btn-fill pull-right">Guardar</button>
    </div>
</div>


<div class="clearfix"></div>