@extends('admin.master')
@section('titulo-pagina', 'Editar Container')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($container, ['route' => ['admin.containers.update', $container->id], 'method' => 'PUT', 'files' => true]) !!}
                        @include('admin.containers.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop