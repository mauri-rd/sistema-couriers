<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del multimedia', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Categoría</label>
        {!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

{{--!<div class="row">
    <div class="col-md-12">
        <label>Descripción</label>
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripción del multimedia']) !!}
    </div>
</div>--}}

<div class="row">
    <div class="col-md-12">
        <label>Link YouTube</label>
        {!! Form::text('link_youtube', null, ['class' => 'form-control', 'placeholder' => 'Link de YouTube, del tipo https://www.youtube.com/watch?v=BOUfPWKdAas']) !!}
        <p class="help-block">Si este campo está completo el campo de imagen será ignorado</p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('inhome', 'Mostrar en Inicio') !!}
        {!! Form::checkbox('inhome', false, null) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imágen</label>
        {!! Form::file('url' . ($multiple != '' ? '[]' : ''), ['accept' => 'image/*', $multiple]) !!}
        @if(isset($multimedia))
            @if($multimedia->isImg)
                <img src="{{ $multimedia->url }}" height="200px">
            @endif
        @endif
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>