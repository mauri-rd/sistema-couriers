@extends('admin.master')
@section('titulo-pagina', 'Editar Multimedia')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($multimedia, ['route' => ['admin.multimedias.update', $multimedia->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.multimedias.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop