@extends('admin.master')
@section('titulo-pagina', 'Empresas')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.empresas.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($empresas as $empresa)
                            <tr>
                                <td>{{ $empresa->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.empresas.edit', [$empresa->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.empresas.delete', [$empresa->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $empresas->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop