@extends('admin.master')
@section('titulo-pagina', 'Editar empresa')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($empresa, ['route' => ['admin.empresas.update', $empresa->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.empresas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop