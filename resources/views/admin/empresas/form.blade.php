<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la empresa/página', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Link</label>
        {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'Link de la empresa/página', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imagen</label>
        {!! Form::file('imagen_url', ['class' => 'form-control', 'accept' => 'image/*', 'required']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>