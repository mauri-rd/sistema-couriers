@extends('admin.master')
@section('titulo-pagina', 'Posts')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.posts.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Categoría</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->titulo }}</td>
                                <td>{{ $post->categoria->nombre }}</td>
                                <td>
                                    <a href="{{ route('admin.posts.edit', [$post->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.posts.delete', [$post->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $posts->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop