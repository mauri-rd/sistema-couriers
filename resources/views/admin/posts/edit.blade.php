@extends('admin.master')
@section('titulo-pagina', 'Editar Post')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($post, ['route' => ['admin.posts.update', $post->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.posts.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop