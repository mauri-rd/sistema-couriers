<div class="row">
    <div class="col-md-12">
        <label>Título</label>
        {!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Título del Post', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Categoría</label>
        {!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Cuerpo</label>
        {!! Form::textarea('cuerpo', null, ['class' => 'form-control', 'placeholder' => 'Cuerpo del Post']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imagen principal</label>
        {!! Form::file('img_url', ['class' => 'form-control', 'accept' => 'image/*']) !!}
    </div>
    @if(isset($post))
        <div class="col-md-12">
            <strong>Imagen actual</strong>
        </div>
        <div class="col-md-12">
            <img src="{{ $post->img_url }}" style="height: 100px;">
        </div>
    @endif
</div>

<div class="row">
    <div class="col-md-12">
        <label>Posición en el panel principal</label>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <label for="no-frente" class="col-md-12 post-posicion">No mostrar en portada</label>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label for="posicion-1" class="col-md-12 post-posicion post-posicion-100">Primera</label>
                            <label for="posicion-2" class="col-md-12 post-posicion post-posicion-100">Segunda</label>
                            <label for="posicion-3" class="col-md-12 post-posicion post-posicion-100">Tercera</label>
                        </div>
                    </div>
                    <label for="posicion-4" class="col-md-6 post-posicion post-posicion-300">Cuarta</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="checkbox">
                    <label>
                        {!! Form::radio('posicion', 0, isset($post) ? true : null, ['id' => 'no-frente'] ) !!} No mostrar en portada
                    </label>
                    {{-- <input type="radio" id="no-frente" name="posicion" value="0" {{ !isset($post) ? 'checked' : '' }}> No mostrar en portada --}}
                </div>
                <div class="checkbox">
                    <label>
                        {!! Form::radio('posicion', 1, null, ['id' => 'posicion-1'] ) !!} Posición 1
                    </label>
                    {{-- <input type="radio" id="posicion-1" name="posicion" value="1"> Posición 1 --}}
                </div>
                <div class="checkbox">
                    <label>
                        {!! Form::radio('posicion', 2, null, ['id' => 'posicion-2'] ) !!} Posición 2
                    </label>
                    {{-- <input type="radio" id="posicion-2" name="posicion" value="2"> Posición 2 --}}
                </div>
                <div class="checkbox">
                    <label>
                        {!! Form::radio('posicion', 3, null, ['id' => 'posicion-3'] ) !!} Posición 3
                    </label>
                    {{-- <input type="radio" id="posicion-3" name="posicion" value="3"> Posición 3 --}}
                </div>
                <div class="checkbox">
                    <label>
                        {!! Form::radio('posicion', 4, null, ['id' => 'posicion-4'] ) !!} Posición 4
                    </label>
                    {{-- <input type="radio" id="posicion-4" name="posicion" value="4"> Posición 4 --}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Imagenes/Videos</label>
    </div>
    @foreach($multimedias as $multimedia)
        <div class="col-md-3">
            <label>{!! Form::checkbox(
            'multimedias[]',
            $multimedia->id,
            isset($post) ? in_array($multimedia->id, $selected) : false
            ) !!} {{ $multimedia->nombre }}</label>
            @if(isset($multimedia))
                @if($multimedia->isImg)
                    <img src="{{ $multimedia->url }}" width="100%">
                @else
                    <video width="100%" controls>
                        <source src="{{ $multimedia->url }}">
                    </video>
                @endif
            @endif
        </div>
    @endforeach
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>