@extends('admin.master')
@section('titulo-pagina', 'Editar Producto')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($producto, ['route' => ['admin.productos.update', $producto->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.productos.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <div class="row hidden" id="image-example">
        <div class="col-md-12">
            {!! Form::file('imagenes[]', ['accept' => 'image/*']) !!}
        </div>
    </div>
    <script>
        function addImage() {
            var html = $('#image-example').html();
            $('#images-container').append('<div class="row">' + html + '</div>')
        }
    </script>
@stop