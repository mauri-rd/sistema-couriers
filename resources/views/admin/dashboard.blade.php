@extends('admin.master')
@section('titulo-pagina', 'Dashboard')
@section('contenido')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    Reporte de paquetes
                </h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card-counter primary" style="min-height: 200px">
                            {{-- <i class="fa fa-code-fork"></i> --}}
                            <span class="count-numbers">{{ $enTransito }}</span>
                            <span class="count-name" style="margin-top: 30px">Cantidad de paquetes en tránsito</span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card-counter danger" style="min-height: 200px">
                            {{-- <i class="fa fa-ticket"></i> --}}
                            <span class="count-numbers">{{ $volumenTransito }}</span>
                            <span class="count-name" style="margin-top: 30px">Volumen total de paquetes en tránsito</span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card-counter success" style="min-height: 200px">
                            {{-- <i class="fa fa-database"></i> --}}
                            <span class="count-numbers">{{ $enOficina }}</span>
                            <span class="count-name" style="margin-top: 30px">Cantidad de paquetes en oficina</span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card-counter info" style="min-height: 200px">
                            {{-- <i class="fa fa-users"></i> --}}
                            <span class="count-numbers">{{ $volumenOficina }}</span>
                            <span class="count-name" style="margin-top: 30px">Volumen total de paquetes en oficina</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <style>
        .card-counter {
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover {
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary {
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger {
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success {
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info {
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i {
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers {
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name {
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }

        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }

        .card-body {
            flex: 1 1 auto;
            padding: 1.25rem;
        }

        .card-title {
            margin-bottom: 0.75rem;
        }

        .card-subtitle {
            margin-top: -0.375rem;
            margin-bottom: 0;
        }

        .card-text {
            margin-top: -0.25rem;
        }

        .card-link {
            text-decoration: none;
        }

        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-footer {
            padding: 0.75rem 1.25rem;
            background-color: rgba(0, 0, 0, 0.03);
            border-top: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-img-top {
            width: 100%;
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
        }

        .card-img-bottom {
            width: 100%;
            border-bottom-left-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }

        .card-header-tabs {
            margin-right: -0.625rem;
            margin-bottom: -1px;
            margin-left: -0.625rem;
            border-bottom: 0;
        }

        .card-header-pills {
            margin-right: -0.625rem;
            margin-left: -0.625rem;
        }

        .card-header-pills .nav-link {
            border-radius: 0.25rem;
        }

        .card-header-pills .nav-link.active {
            margin-bottom: -1px;
            border-bottom: 2px solid #fff;
        }

        .card-group>.card {
            margin-bottom: 15px;
        }
    </style>
@endsection
