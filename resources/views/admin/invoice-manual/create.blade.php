@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo invoice manual')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.invoice-manual.store', 'method' => 'post']) !!}
                    @include('admin.invoice-manual.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
    <script>
        (function () {
            ['.price-text', '.quantity-text'].forEach(function (itemClass) {
                $(itemClass).on('keyup', function () {
                    calculate($(this).index(itemClass));
                });
            });

            $('#item-main').on('keyup', function () {
                repeatItem($(this).val());
            });

            $('#id_user').on('change', function () {
                getUserData($(this).val());
            });

            function getUserData(userId) {
                $.ajax({
                    url: '{{ route('admin.invoice-manual.get-user-data') }}',
                    data: {
                        usuario: userId
                    },
                    success: function(res) {
                        $('#user_data').val(res);
                    }
                })
            }

            function repeatItem(val) {
                $('.item-text').val(val);
            }

            function calculate(index) {
                $('.amount-text').eq(index).val(
                    Number($('.price-text').eq(index).val()) *
                    Number($('.quantity-text').eq(index).val())
                );
                calculateTotal();
            }

            function calculateTotal() {
                const total = $('.amount-text').toArray().reduce(function(a, b) {
                    if (!b.value) {
                        return a;
                    }
                    return a + Number(b.value);
                }, 0);
                $('#total').html(total);
                $('#subtotal').html(total);
            }

            function calculateBalance() {
                $('#balance-due').html(
                    Number($('#total').html()) - Number($('#-paid').html())
                );
            }

            repeatItem($('#item-main').val());
            getUserData($('#id_user').val());
        })();
    </script>
@stop