<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Imprimir Invoice Manual #{{ $invoice->number }}</title>
    <link href="{{ url('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">

        <div class="row top">
            <div class="col-md-6 col-print-6">
                <div style="height: 100px;">{!! nl2br($invoice->header) !!}</div>
                <div>
                    {!! $invoice->usuario->full_name !!} <br>
                    {!! nl2br($invoice->user_data) !!}
                </div>
            </div>
            <div class="col-md-6 col-print-6 text-right">
                <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 130px; margin: 10px 0;">
                <h1>INVOICE</h1>
                <div class="row">
                    <div class="col-md-6 col-print-6">
                        <strong>Invoice #</strong>
                    </div>
                    <div class="col-md-6 col-print-6">{{ $invoice->number }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-print-6">
                        <strong>Invoice Date</strong>
                    </div>
                    <div class="col-md-6 col-print-6">{{ $invoice->invoice_date->format('d/m/Y') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-print-6">
                        <strong>Due Date</strong>
                    </div>
                    <div class="col-md-6 col-print-6">{{ $invoice->due_date->format('d/m/Y') }}</div>
                </div>

            </div>
        </div>

        <div class="row" id="invoice">
            <table class="table">
                <thead class="gray-bg">
                <tr>
                    <th>Item</th>
                    <th>Description</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoice->items as $item)
                    <tr>
                        <td>{{ $item->item }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ number_format($item->price, 2) }}</td>
                        <td>{{ number_format($item->quantity, 2) }}</td>
                        <td>{{ number_format($item->amount, 2) }}</td>
                    </tr>
                @endforeach
                <tr class="limit">
                    <td colspan="2"></td>
                    <td colspan="3" class="totales">
                        <div class="row">
                            <div class="col-md-6 col-print-6 text-left">
                                <strong>Subtotal</strong>
                            </div>
                            <div class="col-md-6 col-print-6 text-right">{{ number_format($invoice->total, 2) }}</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="3" class="limit totales">
                        <div class="row">
                            <div class="col-md-6 col-print-6 text-left">
                                <strong>Total</strong>
                            </div>
                            <div class="col-md-6 col-print-6 text-right">{{ number_format($invoice->total, 2) }}</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="3" class="totales">
                        <div class="row">
                            <div class="col-md-6 col-print-6 text-left">
                                <strong>Amount Paid</strong>
                            </div>
                            <div class="col-md-6 col-print-6 text-right">{{ number_format(0, 2) }}</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="3" class="limit totales gray-bg">
                        <div class="row">
                            <div class="col-md-6 col-print-6 text-left">
                                <strong>Balance Due</strong>
                            </div>
                            <div class="col-md-6 col-print-6 text-right">${{ number_format($invoice->total, 2) }}</div>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>

    <style>
        #invoice table {
            border: 1px solid #000000;
        }

        #invoice .table>tbody>tr>td {
            border: 0;
        }

        #invoice .table>thead>tr>th {
            border-bottom: 1px solid #000;
        }

        .gray-bg {
            background-color: #d3d3d3!important;
            -webkit-print-color-adjust: exact;
        }

        .limit {
            border-top: 1px solid #000000!important;
        }

        .totales {
            border-left: 1px solid #000000!important;
        }

        .top {
            margin-bottom: 50px;
        }

        /* BOOTSTRAP */
        .col-print-1 {width:8%;  float:left;}
        .col-print-2 {width:16%; float:left;}
        .col-print-3 {width:25%; float:left;}
        .col-print-4 {width:33%; float:left;}
        .col-print-5 {width:42%; float:left;}
        .col-print-6 {width:50%; float:left;}
        .col-print-7 {width:58%; float:left;}
        .col-print-8 {width:66%; float:left;}
        .col-print-9 {width:75%; float:left;}
        .col-print-10{width:83%; float:left;}
        .col-print-11{width:92%; float:left;}
        .col-print-12{width:100%; float:left;}

    </style>

    <script>
        window.print();
    </script>
</body>
</html>