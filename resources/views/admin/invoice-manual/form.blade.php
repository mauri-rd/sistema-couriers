<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('header', 'Cabecera') !!}
            {!! Form::textarea('header', !is_null($last) ? $last->header : null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('id_user', 'Usuario') !!}
            {!! Form::select('id_user', $usuarios, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('user_data', 'Datos de usuario') !!}
            {!! Form::textarea('user_data', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('number', 'Invoice #') !!}
            {!! Form::number('number', !is_null($last) ? $last->number + 1 : 500000, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('invoice_date', 'Invoice Date') !!}
            {!! Form::date('invoice_date', $now, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('due_date', 'Due Date') !!}
            {!! Form::date('due_date', $now, ['class' => 'form-control']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

    </div>
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th>Item</th>
            <th>Description</th>
            <th>Unit Price</th>
            <th>Quantity</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody id="invoice-items">
    <tr>
        <td>
            {!! Form::label('item-main', 'Item') !!}
            {!! Form::text('item-main', 'Product', ['class' => 'form-control']) !!}
        </td>
    </tr>
    @for($i = 0; $i < 11; $i++)
        <tr>
            <td>{!! Form::text('item[]', null, ['class' => 'form-control item-text', 'placeholder' => 'Item']) !!}</td>
            <td>{!! Form::text('description[]', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}</td>
            <td>{!! Form::number('price[]', null, ['class' => 'form-control price-text', 'placeholder' => 'Unit Price']) !!}</td>
            <td>{!! Form::number('quantity[]', null, ['class' => 'form-control quantity-text', 'placeholder' => 'Quantity']) !!}</td>
            <td>{!! Form::number('amount[]', null, ['class' => 'form-control amount-text', 'placeholder' => 'Amount', 'readonly']) !!}</td>
        </tr>
    @endfor
    <tr>
        <td colspan="2"></td>
        <td colspan="3">
            <strong>Subtotal:</strong> <span id="subtotal"></span>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="3">
            <strong>Total:</strong> <span id="total"></span>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="3">
            <strong>Amount Paid:</strong> <span id="amount-paid">{{ isset($paid) ? $paid : 0 }}</span>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="3">
            <strong>Balance Due:</strong> <span id="balance-due"></span>
        </td>
    </tr>

    </tbody>
</table>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>