@extends('admin.master')
@section('titulo-pagina', 'Invoice #' . $invoice->id)
@section('contenido')
    <div class="row">
        <div class="col-md-12" style="width: 100%">
            <div class="x_panel">
                <div class="content">

                    <div class="row">
                        <div class="col-md-6">
                            <div style="height: 100px;">{!! nl2br($invoice->header) !!}</div>
                            <div>
                                {!! $invoice->usuario->full_name !!} <br>
                                {!! nl2br($invoice->user_data) !!}
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 130px; margin: 10px 0;">
                            <h1>INVOICE</h1>
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Invoice #</strong>
                                </div>
                                <div class="col-md-6">{{ $invoice->number }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Invoice Date</strong>
                                </div>
                                <div class="col-md-6">{{ $invoice->invoice_date->format('d/m/Y') }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Due Date</strong>
                                </div>
                                <div class="col-md-6">{{ $invoice->due_date->format('d/m/Y') }}</div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>Description</th>
                                <th>Unit Price</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoice->items as $item)
                                <tr>
                                    <td>{{ $item->item }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>{{ number_format($item->price, 2) }}</td>
                                    <td>{{ number_format($item->quantity, 2) }}</td>
                                    <td>{{ number_format($item->amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <strong>Subtotal</strong>
                                        </div>
                                        <div class="col-md-6 text-right">{{ number_format($invoice->total, 2) }}</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <strong>Total</strong>
                                        </div>
                                        <div class="col-md-6 text-right">{{ number_format($invoice->total, 2) }}</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <strong>Amount Paid</strong>
                                        </div>
                                        <div class="col-md-6 text-right">0</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <strong>Balance Due</strong>
                                        </div>
                                        <div class="col-md-6 text-right">${{ number_format($invoice->total, 2) }}</div>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <a href="{{ route('admin.invoice-manual.print-view', [$invoice->id]) }}" class="btn btn-primary">Imprimir</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <style>
        #invoice {
            margin: 20px;
            font-family: sans-serif;
        }

        #invoice .invoice-logo {
            text-align: center;
            background-color: #211973;
        }

        #invoice .invoice-header {
            padding: 20px;
            text-align: right;
        }

        #invoice .invoice-table {
            width: 95%;
            margin: 0 auto;
            border-collapse: collapse
        }

        #invoice .invoice-table thead {
            border-bottom: 2px solid #aaa;
        }

        #invoice .invoice-table thead th {
            text-align: center;
        }

        #invoice .invoice-table tbody tr {
            border-top: solid 1px #ddd; text-align: center;
        }
        
        #invoice .invoice-table tbody td {
            padding: 12px 0;
        }

        #invoice .invoice-footer {
            padding-bottom: 20px;
            text-align: right;
        }

        .signature {
            display: none;
            border-top: 1px solid #000000;
            padding: 15px 80px;
            text-align: left;
        }

        @media print {
            .nav_menu, footer, .print {
                display: none;
            }

            .signature {
                display: inline;
            }
        }
    </style>
@endsection