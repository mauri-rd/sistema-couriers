@extends('admin.master')
@section('titulo-pagina', 'Invoices')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    {!! Form::open(['route' => 'admin.invoice-manual.index', 'method' => 'GET']) !!}
                        <div class="form-group">
                            <label for="usuario">Filtrar por usuario</label>
                            {!! Form::select('usuario', $usuarios, $usuario, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Filtrar</button>
                            <a href="{{ route('admin.invoice-manual.create') }}" class="btn btn-primary">Registrar invoice</a>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre del usuario</th>
                            <th>Total</th>
                            <th>Fecha de creación</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ $invoice->usuario->full_name }}</td>
                                <td>US$ {{ $invoice->total }}</td>
                                <td>{{ $invoice->created_at->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('admin.invoice-manual.show', [$invoice->id]) }}" class="btn btn-info">
                                        <i class="fa fa-eye"></i> Ver Invoice
                                    </a>
                                    <a href="{{ route('admin.invoice-manual.print-view', [$invoice->id]) }}" class="btn btn-info">
                                        <i class="fa fa-print"></i> Imprimir Invoice
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $invoices->appends(request()->input())->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
    <style>
        .tracking-list {
            list-style-type: none;
        }
    </style>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-select.min.js') }}"></script>
@endsection