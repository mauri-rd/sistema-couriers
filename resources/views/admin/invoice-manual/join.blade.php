@extends('admin.master')
@section('titulo-pagina', 'Invoice Generado')
@section('contenido')
    <div class="row">
        <div class="col-md-12" style="width: 100%">
            <div class="x_panel">
                <div class="content">
                    <div id="invoice">
                        <div class="invoice-logo">
                            <img src="{{ url('assets/img/logo-strbox.png') }}" alt="StrBox" style="height: 80px; margin: 10px 0;">
                        </div>
                        <div class="invoice-header">
                            <h1>Invoice Generado</h1>
                            <h1>Para: {{ $usuario->full_name }}</h1>
                            <h1>ID: {{ $usuario->id }}</h1>
                            <h3>Data do Invoice: {{ $date->format('d/m/Y H:i:s') }}</h3>
                        </div>
                        <div>
                            <table class="invoice-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Descrição</th>
                                    <th>Tracking</th>
                                    <th>Peso</th>
                                    <th>Volumen</th>
                                    <th>Preço unitario</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $index => $item)
                                    <tr>
                                        <td style="padding: 5px 10px;">{{ $index + 1 }}</td>
                                        <td style="padding: 5px 10px;">{{ $item->paquete->descripcion }}</td>
                                        <td style="padding: 5px 10px;">{{ $item->paquete->tracking }}</td>
                                        <td style="padding: 5px 10px;">{{ $item->paquete->gramos / 1000 }} Kg.</td>
                                        <td style="padding: 5px 10px;">{{ $item->paquete->volumen }}</td>
                                        <td style="padding: 5px 10px;">US$ {{ $item->paquete->precio_kg }}</td>
                                        <td style="padding: 5px 10px;">US$ {{ $item->paquete->valor_total }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="invoice-footer">
                            <h1>Total: US$ {{ $total }}</h1>
                            <button class="btn btn-primary print">
                                <i class="fa fa-print"></i> Imprimir
                            </button>
                        </div>
                        <span class="signature">
                                {{ $usuario->name }}
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <style>
        #invoice {
            margin: 20px;
            font-family: sans-serif;
        }

        #invoice .invoice-logo {
            text-align: center;
            background-color: #211973;
        }

        #invoice .invoice-header {
            padding: 20px;
            text-align: right;
        }

        #invoice .invoice-table {
            width: 95%;
            margin: 0 auto;
            border-collapse: collapse
        }

        #invoice .invoice-table thead {
            border-bottom: 2px solid #aaa;
        }

        #invoice .invoice-table thead th {
            text-align: center;
        }

        #invoice .invoice-table tbody tr {
            border-top: solid 1px #ddd; text-align: center;
        }

        #invoice .invoice-table tbody td {
            padding: 12px 0;
        }

        #invoice .invoice-footer {
            padding-bottom: 20px;
            text-align: right;
        }

        .signature {
            display: none;
            border-top: 1px solid #000000;
            padding: 15px 80px;
            text-align: left;
        }

        @media print {
            .nav_menu, footer, .print {
                display: none;
            }

            .signature {
                display: inline;
            }
        }
    </style>
@endsection
@section('js')
    <script>
        $('.print').click(function() {
            print();
        });
    </script>
@endsection