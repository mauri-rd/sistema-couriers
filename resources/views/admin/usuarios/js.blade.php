<script src="{{ url('js/bootstrap-select.min.js') }}"></script>
<script>
    function checkUserOperador() {
        $('.user-form-operador-only').css({ display: $('#type').val().toString() === '{{ \App\Enum\UserType::OPERADOR }}' ? 'block' : 'none' })
    }

    function checkUserOperadorCaja() {
        $('.user-form-operador-caja-only').css({
            display: ( $('#type').val().toString() === '{{ \App\Enum\UserType::OPERADOR }}' && $('#operador_caja:checked').length > 0 ) ? 'block' : 'none'
        })
    }

    $('#type').on('change', function () {
        checkUserOperador()
        checkUserOperadorCaja()
    })

    $('#operador_caja').on('change', function () {
        checkUserOperadorCaja()
    })

    checkUserOperador()
    checkUserOperadorCaja()
</script>