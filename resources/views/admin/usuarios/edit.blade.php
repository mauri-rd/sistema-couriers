@extends('admin.master')
@section('titulo-pagina', 'Editar Usuario')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($usuario, ['route' => ['admin.usuarios.update', $usuario->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.usuarios.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop
@section('js')
    @include('admin.usuarios.js')
@stop