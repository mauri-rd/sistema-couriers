<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del usuario', 'required', 'autofocus']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Apellido</label>
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Apellido del usuario', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Teléfono</label>
        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Teléfono del usuario']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Fecha de Nacimiento</label>
        {!! Form::date('birthday', null, ['class' => 'form-control', 'placeholder' => 'Fecha de nacimiento']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Dirección</label>
        {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Dirección del usuario']) !!}
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>País</label>--}}
        {{--{!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'País del usuario']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Ciudad</label>--}}
        {{--{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Ciudad del usuario']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Estado</label>--}}
        {{--{!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'Estado del usuario']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Código Postal</label>--}}
        {{--{!! Form::text('zip', null, ['class' => 'form-control', 'placeholder' => 'Código Postal del usuario']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label>E-mail</label>
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail del usuario', 'autocomplete' => 'new-email']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Contraseña</label>
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña del usuario', 'autocomplete' => 'new-password']) !!}
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<label>Casilla</label>--}}
        {{--{!! Form::text('casilla', null, ['class' => 'form-control', 'placeholder' => 'Casilla del usuario']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-12">
        <label>Sucursal</label>
        {!! Form::select('id_sucursal', $sucursales, null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>
@if(auth()->user()->type !== \App\Enum\UserType::OPERADOR)
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('type', 'Tipo de Usuario') !!}
            {!! Form::select('type', $tipos, isset($registerCliente) ? \App\Enum\UserType::CLIENTE :  null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_paquetes', 'Permitir ver paquetes (operadores)') !!}
            {!! Form::checkbox('operador_paquetes', 1, isset($usuario) ? $usuario->operador_paquetes : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_caja', 'Permitir ver caja (operadores)') !!}
            {!! Form::checkbox('operador_caja', 1, isset($usuario) ? $usuario->operador_caja : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-caja-only">
        <div class="col-md-12">
            {!! Form::label('id_caja[]', 'Cajas habilitadas (operadores)') !!}
            {!! Form::select('id_caja[]', $cajas, isset($usuario) ? $usuario->cajas()->pluck('id_caja')->toArray() : null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple']) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_invoices', 'Permitir ver invoices (operadores)') !!}
            {!! Form::checkbox('operador_invoices', 1, isset($usuario) ? $usuario->operador_invoices : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_pagos', 'Permitir ver pagos (operadores)') !!}
            {!! Form::checkbox('operador_pagos', 1, isset($usuario) ? $usuario->operador_pagos: false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_usuarios', 'Permitir ver usuarios (operadores)') !!}
            {!! Form::checkbox('operador_usuarios', 1, isset($usuario) ? $usuario->operador_usuarios: false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_mensajes', 'Permitir ver mensajes (operadores)') !!}
            {!! Form::checkbox('operador_mensajes', 1, isset($usuario) ? $usuario->operador_mensajes : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_sucursales', 'Permitir ver sucursales (operadores)') !!}
            {!! Form::checkbox('operador_sucursales', 1, isset($usuario) ? $usuario->operador_sucursales : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_reportes', 'Permitir ver reportes (operadores)') !!}
            {!! Form::checkbox('operador_reportes', 1, isset($usuario) ? $usuario->operador_reportes : false) !!}
        </div>
    </div>

    <div class="row user-form-operador-only">
        <div class="col-md-12">
            {!! Form::label('operador_informaciones', 'Permitir ver informaciones (operadores)') !!}
            {!! Form::checkbox('operador_informaciones', 1, isset($usuario) ? $usuario->operador_informaciones : false) !!}
        </div>
    </div>

@endif
@if(!isset($usuario))
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('crear_casilla', 'Crear casilla') !!}
            {!! Form::checkbox('crear_casilla', 1, false) !!}
        </div>
    </div>
@endif

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>