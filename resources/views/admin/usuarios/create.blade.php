@extends('admin.master')
@section('titulo-pagina', 'Registrar nuevo Usuario')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.usuarios.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.usuarios.form')
                    @if($closeWhenFinished && !is_null($closeWhenFinished))
                        {!! Form::hidden('closeWhenFinished', 1) !!}
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css') }}">
@stop
@section('js')
    @include('admin.usuarios.js')
@stop