@extends('admin.master')
@section('titulo-pagina', 'Usuarios')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.usuarios.create') }}" class="btn btn-fill btn-success">Registrar</a>
                    <a href="{{ route('admin.usuarios.exportar-csv') }}" class="btn btn-fill btn-success">Exportar planilla CSV</a>
                </div>
                <div class="header">
                    {!! Form::open(['route' => 'admin.usuarios.index', 'method' => 'GET', 'class' => 'form-inline']) !!}
                        <div class="form-group">
                            <label for="q">Buscar</label>
                            {!! Form::text('q', $prevQuery, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="query">Buscar por</label>
                            {!! Form::select('queryby', $queryBy, $prevQueryBy, ['class' => 'form-control']) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Buscar</button>
                        @if(!is_null($prevQuery))
                            <a href="{{ route('admin.usuarios.index') }}" class="btn btn-warning">Limpiar busqueda</a>
                        @endif
                    {!! Form::close() !!}
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Casilla</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>E-mail</th>
                            {{--<th>Teléfono</th>--}}
                            {{--<th>Documento</th>--}}
                            {{-- <th>Edad</th> --}}
                            <th>Fecha de registro</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($usuarios as $usuario)
                            <tr>
                                <td>{!! isset($usuario->sucursal) ? $usuario->sucursal->nombre : 'Sin Sucursal' !!}</td>
                                <td>{{ $usuario->prefix . $usuario->casilla }}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{!! !is_null($usuario->last_name) && $usuario->last_name !== '' ? $usuario->last_name : '--' !!}</td>
                                <td>{!! !is_null($usuario->email) ? $usuario->email : 'Sin Email' !!}</td>
                                {{--<td>{{ $usuario->phone }}</td>--}}
                                {{--<td>--}}
                                    {{--@if($usuario->ci == '')--}}
                                        {{--<i>Sin documento cargado</i>--}}
                                    {{--@else--}}
                                        {{--<a target="_blank" href="{{ url($usuario->ci) }}" class="btn btn-primary">--}}
                                            {{--Ver documento--}}
                                        {{--</a>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td>{{ $usuario->created_at->format('d/m/Y') }}</td>
                                <td>
                                    <a href="{{ route('admin.usuarios.edit', [$usuario->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.usuarios.delete', [$usuario->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                    <a href="{{ route('admin.usuarios.activate', [$usuario->id, 'state' => $usuario->active ? 0 : 1]) }}" class="btn btn-warning">{{ $usuario->active ? 'Desactivar' : 'Activar' }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $usuarios->appends(request()->input())->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @if(session()->get('closeWhenFinished'))
        <script>
            window.close()
        </script>
    @endif
@stop