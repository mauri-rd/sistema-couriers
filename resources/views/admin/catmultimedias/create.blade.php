@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva categoría de multimedias')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.catmultimedias.store', 'method' => 'post']) !!}
                    @include('admin.catmultimedias.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop