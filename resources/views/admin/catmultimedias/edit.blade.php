@extends('admin.master')
@section('titulo-pagina', 'Editar categoría de multimedias')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($cat, ['route' => ['admin.catmultimedias.update', $cat->id], 'method' => 'post']) !!}
                    @include('admin.catmultimedias.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop