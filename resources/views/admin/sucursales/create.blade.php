@extends('admin.master')
@section('titulo-pagina', 'Registrar nueva sucursal')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::open(['route' => 'admin.sucursales.store', 'method' => 'post', 'files' => true]) !!}
                    @include('admin.sucursales.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop