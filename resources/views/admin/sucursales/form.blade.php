<div class="row">
    <div class="col-md-12">
        <label>Nombre</label>
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la sucursal', 'required', 'autofocus']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label>Prefijo</label>
        {!! Form::text('prefix', null, ['class' => 'form-control', 'placeholder' => 'Prefijo de la sucursal', 'required']) !!}
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">Guardar Cambios</button>
<div class="clearfix"></div>