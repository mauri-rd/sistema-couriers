@extends('admin.master')
@section('titulo-pagina', 'Editar sucursal')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="content">
                    {!! Form::model($sucursal, ['route' => ['admin.sucursales.update', $sucursal->id], 'method' => 'post', 'files' => true]) !!}
                    @include('admin.sucursales.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop