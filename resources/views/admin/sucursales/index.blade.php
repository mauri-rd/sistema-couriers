@extends('admin.master')
@section('titulo-pagina', 'Sucursales')
@section('contenido')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="header">
                    <a href="{{ route('admin.sucursales.create') }}" class="btn btn-fill btn-success">Registrar</a>
                </div>
                <div class="content">
                    <table class="table table-full-width table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Prefijo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sucursales as $sucursal)
                            <tr>
                                <td>{{ $sucursal->nombre }}</td>
                                <td>{{ $sucursal->prefix }}</td>
                                <td>
                                    <a href="{{ route('admin.sucursales.edit', [$sucursal->id]) }}" class="btn btn-info">Editar</a>
                                    <a href="{{ route('admin.sucursales.delete', [$sucursal->id]) }}" class="btn btn-danger to-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="footer">
                    {!! $sucursales->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop