@extends('front.master')
@section('contenido')
    <section class="home-section bg-dark-alfa-90 parallax-2" data-background="rgba(0,0,0,0.3)">
        <div class="js-height-full">

            <!-- Hero Content -->
            <div class="home-content container">
                <div class="home-text">
                    <div class="hs-cont">

                        <!-- Headings -->


                        <div class="hs-line-9 mb-10">
                            404
                        </div>

                        <div class="hs-line-4 mb-40">
                            La página que está buscando no pudo ser encontrada.
                        </div>

                        <div class="local-scroll">
                            <a href="{{ route('front.home') }}" class="btn btn-mod btn-w btn-medium btn-circle"><i class="fa fa-angle-left"></i> Volver al Inicio</a>
                        </div>


                        <!-- End Headings -->

                    </div>
                </div>
            </div>
            <!-- End Hero Content -->

        </div>
    </section>
@stop