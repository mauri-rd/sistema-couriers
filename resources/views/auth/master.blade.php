<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ url('favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>StrBox | @yield('titulo-pagina')</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ url('css/login.css') }}">
</head>
<body>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="login-page">
    <div class="form">
        <img src="{{ url('assets/img/logo-strbox.png') }}" alt="Logo" class="img-responsive logo">
        @yield('contenido')
    </div>
</div>

<script src="{{ url('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
@yield('js')
</body>
</html>