<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nombre',
        'prefix',
    ];

    public function usuarios()
    {
        return $this->hasMany(User::class, 'id_sucursal', 'id');
    }
}
