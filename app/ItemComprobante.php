<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemComprobante extends Model
{
    protected $fillable = [
        'id_comprobante',
        'id_paquete',
    ];

    public function comprobante()
    {
        return $this->belongsTo(Comprobante::class, 'id_comprobante', 'id');
    }

    public function paquete()
    {
        return $this->belongsTo(Paquete::class, 'id_paquete', 'id');
    }
}
