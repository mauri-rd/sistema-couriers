<?php

namespace App;

use App\Enum\Destino;
use App\Enum\TipoPrecioPaquete;
use Illuminate\Database\Eloquent\Model;

use App\Enum\EstadoPaquete;

class Paquete extends Model
{
    protected $fillable = [
        'id_usuario',
        'id_packlist',
        'codigo',
        'descripcion',
        'cant_cajas',
        'tracking',
        'courier',
        'gramos',
        'precio_kg',
        'precio_fijo',
        'estado',
        'tipo_flete',
        'destino',
        'seguro',
        'consolidacao',
        'consolidacao_trackings',
        'tipo_precio',
        'unidades',
        'rating',
        'wh',
        'volumen',
        'id_container'
    ];

    public function getTotalSumadoAttribute()
    {
        return $this->valor_total + $this->valor_seguro + $this->valor_consolidacao;
    }

    public function getValorTotalAttribute()
    {
        if ($this->attributes['precio_fijo']) {
            return $this->attributes['precio_fijo'];
        }
        if ($this->attributes['tipo_precio'] === TipoPrecioPaquete::POR_UNIDAD) {
            return $this->precio_kg * $this->attributes['unidades'];
        }
        return $this->precio_kg * ($this->gramos/1000);
    }

    public function getValorSeguroAttribute()
    {
        return $this->seguro ? ( $this->valor_total * 0.03 ) : 0;
    }

    public function getValorConsolidacaoAttribute()
    {
        if (!$this->consolidacao_trackings) {
            return 0;
        }
        return count(explode(',', $this->consolidacao_trackings)) * 2;
    }

    public function usuario()
    {
        return $this->belongsTo('App\User', 'id_usuario', 'id');
    }

    public function item()
    {
        return $this->hasOne('App\ItemInvoice', 'id_paquete', 'id');
    }

    public function packlist()
    {
        return $this->belongsTo('App\Packlist', 'id_packlist', 'id');
    }

    public function history()
    {
        return $this->hasMany(PackageHistory::class, 'id_paquete', 'id');
    }

    public function scopeFiltrar($q, $request)
    {
        if (!is_null($request->show)) {
            $q = $q->whereIn('estado', $request->show);
            if ($request->from) {
                // dd($request->from);
                $q = $q->whereHas('history', function ($w) use ($request) {
                    $w->whereIn('estado', $request->show)
                        ->where('created_at', '>=', $request->from);
                });
            }
            if ($request->to) {
                // dd($request->to);
                $q = $q->whereHas('history', function ($w) use ($request) {
                    $w->whereIn('estado', $request->show)
                        ->where('created_at', '<=', $request->to);
                });
            }

        }
        if (!is_null($request->usuario) && $request->usuario != '') {
            $q = $q->where('id_usuario', '=', $request->usuario);
        }
        if (!is_null($request->tracking) && $request->tracking != '') {
            $q = $q->where('tracking', '=', $request->tracking);
        }
        return $q;
    }

    public function scopeFiltrarEstado($q, $estado)
    {
        if (!is_null($estado)) {
            $q = $q->whereIn('estado', $estado);
        }
        return $q;
    }

    public function scopeFiltrarEstadoDestino($q, $estado)
    {
        if (!is_null($estado)) {
            if (in_array(EstadoPaquete::$PARAGUAY, $estado)) {
                $q = $q->where(function ($w) {
                    $w->where('destino', '=', Destino::BRASIL)
                        ->where('estado', '=', EstadoPaquete::$BRASIL);
                })->orWhere(function ($w) {
                    $w->where('destino', '=', Destino::PARAGUAY)
                        ->where('estado', '=', EstadoPaquete::$PARAGUAY);
                });
            } elseif (in_array(EstadoPaquete::$TRANSITO, $estado)) {
                $q = $q->where(function ($w) {
                    $w->where('destino', '=', Destino::BRASIL)
                        ->where('estado', '=', EstadoPaquete::$PARAGUAY);
                })->orWhere(function ($w) {
                    $w->where('destino', '=', Destino::PARAGUAY)
                        ->where('estado', '=', EstadoPaquete::$TRANSITO);
                })->orWhere(function ($w) {
                    $w->where('destino', '=', Destino::BRASIL)
                        ->where('estado', '=', EstadoPaquete::$TRANSITO);
                });
            } else {
                $q = $q->whereIn('estado', $estado);
            }
        }
        return $q;
    }

    public function scopeEnMiami($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$MIAMI);
    }

    public function scopeEnTransito($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$TRANSITO);
    }

    public function scopeEnParaguay($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$PARAGUAY);
    }

    public function scopeEnOficina($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$OFICINA);
    }

    public function scopeRetirado($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$RETIRADO);
    }

    public function scopeEliminado($q)
    {
        return $q->where('estado', '=', EstadoPaquete::$RETIRADO);
    }

    public function scopeActivo($q)
    {
        return $q->where('estado', '<>', EstadoPaquete::$ELIMINADO);
    }
}
