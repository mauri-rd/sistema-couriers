<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemInvoice extends Model
{
    protected $fillable = [
        'id_invoice',
        'id_paquete',
        'pago'
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice', 'id_invoice', 'id');
    }

    public function paquete()
    {
        return $this->belongsTo('App\Paquete', 'id_paquete', 'id');
    }
}
