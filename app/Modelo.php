<?php

namespace App;

class Modelo extends BaseModel
{
    protected $fillable = ['nombre', 'id_marca', 'estado'];

    public function marca()
    {
        return $this->belongsTo('App\Marca', 'id_marca', 'id');
    }

    public function productos()
    {
        return $this->hasMany('App\Producto', 'id_modelo', 'id');
    }
}
