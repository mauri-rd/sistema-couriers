<?php
namespace App\Helpers\Admin;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class SelectHelper
{
    public static function make(Collection $set, $default = null)
    {
        $res = [];
        if (!is_null($default)) {
            $res[0] = $default;
        }
        foreach ($set as $item) {
            $res[$item->id] = $item->nombre;
        }
        return $res;
    }
}