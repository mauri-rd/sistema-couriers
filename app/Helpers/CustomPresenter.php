<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05/03/17
 * Time: 02:31 PM
 */

namespace App\Helpers;


use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Support\HtmlString;

class CustomPresenter extends BootstrapThreePresenter
{
    public function render()
    {
        if ($this->hasPages()) {
            return new HtmlString(sprintf(
                '<div class="pagination">%s %s %s</div>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            ));
        }

        return '';
    }

    protected function getAvailablePageWrapper($url, $page, $rel = null)
    {
        $rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

        return '<a href="'.htmlentities($url).'"'.$rel.'>'.$page.'</a>';
    }

    protected function getDisabledTextWrapper($text)
    {
        return '<a href="" class="no-active">'.$text.'</a>';
    }

    protected function getActivePageWrapper($text)
    {
        return '<a href="" class="active">'.$text.'</a>';
    }
}