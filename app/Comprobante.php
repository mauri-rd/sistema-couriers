<?php

namespace App;

use App\Enum\TipoCambioComprobante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Comprobante extends Model
{
    protected $fillable = [
        'total_usd',
        'total_brl',
        'total_pyg',
        'tipo_cambio',
        'arbitraje',
        'comprobante',
        'aceptado',
        'procesado',
        'id_cliente',
    ];

    public function setComprobanteAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . auth()->id() . "." . $file->getClientOriginalExtension();
            $dir = "uploads/comprobantes";
            $fullPath = $dir . '/' . $name;
            $file->move($dir, $name);
            $this->attributes['comprobante'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['comprobante'];
    }

    public function getComprobanteAttribute($url)
    {
        return url($url);
    }

    public function getMonedaAttribute()
    {
        return $this->attributes['tipo_cambio'] == TipoCambioComprobante::BRL ? 'Reales' : 'Guaranies';
    }

    public function getValorCambioFormateadoAttribute()
    {
        if ($this->attributes['tipo_cambio'] == TipoCambioComprobante::BRL) {
            return 'RS$ ' . number_format($this->attributes['arbitraje'], 2, ',', '.');
        }
        return 'Gs. ' . number_format($this->attributes['arbitraje'], 0, ',', '.');
    }

    public function getTotalCambioFormateadoAttribute()
    {
        if ($this->attributes['tipo_cambio'] == TipoCambioComprobante::BRL) {
            return 'RS$ ' . number_format($this->attributes['total_brl'], 2, ',', '.');
        }
        return 'Gs. ' . number_format($this->attributes['total_pyg'], 0, ',', '.');
    }

    public function cliente()
    {
        return $this->belongsTo(User::class, 'id_cliente', 'id');
    }

    public function items()
    {
        return $this->hasMany(ItemComprobante::class, 'id_comprobante', 'id');
    }

    public function scopeRechazados($q)
    {
        return $q->where('aceptado', false);
    }

    public function scopeAceptados($q)
    {
        return $q->where('aceptado', true);
    }

    public function scopeAbiertos($q)
    {
        return $q->where('procesado', false);
    }

    public function scopeProcesados($q)
    {
        return $q->where('procesado', true);
    }

}
