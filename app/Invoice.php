<?php

namespace App;

use App\Enum\MonedaCaja;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'id_usuario',
        'id_packlist',
        'hidden',
        'total',
    ];

    public function getTotalAttribute()
    {
        return $this->valoritems + $this->valorseguro + $this->valorconsolidacao;
    }

    public function getPagadoAttribute()
    {
        return $this->movimientosFinancieros()->whereHas('caja', function ($w) { $w->whereMoneda(MonedaCaja::DOLARES); })->sum('monto') + $this->movimientosFinancieros()->whereHas('caja', function ($w) { $w->whereMoneda(MonedaCaja::GUARANIES); })->sum('monto_extranjero');
    }

    public function getSaldoAttribute()
    {
        return $this->total - $this->pagado;
    }

    public function getHasSeguroAttribute()
    {
        return $this
            ->items
            ->contains('paquete.seguro', 1);
    }

    public function getValorItemsAttribute()
    {
        return $this->items->sum('paquete.valortotal');
    }

    public function getValorSeguroAttribute()
    {
        return $this->items->sum('paquete.valorseguro');
    }

    public function getValorConsolidacaoAttribute()
    {
        return $this->items->sum('paquete.valor_consolidacao');
    }

    public function usuario()
    {
        return $this->belongsTo('App\User', 'id_usuario', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\ItemInvoice', 'id_invoice', 'id');
    }

    public function packlist()
    {
        return $this->belongsTo('App\Packlist', 'id_packlist', 'id');
    }

    public function movimientosFinancieros()
    {
        return $this->hasMany(MovimientoFinanciero::class, 'id_invoice', 'id');
    }

    public function scopeFiltrar($q, $usuario, $tracking)
    {
        if (!is_null($usuario) && $usuario != '') {
            $q = $q->where('id_usuario', '=', $usuario);
        }
        if (!is_null($tracking) && $tracking != '') {
            $q = $q->whereHas('items.paquete', function($w) use($tracking) {
                $w->where('tracking', '=', $tracking);
            });
        }
        return $q;
    }
}
