<?php

namespace App;

use App\Enum\MonedaCaja;
use App\Enum\TipoMovimientoFinanciero;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class MovimientoFinanciero extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tipo_movimiento',
        'titulo',
        'observaciones',
        'monto',
        'monto_extranjero',
        'cambio_registrado',
        'fecha',
        'id_caja',
        'id_categoria_ingreso',
        'id_categoria_egreso',
        'id_cliente',
        'id_proveedor',
        'id_registrador',
        'id_invoice',
        'id_arqueo',
    ];

    public function getMontoFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->monto);
        }
        return format_dolares($this->monto);
    }

    public function getMontoExtranjeroFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_dolares($this->monto_extranjero);
        }
        return format_guaranies($this->monto_extranjero);
    }

    public function getEsIngresoAttribute()
    {
        return $this->tipo_movimiento == TipoMovimientoFinanciero::INGRESO;
    }

    public function getEsEgresoAttribute()
    {
        return $this->tipo_movimiento == TipoMovimientoFinanciero::EGRESO;
    }

    public function getEsTransferenciaAttribute()
    {
        return $this->tipo_movimiento == TipoMovimientoFinanciero::TRANSFERENCIA;
    }

    public function getFechaDateAttribute()
    {
        return new Carbon($this->attributes['fecha']);
    }

    public function getBackgroundClassAttribute()
    {
        if ($this->es_ingreso) {
            return 'bg-success';
        } elseif ($this->es_egreso) {
            return 'bg-danger';
        } else {
            return 'bg-info';
        }
    }

    public function getIconAttribute()
    {
        if ($this->es_ingreso) {
            return $this->categoriaIngreso->icon;
        } elseif ($this->es_egreso) {
            return $this->categoriaEgreso->icon;
        } else {
            return 'fa-exchange';
        }
    }

    public function caja()
    {
        return $this->belongsTo(Caja::class, 'id_caja', 'id');
    }

    public function arqueos()
    {
        return $this->hasMany(Arqueo::class, 'id_arqueo', 'id');
    }

    public function categoriaIngreso()
    {
        return $this->belongsTo(CategoriaIngreso::class, 'id_categoria_ingreso', 'id');
    }

    public function categoriaEgreso()
    {
        return $this->belongsTo(CategoriaEgreso::class, 'id_categoria_egreso', 'id');
    }

    public function cliente()
    {
        return $this->belongsTo(User::class, 'id_cliente', 'id');
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class, 'id_proveedor', 'id');
    }

    public function registrador()
    {
        return $this->belongsTo(User::class, 'id_registrador', 'id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'id_invoice', 'id');
    }

    public function scopeBuscar($q, Request $request)
    {
        if ($request->has('q')) {
            $q = $q->where('titulo', 'like', "%$request->q%");
        }
        if ($request->has('from')) {
            $q = $q->whereDate('fecha', '>=', $request->from);
        }
        if ($request->has('to')) {
            $q = $q->whereDate('fecha', '<=', $request->to);
        }

        return $q;
    }

}
