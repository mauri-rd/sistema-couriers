<?php

namespace App;

class Marca extends BaseModel
{
    protected $fillable = ['nombre', 'estado'];

    public function modelos()
    {
        return $this->hasMany('App\Modelo', 'id_marca', 'id');
    }
}
