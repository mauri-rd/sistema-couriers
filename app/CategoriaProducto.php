<?php

namespace App;

class CategoriaProducto extends BaseModel
{
    protected $fillable = ['nombre', 'estado'];

    public function productos()
    {
        return $this->hasMany('App\Producto', 'id_categoria', 'id');
    }
}
