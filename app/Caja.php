<?php

namespace App;

use App\Enum\Moneda;
use App\Enum\MonedaCaja;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caja extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nombre',
        'total',
        'moneda',
    ];

    public function getTotalFormateadoAttribute()
    {
        if ($this->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->total);
        }
        return format_dolares($this->total);
    }

    public function getNombreMonedaAttribute()
    {
        return MonedaCaja::nombres()[$this->attributes['moneda']];
    }

    public function getSimboloMonedaAttribute()
    {
        return MonedaCaja::simbolos()[$this->attributes['moneda']];
    }

    public function arqueos()
    {
        return $this->hasMany(Arqueo::class, 'id_caja', 'id');
    }
}
