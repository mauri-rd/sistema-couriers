<?php

namespace App\Http\Controllers\Admin;

use App\Arqueo;
use App\Caja;
use App\Enum\UserType;
use App\MovimientoFinanciero;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CajasController extends Controller
{
    public function index()
    {
        $cajas = Caja::select('id', 'nombre', 'total', 'moneda');
        if (auth()->user()->type == UserType::OPERADOR) {
            $cajas = $cajas->whereIn('id', auth()->user()->cajas->pluck('id_caja')->toArray());

        }
        $cajas = $cajas->paginate(10);
        return view('admin.financiero.cajas.index', ['cajas' => $cajas]);
    }

    public function create()
    {
        return view('admin.financiero.cajas.create');
    }

    public function store(Request $request)
    {
        Caja::create($request->all());

        return redirect()->route('admin.cajas.index')->with(['msg' => 'Caja registrada con éxito!']);
    }

    public function edit($id)
    {
        $caja = Caja::select('id', 'nombre', 'moneda')
            ->find($id);
        return view('admin.financiero.cajas.edit', [
            'caja' => $caja,
        ]);
    }

    public function update($id, Request $request)
    {
        $caja = Caja::select('id')->find($id);
        $caja->fill($request->all());
        $caja->save();
        return redirect()->route('admin.cajas.index')->with(['msg' => 'Caja actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(Caja::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.cajas.index')->with(['msg' => 'Caja eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors('Error al eliminar caja');
        }
    }

    public function arqueos($id)
    {
        $caja = Caja::find($id);
        $arqueos = Arqueo::whereIdCaja($id)->orderBy('created_at', 'desc')->get();

        return view('admin.financiero.cajas.arqueos', [
            'caja' => $caja,
            'arqueos' => $arqueos,
        ]);
    }
}
