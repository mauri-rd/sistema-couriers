<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Enum\EstadoPaquete;
use App\Paquete;
use App\User;
use Illuminate\Support\Facades\Mail;

class DiasController extends Controller
{
    public function dias(Request $request)
    {
        $ahora = Carbon::now();
        $ids = collect([]);
        $paquetes = Paquete::select(
            'id',
            'id_usuario',
            'id_packlist',
            'codigo',
            'volumen',
            'descripcion',
            'cant_cajas',
            'tracking',
            'courier',
            'gramos',
            'estado',
            'created_at',
            'updated_at'
        )->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name', 'email');
        }])->enOficina()->whereDate('updated_at', '<', $ahora->subDays(14))
        ->paginate(10);

        foreach ($paquetes as $paquete) {
            $ids = $ids->merge($paquete->id_usuario);
        }
        $usuarios = User::select('name', 'last_name','email')->whereIn('id', $ids)->get();
        $emails = $usuarios->pluck('email')->toArray();

        return view('admin.dias.index', [
            'paquetes' => $paquetes,
            'usuarios' => $usuarios,
            'ids' => $ids,
            'emails' => $emails
        ]);
    }
}
