<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaProducto;
use App\Http\Controllers\CRUDControllerInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriaProductosController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $cats = CategoriaProducto::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.catproductos.index', ['cats' => $cats]);
    }

    public function create()
    {
        return view('admin.catproductos.create');
    }

    public function store(Request $request)
    {
        if(CategoriaProducto::create($request->all())) {
            return redirect()->route('admin.catproductos.index')->with(['msg' => 'Categoria registrada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $cat = CategoriaProducto::select('id', 'nombre')->activo()->find($id);
        return view('admin.catproductos.edit', ['cat' => $cat]);
    }

    public function update($id, Request $request)
    {
        if(CategoriaProducto::where('id', '=', $id)->update($request->except(['_token']))) {
            return redirect()->route('admin.catproductos.index')->with(['msg' => 'Categoria actualizada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(CategoriaProducto::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.catproductos.index')->with(['msg' => 'Categoria eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
