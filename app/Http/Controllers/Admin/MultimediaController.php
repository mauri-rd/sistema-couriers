<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaMultimedia;
use App\Helpers\Admin\SelectHelper;
use App\Http\Controllers\CRUDControllerInterface;
use App\Multimedia;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MultimediaController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $multimedias = Multimedia::select('id', 'nombre', 'nombre', 'id_categoria')->activo()->with(['categoria' => function($q) {
            $q->select('id', 'nombre');
        }])->paginate(10);
        return view('admin.multimedias.index', ['multimedias' => $multimedias]);
    }

    public function create()
    {
        return view('admin.multimedias.create', [
            'categorias' => SelectHelper::make(CategoriaMultimedia::select('id', 'nombre')->activo()->get()),
            'multiple' => 'multiple'
        ]);
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $urls = $request->hasFile('url') ? $request->url : [null];
            foreach ($urls as $url) {
                $datos = $request->all();
                $datos['inhome'] = $request->has('inhome');
                if ($url != null) {
                    $datos['url'] = $url;
                } else {
                    unset($datos['url']);
                }
                Multimedia::create($datos);
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.multimedias.index')->with(['msg' => 'Multimedia registrado con éxito!']);
    }

    public function edit($id)
    {
        $multimedia = Multimedia::select('id', 'nombre', 'url', 'link_youtube', 'tipo', 'id_categoria', 'activo', 'inhome')
            ->with(['categoria' => function($q) {
                $q->select('id', 'nombre');
            }])
            ->activo()
            ->find($id);
        return view('admin.multimedias.edit', [
            'multimedia' => $multimedia,
            'categorias' => SelectHelper::make(CategoriaMultimedia::select('id', 'nombre')->activo()->get()),
            'multiple' => ''
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token']);
            $multimedia = Multimedia::select('id')->find($id);
            $multimedia->fill($datos);
            $multimedia->inhome = $request->has('inhome');
            $multimedia->save();
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.multimedias.index')->with(['msg' => 'Multimedia actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Multimedia::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.multimedias.index')->with(['msg' => 'Multimedia eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
