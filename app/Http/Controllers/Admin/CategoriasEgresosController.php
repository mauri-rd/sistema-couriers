<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaEgreso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriasEgresosController extends Controller
{
    public function index()
    {
        $categorias = CategoriaEgreso::select('id', 'nombre', 'icon')->paginate(10);
        return view('admin.financiero.categorias-egresos.index', ['categorias' => $categorias]);
    }

    public function create()
    {
        return view('admin.financiero.categorias-egresos.create', [
            'icons' => $this->getIcons(),
        ]);
    }

    public function store(Request $request)
    {
        CategoriaEgreso::create($request->all());

        return redirect()->route('admin.categorias-egresos.index')->with(['msg' => 'Categoría registrada con éxito!']);
    }

    public function edit($id)
    {
        $categoria = CategoriaEgreso::select('id', 'nombre', 'icon')
            ->find($id);
        return view('admin.financiero.categorias-egresos.edit', [
            'categoria' => $categoria,
            'icons' => $this->getIcons(),
        ]);
    }

    public function update($id, Request $request)
    {
        $categorias = CategoriaEgreso::select('id')->find($id);
        $categorias->fill($request->all());
        $categorias->save();
        return redirect()->route('admin.categorias-egresos.index')->with(['msg' => 'Categoría actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(CategoriaEgreso::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.categorias-egresos.index')->with(['msg' => 'Categoría eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors('Error al eliminar categoria');
        }
    }

    private function getIcons()
    {
        return json_decode(file_get_contents(storage_path('app/fontawesome-icons.json')));
    }
}
