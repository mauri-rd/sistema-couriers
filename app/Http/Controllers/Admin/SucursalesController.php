<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Sucursal;

class SucursalesController extends Controller
{
    public function index()
    {
        $sucursales = Sucursal::select('id', 'nombre', 'prefix')->paginate(10);
        return view('admin.sucursales.index', ['sucursales' => $sucursales]);
    }

    public function create()
    {
        return view('admin.sucursales.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token']);
            $sucursal = Sucursal::create($datos);
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.sucursales.index')->with(['msg' => 'Sucursal registrada con éxito!']);
    }

    public function edit($id)
    {
        $sucursal = Sucursal::select('id', 'nombre', 'prefix')
            ->find($id);
        return view('admin.sucursales.edit', [
            'sucursal' => $sucursal
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token']);
            $sucursal = Sucursal::select('id')->find($id);
            $sucursal->fill($datos);
            $sucursal->save();
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.sucursales.index')->with(['msg' => 'Sucursal actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(Sucursal::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.sucursales.index')->with(['msg' => 'Sucursal eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
