<?php

namespace App\Http\Controllers\Admin;

use App\Caja;
use App\Cotizacion;
use App\Enum\MonedaCaja;
use App\Enum\TipoMovimientoFinanciero;
use App\MovimientoFinanciero;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Enum\Couriers;
use App\Enum\EstadoPaquete;

use App\Paquete;
use App\Invoice;
use App\ItemInvoice;
use App\User;
use Illuminate\Support\Facades\Mail;

class InvoicesController extends Controller
{
    public function index(Request $request)
    {
        $showHidden = $request->has('hidden');
        $invoices = Invoice::select(
            'id',
            'id_usuario',
            'total',
            'hidden',
            'created_at'
        )
        ->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name');
        }, 'items' => function($q) {
            $q->select('id', 'id_invoice', 'id_paquete', 'pago');
        }, 'items.paquete' => function($q) {
            $q->select(
                'id',
                'gramos',
                'volumen',
                'precio_kg',
                'seguro',
                'consolidacao',
                'tipo_precio',
                'unidades',
                'consolidacao_trackings',
                'precio_fijo',
                'tracking'
            );
        }])->orderBy('created_at', 'desc')
            ->filtrar($request->usuario, $request->tracking);
        if (!$showHidden) {
            $invoices = $invoices->where('hidden', false);
        }
        $invoices = $invoices->paginate(20);
        return view('admin.invoices.index', [
            'invoices' => $invoices,
            'usuarios' => $this->getUsuarios(),
            'trackings' => $this->getTrackingNumbers(),
            'usuario' => $request->usuario,
            'tracking' => $request->tracking,
            'hidden' => $showHidden,
        ]);
    }

    public function create()
    {
        return view('admin.invoices.create', [
            'usuarios' => $this->getUsuarios(false)
        ]);
    }

    public function store(Request $request)
    {
        if ($request->has('codigo')) {
            DB::transaction(function () use ($request) {
                $total = 0;
                foreach($request->codigo as $index => $codigo) {
                    $total += $request->precio[$index] * ($request->gramos[$index] / 1000);
                }
                $invoice = Invoice::create([
                    'id_usuario' => $request->id_usuario,
                    'id_packlist' => null,
                    'total' => $total
                ]);
                foreach($request->codigo as $index => $codigo) {
                    $paquete = Paquete::create([
                        'id_usuario' => $request->id_usuario,
                        'id_packlist' => null,
                        'codigo' => $codigo,
                        'volumen' => isset($request->volumen) && !is_null($request->volumen) ? $request->volumen : 0,
                        'descripcion' => $request->descripcion[$index],
                        'tracking' => $request->tracking[$index],
                        'courier' => Couriers::$DHL,
                        'gramos' => $request->gramos[$index],
                        'precio_kg' => $request->precio[$index],
                        'estado' => EstadoPaquete::$OFICINA,
                    ]);
                    ItemInvoice::create([
                        'id_invoice' => $invoice->id,
                        'id_paquete' => $paquete->id
                    ]);
                }
            });
        }
        return redirect()->route('admin.invoices.index')->with([
            'msg' => 'Invoice generado con éxito!'
        ]);
    }

    public function pago($id)
    {
        ItemInvoice::where('id', '=', $id)->update(['pago' => true]);
        return redirect()->back()->with([
            'msg' => 'Pagado exitósamente'
        ]);
    }

    public function payAmount($id, Request $request)
    {
        $caja = Caja::find($request->id_caja);
        $movimiento = [
            'tipo_movimiento' => TipoMovimientoFinanciero::INGRESO,
            'titulo' => "Pago de Invoice #$id",
            'monto' => $request->monto,
            'fecha' => Carbon::now(),
            'id_caja' => $request->id_caja,
            'id_registrador' => auth()->id(),
            'id_invoice' => $id,
        ];

        if ($caja->moneda == MonedaCaja::GUARANIES) {
            $cotizacion = Cotizacion::ultimo()->first();
            $cotizacion = !is_null($cotizacion) ? $cotizacion->valor : 0;

            $movimiento['monto'] = $request->monto_guaranies;
            $movimiento['monto_extranjero'] = $request->monto;
            $movimiento['cambio_registrado'] = $cotizacion;
        }

        MovimientoFinanciero::create($movimiento);
        Caja::whereId($request->id_caja)->increment('total', $caja->moneda == MonedaCaja::GUARANIES ? $request->monto_guaranies : $request->monto);

        return redirect()->back()->with(([
            'msg' => 'Pago registrado exitósamente'
        ]));
    }

    public function show(Request $resuest, $id)    
    {
        $invoice = Invoice::select('id', 'id_usuario', 'total', 'created_at')
        ->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name', 'id_sucursal', 'casilla');
        }, 'items' => function($q) {
            $q->select('id', 'id_invoice', 'id_paquete', 'pago');
        }, 'items.paquete' => function($q) {
            $q->select(
                'id',
                'descripcion',
                'gramos',
                'volumen',
                'precio_kg',
                'tracking',
                'seguro',
                'consolidacao',
                'consolidacao_trackings',
                'tipo_precio',
                'unidades'
            );
        }])->find($id);

        $pagado = 0;
        $totalGramos = 0;
        foreach ($invoice->items as $item) {
            $totalGramos += $item->paquete->gramos;
            if ($item->pago == true) {
                $pagado += ($item->paquete->valor_total);
            }
        }

        $cotizacion = Cotizacion::ultimo()->first();
        $cotizacion = !is_null($cotizacion) ? $cotizacion->valor : 0;

        return view('admin.invoices.show', [
            'invoice' => $invoice,
            'pagado' => $pagado,
            'cajas' => Caja::select('id', DB::raw("concat(nombre, ' (', if(moneda = " . MonedaCaja::GUARANIES . ", 'Guaranies', 'Dolares'), ')') as nombre"))->pluck('nombre', 'id'),
            'print' => $resuest->has('print'),
            'cotizacion' => $cotizacion,
        ]);
    }

    public function hide($id)
    {
        Invoice::where('id', '=', $id)->update(['hidden' => true]);
        return redirect()->back()->with([
            'msg' => 'Invoice ocultado exitósamente'
        ]);
    }

    public function join(Request $request)
    {
        if (!$request->has('invoice')) {
            return redirect()->back()->withErrors([
                'Debe seleccionar por lo menos un invoice'
            ]);
        }
        $invoices = Invoice::select('id', 'id_usuario', 'total', 'created_at')
            ->with(['usuario' => function($q) {
                $q->select('id', 'name', 'last_name');
            }, 'items' => function($q) {
                $q->select('id', 'id_invoice', 'id_paquete');
            }, 'items.paquete' => function($q) {
                $q->select(
                    'id',
                    'descripcion',
                    'gramos',
                    'volumen',
                    'precio_kg',
                    'tipo_precio',
                    'tracking'
                );
            }])
            ->whereIn('id', $request->invoice)
            ->get();
        $items = collect([]);
        $total = 0;
        foreach ($invoices as $invoice) {
            $total += $invoice->total;
            $items = $items->merge($invoice->items);
        }
        return view('admin.invoices.join', [
            'items' => $items,
            'date' => Carbon::now(),
            'total' => $total,
            'usuario' => $invoices[0]->usuario,
        ]);
    }

    public function email($id)
    {
        $invoice = Invoice::select(
                'id',
                'id_usuario',
                'id_packlist',
                'hidden',
                'total',
                'created_at'
            )->with(['usuario' => function($q) {
                    $q->select('id', 'name', 'last_name', 'email');
                }, 'items' => function($q) {
                    $q->select('id', 'id_invoice', 'id_paquete');
                }, 'items.paquete' => function($q) {
                    $q->select(
                        'id',
                        'descripcion',
                        'gramos',
                        'precio_kg',
                        'seguro',
                        'volumen',
                        'consolidacao',
                        'consolidacao_trackings',
                        'tipo_precio',
                        'unidades'
                    );
                }])
            ->find($id);
        Mail::send('emails.invoice', [
            'nombre' => $invoice->usuario->full_name,
            'id' => $invoice->usuario->id,
            'paquetes' => $invoice->items->map(function ($item) { return $item->paquete; }),
            'total' => $invoice->total,
            'invoice' => $invoice,
            'date' => $invoice->created_at
        ], function ($message) use($invoice) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to($invoice->usuario->email);
            $message->subject($invoice->usuario->name . ', tiene un nuevo invoice disponible!');
        });
        return redirect()->route('admin.invoices.index')->with([
            'msg' => 'E-mail enviado exitósamente!'
        ]);
    }

    private function getUsuarios($acceptAll = true)
    {
        $res = [];
        if ($acceptAll) {
            $res[''] = "----TODOS LOS USUARIOS----";
        }
        foreach(User::select('id', 'name', 'last_name')->where('type', '=', 2)->get() as $usuario) {
            $res[$usuario->id] = $usuario->id . ' - ' . $usuario->full_name;
        }
        return $res;
    }

    private function getTrackingNumbers()
    {
        // dd(\App\Invoice::get());
        $res = [];
        $res[''] = "----TODOS LOS TRACKING----";
        foreach(Paquete::has('item')->get() as $paquete) {
            $res[$paquete->tracking] = $paquete->tracking;
        }
        return $res;
    }
}
