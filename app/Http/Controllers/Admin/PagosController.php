<?php

namespace App\Http\Controllers\Admin;

use App\Comprobante;
use App\Http\Controllers\Controller;
use App\ItemComprobante;
use App\ItemInvoice;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class PagosController extends Controller
{
    public function index()
    {
        $pagos = Comprobante::select(
            'id',
            'total_usd',
            'total_pyg',
            'total_brl',
            'tipo_cambio',
            'arbitraje',
            'comprobante',
            'aceptado',
            'id_cliente',
            'created_at'
        )->abiertos()
            ->with(['items' => function ($q) {
                $q->select(
                    'id_comprobante',
                    'id_paquete'
                );
            }, 'items.paquete' => function ($q) {
                $q->select(
                    'id',
                    'tracking'
                );
            }, 'cliente' => function ($q) {
                $q->select(
                    'id',
                    'name',
                    'last_name'
                );
            }])
            ->get();
        return view('admin.pagos.index', [
            'pagos' => $pagos
        ]);
    }

    public function procesar($id, Request $request)
    {
        if ($request->has('aceptado')) {
            ItemInvoice::whereIn('id_paquete', ItemComprobante::whereIdComprobante($id)->pluck('id_paquete'))
                ->update([
                    'pago' => true
                ]);
        }
        $comprobante = Comprobante::select(
            'id',
            'id_cliente'
        )
            ->with(['items' => function ($q) {
                $q->select(
                    'id_comprobante',
                    'id_paquete'
                );
            }, 'items.paquete' => function ($q) {
                $q->select(
                    'id',
                    'tracking'
                );
            }, 'cliente' => function ($q) {
                $q->select(
                    'id',
                    'name',
                    'last_name',
                    'email'
                );
            }])
            ->find($id);
        $comprobante->fill([
                'procesado' => true,
                'aceptado' => $request->has('aceptado'),
            ]);
        $comprobante->save();
        Mail::send('emails.pago-cliente', [
            'pago' => $comprobante
        ], function ($message) use ($comprobante) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to($comprobante->cliente->email);
            $message->subject($comprobante->cliente->name . ( $comprobante->aceptado ? ' recibimos su pago!' : ' no encontramos los datos de su pago' ));
        });
        return redirect()->back()->with([
            'msg' => 'Pago procesado con éxito!'
        ]);
    }

}
