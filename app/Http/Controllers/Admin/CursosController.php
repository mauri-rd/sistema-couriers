<?php

namespace App\Http\Controllers\Admin;

use App\Curso;
use App\Http\Controllers\CRUDControllerInterface;
use App\ImagenCurso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CursosController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $cursos = Curso::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.cursos.index', ['cursos' => $cursos]);
    }

    public function create()
    {
        return view('admin.cursos.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token', 'imagenes']);
            $curso = Curso::create($datos);
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenCurso::create([
                        'id_curso' => $curso->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.cursos.index')->with(['msg' => 'Curso registrado con éxito!']);
    }

    public function edit($id)
    {
        $curso = Curso::select('id', 'nombre', 'descripcion')
            ->with(['imagenes' => function($q) {
                $q->select('id', 'id_curso', 'url');
            }])
            ->activo()
            ->find($id);
        return view('admin.cursos.edit', [
            'curso' => $curso
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token', 'imagenes']);
            $curso = Curso::select('id')->find($id);
            $curso->fill($datos);
            $curso->save();
            if ($request->has('eliminar')) {
                foreach ($request->eliminar as $id_imagen) {
                    $imagen = ImagenCurso::select('id', 'url')->find($id_imagen);
                    $path = $imagen->path;
                    if ($path != "") {
                        unlink($path);
                    }
                    $imagen->delete();
                }
            }
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenCurso::create([
                        'id_curso' => $curso->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.cursos.index')->with(['msg' => 'Curso actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Curso::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.cursos.index')->with(['msg' => 'Curso eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
