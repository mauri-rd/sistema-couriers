<?php

namespace App\Http\Controllers\Admin;

use App\Container;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enum\EstadoPaquete;

class ContainersController extends Controller
{
    public function index(Request $request){
        try {
            $containers = Container::select('id', 'nombre', 'slug', 'created_at', 'estado')
            ->with(['paquetes' => function($q){
                $q->select(
                    'id', 'id_usuario', 'id_packlist', 'codigo', 'descripcion', 'cant_cajas', 'tracking', 'courier', 'precio_kg',
                    'precio_fijo', 'gramos', 'volumen', 'estado', 'tipo_flete', 'destino', 'tipo_precio', 'seguro',
                    'consolidacao', 'consolidacao_trackings', 'created_at', 'updated_at', 'wh'
                )->with(['usuario' => function($q) {
                    $q->select('id', 'name', 'last_name', 'casilla', 'id_sucursal');
                }, 'packlist' => function($q) {
                    $q->select(
                        'id', 'tracking', 'courier', 'descripcion',
                        'valor', 'moneda', 'file', 'mime'
                    );
                }])
                ->activo()
                ->orderBy('created_at', 'desc')->get();
            }])
            ->buscar($request->q)
            ->with('paquetes')
            ->orderBy('created_at', 'DESC')->paginate(20);
            return view('admin.containers.index', [
                'containers' => $containers,
                'q' => $request->q,
                'estados_container' => $this->getEstadosContainer(),
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al obtener los containers. '. $th->getMessage()]);
        }
    }

    public function create(){
        return view('admin.containers.create', [
            'estados_container' => $this->getEstadosContainer(),
        ]);
    }

    public function store(Request $request){
        try {
            Container::create($request->all());
            return redirect()->route('admin.containers.index')->with([
                'msg' => 'Container creado con éxito'
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al crear container. '. $th->getMessage()]);
        }
    }

    public function storeModal(Request $request){
        try {
            Container::create($request->all());
            return ['success' => 'Creado con exito!'];
        } catch (\Exception $th) {
            return ['msg' => 'Error al crear container. '. $th->getMessage()];
        }
    }

    public function edit($id){
        try {
            $container = Container::findOrFail($id);
            return view('admin.containers.edit', [
                'container' => $container
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al eliminar el container. '. $th->getMessage()]);
        }
    }

    public function update(Request $request, $id){
        try {
            $container = Container::findOrFail($id);
            $container->update($request->all());
            return redirect()->route('admin.containers.index')->with([
                'msg' => 'Container actualizado con éxito'
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withInput()->withErrors(['msg' => 'Error al actualizar datos del container. '. $th->getMessage()]);
        }
    }

    public function updateEstado(Request $request, $id){
        try {
            $container = Container::with('paquetes')->findOrFail($id);
            if($container->update(['estado' => $request->estado])){
                $container->paquetes->each(function ($paquete) use ($request) {
                    $paquete->update(['estado' => $request->estado]);
                });
            }
            return redirect()->route('admin.containers.index')->with([
                'msg' => 'Estado del container actualizado con éxito'
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withInput()->withErrors(['msg' => 'Error al actualizar datos del container. '. $th->getMessage()]);
        }
    }

    public function delete($id){
        try {
            $container = Container::findOrFail($id);
            $container->delete();
            return redirect()->route('admin.containers.index')->with([
                'msg' => 'Container eliminado con éxito'
            ]);
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al eliminar el container. '. $th->getMessage()]);
        }
    }

    public function getContainers(){
        try {
            $containers = [];
            $containers[] = '-- SELECCIONAR CONTAINER --';
            foreach(Container::select('id', 'nombre')->get() as $container){
                $containers[$container->id] = $container->nombre;
            }
            return ['containers' => $containers];
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al obtener los containers. '. $th->getMessage()]);
        }
    }

    public function getEstadosContainer(){
        try {
            $estados = [];
            $estados[EstadoPaquete::$MIAMI] = 'EN MIAMI';
            $estados[EstadoPaquete::$MIAMI_MARITIMO] = 'EN MIAMMIAMI_MARITIMOI';
            $estados[EstadoPaquete::$TRANSITO] = 'EN TRANSITO';
            $estados[EstadoPaquete::$PARAGUAY] = 'EN PARAGUAY';
            $estados[EstadoPaquete::$OFICINA] = 'EN OFICINA';
            $estados[EstadoPaquete::$RETIRADO] = 'EN RETIRADO';
            $estados[EstadoPaquete::$CHINA] = 'EN CHINA';
            $estados[EstadoPaquete::$BRASIL] = 'EN BRASIL';
            return $estados;
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al obtener los estados de containers. '. $th->getMessage()]);
        }
    }
}
