<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CRUDControllerInterface;
use App\Marca;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MarcasController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $marcas = Marca::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.marcas.index', ['marcas' => $marcas]);
    }

    public function create()
    {
        return view('admin.marcas.create');
    }

    public function store(Request $request)
    {
        if(Marca::create($request->all())) {
            return redirect()->route('admin.marcas.index')->with(['msg' => 'Marca registrada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $marca = Marca::select('id', 'nombre')->activo()->find($id);
        return view('admin.marcas.edit', ['marca' => $marca]);
    }

    public function update($id, Request $request)
    {
        if(Marca::where('id', '=', $id)->update($request->except(['_token']))) {
            return redirect()->route('admin.marcas.index')->with(['msg' => 'Marca actualizada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(Marca::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.marcas.index')->with(['msg' => 'Marca eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
