<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pregunta;

class PreguntasController extends Controller
{
    public function index()
    {
        $preguntas = Pregunta::select('id', 'pregunta', 'respuesta')
            ->activo()
            ->orderBy('posicion', 'asc')
            ->paginate(10);
        // dd($preguntas);
        return view('admin.preguntas.index', ['preguntas' => $preguntas]);
    }

    public function create()
    {
        return view('admin.preguntas.create');
    }

    public function store(Request $request)
    {
        $pregunta = Pregunta::create(array_merge($request->all(), [
            'posicion' => Pregunta::max('posicion') + 1
        ]));
        return redirect()->route('admin.preguntas.index')->with(['msg' => 'Pregunta registrada con éxito!']);
    }

    public function edit($id)
    {
        $pregunta = Pregunta::select(
                'id', 
                'pregunta',
                'respuesta',
                'posicion'
            )
            ->find($id);
        return view('admin.preguntas.edit', [
            'pregunta' => $pregunta
        ]);
    }

    public function update($id, Request $request)
    {
        $pregunta = Pregunta::select('id')->find($id);
        $pregunta->fill($request->all());
        $pregunta->save();
        return redirect()->route('admin.preguntas.index')->with(['msg' => 'Pregunta actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(Pregunta::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.preguntas.index')->with(['msg' => 'Pregunta eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function reordenar(Request $request)
    {
        if ($request->has('id')) {
            foreach($request->id as $pos => $preguntaId) {
                Pregunta::where('id', '=', $preguntaId)->update([
                    'posicion' => $pos + 1
                ]);
            }
        }
        return redirect()->route('admin.preguntas.index')->with([
            'msg' => 'Posiciones actualizadas exitosamente!'
        ]);
    }
}
