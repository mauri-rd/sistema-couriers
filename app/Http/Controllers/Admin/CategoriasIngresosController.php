<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaIngreso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriasIngresosController extends Controller
{
    public function index()
    {
        $categorias = CategoriaIngreso::select('id', 'nombre', 'icon')->paginate(10);
        return view('admin.financiero.categorias-ingresos.index', ['categorias' => $categorias]);
    }

    public function create()
    {
        return view('admin.financiero.categorias-ingresos.create', [
            'icons' => $this->getIcons(),
        ]);
    }

    public function store(Request $request)
    {
        CategoriaIngreso::create($request->all());

        return redirect()->route('admin.categorias-ingresos.index')->with(['msg' => 'Categoría registrada con éxito!']);
    }

    public function edit($id)
    {
        $categoria = CategoriaIngreso::select('id', 'nombre', 'icon')
            ->find($id);
        return view('admin.financiero.categorias-ingresos.edit', [
            'categoria' => $categoria,
            'icons' => $this->getIcons(),
        ]);
    }

    public function update($id, Request $request)
    {
        $categorias = CategoriaIngreso::select('id')->find($id);
        $categorias->fill($request->all());
        $categorias->save();
        return redirect()->route('admin.categorias-ingresos.index')->with(['msg' => 'Categoría actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(CategoriaIngreso::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.categorias-ingresos.index')->with(['msg' => 'Categoría eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors('Error al eliminar categoria');
        }
    }

    private function getIcons()
    {
        return json_decode(file_get_contents(storage_path('app/fontawesome-icons.json')));
    }
}
