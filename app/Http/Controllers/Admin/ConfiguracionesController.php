<?php

namespace App\Http\Controllers\Admin;

use App\Configuracion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Slide;

class ConfiguracionesController extends Controller
{
    public function index()
    {
        return view('admin.configuraciones.index', [
            'banners' => $this->getBanners(),
            'configs' => $this->getConfigs()
        ]);
    }

    public function save(Request $request)
    {
        $getConfigs = $request->all();
        foreach ($this->getConfigs() as $config) {
            $config->valor = $getConfigs[$config->slug];
            $config->save();
        }
        return redirect()->back()->with(['msg' => 'Configuraciones guardadas con éxito!']);
    }
    
    private function getConfigs()
    {
        return collect(Configuracion::select('id', 'slug', 'nombre', 'valor')->get())->keyBy('slug');
    }

    private function getBanners()
    {
        $banners = Slide::select('url')->get();
        $res = [];
        $res[''] = "Sin background";
        foreach($banners as $banner) {
            $res[$banner->url] = $banner->url;
        }
        return $res;
    }
}
