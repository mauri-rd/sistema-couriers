<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaMultimedia;
use App\Http\Controllers\CRUDControllerInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriaMultimediaController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $cats = CategoriaMultimedia::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.catmultimedias.index', ['cats' => $cats]);
    }

    public function create()
    {
        return view('admin.catmultimedias.create');
    }

    public function store(Request $request)
    {
        if(CategoriaMultimedia::create($request->all())) {
            return redirect()->route('admin.catmultimedias.index')->with(['msg' => 'Categoria registrada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $cat = CategoriaMultimedia::select('id', 'nombre')->activo()->find($id);
        return view('admin.catmultimedias.edit', ['cat' => $cat]);
    }

    public function update($id, Request $request)
    {
        if(CategoriaMultimedia::where('id', '=', $id)->update($request->except(['_token']))) {
            return redirect()->route('admin.catmultimedias.index')->with(['msg' => 'Categoria actualizada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(CategoriaMultimedia::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.catmultimedias.index')->with(['msg' => 'Categoria eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
