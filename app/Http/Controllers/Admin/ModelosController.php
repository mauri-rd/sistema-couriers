<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Admin\SelectHelper;
use App\Http\Controllers\CRUDControllerInterface;
use App\Marca;
use App\Modelo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModelosController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $modelos = Modelo::select('id', 'nombre', 'id_marca')->activo()->with(['marca' => function($q) {
            $q->select('id', 'nombre');
        }])->paginate(10);
        return view('admin.modelos.index', ['modelos' => $modelos]);
    }

    public function create()
    {
        return view('admin.modelos.create', [
            'marcas' => SelectHelper::make(Marca::select('id', 'nombre')->activo()->get())
        ]);
    }

    public function store(Request $request)
    {
        if(Modelo::create($request->all())) {
            return redirect()->route('admin.modelos.index')->with(['msg' => 'Modelo registrado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $modelo = Modelo::select('id', 'nombre', 'id_marca')->activo()->find($id);
        return view('admin.modelos.edit', [
            'modelo' => $modelo,
            'marcas' => SelectHelper::make(Marca::select('id', 'nombre')->activo()->get())
        ]);
    }

    public function update($id, Request $request)
    {
        if(Modelo::where('id', '=', $id)->update($request->except(['_token']))) {
            return redirect()->route('admin.modelos.index')->with(['msg' => 'Modelo actualizado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(Modelo::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.modelos.index')->with(['msg' => 'Modelo eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
