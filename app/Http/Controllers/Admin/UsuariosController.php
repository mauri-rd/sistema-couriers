<?php

namespace App\Http\Controllers\Admin;

use App\Caja;
use App\Enum\MonedaCaja;
use App\Enum\UserType;
use App\OperadorCaja;
use App\Sucursal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    public function index(Request $request)
    {
        $usuarios = User::select(
            'id',
            'casilla',
            'id_sucursal',
            'name',
            'last_name',
            'ci',
            'phone',
            'address',
//            'country',
//            'city',
//            'state',
//            'zip',
            'email',
            'password',
            'type',
            'active',
            'created_at'
        )->filtrar($request->q, $request->queryby)
            ->with(['sucursal' => function ($w) {
                $w->select(
                    'id',
                    'nombre',
                    'prefix'
                );
            }])
        ->orderBy('casilla')
        ->paginate(10);
        return view('admin.usuarios.index', [
            'usuarios' => $usuarios,
            'queryBy' => $this->getQueryBy(),
            'prevQuery' => $request->q,
            'prevQueryBy' => $request->queryby
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.usuarios.create', [
            'tipos' => $this->getTypes(),
            'sucursales' => $this->getSucursales(),
            'cajas' => $this->getCajas(),
            'closeWhenFinished' => $request->closeWhenFinished,
            'registerCliente' => $request->registerCliente,
        ]);
    }

    public function store(Request $request)
    {
        DB::raw('LOCK TABLES users WRITE');
        $casilla = User::max('casilla') + 1;
        $usuario = User::create(array_merge($request->all(), [
            'active' => 1,
            'operador_caja' => $request->type == UserType::OPERADOR && $request->has('operador_caja'),
            'operador_paquetes' => $request->type == UserType::OPERADOR && $request->has('operador_paquetes'),
            'operador_invoices' => $request->type == UserType::OPERADOR && $request->has('operador_invoices'),
            'operador_pagos' => $request->type == UserType::OPERADOR && $request->has('operador_pagos'),
            'operador_usuarios' => $request->type == UserType::OPERADOR && $request->has('operador_usuarios'),
            'operador_mensajes' => $request->type == UserType::OPERADOR && $request->has('operador_mensajes'),
            'operador_sucursales' => $request->type == UserType::OPERADOR && $request->has('operador_sucursales'),
            'operador_reportes' => $request->type == UserType::OPERADOR && $request->has('operador_reportes'),
            'operador_informaciones' => $request->type == UserType::OPERADOR && $request->has('operador_informaciones'),
        ], (
            $request->has('crear_casilla') ?
            [
                'casilla' => $casilla,
            ] : []
        ), (
            auth()->user()->type === UserType::OPERADOR ?
            [
                'type' => UserType::CLIENTE,
            ] : []
        )));
        DB::raw('UNLOCK TABLES users WRITE');
        return redirect()->route('admin.usuarios.index')->with([
            'msg' => 'Usuario registrado con éxito!',
            'closeWhenFinished' => $request->has('closeWhenFinished'),
        ]);
    }

    public function edit($id)
    {
        $usuario = User::select(
            'id',
            'name',
            'last_name',
            'phone',
            'birthday',
            'address',
//            'country',
//            'city',
//            'state',
//            'zip',
            'email',
            'password',
            'type',
            'casilla',
            'id_sucursal',
            'operador_paquetes',
            'operador_caja',
            'operador_invoices',
            'operador_pagos',
            'operador_usuarios',
            'operador_mensajes',
            'operador_sucursales',
            'operador_reportes',
            'operador_informaciones',
            'active'
        )
            ->find($id);
        return view('admin.usuarios.edit', [
            'usuario' => $usuario,
            'tipos' => $this->getTypes(),
            'cajas' => $this->getCajas(),
            'sucursales' => $this->getSucursales(),
        ]);
    }

    public function update($id, Request $request)
    {
        $usuario = User::select('id')->find($id);
        $usuario->fill(array_merge($request->all(), [
            'operador_caja' => $request->type == UserType::OPERADOR && $request->has('operador_caja'),
            'operador_paquetes' => $request->type == UserType::OPERADOR && $request->has('operador_paquetes'),
            'operador_invoices' => $request->type == UserType::OPERADOR && $request->has('operador_invoices'),
            'operador_pagos' => $request->type == UserType::OPERADOR && $request->has('operador_pagos'),
            'operador_usuarios' => $request->type == UserType::OPERADOR && $request->has('operador_usuarios'),
            'operador_mensajes' => $request->type == UserType::OPERADOR && $request->has('operador_mensajes'),
            'operador_sucursales' => $request->type == UserType::OPERADOR && $request->has('operador_sucursales'),
            'operador_reportes' => $request->type == UserType::OPERADOR && $request->has('operador_reportes'),
            'operador_informaciones' => $request->type == UserType::OPERADOR && $request->has('operador_informaciones'),
        ]));
        $usuario->save();
        $this->updateOperadorCajas($usuario, $request);
        return redirect()->route('admin.usuarios.index')->with([
            'msg' => 'Usuario actualizado con éxito!'
        ]);
    }

    public function activate($id, Request $request)
    {
        User::where('id', '=', $id)->update([
            'active' => $request->state
        ]);

        return redirect()->back()->with([
            'msg' => 'Usuario ' . ($request->state == 1 ? 'activado' : 'desactivado') . ' exitósamente!'
        ]);
    }

    public function delete($id)
    {
        if (User::where('id', '=', $id)->update(['active' => 0])) {
            return redirect()->route('admin.usuarios.index')->with(['msg' => 'Usuario eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function exportarCsv()
    {
        $users = User::select(
            'id',
            'casilla',
            'name',
            'last_name',
            'phone',
            'address',
//            'country',
//            'city',
//            'state',
//            'zip',
            'email',
//            'password',
            'id_sucursal',
            'type',
            'active',
            'created_at'
        )->get();
        $csv = implode(",", [
            'ID',
            'nombre',
            'apellido',
            'telefono',
            'direccion',
//            'country',
//            'city',
//            'state',
//            'zip',
            'email',
//            'password',
            'tipo',
            'estado',
            'fecha de registro',
        ]) . "\n";
        foreach($users as $user) {
            $csv .= implode(",", [
                $user->prefix . $user->casilla,
                $user->name,
                $user->last_name,
                $user->phone,
                $user->address,
//                $user->country,
//                $user->city,
//                $user->state,
//                $user->zip,
                $user->email,
//                $user->password,
                $user->type == 1 ? 'Administrador' : 'Cliente',
                $user->active ? 'Activo' : 'Inactivo',
                $user->created_at->format('d/m/Y')
            ]) . "\n";
        }
        return response($csv, 200)
            ->header('Content-Type', 'text/csv')
            ->header('Content-Disposition', 'attachment; filename="usuarios.csv"');
    }

    public function updateOperadorCajas($user, Request $request)
    {
        $user->cajas()->delete();

        if ($request->has('operador_caja') && $request->has('id_caja')) {
            foreach ($request->id_caja as $idCaja) {
                $user->cajas()->create([
                    'id_caja' => $idCaja,
                ]);
            }
        }
    }

    private function getQueryBy()
    {
        $res = [];
        $res['casilla'] = "Casilla";
        $res['username'] = "Nombre de usuario";
        $res['email'] = "E-mail de usuario";
        return $res;
    }

    private function getSucursales()
    {
        $res = [];
        $res[''] = 'Seleccione una sucursal';
        foreach (Sucursal::select('id', 'nombre')->get() as $sucursal) {
            $res[$sucursal->id] = $sucursal->nombre;
        }
        return $res;
    }

    private function getTypes()
    {
        $res = [];
        $res[UserType::ADMINISTRADOR] = "Administrador";
        $res[UserType::CLIENTE] = "Usuario/Cliente";
        $res[UserType::OPERADOR] = "Operador";
        return $res;
    }

    private function getCajas()
    {
        return Caja::select('id', DB::raw("concat(nombre, ' (', if(moneda = " . MonedaCaja::GUARANIES . ", 'Guaranies', 'Dolares'), ')') as nombre"))->pluck('nombre', 'id');
    }
}
