<?php

namespace App\Http\Controllers\Admin;

use App\Arqueo;
use App\Caja;
use App\CategoriaEgreso;
use App\CategoriaIngreso;
use App\Cotizacion;
use App\Enum\MonedaCaja;
use App\Enum\TipoMovimientoFinanciero;
use App\Enum\UserType;
use App\MovimientoFinanciero;
use App\Proveedor;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Collective\Html\FormFacade as Form;

class MovimientosFinancierosController extends Controller
{
    public function index($caja, Request $request)
    {
        $caja = Caja::find($caja);
        $lastArqueo = Arqueo::whereIdCaja($caja->id)
            ->orderBy('created_at', 'desc')
            ->abierto()
            ->first();
        if ($request->has('arqueo') && auth()->user()->type != UserType::OPERADOR) {
            $lastArqueo = Arqueo::find($request->arqueo);
        }
        $idArqueo = is_null($lastArqueo) ? null : $lastArqueo->id;

        $movimientos = MovimientoFinanciero::whereIdCaja($caja->id)
            ->orderBy('fecha', 'desc')
            ->whereIdArqueo($idArqueo)
            ->buscar($request)
            ->paginate(10);

        return view('admin.financiero.movimientos.index', [
            'caja' => $caja,
            'lastArqueo' => $lastArqueo,
            'movimientos' => $movimientos,
            'q' => $request->q,
            'from' => $request->from,
            'to' => $request->to,
        ]);
    }

    public function create($caja)
    {
        $caja = Caja::find($caja);
        if (Arqueo::whereIdCaja($caja->id)->abierto()->count() == 0) {
            return redirect()->route('admin.cajas.movimientos.abrir-arqueo-form', [$caja])->withErrors('La caja no está abierta. Por favor, abra la caja antes de registrar un movimiento');
        }
        return view('admin.financiero.movimientos.create', [
            'caja' => $caja,
            'categoriasIngresos' => $this->getCategoriasIngresos(),
            'categoriasEgresos' => $this->getCategoriasEgresos(),
            'tipos' => $this->getTipos(),
            'clientes' => $this->getClientes(),
            'proveedores' => $this->getProveedores(),
        ]);
    }

    public function store($caja, Request $request)
    {
        $movimiento = [
            'tipo_movimiento' => $request->tipo_movimiento,
            'titulo' => $request->titulo,
            'observaciones' => $request->observaciones,
            'monto' => $request->monto,
            'fecha' => $request->fecha,
            'id_caja' => $caja,
            'id_registrador' => auth()->id(),
            'id_arqueo' => Arqueo::whereIdCaja($caja)->abierto()->first()->id,
        ];
        if ($request->tipo_movimiento == TipoMovimientoFinanciero::INGRESO) {
            $movimiento['id_categoria_ingreso'] = $request->id_categoria_ingreso;
            $movimiento['id_categoria_egreso'] = null;
            $movimiento['id_cliente'] = $request->id_cliente;
            $movimiento['id_proveedor'] = null;
        } elseif ($request->tipo_movimiento == TipoMovimientoFinanciero::EGRESO) {
            $movimiento['id_categoria_egreso'] = $request->id_categoria_egreso;
            $movimiento['id_categoria_ingreso'] = null;
            $movimiento['id_proveedor'] = $request->id_proveedor;
            $movimiento['id_cliente'] = null;
        }

        MovimientoFinanciero::create($movimiento);
        if ($request->tipo_movimiento == TipoMovimientoFinanciero::INGRESO) {
            Caja::whereId($caja)->increment('total', $request->monto);
        } else {
            Caja::whereId($caja)->decrement('total', $request->monto);
        }

        return redirect()->route('admin.cajas.movimientos.index', [$caja])->with(['msg' => 'Movimiento registrado con éxito!']);
    }

    public function edit($caja, $id)
    {
        $caja = Caja::find($caja);
        $movimiento = MovimientoFinanciero::select(
            'id',
            'tipo_movimiento',
            'titulo',
            'observaciones',
            'monto',
            'fecha',
            'id_caja',
            'id_categoria_ingreso',
            'id_categoria_egreso',
            'id_cliente',
            'id_proveedor',
            'id_registrador'
        )
            ->find($id);
        return view('admin.financiero.movimientos.edit', [
            'caja' => $caja,
            'movimiento' => $movimiento,
            'categoriasIngresos' => $this->getCategoriasIngresos(),
            'categoriasEgresos' => $this->getCategoriasEgresos(),
            'tipos' => $this->getTipos(),
            'clientes' => $this->getClientes(),
            'proveedores' => $this->getProveedores(),
        ]);
    }

    public function update($caja, $id, Request $request)
    {
        $caja = Caja::find($caja);
        $movimiento = MovimientoFinanciero::select(
            'id',
            'tipo_movimiento',
            'monto'
        )->find($id);

        if ($movimiento->es_ingreso) {
            $caja->decrement('total', $movimiento->monto);
        }
        if ($movimiento->es_egreso) {
            $caja->increment('total', $movimiento->monto);
        }

        $movimiento->fill($request->all());
        if ($request->tipo_movimiento == TipoMovimientoFinanciero::INGRESO) {
            $movimiento->id_categoria_ingreso = $request->id_categoria_ingreso;
            $movimiento->id_categoria_egreso = null;
            $movimiento->id_cliente = $request->id_cliente;
            $movimiento->id_proveedor = null;
        } elseif ($request->tipo_movimiento == TipoMovimientoFinanciero::EGRESO) {
            $movimiento->id_categoria_egreso = $request->id_categoria_egreso;
            $movimiento->id_categoria_ingreso = null;
            $movimiento->id_proveedor = $request->id_proveedor;
            $movimiento->id_cliente = null;
        }
        $movimiento->save();

        if ($movimiento->es_ingreso) {
            $caja->increment('total', $movimiento->monto);
        }
        if ($movimiento->es_egreso) {
            $caja->decrement('total', $movimiento->monto);
        }

        return redirect()->route('admin.cajas.movimientos.index', [$caja])->with(['msg' => 'Movimiento actualizado con éxito!']);
    }

    public function delete($caja, $id)
    {
        $caja = Caja::find($caja);
        $movimiento = MovimientoFinanciero::find($id);

        if ($movimiento->es_ingreso) {
            $caja->decrement('total', $movimiento->monto);
        }
        if ($movimiento->es_egreso) {
            $caja->increment('total', $movimiento->monto);
        }

        if($movimiento->delete()) {
            return redirect()->route('admin.cajas.movimientos.index', [$caja])->with(['msg' => 'Movimiento eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors('Error al eliminar movimiento');
        }
    }

    public function transferenciaForm($caja, Request $request)
    {
        if (Arqueo::whereIdCaja($caja)->abierto()->count() == 0) {
            return redirect()->route('admin.cajas.movimientos.abrir-arqueo-form', [$caja])->withErrors('La caja no está abierta. Por favor, abra la caja antes de registrar un movimiento');
        }
        $cajas = $this->getCajas();

        $cotizacion = Cotizacion::ultimo()->first();
        $cotizacion = !is_null($cotizacion) ? $cotizacion->valor : 0;

        return view('admin.financiero.movimientos.transferencia', [
            'from' => $caja,
            'cajas' => $cajas,
            'cotizacion' => $cotizacion,
        ]);
    }

    public function transferencia(Request $request)
    {
        $cajaFrom = Caja::find($request->from);
        $cajaTo = Caja::find($request->to);

        $dataFrom = [
            'tipo_movimiento' => TipoMovimientoFinanciero::TRANSFERENCIA,
            'titulo' => 'TRANSFERENCIA A CAJA "' . $cajaTo->nombre . '"',
            'monto' => $request->monto * -1,
            'fecha' => Carbon::now(),
            'id_caja' => $cajaFrom->id,
            'id_registrador' => auth()->id(),
        ];

        $dataTo = [
            'tipo_movimiento' => TipoMovimientoFinanciero::TRANSFERENCIA,
            'titulo' => 'TRANSFERENCIA DESDE CAJA "' . $cajaFrom->nombre . '"',
            'monto' => $request->monto,
            'fecha' => Carbon::now(),
            'id_caja' => $cajaTo->id,
            'id_registrador' => auth()->id(),
        ];

        $montoTo = $request->monto;

        if ($cajaFrom->moneda != $cajaTo->moneda) {
            $cotizacion = Cotizacion::ultimo()->first();
            $cotizacion = !is_null($cotizacion) ? $cotizacion->valor : 0;

            $dataFrom['monto_extranjero'] = $request->monto_extranjero * -1;
            $dataFrom['cambio_registrado'] = $cotizacion;

            $dataTo['monto'] = $request->monto_extranjero;
            $dataTo['monto_extranjero'] = $request->monto;
            $dataTo['cambio_registrado'] = $cotizacion;

            $montoTo = $request->monto_extranjero;
        }

        MovimientoFinanciero::create($dataFrom);
        MovimientoFinanciero::create($dataTo);

        $cajaFrom->decrement('total', $request->monto);
        $cajaTo->increment('total', $montoTo);

        return redirect()->route('admin.cajas.movimientos.index', [$cajaFrom->id])->with([
            'msg' => 'Transferencia realizada con éxito!'
        ]);
    }

    public function abrirArqueoForm($caja)
    {
        $lastArqueo = Arqueo::whereIdCaja($caja)->orderBy('created_at', 'desc')->first();
        return view('admin.financiero.movimientos.arqueo', [
            'caja' => Caja::find($caja),
            'lastArqueo' => $lastArqueo,
        ]);
    }

    public function abrirArqueo($caja, Request $request)
    {
        $lastArqueo = Arqueo::whereIdCaja($caja)->orderBy('created_at', 'desc')->first();

        Arqueo::create([
            'id_caja' => $caja,
            'monto_inicio' => !is_null($lastArqueo) ? $lastArqueo->monto_fin : $request->monto,
        ]);

        if (is_null($lastArqueo)) {
            Caja::whereId($caja)->update([
                'total' => $request->monto,
            ]);
        }

        return redirect()->route('admin.cajas.movimientos.index', [$caja])->with([
            'msg' => 'Caja abierta exitósamente!'
        ]);
    }

    public function cerrarArqueo($caja)
    {
        $lastArqueo = Arqueo::whereIdCaja($caja)->orderBy('created_at', 'desc')->first();
        $lastArqueo->monto_fin = $lastArqueo->total;
        $lastArqueo->fecha_fin = DB::raw('NOW()');
        $lastArqueo->save();

        return redirect()->back()->with([
            'msg' => 'Caja cerrada exitósamente',
        ]);
    }

    private function getTipos()
    {
        return [
            TipoMovimientoFinanciero::INGRESO => 'Ingreso',
            TipoMovimientoFinanciero::EGRESO => 'Egreso',
        ];
    }

    private function getCajas()
    {
        return Caja::select('id', DB::raw("concat(nombre, ' (', if(moneda = " . MonedaCaja::GUARANIES . ", 'Guaranies', 'Dolares'), ')') as nombre"))->pluck('nombre', 'id');
    }

    private function getCategoriasIngresos()
    {
        return CategoriaIngreso::select('id', 'nombre', 'icon')->get();
//        $categorias = [];
//        foreach (CategoriaIngreso::all() as $categoria) {
//            $categorias[$categoria->id] = [
//                'icon' => $categoria->icon,
//                'text' => $categoria->nombre
//            ];
//        }
//        return $categorias;
    }

    private function getCategoriasEgresos()
    {
        return CategoriaEgreso::select('id', 'nombre', 'icon')->get();
//        $categorias = [];
//        foreach (CategoriaEgreso::all() as $categoria) {
//            $categorias[$categoria->id] = [
//                'icon' => $categoria->icon,
//                'text' => $categoria->nombre
//            ];
//        }
//        return $categorias;
    }

    public function getClientes()
    {
        $select = User::select('id', DB::raw("concat(name, ' ', last_name) as nom_completo"))
            ->orderBy('nom_completo')
            ->whereType(UserType::CLIENTE)
            ->pluck('nom_completo', 'id');

//        if ($form) {
//            return Form::select('id_cliente', $select, null, ['class' => 'form-control selectpicker']);
//        }

        return $select;
    }

    public function getProveedores()
    {
        $select = Proveedor::select('id', 'nombre')
            ->orderBy('nombre')
            ->pluck('nombre', 'id');

//        if ($form) {
//            return Form::select('id_cliente', $select, null, ['class' => 'form-control selectpicker']);
//        }

        return $select;
    }
}
