<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CRUDControllerInterface;
use App\ImagenServicio;
use App\Servicio;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ServiciosController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $servicios = Servicio::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.servicios.index', ['servicios' => $servicios]);
    }

    public function create()
    {
        return view('admin.servicios.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token', 'imagenes']);
            $servicio = Servicio::create($datos);
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenServicio::create([
                        'id_servicio' => $servicio->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.servicios.index')->with(['msg' => 'Servicio registrado con éxito!']);
    }

    public function edit($id)
    {
        $servicio = Servicio::select('id', 'nombre', 'direccion', 'telefono', 'gmap', 'activo')
            ->with(['imagenes' => function($q) {
                $q->select('id', 'id_servicio', 'url');
            }])
            ->activo()
            ->find($id);
        return view('admin.servicios.edit', [
            'servicio' => $servicio
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token', 'imagenes']);
            $servicio = Servicio::select('id')->find($id);
            $servicio->fill($datos);
            $servicio->save();
            if ($request->has('eliminar')) {
                foreach ($request->eliminar as $id_imagen) {
                    $imagen = ImagenServicio::select('id', 'url')->find($id_imagen);
                    $path = $imagen->path;
                    if ($path != "") {
                        unlink($path);
                    }
                    $imagen->delete();
                }
            }
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenServicio::create([
                        'id_servicio' => $servicio->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.servicios.index')->with(['msg' => 'Servicio actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Servicio::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.servicios.index')->with(['msg' => 'Servicio eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
