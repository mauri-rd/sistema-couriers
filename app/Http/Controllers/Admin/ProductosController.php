<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaProducto;
use App\Helpers\Admin\SelectHelper;
use App\Http\Controllers\CRUDControllerInterface;
use App\ImagenProducto;
use App\Modelo;
use App\Producto;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $productos = Producto::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.productos.index', ['productos' => $productos]);
    }

    public function create()
    {
        return view('admin.productos.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token', 'imagenes']);
            $producto = Producto::create($datos);
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenProducto::create([
                        'id_producto' => $producto->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.productos.index')->with(['msg' => 'Producto registrado con éxito!']);
    }

    public function edit($id)
    {
        $producto = Producto::select('id', 'nombre')
            ->with(['imagenes' => function($q) {
                $q->select('id', 'id_producto', 'url');
            }])
            ->activo()
            ->find($id);
        return view('admin.productos.edit', [
            'producto' => $producto
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token', 'imagenes']);
            $producto = Producto::select('id')->find($id);
            $producto->fill($datos);
            $producto->save();
            if ($request->has('eliminar')) {
                foreach ($request->eliminar as $id_imagen) {
                    $imagen = ImagenProducto::select('id', 'url')->find($id_imagen);
                    $path = $imagen->path;
                    if ($path != "") {
                        unlink($path);
                    }
                    $imagen->delete();
                }
            }
            foreach ($request->imagenes as $imagen) {
                if (!is_null($imagen)) {
                    ImagenProducto::create([
                        'id_producto' => $producto->id,
                        'url' => $imagen
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.productos.index')->with(['msg' => 'Producto actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Producto::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.productos.index')->with(['msg' => 'Producto eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
