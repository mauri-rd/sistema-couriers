<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Cotizacion;

class CotizacionesController extends Controller
{
    public function index()
    {
        $cotizaciones = Cotizacion::select('id', 'valor', 'created_at')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('admin.cotizaciones.index', ['cotizaciones' => $cotizaciones]);
    }

    public function create()
    {
        return view('admin.cotizaciones.create');
    }

    public function store(Request $request)
    {
        Cotizacion::create($request->all());

        return redirect()->route('admin.cotizaciones.index')->with([
            'msg' => 'Cotizacion registrada con éxito!'
        ]);
    }

    public function edit($id)
    {
        $cotizacion = Cotizacion::select('id', 'valor')
            ->find($id);
        return view('admin.cotizaciones.edit', [
            'cotizacion' => $cotizacion
        ]);
    }

    public function update($id, Request $request)
    {
        $cotizacion = Cotizacion::select('id')->find($id);
        $cotizacion->fill($request->all());
        $cotizacion->save();

        return redirect()->route('admin.cotizaciones.index')->with([
            'msg' => 'Cotizacion actualizada con éxito!'
        ]);
    }

    public function delete($id)
    {
        if(Cotizacion::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.cotizaciones.index')->with([
                'msg' => 'Cotizacion eliminada con éxito!'
            ]);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
