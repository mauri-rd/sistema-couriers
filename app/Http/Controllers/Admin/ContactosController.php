<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\Contacto;

class ContactosController extends Controller
{
    public function index()
    {
        $contactos = Contacto::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.contactos.index', ['contactos' => $contactos]);
    }

    public function create()
    {
        return view('admin.contactos.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token']);
            $contacto = Contacto::create($datos);
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.contactos.index')->with(['msg' => 'Contacto registrado con éxito!']);
    }

    public function edit($id)
    {
        $contacto = Contacto::select('id', 'nombre', 'departamento', 'email')
            ->activo()
            ->find($id);
        return view('admin.contactos.edit', [
            'contacto' => $contacto
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token']);
            $contacto = Contacto::select('id')->find($id);
            $contacto->fill($datos);
            $contacto->save();
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.contactos.index')->with(['msg' => 'Contacto actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Contacto::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.contactos.index')->with(['msg' => 'Contacto eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
