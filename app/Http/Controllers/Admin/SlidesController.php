<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CRUDControllerInterface;
use App\Slide;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SlidesController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $patrocinadores = Slide::select('id', 'url', 'type')->activo()->paginate(10);
        return view('admin.patrocinadores.index', ['patrocinadores' => $patrocinadores]);
    }

    public function create()
    {
        return view('admin.patrocinadores.create');
    }

    public function store(Request $request)
    {
        if(Slide::create(array_merge($request->all(), [
            'tipo' => 1
        ]))) {
            return redirect()->route('admin.patrocinadores.index')->with(['msg' => 'Slide registrado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $patrocinador = Slide::select('id', 'url', 'type')->activo()->find($id);
        return view('admin.patrocinadores.edit', [
            'patrocinador' => $patrocinador,
        ]);
    }

    public function update($id, Request $request)
    {
        $patrocinador = Slide::select('id')->find($id);
        $patrocinador->fill($request->all());
        if($patrocinador->save()) {
            return redirect()->route('admin.patrocinadores.index')->with(['msg' => 'Slide actualizado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(Slide::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.patrocinadores.index')->with(['msg' => 'Slide eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    private function getSelects()
    {
        return [
            '1' => 'Banner Superior',
            '2' => 'Banner de Informaciones'
        ];
    }
}
