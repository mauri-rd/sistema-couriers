<?php

namespace App\Http\Controllers\Admin;

use App\Enum\EstadoPaquete;
use App\Enum\UserType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Paquete;

class DashboardController extends Controller
{
    public function index()
    {
        $enTransito = Paquete::where('estado', EstadoPaquete::$TRANSITO)->count();
        $volumenTransito = Paquete::where('estado', EstadoPaquete::$TRANSITO)->sum('volumen');
        $enOficina = Paquete::where('estado', EstadoPaquete::$OFICINA)->count();
        $volumenOficina = Paquete::where('estado', EstadoPaquete::$OFICINA)->sum('volumen');
        return view('admin.dashboard', [
            'enTransito' => $enTransito,
            'volumenTransito' => $volumenTransito,
            'enOficina' => $enOficina,
            'volumenOficina' => $volumenOficina,
        ]);
        // if (auth()->user()->type == UserType::OPERADOR) {
        //     if (auth()->user()->operador_paquetes) {
        //         return redirect()->route('admin.paquetes.index');
        //     }
        //     if (auth()->user()->operador_caja) {
        //         return redirect()->route('admin.cajas.index');
        //     }

        // }
        // return redirect()->route('admin.configuraciones.index');
    }
}
