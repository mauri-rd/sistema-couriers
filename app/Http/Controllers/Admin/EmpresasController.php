<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Empresa;

class EmpresasController extends Controller
{
    public function index()
    {
        $empresas = Empresa::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.empresas.index', ['empresas' => $empresas]);
    }

    public function create()
    {
        return view('admin.empresas.create');
    }

    public function store(Request $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->except(['_token']);
            $empresa = Empresa::create($datos);
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.empresas.index')->with(['msg' => 'Empresa registrada con éxito!']);
    }

    public function edit($id)
    {
        $empresa = Empresa::select('id', 'nombre', 'imagen_url', 'link')
            ->activo()
            ->find($id);
        return view('admin.empresas.edit', [
            'empresa' => $empresa
        ]);
    }

    public function update($id, Request $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token']);
            $empresa = Empresa::select('id')->find($id);
            $empresa->fill($datos);
            $empresa->save();
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.empresas.index')->with(['msg' => 'Empresa actualizada con éxito!']);
    }

    public function delete($id)
    {
        if(Empresa::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.empresas.index')->with(['msg' => 'Empresa eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
