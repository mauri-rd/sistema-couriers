<?php

namespace App\Http\Controllers\Admin;

use App\InvoiceManual;
use App\ItemInvoiceManual;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InvoiceManualController extends Controller
{
    public function index(Request $request)
    {
        $invoices = InvoiceManual::orderBy('created_at', 'desc')
            ->filtrar($request)
            ->paginate(20);
        return view('admin.invoice-manual.index', [
            'invoices' => $invoices,
            'usuarios' => $this->getUsuarios(),
            'usuario' => $request->usuario,
        ]);
    }

    public function getUserData(Request $request)
    {
        $invoice = InvoiceManual::select('user_data')->orderBy('created_at', 'desc')
            ->filtrar($request)
            ->first();
        return !is_null($invoice) ? $invoice->user_data : '';
    }

    public function create()
    {
        $last = InvoiceManual::orderBy('created_at', 'desc')->first();
        return view('admin.invoice-manual.create', [
            'usuarios' => $this->getUsuarios(false),
            'now' => Carbon::today()->toDateString(),
            'last' => $last
        ]);
    }

    public function store(Request $request)
    {
        $allOk = false;
        $invoice = null;
        $index = -1;
        $items = array_map(function ($item) use ($request, &$index) {
            $index++;
            return [
                'item' => $item,
                'description' => $request->description[$index],
                'price' => $request->price[$index],
                'quantity' => $request->quantity[$index],
                'amount' => $request->amount[$index],
            ];
        }, $request->item);
        $items = array_filter($items, function ($item) {
            return !is_null($item['description']) != '' && $item['description'];
        });
        if (count($items) === 0) {
            return redirect()->back()->withErrors('Es obligatorio crear items');
        }
        DB::transaction(function () use ($request, $items, &$allOk, &$invoice) {
            $invoice = InvoiceManual::create($request->all());
            foreach ($items as $item) {
                ItemInvoiceManual::create([
                    'id_invoice' => $invoice->id,
                    'item' => $item['item'],
                    'description' => $item['description'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                    'amount' => $item['amount'],
                ]);
            }
            $allOk = true;
        });
        if (!$allOk) {
            return redirect()->back()->withInput()->withErrors('Hubo un error al procesar la transacción, intente de nuevo');
        }
        return redirect()->route('admin.invoice-manual.show', [$invoice->id])->with([
            'msg' => 'Invoice creado con éxito'
        ]);
    }

    public function show($id)
    {
        $invoice = InvoiceManual::find($id);
        return view('admin.invoice-manual.show', [
            'invoice' => $invoice
        ]);
    }

    public function printView($id)
    {
        $invoice = InvoiceManual::find($id);
        return view('admin.invoice-manual.print-view', [
            'invoice' => $invoice
        ]);
    }

    private function getUsuarios($acceptAll = true)
    {
        $res = [];
        if ($acceptAll) {
            $res[''] = "----TODOS LOS USUARIOS----";
        }
        foreach(User::select('id', 'name', 'last_name')->where('type', '=', 2)->get() as $usuario) {
            $res[$usuario->id] = $usuario->id . ' - ' . $usuario->full_name;
        }
        return $res;
    }
}
