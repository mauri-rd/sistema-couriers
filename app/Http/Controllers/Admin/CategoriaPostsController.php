<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaPost;
use App\Http\Controllers\CRUDControllerInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriaPostsController extends Controller implements CRUDControllerInterface
{
    public function index()
    {
        $cats = CategoriaPost::select('id', 'nombre')->activo()->paginate(10);
        return view('admin.catposts.index', ['cats' => $cats]);
    }

    public function create()
    {
        return view('admin.catposts.create');
    }

    public function store(Request $request)
    {
        if(CategoriaPost::create($request->all())) {
            return redirect()->route('admin.catposts.index')->with(['msg' => 'Categoria registrada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function edit($id)
    {
        $cat = CategoriaPost::select('id', 'nombre')->activo()->find($id);
        return view('admin.catposts.edit', ['cat' => $cat]);
    }

    public function update($id, Request $request)
    {
        if(CategoriaPost::where('id', '=', $id)->update($request->except(['_token']))) {
            return redirect()->route('admin.catposts.index')->with(['msg' => 'Categoria actualizada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function delete($id)
    {
        if(CategoriaPost::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.catposts.index')->with(['msg' => 'Categoria eliminada con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
