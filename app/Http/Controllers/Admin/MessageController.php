<?php

namespace App\Http\Controllers\Admin;

use App\UserNotification;
use App\UserNotificationRead;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;
use Carbon\Carbon;
use App\User;

class MessageController extends Controller
{
    public function index()
    {
        return view('admin.messages.index', [
            'usuarios' => $this->getUsers(),
            'maxSize' => $this->getMaxFileSize(),
            'date' => Carbon::now()
        ]);
    }

    public function submit(Request $request)
    {
        $filePath = null;
        if ($request->file('emailfile')) {
            $fileDir = "uploads/attachments";
            $fileName = time()
                . strlen($request->subject)
                . "."
                . $request->emailfile->getClientOriginalExtension();
            $request->emailfile->move(
                "uploads/attachments",
                $fileName
            );
            $filePath = $fileDir . "/" . $fileName;
        }
        $this->sendMessage(
            $request->subject,
            $request->text,
            $filePath,
            Carbon::now(),
            $request->emails
        );
        return redirect()->back()->with([
            'msg' => 'Mensajes enviados con éxito!'
        ]);
    }

    private function getMaxFileSize()
    {
        $max_upload = min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
        $max_upload = str_replace('M', '', $max_upload);
        // $max_upload = $max_upload * 1024;
        return $max_upload;
    }

    public function sendMessage(
        $subject,
        $text,
        $image,
        $date,
        $emails
    ) {
        $notification = UserNotification::create([
            'title' => $subject,
            'content' => $text,
            'image_url' => $image,
        ]);
        foreach ($emails as $email) {
            UserNotificationRead::create([
                'id_user' => User::whereEmail($email)->first()->id,
                'id_user_notification' => $notification->id,
            ]);
            Mail::send('emails.message', [
                'text' => $text,
                'image' => $image,
                'date' => $date
            ], function ($message) use ($subject, $email) {
                $message->from('info@strbox.com.py', 'StrBox');
                $message->to($email);
                $message->subject($subject);
            });
        }
    }

    private function getUsers()
    {
        $res = [];
        foreach(User::select('email', 'name', 'last_name')->get() as $user) {
            $res[$user->email] = $user->full_name . ' (' . $user->email . ')';
        }
        return $res;
    }
}
