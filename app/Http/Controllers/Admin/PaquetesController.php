<?php

namespace App\Http\Controllers\Admin;

use App\Enum\Destino;
use App\Enum\TipoFlete;
use App\Enum\TipoPrecioPaquete;
use App\Enum\UserType;
use App\PackageHistory;
use Fcm\FcmClient;
use Fcm\Push\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Enum\EstadoPaquete;
use App\Enum\Couriers;
use App\Configuracion;
use App\Container;
use App\Paquete;
use App\Packlist;
use App\Invoice;
use App\ItemInvoice;
use App\User;
use Mail;
use DB;
use Carbon\Carbon;

class PaquetesController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->all());
        $paquetes = Paquete::select(
            'id', 'id_usuario', 'id_packlist', 'codigo', 'descripcion', 'cant_cajas', 'tracking', 'courier', 'precio_kg',
            'precio_fijo', 'gramos', 'volumen', 'estado', 'tipo_flete', 'destino', 'tipo_precio', 'seguro',
            'consolidacao', 'consolidacao_trackings', 'created_at', 'updated_at', 'wh'
        )->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name', 'casilla', 'id_sucursal');
        }, 'packlist' => function($q) {
            $q->select(
                'id', 'tracking', 'courier', 'descripcion',
                'valor', 'moneda', 'file', 'mime'
            );
        }])->filtrar($request)
        ->activo()
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        return view('admin.paquetes.index', [
            'paquetes' => $paquetes,
            'estados' => $this->getEstadosPaquetes(),
            'usuarios' => $this->getUsuarios(),
            'trackings' => $this->getTrackingNumbers(),
            'show' => $request->show,
            'usuario' => $request->usuario,
            'tracking' => $request->tracking,
            'from' => $request->from,
            'to' => $request->to,
        ]);
    }

    public function create()
    {
        $estados = [];
        $estados[EstadoPaquete::$MIAMI] = "MIAMI (AEREO)";
        $estados[EstadoPaquete::$MIAMI_MARITIMO] = "MIAMI (MARITIMO)";
        $estados[EstadoPaquete::$CHINA] = "CHINA";
        return view('admin.paquetes.create', [
            'usuarios' => $this->getUsuarios(false),
            'couriers' => Couriers::list(),
            'estados' => $estados,
            'fletes' => [
                TipoFlete::NORMAL => 'NORMAL',
//                TipoFlete::EXPRESS => 'EXPRESS',
            ],
            'destinos' => [
                Destino::PARAGUAY => 'PARAGUAY',
//                Destino::BRASIL => 'BRASIL',
            ],
            'tiposPrecio' => $this->getTipoPrecio(),
            'containers' => $this->getContainers(),
            'estados_container' => $this->getEstadosContainer(),
        ]);
    }

    public function store(Request $request)
    {
        $servicios = [
            'tipo_flete' => $request->tipo_flete,
            'destino' => $request->destino,
            'consolidacao' => $request->has('consolidacao'),
            'consolidacao_trackings' => $request->has('consolidacao') ? $request->consolidacao_trackings : null,
            'seguro' => $request->has('seguro'),
            'tipo_precio' => TipoPrecioPaquete::POR_PESO,
            'unidades' => 0,
        ];
        $configuraciones = collect(Configuracion::select('id', 'slug', 'nombre', 'valor')->get())->keyBy('slug');
        $precio = [
            'estado' => $request->estado,
        ];

        $estado = intval($request->estado);

        if ($estado == EstadoPaquete::$MIAMI) {
            $precio['precio_kg'] = $configuraciones['envio-miami']->valor;
        } else if ($estado == EstadoPaquete::$MIAMI_MARITIMO) {
            $precio['estado'] = EstadoPaquete::$MIAMI;
            $precio['precio_kg'] = $configuraciones['envio-miami-maritimo']->valor;
        } else {
            $precio['precio_kg'] = $configuraciones['envio-china']->valor;
        }
        $request['codigo'] = uniqid(9999);
        // dd($request->all());
        $paquete = Paquete::create(
            array_merge(
                $request->all(),
                $precio,
                $servicios,
                $request->has('id_packlist') ? ['id_packlist' => $request->id_packlist] : ['id_packlist' => null]
            )
        );
        $paquete->codigo = uniqid($paquete->id);
        $paquete->save();

        PackageHistory::create([
            'id_paquete' => $paquete->id,
            'estado' => $paquete->estado,
        ]);

        //$this->sendEmail($paquete, $request->estado);
        //$this->sendNotification($paquete, $request->estado);
//        if (auth()->user()->type == UserType::OPERADOR) {
//            return redirect()->route('admin.paquetes.generar-etiquetas', [$paquete->id]);
//        } else {
            return redirect()->route('admin.paquetes.index', [$paquete->id])->with([
                'msg' => 'Paquete registrado con éxito!'
            ]);
//        }
    }

    public function edit($id)
    {
        $estados = [];
        $estados[EstadoPaquete::$MIAMI] = "MIAMI";
        $estados[EstadoPaquete::$CHINA] = "CHINA";

        $paquete = Paquete::select(
            'id',
            'id_usuario',
            'id_packlist',
            'codigo',
            'descripcion',
            'cant_cajas',
            'tracking',
            'courier',
            'gramos',
            'volumen',
            'precio_kg',
            'estado',
            'tipo_flete',
            'destino',
            'seguro',
            'consolidacao',
            'consolidacao_trackings',
            'tipo_precio',
            'unidades',
            'wh'
        )
            ->activo()
            ->find($id);
        return view('admin.paquetes.edit', [
            'paquete' => $paquete,
            'usuarios' => $this->getUsuarios(false),
            'couriers' => Couriers::list(),
            'estados' => $estados,
            'fletes' => [
                TipoFlete::NORMAL => 'NORMAL',
                TipoFlete::EXPRESS => 'EXPRESS',
            ],
            'destinos' => [
                Destino::PARAGUAY => 'PARAGUAY',
                Destino::BRASIL => 'BRASIL',
            ],
            'tiposPrecio' => $this->getTipoPrecio(),
            'estados_container' => $this->getEstadosContainer(),
        ]);
    }

    public function generarEtiquetas($id)
    {
        $paquete = Paquete::select(
            'id',
            'id_usuario',
            'codigo',
            'descripcion',
            'cant_cajas',
            'tracking',
            'gramos',
            'volumen',
            'tipo_flete',
            'destino',
            'seguro',
            'consolidacao',
            'consolidacao_trackings',
            'created_at',
            'wh'
        )->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name');
        }])->find($id);
        return view('admin.paquetes.generar-etiquetas', [
            'paquete' => $paquete
        ]);
    }

    public function update($id, Request $request)
    {
        $paquete = Paquete::select('id', 'estado')->find($id);

        $configuraciones = collect(Configuracion::select('id', 'slug', 'nombre', 'valor')->get())->keyBy('slug');

        $precio = [
            'estado' => $paquete->estado,
        ];

        if (intval($request->destino) === Destino::BRASIL) {
            $precio['precio_kg'] = $configuraciones['envio-brasil']->valor;
        } else {
            if (intval($request->estado) == EstadoPaquete::$MIAMI) {
                if (intval($request->tipo_flete) === TipoFlete::NORMAL) {
                    $precio['precio_kg'] = $configuraciones['envio-miami']->valor;
                } else {
                    $precio['precio_kg'] = $configuraciones['envio-express']->valor;
                }

            } else {
                $precio['precio_kg'] = $configuraciones['envio-china']->valor;
            }
        }

        $paquete->fill(array_merge($request->all(), $precio, $request->has('id_packlist') ? ['id_packlist' => $request->id_packlist] : ['id_packlist' => null]));
        $paquete->save();
        $this->sendEmail($paquete, $request->estado);
        return redirect()->route('admin.paquetes.index')->with([
            'msg' => 'Paquete actualizado con éxito!'
        ]);
    }

    public function updateEstado(Request $request)
    {
        if ($request->has('id_paquete')) {
            $paquetes = Paquete::select(
                    'id',
                    'codigo',
                    'tracking',
                    'destino',
                    'id_usuario',
                    'descripcion',
                    'gramos',
                    'volumen',
                    'precio_kg',
                    'consolidacao',
                    'consolidacao_trackings'
                )
                ->with(['usuario' => function($q) {
                        $q->select(
                            'id',
                            'name',
                            'last_name',
                            'email',
                            'fcm_token'
                        );
                    }
                ])
                ->whereIn('id', $request->id_paquete)
                ->get();
            foreach($paquetes as $index => $paquete) {
                $paquete->estado = $request->estado;
                if ($request->has('gramos')) {
                    $paquete->gramos = $request->gramos[$index];
                }
                if($request->has('volumen')){
                    $paquete->volumen = $request->volumen[$index];
                }
                if ($request->has('precio_kg')) {
                    $paquete->precio_kg = $request->precio_kg[$index];
                }
                if ($request->has('precio_fijo')) {
                    $paquete->precio_fijo = $request->precio_fijo[$index];
                }
                // if ($request->has('usuario')) {
                //     $paquete->id_usuario = $request->usuario[$index];
                // }
                PackageHistory::create([
                    'id_paquete' => $paquete->id,
                    'estado' => $request->estado,
                ]);
                $paquete->save();
                if ($paquete->destino == Destino::BRASIL) {
                    if ($paquete->estado != EstadoPaquete::$PARAGUAY) {
                        $this->sendEmail($paquete, $request->estado);
                    }
                } else {
                    $this->sendEmail($paquete, $request->estado);
                    if (in_array($paquete->estado, [
                        EstadoPaquete::$MIAMI,
                        EstadoPaquete::$OFICINA,
                        EstadoPaquete::$RETIRADO,
                    ])) {
//                        $this->sendNotification($paquete, $request->estado);
                    }
                }
            }
            if ($request->estado == EstadoPaquete::$BRASIL) {
                $this->generateAndSendInvoice($paquetes, $request, false);
            }
            if ($request->estado == EstadoPaquete::$OFICINA) {
                $this->generateAndSendInvoice($paquetes, $request);
            }
        }
        $data = [
            'estado' => $request->estado
        ];

        // return redirect()->route('admin.paquetes.index')->with([
        //     'msg' => 'Estados de paquetes actualizados exitósamente. Los usuarios de dichos paquetes serán informados del cambio.'
        // ]);
        return ['status' => '200', 'msg' => 'Estados actualizados con éxito!'];
    }

    public function changeUserPaquete(Request $request){
        $usuarioId = $request['userPackageId'];
        $paquete = Paquete::findOrFail($request['packageId']);
        $paquete->update([
            'id_usuario' => $usuarioId
        ]);
        return ['status' => 200];
    }

    public function formCambioEstado()
    {
        return view('admin.paquetes.cambio-estado', [
            'estados' => $this->getEstadosPaquetes(),
            'usuarios' => $this->getUsuarios(false),
        ]);
    }
    
    public function formCambioEstadoTest(){
        return view('admin.paquetes.cambio-estado-testVue', [
            'estados' => $this->getEstadosPaquetes(),
            'usuarios' => $this->getUsuarios(false),
        ]);
    }

    public function dataPaquete(){
        return $dataPaquete = [
            'estados' => $this->getEstadosPaquetes(),
            'usuarios' => $this->getUsuarios(false),
        ];
    }

    public function buscarAjax(Request $request)
    {
        return Paquete::where('codigo', '=', $request->search)
            ->orWhere('tracking', '=', $request->search)
            ->with(['usuario' => function($q) {
                $q->select(
                    'id',
                    'id_sucursal',
                    'casilla',
                    'name',
                    'last_name'
                );
            }, 'usuario.sucursal' => function ($w) {
                $w->select('id', 'prefix');
            }])
            ->first();
    }

    public function buscarAjaxMulti(Request $request)
    {
        return Paquete::whereDate('updated_at', '>=', $request->from . ' 00:00:00')
            ->whereDate('updated_at', '<=', $request->to . ' 23:59:59')
            ->where('estado', '=', $request->estado)
            ->where('wh', '=', $request->wh)
            ->with(['usuario' => function($q) {
                $q->select(
                    'id',
                    'id_sucursal',
                    'casilla',
                    'name',
                    'last_name'
                );
            }, 'usuario.sucursal' => function ($w) {
                $w->select('id', 'prefix');
            }])
            ->get();
    }

    public function cambioEstado(Request $request)
    {
        //
    }

    public function delete($id)
    {
        if(Paquete::where('id', '=', $id)->update(['estado' => EstadoPaquete::$ELIMINADO])) {
            return redirect()->route('admin.paquetes.index')->with([
                'msg' => 'Paquete eliminado con éxito!'
            ]);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function findPacklist(Request $request)
    {
        $packlists = Packlist::select(
            'id',
            'tracking',
            'id_usuario',
            'courier',
            'descripcion',
            'valor',
            'moneda',
            'file',
            'mime',
            'tipo_flete',
            'destino',
            'seguro',
            'consolidacao',
            'consolidacao_trackings'
        )->doesntHave('paquete')->get();
        $options = [];
        foreach ($packlists as $packlist) {
            $length = strlen($packlist->tracking);
            if (substr($request->tracking, -$length) === $packlist->tracking) {
                array_push($options, $packlist);
            }
        }
        return $options;
    }

    private function sendEmail($paquete, $estado)
    {
        Mail::send('emails.estado', [
            'estado' => $estado,
            'tracking' => $paquete->tracking,
            'nombre' => $paquete->usuario->name,
            'destino' => $paquete->destino,
            'date' => $paquete->updated_at
        ], function ($message) use($paquete) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to($paquete->usuario->email);
            $message->subject($paquete->usuario->name . ', hubo una actualización en el estado de su paquete!');
        });
    }

    private function sendNotification($paquete, $estado)
    {
        if ($paquete->usuario->fcm_token) {
            $client = new FcmClient(env('FIREBASE_SERVER_TOKEN'), env('FIREBASE_SENDER_ID'));
            $notification = new Notification();
            $body = "";
            switch ($paquete->estado) {
                case EstadoPaquete::$MIAMI:
                    $body = "Su paquete con tracking $paquete->tracking fue recibido en nuestras oficinas de Miami!";
                    break;
                case EstadoPaquete::$OFICINA:
                    $body = "Su paquete con tracking $paquete->tracking ya está en nuestras oficinas de Paraguay! Puede pasar a recogerlo!";
                    break;
                case EstadoPaquete::$RETIRADO:
                    $body = "Su paquete con tracking $paquete->tracking fue retirado de nuestras oficinas. Gracias por la confianza y esperamos que haya tenido una buena experiencia comprando con StrBox!";
            }
            $notification
                ->addRecipient($paquete->usuario->fcm_token)
                ->setTitle($paquete->usuario->name . ', hubo una actualización en el estado de su paquete!')
                ->setBody($body)
                ->addData('package_id', $paquete->id);

            $client->send($notification);
        }
    }

    private function generateAndSendInvoice($paquetes, Request $request, $send = true)
    {
        foreach($paquetes->groupBy('id_usuario') as $group) {
            $total = 0;
            foreach($group as $paquete) {
                $total += $paquete->valor_total;
            }
            $invoice = null;
            DB::transaction(function() use($group, $total, &$invoice) {
                $invoice = Invoice::create([
                    'id_usuario' => $group[0]->usuario->id,
                    'total' => $total
                ]);
                foreach($group as $paquete) {
                    ItemInvoice::create([
                        'id_invoice' => $invoice->id,
                        'id_paquete' => $paquete->id
                    ]);
                }
            });
            $total = $invoice->total;
            if ($send && $request->has('email')) {
                Mail::send('emails.invoice', [
                    'nombre' => $group[0]->usuario->full_name,
                    'id' => $group[0]->usuario->id,
                    'paquetes' => $group,
                    'total' => $total,
                    'invoice' => $invoice,
                    'date' => Carbon::now()
                ], function ($message) use($paquete) {
                    $message->from('info@strbox.com.py', 'StrBox');
                    $message->to($paquete->usuario->email);
                    $message->subject($paquete->usuario->name . ', tiene un nuevo invoice disponible!');
                });
            }
        }
    }

    private function getEstadosPaquetes()
    {
        $res = [];
        $res[EstadoPaquete::$MIAMI] = "En MIAMI";
        $res[EstadoPaquete::$CHINA] = "En CHINA";
        $res[EstadoPaquete::$TRANSITO] = "En TRANSITO";
        $res[EstadoPaquete::$PARAGUAY] = "En PARAGUAY";
        $res[EstadoPaquete::$BRASIL] = "En BRASIL";
        $res[EstadoPaquete::$OFICINA] = "En OFICINA/PARA RETIRAR";
        $res[EstadoPaquete::$RETIRADO] = "RETIRADO";
        return $res;
    }

    private function getUsuarios($acceptAll = true)
    {
        $res = [];
        if ($acceptAll) {
            $res[''] = "----TODOS LOS USUARIOS----";
        }
        foreach(User::select('id', 'name', 'last_name', 'casilla', 'id_sucursal')->where('type', '=', 2)->get() as $usuario) {
            $res[$usuario->id] = $usuario->prefix . $usuario->casilla . ' - ' . $usuario->full_name;
        }
        return $res;
    }
    
    private function getTrackingNumbers()
    {
        // dd(\App\Invoice::get());
        $res = [];
        $res[''] = "----TODOS LOS TRACKING----";
        foreach(Paquete::all() as $paquete) {
            $res[$paquete->tracking] = $paquete->tracking;
        }
        return $res;
    }

    private function getTipoPrecio()
    {
        $res = [];
        $res[TipoPrecioPaquete::POR_PESO] = "OTROS PRODUCTOS";
        $res[TipoPrecioPaquete::POR_UNIDAD] = "TELEFONOS";
        return $res;
    }
    
    public function getContainers(){
        try {
            $containers = [];
            $containers[] = '-- SELECCIONAR CONTAINER --';
            foreach(Container::select('id', 'nombre')->get() as $container){
                $containers[$container->id] = $container->nombre;
            }
            return $containers;
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al obtener los containers. '. $th->getMessage()]);
        }
    }

    public function getEstadosContainer(){
        try {
            $estados = [];
            $estados[EstadoPaquete::$MIAMI] = 'EN MIAMI';
            $estados[EstadoPaquete::$MIAMI_MARITIMO] = 'EN MIAMMIAMI_MARITIMOI';
            $estados[EstadoPaquete::$TRANSITO] = 'EN TRANSITO';
            $estados[EstadoPaquete::$PARAGUAY] = 'EN PARAGUAY';
            $estados[EstadoPaquete::$OFICINA] = 'EN OFICINA';
            $estados[EstadoPaquete::$RETIRADO] = 'EN RETIRADO';
            $estados[EstadoPaquete::$CHINA] = 'EN CHINA';
            $estados[EstadoPaquete::$BRASIL] = 'EN BRASIL';
            return $estados;
        } catch (\Exception $th) {
            return redirect()->back()->withErrors(['msg' => 'Error al obtener los estados de containers. '. $th->getMessage()]);
        }
    }
}
