<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaPost;
use App\EntradaGaleriaPost;
use App\Helpers\Admin\SelectHelper;
use App\Http\Controllers\CRUDControllerInterface;
use App\Multimedia;
use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::select('id', 'titulo', 'cuerpo', 'img_url', 'id_categoria')
            ->activo()
            ->with(['categoria' => function($q) {
            $q->select('id', 'nombre');
        }])->paginate(10);
        return view('admin.posts.index', ['posts' => $posts]);
    }

    public function create()
    {
        $multimedias = Multimedia::select('id', 'nombre', 'url', 'tipo')
            ->get();
        return view('admin.posts.create', [
            'categorias' => SelectHelper::make(CategoriaPost::select('id', 'nombre')->activo()->get()),
            'multimedias' => $multimedias
        ]);
    }

    public function store(PostRequest $request)
    {
        $exception = DB::transaction(function() use($request) {
            $datos = $request->all();
            $datos['id_autor'] = auth()->user()->id;
            $post = Post::create($datos);
            if ($request->has('multimedias')) {
                foreach ($request->multimedias as $multimedia) {
                    EntradaGaleriaPost::create([
                        'id_post' => $post->id,
                        'id_multimedia' => $multimedia
                    ]);
                }
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.posts.index')->with(['msg' => 'Post registrado con éxito!']);
    }

    public function edit($id)
    {
        $post = Post::select('id', 'titulo', 'cuerpo', 'img_url', 'posicion', 'id_categoria')
            ->with(['categoria' => function($q) {
                $q->select('id', 'nombre');
            }, 'multimedias' => function($q) {
                $q->select('id', 'id_post', 'id_multimedia');
            }])
            ->activo()
            ->find($id);
        $multimediasSelected = array_map(function($item) {
            return $item['id_multimedia'];
        }, $post->multimedias->toArray());
        $multimedias = $multimedias = Multimedia::select('id', 'nombre', 'url', 'tipo')
            ->get();
        return view('admin.posts.edit', [
            'post' => $post,
            'categorias' => SelectHelper::make(CategoriaPost::select('id', 'nombre')->activo()->get()),
            'multimedias' => $multimedias,
            'selected' => $multimediasSelected
        ]);
    }

    public function update($id, PostRequest $request)
    {
        $exception = DB::transaction(function() use($request, $id) {
            $datos = $request->except(['_token']);
            $post = Post::select('id')->find($id);
            $post->fill($datos);
            $post->save();
            $multimedias = $request->has('multimedias') ? $request->multimedias : [];
            EntradaGaleriaPost::whereNotIn('id', $multimedias)->delete();
            foreach ($multimedias as $multimedia) {
                EntradaGaleriaPost::firstOrCreate([
                    'id_post' => $post->id,
                    'id_multimedia' => $multimedia
                ]);
            }
        });
        if ($exception) return redirect()->back()->withErrors();
        return redirect()->route('admin.posts.index')->with(['msg' => 'Post actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Post::where('id', '=', $id)->update(['activo' => 0])) {
            return redirect()->route('admin.posts.index')->with(['msg' => 'Post eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }
}
