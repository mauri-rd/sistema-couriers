<?php

namespace App\Http\Controllers\Admin;

use App\Proveedor;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProveedoresController extends Controller
{
    public function index()
    {
        $proveedores = Proveedor::select('id', 'nombre')->paginate(10);
        return view('admin.financiero.proveedores.index', ['proveedores' => $proveedores]);
    }

    public function create(Request $request)
    {
        return view('admin.financiero.proveedores.create', [
            'closeWhenFinished' => $request->closeWhenFinished,
        ]);
    }

    public function store(Request $request)
    {
        Proveedor::create($request->all());

        return redirect()->route('admin.proveedores.index')->with([
            'msg' => 'Proveedor registrado con éxito!',
            'closeWhenFinished' => $request->has('closeWhenFinished'),
        ]);
    }

    public function edit($id)
    {
        $proveedor = Proveedor::select('id', 'nombre')
            ->find($id);
        return view('admin.financiero.proveedores.edit', [
            'proveedor' => $proveedor,
        ]);
    }

    public function update($id, Request $request)
    {
        $proveedor = Proveedor::select('id')->find($id);
        $proveedor->fill($request->all());
        $proveedor->save();
        return redirect()->route('admin.proveedores.index')->with(['msg' => 'Proveedor actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Proveedor::where('id', '=', $id)->delete()) {
            return redirect()->route('admin.proveedores.index')->with(['msg' => 'Proveedor eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors('Error al eliminar proveedor');
        }
    }
}
