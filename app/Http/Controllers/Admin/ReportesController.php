<?php

namespace App\Http\Controllers\Admin;

use App\Enum\EstadoPaquete;
use App\Http\Controllers\Controller;
use App\Paquete;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ReportesController extends Controller
{
    const ESCRITORIO = 'escritorio';
    const ENTREGADA = 'entregada';
    const WAREHOUSE = 'warehouse';

    public function index(Request $request)
    {
        return view('admin.reportes.index', [
            'estados' => $this->getEstados(),
        ]);
    }

    public function generar(Request $request)
    {
        // dd($request->all());
        $paquetes = Paquete::whereHas('history', function ($w) use ($request) {
            return $w->whereDate('created_at', '>=', $request->from)
                ->whereDate('created_at', '<=', $request->to)
                ->whereEstado($request->show);
        });

        return view('admin.reportes.generar', [
            'estado' => $this->getEstados()[$request->show],
            'cant' => $paquetes->count(),
            'peso' => $paquetes->sum('gramos'),
            'paquetes' => $paquetes->orderBy('created_at', 'desc')->get(),
            'from' => Carbon::createFromFormat('Y-m-d', $request->from),
            'to' => Carbon::createFromFormat('Y-m-d', $request->to),
            'total' => $paquetes->sum(DB::raw('gramos/1000*precio_kg'))
        ]);
    }

//    private function getTipos()
//    {
//        $res = [];
//        $res[self::ESCRITORIO] = "En escritorio";
//        $res[self::ENTREGADA] = "Entregada";
//        $res[self::WAREHOUSE] = "En warehouse";
//        return $res;
//    }

    private function getEstados()
    {
        $res = [];
        $res[EstadoPaquete::$MIAMI] = "En Miami";
        $res[EstadoPaquete::$CHINA] = "En China";
        $res[EstadoPaquete::$TRANSITO] = "En Transito";
        $res[EstadoPaquete::$PARAGUAY] = "En Paraguay";
        $res[EstadoPaquete::$OFICINA] = "En Oficina";
        $res[EstadoPaquete::$RETIRADO] = "Retirado";
        return $res;
    }
}
