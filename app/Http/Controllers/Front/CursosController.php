<?php

namespace App\Http\Controllers\Front;

use App\Curso;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CursosController extends Controller
{
    public function index(Request $request)
    {
        $cursos = Curso::activo()
            ->select('id', 'nombre', 'descripcion')
            ->with(['imagenes' => function($q) {
                $q->select('id', 'id_curso', 'url');
            }])
            ->filtrarBusqueda($request)
            ->paginate(12);
        return view('front.cursos.index', [
            'cursos' => $cursos->appends($request->all())
        ]);
    }

    public function find($id)
    {
        try {
            $curso = Curso::activo()
                ->select('id', 'nombre', 'descripcion')
                ->with(['imagenes' => function($q) {
                    $q->select('id', 'id_curso', 'url');
                }])
                ->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
        return view('front.cursos.find', [
            'curso' => $curso
        ]);
    }
}
