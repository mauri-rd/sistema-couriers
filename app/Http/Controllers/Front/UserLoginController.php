<?php

namespace App\Http\Controllers\Front;

use App\Sucursal;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use Auth;

use App\User;
use Illuminate\Support\Facades\DB;

class UserLoginController extends Controller
{
    public function loginForm() {
        return view('front.my-account.login', [
            'sucursales' => $this->getSucursales(),
        ]);
    }

    public function doLogin(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
            'type' => 2,
            'active' => true,
        ])) {
            return redirect()->route('front.my-account.addresses');
        }
        return redirect()->back()->withInput()->withErrors([
            'Datos no encontrados'
        ]);
    }

    public function doRegister(RegisterRequest $request)
    {
        DB::raw('LOCK TABLES users WRITE');
        $casilla = User::max('casilla') + 1;
        $user = User::create(array_merge($request->all(), [
            'type' => 2,
            'active' => true,
            'casilla' => $casilla,
        ]));
        DB::raw('UNLOCK TABLES users WRITE');
        Auth::login($user, true);
        return redirect()->route('front.my-account.addresses');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    private function getSucursales()
    {
        $res = [];
        $res[''] = 'Seleccione una sucursal';
        foreach (Sucursal::select('id', 'nombre')->get() as $sucursal) {
            $res[$sucursal->id] = $sucursal->nombre;
        }
        return $res;
    }

}
