<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Configuracion;

class AddressesController extends Controller
{
    public function index()
    {
        $configuraciones = collect(Configuracion::select('id', 'slug', 'nombre', 'valor')->get())->keyBy('slug');
        return view('front.my-account.addresses.index', [
            'configuraciones' => $configuraciones
        ]);
    }
}
