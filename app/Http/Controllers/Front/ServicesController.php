<?php

namespace App\Http\Controllers\Front;

use App\Enum\TipoFlete;
use App\Servicio;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index(Request $request)
    {
        return view('front.my-account.services.index');
//        return view('front.my-account.services.index', [
//            'user' => auth()->user(),
//            'fletes' => [
//                TipoFlete::NORMAL => 'NORMAL (10 dias)',
//                TipoFlete::EXPRESS => 'EXPRESS (4 dias)',
//            ]
//        ]);
    }

    public function store(Request $request)
    {
        User::where('id', auth()->id())->update([
            'tipo_flete' => $request->tipo_flete,
            'seguro' => $request->has('seguro'),
            'consolidacao' => $request->has('consolidacao'),
        ]);
        return redirect()->route('front.my-account.services.index')->with([
            'msg' => 'Serviços atualizados com sucesso!'
        ]);
    }
}
