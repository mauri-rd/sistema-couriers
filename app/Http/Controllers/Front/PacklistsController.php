<?php

namespace App\Http\Controllers\Front;

use App\Enum\Destino;
use App\Enum\TipoFlete;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PacklistRequest;
use App\Http\Controllers\Controller;

use App\Packlist;
use App\Enum\Couriers;
use App\Enum\Moneda;

class PacklistsController extends Controller
{
    public function index()
    {
        $packlists = Packlist::select(
                'id',
                'id_usuario',
                'tracking',
                'courier',
                'descripcion',
                'valor',
                'moneda',
                'file',
                'tipo_flete',
                'destino',
                'mime'
            )->with(['paquete' => function($q) {
                $q->select('id', 'id_packlist');
            }])->delUsuario(auth()->id())
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('front.my-account.packlists.index', ['packlists' => $packlists]);
    }

    public function create()
    {
        return view('front.my-account.packlists.create', [
            'couriers' => Couriers::list(),
            'monedas' => Moneda::list(),
            'fletes' => [
                TipoFlete::NORMAL => 'NORMAL',
                TipoFlete::EXPRESS => 'EXPRESS',
            ],
            'destinos' => [
                Destino::PARAGUAY => 'PARAGUAY',
//                Destino::BRASIL => 'BRASIL',
            ],
            'user' => auth()->user()
        ]);
    }

    public function store(PacklistRequest $request)
    {
        $servicios = [
            'tipo_flete' => $request->tipo_flete,
            'destino' => $request->destino,
            'consolidacao' => $request->has('consolidacao'),
            'consolidacao_trackings' => $request->has('consolidacao') ? $request->consolidacao_trackings : null,
            'seguro' => $request->has('seguro'),
        ];
        if (intval($request->destino) === Destino::BRASIL) {
            $servicios['tipo_flete'] = TipoFlete::NORMAL;
        }
        $packlist = Packlist::create(array_merge($request->all(), [
            'id_usuario' => auth()->id()
        ], $servicios));
        return redirect()->route('front.my-account.packlists.index')->with(['msg' => 'Packlist registrado con éxito!']);
    }

    public function edit($id)
    {
        $packlist = Packlist::select(
                'id',
                'id_usuario',
                'tracking',
                'courier',
                'descripcion',
                'valor',
                'moneda',
                'file',
                'mime',
                'tipo_flete',
                'destino',
                'seguro',
                'consolidacao',
                'consolidacao_trackings'
            )
            ->delUsuario(auth()->id())->where('id', '=', $id)->first();
        return view('front.my-account.packlists.edit', [
            'packlist' => $packlist,
            'couriers' => Couriers::list(),
            'monedas' => Moneda::list(),
            'fletes' => [
                TipoFlete::NORMAL => 'NORMAL',
                TipoFlete::EXPRESS => 'EXPRESS',
            ],
            'destinos' => [
                Destino::PARAGUAY => 'PARAGUAY',
//                Destino::BRASIL => 'BRASIL',
            ],
            'user' => auth()->user()
        ]);
    }

    public function update($id, PacklistRequest $request)
    {
        $servicios = [
            'tipo_flete' => $request->tipo_flete,
            'destino' => $request->destino,
            'consolidacao' => $request->has('consolidacao'),
            'seguro' => $request->has('seguro'),
        ];
        if (intval($request->destino) === Destino::BRASIL) {
            $servicios['tipo_flete'] = TipoFlete::NORMAL;
        }
        $packlist = Packlist::select('id')->find($id);
        $packlist->fill(array_merge($request->all(), $servicios));
        $packlist->save();
        return redirect()->route('front.my-account.packlists.index')->with(['msg' => 'Packlist actualizado con éxito!']);
    }

    public function delete($id)
    {
        if(Packlist::where('id', '=', $id)->delete()) {
            return redirect()->route('front.my-account.packlists.index')->with(['msg' => 'Packlist eliminado con éxito!']);
        } else {
            return redirect()->back()->withErrors();
        }
    }

    public function checkTracking(Request $request)
    {
        $packlist = Packlist::paraConsolidarTracking($request->tracking)->first();
        return !is_null($packlist) ? $packlist->consolidacao_trackings : '';
    }
}
