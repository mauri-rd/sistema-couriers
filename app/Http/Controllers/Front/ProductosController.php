<?php

namespace App\Http\Controllers\Front;

use App\CategoriaProducto;
use App\Marca;
use App\Modelo;
use App\Producto;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductosController extends Controller
{
    public function index(Request $request)
    {
        $order = $request->has('order') ? $request->order : "az";
        $productos = Producto::activo()
            ->select('id', 'nombre', 'descripcion', 'id_modelo', 'id_categoria', 'precio')
            ->with(['modelo' => function($q) {
                $q->select('id', 'nombre', 'id_marca');
            }, 'imagenes' => function($q) {
                $q->select('id', 'url', 'id_producto');
            }, 'categoria' => function($q) {
                $q->select('id', 'nombre');
            }])
            ->filtrarCategoria($request)
            ->filtrarMarca($request)
            ->filtrarModelo($request)
            ->filtrarBusqueda($request)
            ->ordenar($order)
            ->paginate(12);

        $marcas = Marca::activo()
            ->select('id', 'nombre')
            ->with(['modelos' => function($q) {
                $q->select('id', 'nombre', 'id_marca');
            }, 'modelos.productos' => function($q) {
                $q->select('id_modelo');
            }])
            ->get();

        $categorias = CategoriaProducto::activo()
            ->select('id', 'nombre')
            ->with(['productos' => function($q) {
                $q->select('id', 'id_categoria');
            }])
            ->get();
        return view('front.productos.index', [
            'productos' => $productos->appends($request->all()),
            'categorias' => $categorias,
            'marcas' => $marcas
        ]);
    }

    public function find($id)
    {
        try {
            $producto = Producto::activo()
                ->select('id', 'nombre', 'descripcion', 'id_modelo', 'id_categoria', 'precio')
                ->with(['modelo' => function ($q) {
                    $q->select('id', 'nombre', 'id_marca');
                }, 'imagenes' => function ($q) {
                    $q->select('id', 'url', 'id_producto');
                }, 'categoria' => function ($q) {
                    $q->select('id', 'nombre');
                }])
                ->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
        return view('front.productos.find', [
            'producto' => $producto
        ]);
    }
}
