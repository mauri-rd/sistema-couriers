<?php

namespace App\Http\Controllers\Front;

use App\Comprobante;
use App\Enum\Destino;
use App\Enum\TipoCambioComprobante;
use App\ItemComprobante;
use App\ItemInvoice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Paquete;
use App\Enum\EstadoPaquete;
use Illuminate\Support\Facades\Mail;

class PaquetesController extends Controller
{
    public function index()
    {
        return redirect()->route();
    }

    public function miami()
    {
        return $this->getPaquetesResponse([
            EstadoPaquete::$MIAMI,
            EstadoPaquete::$CHINA,
        ]);
    }

    public function transito()
    {
        return $this->getPaquetesResponse(EstadoPaquete::$TRANSITO);
    }

    public function paraguay()
    {
        return $this->getPaquetesResponse(EstadoPaquete::$PARAGUAY);
    }

    public function oficina()
    {
        return $this->getPaquetesResponse(EstadoPaquete::$OFICINA);
    }

    public function retirado()
    {
        return $this->getPaquetesResponse(EstadoPaquete::$RETIRADO);
    }

    private function getPaquetesResponse($estado)
    {
        $paquetes = Paquete::select(
            'id',
            'codigo',
            'tracking',
            'gramos',
            'estado',
            'destino',
            'precio_kg',
            'descripcion',
            'seguro',
            'consolidacao',
            'consolidacao_trackings',
            'tipo_precio',
            'unidades',
            'updated_at',
            'volumen'
        )->filtrarEstadoDestino(is_array($estado) ? $estado : [$estado])
        ->activo()
        ->where('id_usuario', '=', auth()->id())
        ->orderBy('updated_at')->get();
        $paquetesPagados = collect([]);
        if ($estado === EstadoPaquete::$OFICINA) {
            $paquetesPagados = ItemInvoice::where('pago', true)->whereIn('id_paquete', $paquetes->pluck('id'))->pluck('id_paquete');
        }
        return view('front.my-account.paquetes.index', array_merge([
            'paquetes' => $paquetes,
            'estado' => $estado,
            'paquetesPagados' => $paquetesPagados,
        ], $this->getPaquetesCount()));
    }

    private function getPaquetesCount()
    {
        return [
            'miami' => Paquete::where('id_usuario', '=', auth()->id())
                ->where(function ($w) {
                    $w->where('estado', EstadoPaquete::$CHINA)
                        ->orWhere('estado', EstadoPaquete::$MIAMI);
                })
                ->activo()
                ->count(),
            'transito' => Paquete::where('id_usuario', '=', auth()->id())
                ->where(function ($w) {
                    $w->where(function ($w) {
                        $w->where('destino', '=', Destino::BRASIL)
                            ->where('estado', '=', EstadoPaquete::$PARAGUAY);
                    })->orWhere(function ($w) {
                        $w->where('destino', '=', Destino::PARAGUAY)
                            ->where('estado', '=', EstadoPaquete::$TRANSITO);
                    })->orWhere(function ($w) {
                        $w->where('destino', '=', Destino::BRASIL)
                            ->where('estado', '=', EstadoPaquete::$TRANSITO);
                    });
                })
                ->activo()
                ->count(),
            'paraguay' => Paquete::where('id_usuario', '=', auth()->id())
                ->where(function ($w) {
                    $w->where(function ($w) {
                        $w->where('destino', '=', Destino::BRASIL)
                            ->where('estado', '=', EstadoPaquete::$BRASIL);
                    })->orWhere(function ($w) {
                        $w->where('destino', '=', Destino::PARAGUAY)
                            ->where('estado', '=', EstadoPaquete::$PARAGUAY);
                    });
                })
                ->activo()
                ->count(),
            'oficina' => Paquete::where('id_usuario', '=', auth()->id())
                ->where('estado', '=', EstadoPaquete::$OFICINA)
                ->activo()
                ->count(),
            'retirado' => Paquete::where('id_usuario', '=', auth()->id())
                ->where('estado', '=', EstadoPaquete::$RETIRADO)
                ->activo()
                ->count()
        ];
    }

    public function pagarForm(Request $request)
    {
        if (!$request->has('id_paquete')) {
            return redirect()->back()->withErrors('Por favor, seleccione un paquete como mínimo para pagar');
        }
        $paquetes = Paquete::select(
            'id',
            'codigo',
            'volumen',
            'tracking',
            'gramos',
            'precio_kg',
            'descripcion',
            'seguro',
            'consolidacao',
            'consolidacao_trackings',
            'tipo_precio',
            'unidades',
            'updated_at'
        )->whereIn('id', $request->id_paquete)
            ->activo()
            ->where('id_usuario', '=', auth()->id())
            ->orderBy('updated_at')->get();

        $totalUsd = $this->getTotalPaquetes($paquetes);

        $totalUsd = round($totalUsd, 2);

        $precioArbitrage = $this->getArbitragePrice();
        $totalCambio = $totalUsd * $precioArbitrage;
        return view('front.my-account.paquetes.pagar', [
            'paquetes' => $paquetes,
            'totalUsd' => $totalUsd,
            'totalCambio' => $totalCambio,
            'cambioSymbol' => $this->getArbitrageSymbol(),
            'precioArbitrage' => $precioArbitrage
        ]);
    }

    public function pagar(Request $request)
    {
        $ok = false;
        $comprobante = null;
        DB::transaction(function () use (&$ok, &$comprobante, $request) {
            $totalUsd = $this->getTotalPaquetes(null, $request->id_paquete);

            $precioArbitrage = $this->getArbitragePrice();
            $totalCambio = $totalUsd * $precioArbitrage;

            $cambioSymbol = $this->getArbitrageName();

            $comprobante = Comprobante::create([
                'total_usd' => $totalUsd,
                'total_brl' => $cambioSymbol == 'BRL' ? $totalCambio : 0,
                'total_pyg' => $cambioSymbol == 'USD' ? $totalCambio : 0,
                'tipo_cambio' => $cambioSymbol == 'BRL' ? TipoCambioComprobante::BRL : TipoCambioComprobante::PYG,
                'arbitraje' => $precioArbitrage,
                'comprobante' => $request->comprobante,
                'id_cliente' => auth()->id()
            ]);
            foreach ($request->id_paquete as $idPaquete) {
                ItemComprobante::create([
                    'id_comprobante' => $comprobante->id,
                    'id_paquete' => $idPaquete,
                ]);
            }
            $ok = true;
        });
        if ($ok) {
            Mail::send('emails.pago-admin', [
                'pago' => $comprobante
            ], function ($message) {
                $message->from('info@strbox.com.py', 'StrBox');
                $message->to('contacto@strbox.com.py');
                $message->subject('Se recibieron datos de pago por algunos paquetes!');
            });
            return redirect()->route('front.my-account.paquetes-oficina')->with([
                'msg' => 'Comprobante enviado exitósamente. Puede acompañar el estado de su pago desde el panel. Una vez que comprobemos el estado del pago, cambiaremos el estado de pago de su paquete',
            ]);
        }
        return redirect()->back()->withErrors('Hubo un problema en el envío. Por favor, intente nuevamente');
    }

    private function getArbitragePrice()
    {
        $baseUrl = "http://www.cambioschaco.com.py/api/branch_office/9/exchange";
        $data = json_decode(file_get_contents($baseUrl));
        $prices = collect($data->items);
        $cambio = $prices->first(function ($index, $price) {
            return $price->isoCode === $this->getArbitrageName();
        });
        if (env('PAYMENT_ARBITRAGE_TYPE') == TipoCambioComprobante::BRL) {
            return $cambio->saleArbitrage;
        }
        return $cambio->salePrice;
    }

    private function getArbitrageName()
    {
        return env('PAYMENT_ARBITRAGE_TYPE') == TipoCambioComprobante::BRL ? 'BRL' : 'USD';
    }

    private function getArbitrageSymbol()
    {
        return env('PAYMENT_ARBITRAGE_TYPE') == TipoCambioComprobante::BRL ? 'RS$' : 'Gs.';
    }

    private function getTotalPaquetes($paquetes = null, $idPaquetes = [])
    {
        if (is_null($paquetes)) {
            $paquetes = Paquete::select(
                'id',
                'codigo',
                'volumen',
                'tracking',
                'gramos',
                'precio_kg',
                'descripcion',
                'seguro',
                'consolidacao',
                'consolidacao_trackings',
                'tipo_precio',
                'unidades',
                'updated_at'
            )->whereIn('id', $idPaquetes)
                ->activo()
                ->where('id_usuario', '=', auth()->id())
                ->orderBy('updated_at')->get();
        }

        return $paquetes->sum(function ($paquete) {
            return $paquete->total_sumado;
        });
    }

}
