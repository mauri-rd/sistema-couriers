<?php

namespace App\Http\Controllers\Front;

use App\Configuracion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{
    public function index()
    {
        $configs = collect(Configuracion::select('id', 'slug', 'nombre', 'valor')
            ->whereIn('slug', ['historia', 'mision', 'vision'])
            ->get())->keyBy('slug');
        return view('front.empresa.index', [
            'configs' => $configs
        ]);
    }
}
