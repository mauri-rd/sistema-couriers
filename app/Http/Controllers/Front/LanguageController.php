<?php

namespace App\Http\Controllers\Front;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    public function setLanguage(Request $request)
    {
        User::whereId(auth()->id())
            ->update([
                'lang' => $request->language,
            ]);
        return redirect()->back();
    }
}
