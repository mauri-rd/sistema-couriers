<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use App\User;
use App\PasswordReset;

class PasswordController extends Controller
{
    public function form()
    {
        return view('front.my-account.reset-password');
    }

    public function requestReset(Request $request)
    {
        if (User::where('email', '=', $request->email)->count() == 0) {
            return redirect()
            ->back()
            ->withInput()
            ->withErrors([
                'No fue encontrado ningún usuario con esta dirección de e-mail'
            ]);
        }

        $token = md5(uniqid(rand(), true));

        $reset = PasswordReset::where('email', '=', $request->email)->first();
        if (!is_null($reset)) {
            $reset->token = $token;
            $reset->save();
        } else {
            PasswordReset::create([
                'email' => $request->email,
                'token' => $token
            ]);
        }

        $link = route('front.password-reset.reset-form', ['email' => $request->email, 'token' => $token]);
        
        Mail::send('emails.password', [
            'link' => $link
        ], function ($message) use($request) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to($request->email);
            $message->subject('Solicitud de cambio de contraseña');
        });
        return redirect()->back()->with([
            'status' => 'Fue enviado un correo con instrucciones para reestablecer su contraseña!'
        ]);
    }

    public function resetForm(Request $request)
    {
        $reset = PasswordReset::where('email', '=', $request->email)
            ->where('token', '=', $request->token)
            ->count();
        if ($reset == 0) {
            return redirect()->route('front.login-form')->withErrors([
                'No fue encotrado este token en nuestra base de datos'
            ]);
        }
        return view('front.my-account.reset-password-token', [
            'email' => $request->email,
            'token' => $request->token
        ]);
    }

    public function reset(Request $request)
    {
        $reset = PasswordReset::where('email', '=', $request->email)
            ->where('token', '=', $request->token)
            ->first();
        if (is_null($reset)) {
            return redirect()->route('front.login-form')->withErrors([
                'No fue encotrado este token en nuestra base de datos'
            ]);
        }
        $user = User::where('email', '=', $request->email)->first();
        $user->password = $request->password;
        $user->save();
        $reset->delete();
        return redirect()->route('front.login-form')->with([
            'status' => 'Contraseña reestablecida con éxito!'
        ]);
    }
}
