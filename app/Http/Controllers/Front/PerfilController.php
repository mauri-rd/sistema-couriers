<?php

namespace App\Http\Controllers\Front;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PerfilController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return view('front.my-account.perfil.index', [
            'user' => $user
        ]);
    }

    public function save(Request $request)
    {
        $user = User::find(auth()->id());
        $user->fill($request->only([
            'name',
            'last_name',
            'phone',
            'address',
//            'country',
//            'city',
//            'state',
//            'zip',
            'ci',
            'password',
        ]));
        $user->save();
        return redirect()->route('front.my-account.perfil.index')
            ->with([
                'msg' => 'Dados atualizados com sucesso!'
            ]);
    }
}
