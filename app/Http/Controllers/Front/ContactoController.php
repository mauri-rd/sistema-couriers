<?php

namespace App\Http\Controllers\Front;

use App\Configuracion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
    public function index()
    {
        $configuraciones = collect(Configuracion::select('id', 'nombre', 'slug', 'valor')
            ->whereIn('slug', ['numeros-de-telefonos', 'correos', 'direccion', 'longitud', 'latitud'])
            ->get())->keyBy('slug');
        return view('front.contacto.index', [
            'configuraciones' => $configuraciones
        ]);
    }

    public function send(Request $request)
    {
        Mail::send('emails.contacto', ['data' => $request->all()], function($message) use($request) {
            $message->from('contacto@espaciovip.com.py');
            $message->to('mauri.rd@gmail.com');
            $message->subject('Tiene un nuevo correo en Espacio Vip');
            $message->replyTo($request->email);
        });
        return redirect()->back()->with(['msg' => 'Mensaje enviado exitosamente!']);
    }
}
