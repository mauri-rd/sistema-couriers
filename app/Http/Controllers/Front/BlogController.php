<?php

namespace App\Http\Controllers\Front;

use App\CategoriaPost;
use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $posts = Post::activo()
            ->select('id', 'titulo', 'cuerpo', 'img_url', 'id_categoria', 'id_autor', 'created_at')
            ->with(['categoria' => function($q) {
                $q->select('id', 'nombre');
            }, 'autor' => function($q) {
                $q->select('id', 'name');
            }])
            ->filtrarCategoria($request)
            ->filtrarPeriodo($request)
            ->filtrarBusqueda($request)
            ->paginate(12);

        return view('front.blog.index', array_merge([
            'posts' => $posts->appends($request->all()),
        ], $this->commons()));
    }

    public function find($id)
    {
        try {
            $post = Post::activo()
                ->select('id', 'titulo', 'cuerpo', 'img_url', 'id_categoria', 'id_autor', 'created_at')
                ->with(['categoria' => function($q) {
                    $q->select('id', 'nombre');
                }, 'autor' => function($q) {
                    $q->select('id', 'name');
                }, 'multimedias' => function($q) {
                    $q->select('id_post', 'id_multimedia');
                }, 'multimedias.multimedia' => function($q) {
                    $q->select('id', 'nombre', 'url', 'tipo', 'id_categoria', 'link_youtube');
                }])
                ->find($id);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
        return view('front.blog.find', array_merge([
            'post' => $post
        ], $this->commons()));
    }

    private function commons()
    {

        $ultimosPosts = Post::activo()
            ->select('id', 'titulo', 'id_autor', 'img_url', 'created_at')
            ->with(['categoria' => function($q) {
                $q->select('id', 'nombre');
            }, 'autor' => function($q) {
                $q->select('id', 'name');
            }])
            ->limit(5)
            ->get();
        $categorias = CategoriaPost::activo()
            ->with(['posts' => function($q) {
                $q->select('id_categoria');
            }])
            ->select('id', 'nombre')
            ->get();
        $periodos = Post::activo()
            ->selectRaw('MONTH(created_at) as mes, YEAR(created_at) as anho')
            ->groupBy('mes', 'anho')
            ->orderBy('anho', 'mes', 'asc')
            ->get();
        return [
            'ultimosPosts' => $ultimosPosts,
            'categorias' => $categorias,
            'periodos' => $periodos
        ];
    }
}
