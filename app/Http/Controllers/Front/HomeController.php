<?php

namespace App\Http\Controllers\Front;

use App\Paquete;
use App\Sucursal;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Configuracion;
use App\Empresa;
use App\Pregunta;
use App\Slide;

class HomeController extends Controller
{
    public function index()
    {
        $configuraciones = collect(Configuracion::select('id', 'slug', 'nombre', 'valor')->get())->keyBy('slug');
        $empresas = Empresa::select('nombre', 'imagen_url', 'link')->get();
        $patrocinadores = Slide::select('id', 'url', 'type')->activo()->get();
        $preguntas = Pregunta::select(
                'id',
                'pregunta',
                'respuesta'
            )->orderBy('posicion', 'asc')
            ->activo()
            ->get();
        $usersCount = User::count();
        $sucursalesCount = Sucursal::count();
        $paquetesCount = Paquete::count();
        return view('front.index', [
            'configuraciones' => $configuraciones,
            'empresas' => $empresas,
            'preguntas' => $preguntas,
            'patrocinadores' => $patrocinadores,
            'usersCount' => $usersCount,
            'sucursalesCount' => $sucursalesCount,
            'paquetesCount' => $paquetesCount,
        ]);
    }
}
