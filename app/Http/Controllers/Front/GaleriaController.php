<?php

namespace App\Http\Controllers\Front;

use App\CategoriaMultimedia;
use App\Multimedia;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GaleriaController extends Controller
{
    public function index()
    {
        $multimedias = Multimedia::activo()
            ->select('nombre', 'url', 'tipo', 'id_categoria', 'link_youtube')
            ->get();
        $categoriasMultimedias = CategoriaMultimedia::activo()
            ->select('id', 'nombre')
            ->whereHas('multimedias', function($q) {
                $q->activo();
            })
            ->get();
        return view('front.galeria.index', [
            'multimedias' => $multimedias,
            'categoriasMultimedias' => $categoriasMultimedias,
        ]);
    }
}
