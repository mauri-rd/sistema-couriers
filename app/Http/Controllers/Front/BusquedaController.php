<?php

namespace App\Http\Controllers\Front;

use App\Curso;
use App\Post;
use App\Producto;
use App\Servicio;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BusquedaController extends Controller
{
    public function find(Request $request)
    {
        $posts = Post::activo()
            ->select('id', 'titulo')
            ->filtrarBusqueda($request)
            ->limit(5)
            ->get();
        $productos = Producto::activo()
            ->select('id', 'nombre')
            ->filtrarBusqueda($request)
            ->limit(5)
            ->get();
        $servicios = Servicio::activo()
            ->select('id', 'nombre')
            ->filtrarBusqueda($request)
            ->limit(5)
            ->get();
        $cursos = Curso::activo()
            ->select('id', 'nombre')
            ->filtrarBusqueda($request)
            ->limit(5)
            ->get();
        return view('front.busqueda.find', [
            'posts' => $posts,
            'productos' => $productos,
            'servicios' => $servicios,
            'cursos' => $cursos,
            'query' => $request->q
        ]);
    }
}
