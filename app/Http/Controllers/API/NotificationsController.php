<?php

namespace App\Http\Controllers\API;

use App\UserNotification;
use App\UserNotificationRead;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function allNotifications(Request $request)
    {
        $userID = $request->userID;
        $date = $request->date;

        $notifications = UserNotification::with(['users.user' => function ($q) use ($userID) {
            return $q->whereCasilla($userID);
        }])
            ->whereHas('users.user', function ($w) use ($userID) {
                return $w->whereCasilla($userID);
            });

        if ($date) {
            $notifications = $notifications->whereDate('created_at', '=', $date);
        }

        $notifications = $notifications->get();

        return [
            'notifications' => $notifications,
        ];
    }

    public function notificationDetails(Request $request)
    {
        $notificationID = $request->notificationID;
        $userID = $request->userID;

        UserNotificationRead::whereIdUserNotification($notificationID)
            ->whereHas('user', function ($w) use ($userID) {
                return $w->whereCasilla($userID);
            })->update([
                'read' => true,
            ]);

        return [
            'notification' => UserNotification::find($notificationID),
        ];
    }
}
