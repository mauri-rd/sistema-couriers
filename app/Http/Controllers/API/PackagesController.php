<?php

namespace App\Http\Controllers\API;

use App\Paquete;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function allPackages(Request $request)
    {
        $userID = $request->userID;
        $status = $request->status;
        $date = $request->date;

        $packages = Paquete::whereHas('usuario', function ($w) use ($userID) {
            return $w->whereCasilla($userID);
        })
            ->whereEstado($status);

        if ($date) {
            $packages = $packages->whereCreatedAt($date);
        }

        $packages = $packages->get();

        return [
            'packages' => $packages
        ];
    }

    public function packageDetails(Request $request)
    {
        $package = Paquete::find($request->packageCode);

        $history = $package
            ->history()
            ->orderBy('created_at', 'asc')
            ->get();

        return [
            'package' => $package,
            'history' => $history,
        ];
    }

    public function rateService(Request $request)
    {
        $packageCode = $request->packageCode;
        $rating = $request->rating;

        Paquete::whereId($packageCode)
            ->update([
                'rating' => $rating,
            ]);

        return [
            'status' => 'ok',
        ];
    }
}
