<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
use DB;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $category = $request->query('category');
        if (is_null($category)) {
            abort(404);
        }
        $posts = Post::select(
            'id',
            'titulo',
            'img_url',
            DB::raw('SUBSTRING(cuerpo, 1, 300) as cuerpo'),
            'id_categoria',
            'updated_at'
        )->where('id_categoria', $category);

        if ($request->has('limit')) {
            $posts = $posts->limit($request->limit);
        }

        if ($request->has('paginate')) {
            return $posts->paginate(10);
        }

        return $posts->get();
    }

    public function portada()
    {
        return Post::select('id', 'posicion', 'titulo', 'img_url', 'id_categoria', 'updated_at')
            ->with(['categoria' => function($w) {
                $w->select('id', 'nombre');
            }])->where('posicion', '>', 0)
            ->get();
    }

    public function show($id)
    {
        return Post::select(
            'id',
            'titulo',
            'cuerpo',
            'img_url',
            'id_categoria',
            'updated_at'
        )->find($id);
    }
}
