<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUserData(Request $request)
    {
        $userID = $request->userID;

        $usuario = User::whereCasilla($userID)->first();

        if (is_null($usuario)) {
            return [
                'msg' => 'User not found!'
            ];
        }

        return [
            'user' => $usuario
        ];
    }

    public function updateUserData(Request $request)
    {
        $userID = $request->userID;
        $name = $request->name;
        $lastname = $request->lastname;
        $email = $request->email;
        $document = $request->document;
        $country = $request->country;
        $city = $request->city;
        $birthday = $request->birthday;
        $cellphone = $request->cellphone;
        $telephone = $request->telephone;
        $address = $request->address;
        $prefix = $request->prefix;

        User::whereCasilla($userID)
            ->update([
                'name' => $name,
                'last_name' => $lastname,
                'email' => $email,
                'ci' => $document,
                'address' => $address,
//                'country' => $country,
//                'city' => $city,
                'phone' => $cellphone,
                'birthday' => $birthday,
            ]);

        return [
            'status' => 'ok',
        ];
    }

    public function updateUserPassword(Request $request)
    {
        $userID = $request->userID;
        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;

        $user = User::select('id', 'password')->whereCasilla($userID)->first();

        if (!Hash::check($oldPassword, $user->password)) {
            return [
                'msg' => 'Password didn\'t match',
            ];
        }

        $user->password = $newPassword;
        $user->save();

        return [
            'msg' => 'Password changed',
        ];
    }

    public function updateUserFcmToken(Request $request)
    {
        $userID = $request->userID;
        $fcmToken = $request->fcmToken;

        User::whereCasilla($userID)
            ->update([
                'fcm_token' => $fcmToken,
            ]);

        return [
            'status' => 'ok',
        ];
    }

}
