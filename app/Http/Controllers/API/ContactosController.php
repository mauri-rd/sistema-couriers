<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contacto;

class ContactosController extends Controller
{
    public function index()
    {
        return Contacto::select('id', 'nombre', 'departamento', 'email')->get();
    }
}
