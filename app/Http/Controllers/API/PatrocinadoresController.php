<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Slide;

class PatrocinadoresController extends Controller
{
    public function index(Request $request)
    {
        return Slide::select('url')
            ->where('type', '=', ($request->type == 'header' ? 1 : 2))
            ->get();
    }
}
