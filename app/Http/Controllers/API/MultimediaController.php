<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Multimedia;

class MultimediaController extends Controller
{
    public function index(Request $request)
    {
        $category = $request->query('category');
        if (is_null($category)) {
            abort(404);
        }
        return Multimedia::select(
            'id',
            'nombre',
            'url',
            'link_youtube',
            'tipo',
            'id_categoria',
            'updated_at'
        )->where('id_categoria', $category)
        ->orderBy('updated_at', 'desc')
        ->get();
    }

    public function show($id)
    {
        return Multimedia::select(
            'id',
            'nombre',
            'url',
            'link_youtube',
            'tipo',
            'id_categoria',
            'updated_at'
        )->find($id);
    }
}
