<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CategoriaPost;

class BlogCategoryController extends Controller
{
    public function index()
    {
        return CategoriaPost::select('id', 'nombre')
            ->where('activo', 1)
            ->get();
    }
}
