<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contacto;
use App\Configuracion;

class EmailController extends Controller
{
    public function send(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $area = $request->area;
        $phone = $request->phone;
        $message = $request->message;
        $destination = null;

        $contacto = Contacto::select('email')->find($area);
        if (is_null($contacto)) {
            die();
        }

        $configuraciones = Configuracion::select('slug', 'valor')
            ->whereIn('slug', [
                'correo-de-la-gerencia',
                'correo-de-envio-de-correos',
                'asunto-de-correos-de-la-pagina',
            ])->get();

        $correoCc = $configuraciones->filter(function($item) {
            return $item->slug == 'correo-de-la-gerencia';
        })->first()->valor;
        $correoFrom = $configuraciones->filter(function($item) {
            return $item->slug == 'correo-de-envio-de-correos';
        })->first()->valor;
        $subject = $configuraciones->filter(function($item) {
            return $item->slug == 'asunto-de-correos-de-la-pagina';
        })->first()->valor;

        $SUBJECT = $subject;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <' . $correoFrom . '>' . "\r\n";
        $headers .= 'Cc: ' . $correoCc . "\r\n";
        $headers .= 'Reply-To: ' . $email . "\r\n";
        $destination = $contacto->email;

        $html = "
        <!DOCTYPE html>
        <html lang=\"en\">

        <head>
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
            <title>Document</title>
        </head>

        <body style=\"font-family: sans-serif; background-color: #aaa\">
            <table style=\"width: 800px; margin: 0 auto; background-color: #fff; padding: 15px; padding-bottom: 40px; border-radius: 10px;\">
                <tbody>
                    <tr>
                        <td colspan=\"2\">
                            <h2>RECIBIO UN NUEVO MENSAJE!</h2>
                        </td>
                    </tr>
                    <tr>
                        <td><b>De:</b></td>
                        <td>
                            $name
                        </td>
                    </tr>
                    <tr>
                        <td><b>Email:</b></td>
                        <td>
                            $email
                        </td>
                    </tr>
                    <tr>
                        <td><b>Telefono:</b></td>
                        <td>
                            $phone
                        </td>
                    </tr>
                    <tr>
                        <td><b>Mensaje:</b></td>
                        <td>
                            <p>$message</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" style=\"padding-top: 50px; text-align: center;\">
                            <i>Al responder este correo será automáticamente enviado al correo del usuario.</i>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>

        </html>
        ";

        mail(
            $destination,
            $SUBJECT,
            $html,
            $headers
        );
        return "ENVIADO mensaje a $contacto->email";
    }
}
