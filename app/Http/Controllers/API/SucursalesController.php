<?php

namespace App\Http\Controllers\API;

use App\Sucursal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Servicio;

class SucursalesController extends Controller
{
    public function index()
    {
        return [
            'data' => Sucursal::all(),
        ];
//        return Servicio::select(
//            'id', 'nombre', 'direccion', 'telefono', 'gmap'
//        )->where('activo', 1)->with(['imagenes' => function($q) {
//            $q->select('id', 'id_servicio', 'url');
//        }])->get();
    }
}
