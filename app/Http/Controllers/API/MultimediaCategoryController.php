<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CategoriaMultimedia;

class MultimediaCategoryController extends Controller
{
    public function index()
    {
        return CategoriaMultimedia::select('id', 'nombre')
            ->where('activo', 1)
            ->get();
    }
}
