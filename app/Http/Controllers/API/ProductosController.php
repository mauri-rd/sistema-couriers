<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Producto;

class ProductosController extends Controller
{
    public function index()
    {
        return Producto::select('id', 'nombre')
        ->where('activo', 1)
        ->with(['imagenes' => function($q) {
            $q->select('id', 'id_producto', 'url');
        }])->get();
    }
}
