<?php

namespace App\Http\Controllers\API;

use App\Enum\UserType;
use App\PasswordReset;
use App\Sucursal;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', '=', $email)->first();

        if (!$user) {
            return [
                'error' => 'User not found'
            ];
        }
        if (!Hash::check($password, $user->password)) {
            return [
                'error' => 'User not found'
            ];
        }

        $iat = time();

        $payload = array(
            "iss" => "http://strbox.com.py",
            "aud" => "http://strbox.com.py",
            "iat" => $iat,
            "exp" => $iat + 14400
        );
        $jwt = JWT::encode($payload, env('JWT_SECRET_KEY'));

        return [
            'msg' => 'Login successful!',
            'data' => [
                'code' => $user->casilla,
                'name' => $user->name,
                'lastname' => $user->last_name,
                'short_name' => $user->short_name,
                'prefix' => $user->prefix,
                'jwt' => $jwt
            ]
        ];
    }

    public function register(Request $request)
    {
        $name = $request->name;
        $lastname = $request->lastname;
        $email = $request->email;
        $password = $request->password;
        $document = $request->document;
        $country = $request->country;
        $city = $request->city;
        $address = $request->address;
        $birthday = $request->birthday;
        $cellphone = $request->cellphone;
        $telephone = $request->telephone;
        $idSucursal = $request->id_sucursal;
//        $prefix = $request->prefix;

        DB::raw('LOCK TABLES users WRITE');
        $casilla = User::max('casilla') + 1;

        User::create([
            'name' => $name,
            'last_name' => $lastname,
            'phone' => $cellphone,
            'address' => $address,
//            'country' => $country,
//            'city' => $city,
//            'state' => 'PY',
//            'zip' => 7000,
            'ci' => $document,
            'birthday' => $birthday,
            'email' => $email,
            'password' => $password,
            'type' => UserType::CLIENTE,
            'active' => 1,
            'casilla' => $casilla,
            'id_sucursal' => $idSucursal,
        ]);
        DB::raw('UNLOCK TABLES users WRITE');

        return [
            'msg' => 'Register successful!'
        ];
    }

    public function resetPassword(Request $request)
    {
        $email = $request->email;

        if (User::where('email', '=', $email)->count() == 0) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors([
                    'Não foi encontrado nenhum usuario com esse endereço de email'
                ]);
        }

        $token = md5(uniqid(rand(), true));

        $reset = PasswordReset::where('email', '=', $email)->first();
        if (!is_null($reset)) {
            $reset->token = $token;
            $reset->save();
        } else {
            PasswordReset::create([
                'email' => $email,
                'token' => $token
            ]);
        }

        $link = route('front.password-reset.reset-form', ['email' => $email, 'token' => $token]);

        Mail::send('emails.password', [
            'link' => $link
        ], function ($message) use($email) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to($email);
            $message->subject('Solicitud de cambio de contraseña');
        });

        return [
            'msg' => 'E-mail sent successfully!'
        ];
    }
}
