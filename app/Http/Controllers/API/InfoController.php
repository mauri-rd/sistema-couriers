<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Configuracion;

class InfoController extends Controller
{
    public function index()
    {
        return Configuracion::select('slug', 'valor')
            ->get();
    }
}
