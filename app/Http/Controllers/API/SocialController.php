<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Configuracion;

class SocialController extends Controller
{
    public function all()
    {
        return collect(Configuracion::select('id', 'slug', 'nombre', 'valor')
        ->whereIn('slug', [
            'facebook',
            'twitter',
            'instagram',
            'youtube',
            'google'
        ])
        ->get())->keyBy('slug');;
    }
}
