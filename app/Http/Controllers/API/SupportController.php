<?php

namespace App\Http\Controllers\API;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

const SUPPORT_EMAIL = 'mauricio@nineteen.solutions';

class SupportController extends Controller
{
    public function submit(Request $request)
    {
        $userID = $request->userID;
        $subject = $request->subject;
        $message = $request->message;

        $usuario = User::whereCasilla($userID)
            ->first();

        $userName = $usuario->name . ' ' . $usuario->last_name . ' (' . $usuario->prefix . $usuario->casilla . ')';
        $message = nl2br($message);

        $mail = "<h1>Mensaje recibido desde la Aplicación</h1>";
        $mail .= "<p><h2>Usuario: </h2>$userName</p>";
        $mail .= "<p><h2>Asunto: </h2>$subject</p>";
        $mail .= "<p><h2>Mensaje:</h2><br><i>$message</i></p><br>";
        $mail .= "<p><strong><i>Al responder este mensaje, la respuesta será encaminada al correo del usuario (" . $usuario->email . ")</i></strong></p>";

        Mail::send('emails.message', [
            'image' => null,
            'text' => $mail,
            'date' => Carbon::now(),
        ], function (Message $message) use ($mail, $usuario, $subject) {
            $message->from('info@strbox.com.py', 'StrBox');
            $message->to(SUPPORT_EMAIL);
            $message->replyTo($usuario->email);
            $message->subject("Mensaje de soporte recibido desde la aplicación");
        });

        return [
            'status' => 'ok',
        ];
    }
}
