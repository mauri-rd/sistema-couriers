<?php

namespace App\Http\Controllers\API;

use App\Paquete;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MultasController extends Controller
{
    public function send()
    {
        $date = Carbon::now();
        $ahora = clone Carbon::now();
        $ids = collect([]);
        $paquetes = Paquete::select(
            'id',
            'id_usuario',
            'id_packlist',
            'codigo',
            'volumen',
            'descripcion',
            'cant_cajas',
            'tracking',
            'courier',
            'gramos',
            'estado',
            'created_at',
            'updated_at'
        )->with(['usuario' => function($q) {
            $q->select('id', 'name', 'last_name', 'email');
        }])->enOficina()->whereDate('updated_at', '<', $ahora->subDays(14))
            ->paginate(10);

        foreach ($paquetes as $paquete) {
            $ids = $ids->merge($paquete->id_usuario);
        }

        $usuarios = User::select('name', 'last_name','email')->whereIn('id', $ids)->get();

        foreach ($usuarios as $usuario) {
            Mail::send('emails.multa', [
                'usario' => $usuario,
                'date' => $date,
            ], function ($message) use ($usuario) {
                $message->from('info@strbox.com.py', 'StrBox');
                $message->to($usuario->email);
                $message->subject($usuario->name . ', tiene paquetes pendientes de retiro!');
            });
        }

        return 'OK';
    }
}
