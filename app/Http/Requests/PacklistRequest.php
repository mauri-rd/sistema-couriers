<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PacklistRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = "unique:packlists";
        if ($this->route()->getName() == "front.my-account.packlists.update") {
            $validation = 'unique:packlists,tracking,' . $this->route('id');
        }
        return [
            'tracking' => $validation
        ];
    }

    public function messages()
    {
        return [
            'tracking.unique' => 'Esse tracking number já foi cadastrado'
        ];
    }
}
