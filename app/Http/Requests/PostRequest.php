<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->posicion);
        if ($this->has('posicion') && $this->posicion > 0) {
            return [
                'posicion' => 'unique:posts'
            ];
        }
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'posicion.unique' => 'Esta posición ya fue seleccionada en otro post.'
        ];
    }
}
