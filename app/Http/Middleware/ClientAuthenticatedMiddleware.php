<?php

namespace App\Http\Middleware;

use Closure;
use App\Enum\UserType;

class ClientAuthenticatedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->type == UserType::CLIENTE) {
            return redirect()->route('front.my-account.addresses');
        }
        return $next($request);
    }
}
