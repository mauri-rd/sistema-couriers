<?php

namespace App\Http\Middleware;

use Closure;

class CiObligatorioMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->last_name == '') {
            return redirect()->route('front.my-account.perfil.index');
        }
        return $next($request);
    }
}
