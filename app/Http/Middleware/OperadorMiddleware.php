<?php

namespace App\Http\Middleware;

use App\Enum\UserType;
use Closure;

class OperadorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->type == UserType::OPERADOR) {
            if (
                (
                    !$request->user()->operador_paquetes &&
                    !$request->user()->operador_caja &&
                    !$request->user()->operador_invoices
                ) ||
                ($request->user()->operador_caja && $request->user()->cajas()->count() == 0)
            ) {
                return redirect()->to('admin/logout')->withErrors('No tiene ningún permiso asignado. Por favor, contacte con el administrador');
            }
            if ($request->user()->operador_invoices && str_contains($request->path(), ['admin/invoices', 'admin/invoice-manual'])) {
                return $next($request);
            }
            if ($request->user()->operador_pagos && str_contains($request->path(), ['admin/pagos'])) {
                return $next($request);
            }
            if ($request->user()->operador_usuarios && str_contains($request->path(), ['admin/usuarios'])) {
                return $next($request);
            }
            if ($request->user()->operador_mensajes && str_contains($request->path(), ['admin/message'])) {
                return $next($request);
            }
            if ($request->user()->operador_sucursales && str_contains($request->path(), ['admin/sucursales'])) {
                return $next($request);
            }
            if ($request->user()->operador_reportes && str_contains($request->path(), ['admin/reportes'])) {
                return $next($request);
            }
            if ($request->user()->operador_informaciones && str_contains($request->path(), ['admin/informaciones'])) {
                return $next($request);
            }

            if ($request->user()->operador_paquetes && $request->user()->operador_caja && !str_contains($request->path(), ['admin/cajas', 'admin/paquetes', 'admin/usuarios/registrar', 'admin/operadores/registrar'])) {
                return redirect()->route('admin.paquetes.index');
            } elseif ($request->user()->operador_paquetes && !$request->user()->operador_caja && !str_contains($request->path(), ['admin/paquetes'])) {
                return redirect()->route('admin.paquetes.index');
            } elseif (!$request->user()->operador_paquetes && $request->user()->operador_caja && !str_contains($request->path(), ['admin/cajas', 'admin/usuarios/registrar', 'admin/operadores/registrar'])) {
                return redirect()->route('admin.cajas.index');
            }

            if (str_contains($request->path(), 'admin/cajas')) {
                if (
                    ( str_contains($request->path(), 'transferencia') ) ||
                    ( $request->path() !== 'admin/cajas' && !in_array($request->route('caja'), $request->user()->cajas()->pluck('id_caja')->toArray()) )
                ) {
                    return redirect()->route('admin.cajas.index');
                }
            }

        }
        return $next($request);
    }
}
