<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class InvoiceManual extends Model
{
    protected $fillable = [
        'header',
        'number',
        'invoice_date',
        'due_date',
        'id_user',
        'user_data',
    ];

    protected $dates = [
        'invoice_date',
        'due_date',
    ];

    public function getTotalAttribute()
    {
        return $this->items->reduce(function($a, $b) {
            return $a + $b->amount;
        }, 0);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function items()
    {
        return $this->hasMany(ItemInvoiceManual::class, 'id_invoice', 'id');
    }

    public function scopeFiltrar($q, Request $request)
    {
        if ($request->has('usuario')) {
            $q = $q->where('id_user', '=', $request->usuario);
        }

        return $q;
    }
}
