<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Enum\EstadoPaquete;

class Container extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nombre',
        'slug',
        'estado'
    ];

    public function setNombreAttribute($val){
        $this->attributes['nombre'] = $val;
        $this->attributes['slug'] = Str::slug($val);
    }

    public function getEstadoStringAttribute(){
        switch ($this->attributes['estado']){
            case EstadoPaquete::$MIAMI;
                return 'EN MIAMI'; 
            case EstadoPaquete::$MIAMI_MARITIMO;
                return 'MIAMI MARITIMO';
            case EstadoPaquete::$TRANSITO;
                return 'EN TRANSITO'; 
            case EstadoPaquete::$PARAGUAY;
                return 'EN PARAGUAY';
            case EstadoPaquete::$OFICINA;
                return 'EN OFICINA'; 
            case EstadoPaquete::$RETIRADO;
                return 'EN RETIRADO';
            case EstadoPaquete::$CHINA;
                return 'EN CHINA';
            case EstadoPaquete::$BRASIL;
                return 'EN BRASIL';
        }
    }

    public function paquetes(){
        return $this->hasMany(Paquete::class, 'id_container', 'id');
    }

    public function scopeBuscar($containers, $q){
        return $containers->where('nombre', 'LIKE', "%$q%");
    }
}
