<?php

namespace App;

use Illuminate\Http\Request;

class Producto extends BaseModel
{
    protected $fillable = ['nombre', 'activo'];

    public function categoria()
    {
        return $this->belongsTo('App\CategoriaProducto', 'id_categoria', 'id');
    }

    public function modelo()
    {
        return $this->belongsTo('App\Modelo', 'id_modelo', 'id');
    }

    public function imagenes()
    {
        return $this->hasMany('App\ImagenProducto', 'id_producto', 'id');
    }

    public function scopeFiltrarCategoria($q, Request $request)
    {
        if ($request->has('cat_id')) {
            return $q->where('id_categoria', '=', $request->cat_id);
        }
        return $q;
    }

    public function scopeFiltrarModelo($q, Request $request)
    {
        if ($request->has('modelo_id')) {
            return $q->where('id_modelo', '=', $request->modelo_id);
        }
        return $q;
    }

    public function scopeFiltrarMarca($q, Request $request)
    {
        if ($request->has('marca_id')) {
            return $q->whereHas('modelo.marca', function($q) use($request) {
                $q->where('id', '=', $request->marca_id);
            });
        }
        return $q;
    }

    public function scopeOrdenar($q, $order)
    {
        switch ($order) {
            case "az":
                return $q->orderBy('nombre', 'asc');
            case "za":
                return $q->orderBy('nombre', 'desc');
            case "09":
                return $q->orderBy('precio', 'asc');
            case "90":
                return $q->orderBy('precio', 'desc');
            default:
                return $q->orderBy('nombre', 'asc');
        }
    }

    public function scopeFiltrarBusqueda($q, Request $request)
    {
        if ($request->has('q')) {
            return $q->where('nombre', 'like', '%'. $request->q .'%');
        }
        return $q;
    }
}
