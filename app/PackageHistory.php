<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageHistory extends Model
{
    protected $fillable = [
        'id_paquete',
        'estado',
    ];

    public function paquete()
    {
        return $this->belongsTo(Paquete::class, 'id_paquete', 'id');
    }
}
