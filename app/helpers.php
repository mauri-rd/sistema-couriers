<?php

function get_active_carrito() {
    $carrito = null;
    if (auth()->check()) {
        $carrito = auth()->user()->carritoActivo()->first();
        if (is_null($carrito)) {
            $carrito = auth()->user()->carritos()->create([]);
        }
    } else {
        $carrito = \App\Carrito::whereSessionId(session()->getId())->activo()->first();
        if (is_null($carrito)) {
            $carrito = \App\Carrito::create([
                'session_id' => session()->getId(),
            ]);
        }
    }
    return $carrito;
}

function product_is_in_cart($productId) {
    return get_active_carrito()->items()->whereIdProducto($productId)->count() > 0;
}

function extract_numbers($str) {
    $matches = null;
    preg_match_all('!\d+!', $str, $matches);
    if (count($matches) > 0) {
        return implode('', $matches[0]);
    }
    return null;
}

function format_guaranies($num) {
    return 'Gs. ' . number_format($num, 0, ',', '.');
}

function format_dolares($num) {
    return 'US$ ' . number_format($num, 2, ',', '.');
}