<?php

namespace App;

use App\Enum\UserLanguage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Illuminate\Http\UploadedFile;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'phone',
        'address',
//        'country',
//        'city',
//        'state',
//        'zip',
        'ci',
        'birthday',
        'email',
        'password',
        'type',
        'active',
        'casilla',
        'id_sucursal',
        'tipo_flete',
        'seguro',
        'consolidacao',
        'lang',
        'fcm_token',
        'operador_paquetes',
        'operador_caja',
        'operador_invoices',
        'operador_pagos',
        'operador_usuarios',
        'operador_mensajes',
        'operador_sucursales',
        'operador_reportes',
        'operador_informaciones',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'operador_paquetes' => 'boolean',
        'operador_caja' => 'boolean',
    ];

    public function getPreferedLanguageAttribute()
    {
        return $this->attributes['lang'] == UserLanguage::ESPANOL ? 'es' : 'pt';
    }

    public function getFullNameAttribute()
    {
        return $this->name . ( $this->last_name != '' ? ( ' ' . $this->last_name ) : '' );
    }

    public function getShortNameAttribute()
    {
        if ($this->name && $this->last_name) {
            return explode(" ", $this->name)[0] . " " . explode(" ", $this->last_name)[0];
        }
        return $this->full_name;
    }

    public function setPasswordAttribute($password)
    {
        if ($password != '') {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    public function setCiAttribute($ci)
    {
        if ($ci instanceof UploadedFile && $ci->isValid()) {
            $name = str_slug($ci->getClientOriginalName()) . auth()->id() . "." . $ci->getClientOriginalExtension();
            $dir = "uploads/ci";
            $fullPath = $dir . '/' . $name;
            $ci->move($dir, $name);
            $this->attributes['ci'] = $fullPath;
        }
    }

    public function getPrefixAttribute()
    {
        if (is_null($this->sucursal)) {
            return "";
        }
        return $this->sucursal->prefix;
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id');
    }

    public function cajas()
    {
        return $this->hasMany(OperadorCaja::class, 'id_user', 'id');
    }

    public function scopeFiltrar($q, $query, $queryBy)
    {
        switch($queryBy) {
            case "casilla":
                return $q->where('casilla', 'like', '%' . filter_var($query, FILTER_SANITIZE_NUMBER_INT) . '%');
            case "username":
                return $q->whereRaw('concat(name, " ", last_name) like ?', ['%' . $query . '%']);
            case "email":
                return $q->where('email', 'like', '%' . $query . '%');
            default:
                return $q;
        }
    }
}
