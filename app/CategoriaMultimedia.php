<?php

namespace App;

class CategoriaMultimedia extends BaseModel
{
    protected $fillable = ['nombre', 'estado'];

    public function multimedias()
    {
        return $this->hasMany('App\Multimedia', 'id_categoria', 'id');
    }
}
