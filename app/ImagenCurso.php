<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Intervention\Image\ImageManagerStatic as Image;

class ImagenCurso extends Model
{
    protected $fillable = ['id_curso', 'url'];

    public function setUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . ".jpg";
            $dir = "uploads/cursos";
            $image = Image::make($file);
            $fullPath = $dir . '/' . $name;
            $image->resize(700, null, function($c) {
                $c->aspectRatio();
            })->save($fullPath);
            $this->attributes['url'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['url'];
    }

    public function getUrlAttribute($url)
    {
        return url($url);
    }
}
