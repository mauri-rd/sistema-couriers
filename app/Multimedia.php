<?php

namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;

class Multimedia extends BaseModel
{
    protected $fillable = ['nombre', 'url', 'link_youtube', 'tipo', 'id_categoria', 'activo', 'inhome'];

    public function setLinkYoutubeAttribute($val)
    {
        if (!empty($val)) {
            $this->attributes['link_youtube'] = $val;
            $id = $this->getYouTubeId($val);
            $url = "uploads/multimedia/vid/{$id}.jpg";
            $image = Image::make(file_get_contents($this->getYouTubeThumb($id)));
            $image->insert(public_path('img/play.png'), 'center');
            $this->attributes['tipo'] = 'vid';
            $this->attributes['url'] = $url;
            $image->save(public_path($url));
        }
    }

    public function setUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            if (str_contains($file->getMimeType(), 'image')) {
                $this->attributes['tipo'] = 'img';
            }
            elseif (str_contains($file->getMimeType(), 'video')) {
                $this->attributes['tipo'] = 'vid';
            }
            if ($this->attributes['tipo'] == 'img' || $this->attributes['tipo'] == 'vid') {
                $name = str_slug($file->getClientOriginalName()) . ".jpg";
                $dir = "uploads/multimedia/" . $this->attributes['tipo'];
                $image = Image::make($file);
                $fullPath = $dir . '/' . $name;
                $image->resize(700, null, function($c) {
                    $c->aspectRatio();
                })->save($fullPath);
                $this->attributes['url'] = $fullPath;
            }
        }
    }

    public function getIsImgAttribute()
    {
        return $this->attributes['tipo'] == 'img';
    }

    public function getPathAttribute()
    {
        return $this->attributes['url'];
    }

    public function getUrlAttribute($url)
    {
        return url($url);
    }

    public function categoria()
    {
        return $this->belongsTo('App\CategoriaMultimedia', 'id_categoria', 'id');
    }

    private function getYouTubeId($url)
    {
        $url = urldecode($url);
        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
        return $matches[1];
    }

    private function getYouTubeThumb($id)
    {
        return "https://img.youtube.com/vi/{$id}/0.jpg";
    }

    public function scopeInhome($q)
    {
        return $q->where('inhome', '=', 1);
    }
}
