<?php

namespace App;

use App\Enum\MonedaCaja;
use App\Enum\TipoMovimientoFinanciero;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Arqueo extends Model
{
    protected $fillable = [
        'id_caja',
        'monto_inicio',
        'monto_fin',
        'fecha_inicio',
        'fecha_fin',
    ];

    public function getTotalMovimientosAttribute()
    {
        $ingreso = $this
            ->movimientosFinancieros()
            ->whereTipoMovimiento(TipoMovimientoFinanciero::INGRESO)
            ->sum('monto');
        $egreso = $this
            ->movimientosFinancieros()
            ->whereTipoMovimiento(TipoMovimientoFinanciero::EGRESO)
            ->sum('monto');

        return $ingreso - $egreso;
    }

    public function getTotalMovimientosFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->total_movimientos);
        }
        return format_dolares($this->total_movimientos);
    }

    public function getTotalAttribute()
    {

        return $this->monto_inicio + $this->total_movimientos;
    }

    public function getTotalFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->total);
        }
        return format_dolares($this->total);
    }

    public function getMontoInicioFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->monto_inicio);
        }
        return format_dolares($this->monto_inicio);
    }

    public function getMontoFinFormateadoAttribute()
    {
        if ($this->caja->moneda == MonedaCaja::GUARANIES) {
            return format_guaranies($this->monto_fin);
        }
        return format_dolares($this->monto_fin);
    }

    public function getFechaInicioDateAttribute()
    {
        return new Carbon($this->attributes['fecha_inicio']);
    }

    public function getFechaFinDateAttribute()
    {
        return new Carbon($this->attributes['fecha_fin']);
    }

    public function getAbiertoAttribute()
    {
        return is_null($this->fecha_fin);
    }

    public function caja()
    {
        return $this->belongsTo(Caja::class, 'id_caja', 'id');
    }

    public function movimientosFinancieros()
    {
        return $this->hasMany(MovimientoFinanciero::class, 'id_arqueo', 'id');
    }

    public function scopeAbierto($q)
    {
        return $q->whereFechaFin(null);
    }
}
