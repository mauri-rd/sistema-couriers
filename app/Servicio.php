<?php

namespace App;

use Illuminate\Http\Request;

class Servicio extends BaseModel
{
    protected $fillable = ['nombre', 'direccion', 'telefono', 'gmap', 'activo'];

    public function imagenes()
    {
        return $this->hasMany('App\ImagenServicio', 'id_servicio', 'id');
    }

    public function scopeFiltrarBusqueda($q, Request $request)
    {
        if ($request->has('q')) {
            return $q->where('nombre', 'like', '%'. $request->q .'%');
        }
        return $q;
    }
}
