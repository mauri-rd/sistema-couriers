<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Image;

class ImagenProducto extends Model
{
    protected $fillable = ['id_producto', 'url'];

    public function setUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . ".jpg";
            $dir = "uploads/productos";
            // $image = Image::make($file);
            $fullPath = $dir . '/' . $name;
            // $image->resize(1000, null, function($c) {
            //     $c->aspectRatio();
            // })->save($fullPath);
            $file->move($dir, $name);
            $this->attributes['url'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['url'];
    }

    public function getUrlAttribute($url)
    {
        return url($url);
    }
}
