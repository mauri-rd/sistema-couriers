<?php

namespace App\Enum;

class EstadoPaquete
{
    static $MIAMI = 1;
    static $MIAMI_MARITIMO = 11;
    static $TRANSITO = 2;
    static $PARAGUAY = 3;
    static $OFICINA = 4;
    static $RETIRADO = 5;
    static $CHINA = 6;

    static $ELIMINADO = 7;

    static $BRASIL = 8;
}