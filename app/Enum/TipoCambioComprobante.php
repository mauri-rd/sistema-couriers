<?php
namespace App\Enum;

class TipoCambioComprobante
{
    const BRL = 10;
    const PYG = 20;
}