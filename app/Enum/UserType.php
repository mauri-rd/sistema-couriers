<?php
namespace App\Enum;

class UserType
{
    const ADMINISTRADOR = 1;
    const CLIENTE = 2;
    const OPERADOR = 3;
}