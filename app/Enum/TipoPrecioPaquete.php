<?php
namespace App\Enum;

class TipoPrecioPaquete
{
    const POR_PESO = 1;
    const POR_UNIDAD = 2;
}