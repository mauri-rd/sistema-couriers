<?php
namespace App\Enum;

class Moneda
{
    static $DOLAR = "US$";

    static function list()
    {
        $res = [];
        $res[Moneda::$DOLAR] = "Dolares Americanos (US$)";
        return $res;
    }
}