<?php

namespace App\Enum;

class MonedaCaja
{
    const GUARANIES = 10;
    const DOLARES = 20;

    static function constants()
    {
        $reflectionClass = new \ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    static function nombres()
    {
        $monedas = array_flip(self::constants());

        return array_map(function ($moneda) {
            return title_case($moneda);
        }, $monedas);
    }

    static function simbolos()
    {
        return [
            self::GUARANIES => 'Gs.',
            self::DOLARES => 'US$',
        ];
    }
}