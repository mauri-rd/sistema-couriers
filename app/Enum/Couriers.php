<?php
namespace App\Enum;

class Couriers
{
    static $DHL = 1;
    static $UPS = 2;
    static $USPS = 3;
    static $FedEx = 4;
    static $LS = 5;

    static function list()
    {
        $res = [];
        $res[Couriers::$DHL] = "DHL";
        $res[Couriers::$UPS] = "UPS";
        $res[Couriers::$USPS] = "USPS";
        $res[Couriers::$FedEx] = "FedEx";
        $res[Couriers::$LS] = "LS";
        return $res;
    }
}