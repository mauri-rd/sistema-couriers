<?php

namespace App\Enum;

class TipoMovimientoFinanciero
{
    const INGRESO = 10;
    const EGRESO = 20;
    const TRANSFERENCIA = 30;
}