<?php

namespace App\Enum;

class UserLanguage
{
    const ESPANOL = 10;
    const PORTUGUES = 20;
}