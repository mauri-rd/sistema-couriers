<?php
namespace App\Enum;

class TipoFlete
{
    const NORMAL = 1;
    const EXPRESS = 2;
}