<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function scopeActivo($q)
    {
        return $q->where('activo', '=', 1);
    }
}