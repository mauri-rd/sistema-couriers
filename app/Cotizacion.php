<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotizacion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'valor',
    ];

    public function getCreatedAtDateAttribute()
    {
        return new Carbon($this->attributes['created_at']);
    }

    public function scopeUltimo($q)
    {
        return $q->orderBy('created_at', 'desc')->limit(1);
    }
}
