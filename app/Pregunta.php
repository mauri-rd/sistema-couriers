<?php

namespace App;

class Pregunta extends BaseModel
{
    protected $fillable = [
        'pregunta',
        'respuesta',
        'posicion',
        'activo',
    ];
}
