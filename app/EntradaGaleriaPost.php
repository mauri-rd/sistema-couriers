<?php

namespace App;

class EntradaGaleriaPost extends BaseModel
{
    protected $fillable = ['id_post', 'id_multimedia'];

    public function post()
    {
        return $this->belongsTo('App\Post', 'id_post', 'id');
    }

    public function multimedia()
    {
        return $this->belongsTo('App\Multimedia', 'id_multimedia', 'id');
    }
}
