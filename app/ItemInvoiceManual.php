<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemInvoiceManual extends Model
{
    protected $fillable = [
        'id_invoice',
        'item',
        'description',
        'price',
        'quantity',
        'amount',
        'paid',
    ];

    public function invoice()
    {
        return $this->belongsTo(InvoiceManual::class, 'id_invoice', 'id');
    }
}
