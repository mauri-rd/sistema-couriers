<?php

namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Image;

class Slide extends BaseModel
{
    protected $fillable = [
        'url',
        'type'
    ];

    public function setUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . ".jpg";
            $dir = "uploads/slides";
            // $image = Image::make($file);
            $fullPath = $dir . '/' . $name;
            // $image->resize(1280, null, function($c) {
            //     $c->aspectRatio();
            // })->save($fullPath);
            $file->move($dir, $name);
            $this->attributes['url'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['url'];
    }

    public function getUrlAttribute($url)
    {
        return url($url);
    }
}
