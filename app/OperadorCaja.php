<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperadorCaja extends Model
{
    protected $fillable = [
        'id_user',
        'id_caja',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function caja()
    {
        return $this->belongsTo(Caja::class, 'id_caja', 'id');
    }

}
