<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends BaseModel
{
    protected $fillable = [
        'nombre',
        'activo',
        'departamento',
        'email',
    ];

    public function scopeFiltrarBusqueda($q, Request $request)
    {
        if ($request->has('q')) {
            return $q->where('nombre', 'like', '%'. $request->q .'%');
        }
        return $q;
    }
}
