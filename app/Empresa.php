<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Empresa extends BaseModel
{
    protected $fillable = [
        'nombre',
        'imagen_url',
        'link',
    ];

    public function setImagenUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . ".jpg";
            $dir = "uploads/empresas";
            // $image = Image::make($file);
            $fullPath = $dir . '/' . $name;
            // $image->resize(1000, null, function($c) {
            //     $c->aspectRatio();
            // })->save($fullPath);
            $file->move($dir, $name);
            $this->attributes['imagen_url'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['imagen_url'];
    }

    public function getImagenUrlAttribute($url)
    {
        return url($url);
    }
}
