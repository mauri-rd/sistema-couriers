<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Configuracion extends Model
{
    protected $fillable = ['nombre', 'slug', 'valor'];

    public function setNombreAttribute($val)
    {
        $this->attributes['slug'] = Str::slug($val);
        $this->attributes['nombre'] = $val;
    }
}
