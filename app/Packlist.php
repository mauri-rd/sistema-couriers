<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Packlist extends Model
{
    protected $fillable = [
        'id_usuario',
        'tracking',
        'courier',
        'descripcion',
        'valor',
        'moneda',
        'file',
        'mime',
        'tipo_flete',
        'destino',
        'seguro',
        'consolidacao',
        'consolidacao_trackings',
    ];

    public function setFileAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . auth()->id() . "." . $file->getClientOriginalExtension();
            $mime = $file->getMimeType();
            $dir = "uploads/packlists";
            $fullPath = $dir . '/' . $name;
            $file->move($dir, $name);
            $this->attributes['file'] = $fullPath;
            $this->attributes['mime'] = $mime;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['file'];
    }

    public function getFileAttribute($url)
    {
        return url($url);
    }

    public function usuario()
    {
        return $this->belongsTo('App\User', 'id_usuario', 'id');
    }

    public function paquete()
    {
        return $this->hasOne('App\Paquete', 'id_packlist', 'id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice', 'id_packlist', 'id');
    }

    public function scopeDelUsuario($q, $userId)
    {
        return $q->where('id_usuario', '=', $userId);
    }

    public function scopeConTracking($q, $tracking)
    {
        return $q->where('tracking', 'like', '%' . $tracking);
    }

    public function scopeParaConsolidarTracking($q, $tracking)
    {
        return $q->where('consolidacao_trackings', 'like', $tracking . ',%')
            ->orWhere('consolidacao_trackings', 'like', '%,' . $tracking . ',%')
            ->orWhere('consolidacao_trackings', 'like', '%,' . $tracking);
    }
}
