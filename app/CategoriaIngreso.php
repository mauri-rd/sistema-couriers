<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoriaIngreso extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nombre',
        'icon',
    ];
}
