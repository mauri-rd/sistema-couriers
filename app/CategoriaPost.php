<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaPost extends BaseModel
{
    protected $fillable = ['nombre', 'estado'];

    public function posts()
    {
        return $this->hasMany('App\Post', 'id_categoria', 'id');
    }
}
