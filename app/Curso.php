<?php

namespace App;

use Illuminate\Http\Request;

class Curso extends BaseModel
{
    protected $fillable = ['nombre', 'descripcion', 'activo'];

    public function imagenes()
    {
        return $this->hasMany('App\ImagenCurso', 'id_curso', 'id');
    }

    public function scopeFiltrarBusqueda($q, Request $request)
    {
        if ($request->has('q')) {
            return $q->where('nombre', 'like', '%'. $request->q .'%');
        }
        return $q;
    }
}
