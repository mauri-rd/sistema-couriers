<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;

class Post extends BaseModel
{
    protected $fillable = ['titulo', 'cuerpo', 'img_url', 'posicion', 'id_categoria', 'id_autor'];

    public function autor()
    {
        return $this->belongsTo('App\User', 'id_autor', 'id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\CategoriaPost', 'id_categoria', 'id');
    }

    public function multimedias()
    {
        return $this->hasMany('App\EntradaGaleriaPost', 'id_post', 'id');
    }

    public function setImgUrlAttribute(UploadedFile $file)
    {
        if ($file->isValid()) {
            $name = str_slug($file->getClientOriginalName()) . ".jpg";
            $dir = "uploads/posts";
            $image = Image::make($file);
            $fullPath = $dir . '/' . $name;
            $image->resize(700, null, function($c) {
                $c->aspectRatio();
            })->save($fullPath);
            $this->attributes['img_url'] = $fullPath;
        }
    }

    public function getPathAttribute()
    {
        return $this->attributes['img_url'];
    }

    public function getImgUrlAttribute($url)
    {
        return url($url);
    }

    public function getFechaAttribute()
    {
        $date = $this->attributes['created_at'];
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function scopeFiltrarCategoria($q, Request $request)
    {
        if ($request->has('cat_id')) {
            return $q->where('id_categoria', '=', $request->cat_id);
        }
        return $q;
    }

    public function scopeFiltrarPeriodo($q, Request $request)
    {
        if ($request->has('mes') && $request->has('anho')) {
            return $q->whereRaw('MONTH(created_at) = ? and YEAR(created_at) = ?', [
                $request->mes, $request->anho
            ]);
        }
        return $q;
    }

    public function scopeFiltrarBusqueda($q, Request $request)
    {
        if ($request->has('q')) {
            return $q->where('titulo', 'like', '%'. $request->q .'%');
        }
        return $q;
    }
}
