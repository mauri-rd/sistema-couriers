<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotificationRead extends Model
{
    protected $fillable = [
        'id_user',
        'id_user_notification',
        'read',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function notification()
    {
        return $this->belongsTo(UserNotification::class, 'id_user_notification', 'id');
    }
}
