<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $fillable = [
        'title',
        'content',
        'image_url',
    ];

    public function users()
    {
        return $this->hasMany(UserNotificationRead::class, 'id_user_notification', 'id');
    }
}
